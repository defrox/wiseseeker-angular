#!/usr/bin/env bash

echo "Setting dev branch and pulling updates"
cd /var/www/wiseseeker-angular/
git reset --hard HEAD
git checkout branch develop
git pull origin develop
rm ./package-lock.json
npm install
npm run build-dev
# deploy
echo "All done!"
exit 0