export class KnowledgeFakeDb
{
    public static categories = [
        {
            'id'            : 1,
            'title'         : 'Public Cloud',
            'description'   : 'Public Cloud',
            'style'         : '',
            'icon'          : '',
            'image'         : '',
            'active'        : true
        },
        {
            'id'            : 2,
            'title'         : 'Databases',
            'description'   : 'Databases',
            'style'         : '',
            'icon'          : '',
            'image'         : '',
            'active'        : true
        },
        {
            'id'            : 3,
            'title'         : 'Systems Architecture',
            'description'   : 'Systems Architecture',
            'style'         : '',
            'icon'          : '',
            'image'         : '',
            'active'        : true
        }
    ];
}
