import {InMemoryDbService} from 'angular-in-memory-web-api';

import {KnowledgeFakeDb} from 'app/fake-db/knowledge';

export class FakeDbService implements InMemoryDbService {
    createDb(): any {
        return {
            // Wise Seeker API
            'knowledge-categories': KnowledgeFakeDb.categories,
            'roles-categories': KnowledgeFakeDb.categories,
        };
    }
}
