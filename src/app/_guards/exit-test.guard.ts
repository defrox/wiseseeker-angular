import {Injectable} from '@angular/core';
import {CanDeactivate} from '@angular/router';
import {ExitTestComponent} from 'app/_components';

@Injectable()
export class ExitTestGuard implements CanDeactivate<ExitTestComponent> {
    canDeactivate(component: ExitTestComponent): Promise<any> {
        return new Promise((resolve) => {
            if (!component.canDeactivate()) {
                const componentFunctions = Object.getPrototypeOf(component);
                component.confirmExit().then(res => {
                    resolve(res === true);
                }, err => {
                    resolve(false);
                });
            } else {
                resolve(true);
            }
        });
    }
}
