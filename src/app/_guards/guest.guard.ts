﻿import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild} from '@angular/router';
import {AuthenticationService} from 'app/_services';
import {Group, User} from 'app/_models';

@Injectable({providedIn: 'root'})
export class GuestGuard implements CanActivateChild {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        console.log('hete');
        const currentUser = this.authenticationService.currentUserValue;
        if (!currentUser) {
            // not logged in so create guest user
            this.authenticationService.currentUserValue;
            const guest = new User(0, 'guest@wiseseeker.com', null, 'Guest', [Group.Guest], 'guest', false, false, null, 'assets/images/avatars/profile.jpg');
            this.authenticationService.setCurrentUser(guest);
        }
        return true;
    }
}
