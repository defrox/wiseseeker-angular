import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild} from '@angular/router';
import {Group} from 'app/_models';
import {AuthenticationService} from 'app/_services';

@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate, CanActivateChild {
    constructor(
        private router: Router,
        private authenticationService: AuthenticationService
    ) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser) {
            // check if user is admin, give access
            if (currentUser.groups.includes(Group.Admin)) {
                return true;
            }
            // check if route is restricted by group
            if (route.data.groups && route.data.groups.filter(value => -1 !== currentUser.groups.indexOf(value)).length < 1) {
                // group not authorised so redirect to 403
                this.router.navigate(['/error/403']);
                return false;
            }
            // authorised so return true
            return true;
        }
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/auth/login'], {queryParams: {returnUrl: state.url}});
        return false;
    }

    canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser) {
            // check if user is admin, give access
            if (currentUser.groups.includes(Group.Admin)) {
                return true;
            }
            // check if route is restricted by group
            if (route.data.groups && route.data.groups.filter(value => -1 !== currentUser.groups.indexOf(value)).length < 1) {
                // group not authorised so redirect to 403
                this.router.navigate(['/error/403']);
                return false;
            }
            // authorised so return true
            return true;
        }
        // not logged in so redirect to login page with the return url
        this.router.navigate(['/auth/login'], {queryParams: {returnUrl: state.url}});
        return false;
    }
}
