import {ExitTestComponent} from 'app/_components/exit-test/exit-test.component';

export abstract class ExitTestVarComponent extends ExitTestComponent{

    abstract get submitted(): boolean;
    abstract get lastResult(): any | null;

    canDeactivate(): any {
        return this.submitted || this.lastResult;
    }
}