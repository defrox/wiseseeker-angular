import {HostListener} from '@angular/core';
import {NavigationStart, Router} from '@angular/router';

export abstract class ExitTestComponent {

    abstract canDeactivate(): Promise<any>;
    abstract confirmExit(): any;

    @HostListener('window:beforeunload', ['$event'])
    @HostListener('window:pagehide', ['$event'])
    unloadNotification($event: any): void {
        if ($event instanceof NavigationStart || $event instanceof BeforeUnloadEvent) {
            // console.log($event);
        }
        if (!this.canDeactivate()) {
            $event.returnValue = true;
        }
    }
    constructor(
        private _routerExit: Router,
    ) {
        _routerExit.events.subscribe((event) => {
            this.unloadNotification(event);
        });
    }
}
