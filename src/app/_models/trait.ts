export class PersonalityTrait {
    openess: number | string;
    conscientiousness: number | string;
    extroversion: number | string;
    agreeableness: number | string;
    neuroticism: number | string;
}
