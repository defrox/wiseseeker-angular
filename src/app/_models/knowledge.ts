﻿export class Knowledge {
    id: number | string;
    title: string;
    description: string;
    category: KnowledgeCategory | any | null;
    active: boolean;
    icon?: string;
    image?: string;
    constructor(id: number | string, title: string, description: string, category: KnowledgeCategory | any | null, active: boolean, icon?: string, image?: string) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.category = category;
        this.active = active;
        this.icon = icon;
        this.image = image;
    }
}

export class KnowledgeCategory {
    id: number | string;
    title: string;
    description: string;
    active: boolean;
    style?: string;
    icon?: string;
    image?: string;
    constructor(id: number | string, title: string, description: string, active: boolean, style?: string, icon?: string, image?: string) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.active = active;
        this.style = style;
        this.icon = icon;
        this.image = image;
    }
}