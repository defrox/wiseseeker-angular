export class JwtObject {
    constructor(
        private _orig_iat: number,
        private _exp: Date,
        private _username: string,
        private _email: string,
        private _user_id: string,
    ) {  }

    get orig_iat(): number {
        return this._orig_iat;
    }

    set orig_iat(value: number) {
        this._orig_iat = value;
    }

    get exp(): Date {
        return this._exp;
    }

    set exp(value: Date) {
        this._exp = value;
    }

    get username(): string {
        return this._username;
    }

    set username(value: string) {
        this._username = value;
    }

    get email(): string {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    get user_id(): string {
        return this._user_id;
    }

    set user_id(value: string) {
        this._user_id = value;
    }
}