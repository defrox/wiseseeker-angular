﻿import {Group} from './group';
import {JwtObject} from './jwt-object';

export class User {
    constructor(
        protected _id: number | string,
        protected _email: string,
        protected _firstName: string,
        protected _lastName: string,
        protected _groups: Group[],
        protected _username: string,
        protected _is_active: boolean = false,
        protected _is_staff: boolean = false,
        protected _password?: string,
        protected _avatar?: string,
        protected _token?: string,
        protected _jwt?: JwtObject,
        protected _profile?: UserProfile,
    ) {  }

    get id(): number | string {
        return this._id;
    }

    set id(value: number | string) {
        this._id = value;
    }

    get email(): string {
        return this._email;
    }

    set email(value: string) {
        this._email = value;
    }

    get first_name(): string {
        return this._firstName;
    }

    get firstName(): string {
        return this._firstName;
    }

    set firstName(value: string) {
        this._firstName = value;
    }

    get last_name(): string {
        return this._lastName;
    }

    get lastName(): string {
        return this._lastName;
    }

    set lastName(value: string) {
        this._lastName = value;
    }

    get groups(): Group[] {
        return this._groups;
    }

    set groups(value: Group[]) {
        this._groups = value;
    }

    get username(): string {
        return this._username;
    }

    set username(value: string) {
        this._username = value;
    }

    get is_active(): boolean {
        return this._is_active;
    }

    set is_active(value: boolean) {
        this._is_active = value;
    }

    get is_staff(): boolean {
        return this._is_staff;
    }

    set is_staff(value: boolean) {
        this._is_staff = value;
    }
    get password(): string {
        return this._password;
    }

    set password(value: string) {
        this._password = value;
    }

    get avatar(): string {
        return this._avatar;
    }

    set avatar(value: string) {
        this._avatar = value;
    }

    get profile(): UserProfile {
        return this._profile;
    }

    set token(value: string) {
        this._token = value;
    }

    get jwt(): JwtObject {
        return this._jwt;
    }

    set jwt(value: JwtObject) {
        this._jwt = value;
    }

    set profile(value: UserProfile) {
        this._profile = value;
    }

    get token(): string {
        return this._token;
    }
    
    public hasToken(): boolean {
        return this._token !== null;
    }

    public getToken(): string {
        return this._token;
    }
}

export class UserProfile {
    [key: string]: any;
}

enum Gender {
    male = 'male',
    female = 'female'
}
