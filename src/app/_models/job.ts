﻿export class Job {
    [key: string]: any;
}

export class JobApplication {
    [key: string]: any;
}

export class JobSkill {
    [key: string]: any;
}

/*export class JobApplicationStatus {
    [key: string]: any;
}*/

export const JobApplicationState: any = {
    1: 'REQUESTED',
    2: 'CONFIRMED',
    3: 'EXECUTED',
    4: 'ASSESSED'
};
