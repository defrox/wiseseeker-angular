export interface AppConfig
{
    logo: {
        icon: string;
        iconInvert: string;
        iconBig: string;
        small: string;
        medium: string;
        big: string;
        small_var1: string;
        medium_var1: string;
        big_var1: string;
        small_var2: string;
        medium_var2: string;
        big_var2: string;
    };
    appName: string;
    appSettings: any;
    timerConfig: any;
    installedLangs: string[];
    defaultLang: string;
    solidGaugeOptions: any;
    solidGaugeOptionsCenter: any;
    solidGaugeOptionsCenterColors: any;
    solidDonutOptions: any;
    solidDonut2Options: any;
    solidDonutOptionsColors: any;
    spiderWebOptions: any;
}
