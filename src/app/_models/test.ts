﻿export class Test {
    [key: string]: any;
}

export class TestExecution {
    [key: string]: any;
}

export class TestType {
    [key: string]: any;
}

export const TestSubType: any = {
    1: 'IPV',
    2: 'Digital Skill'
};

export const TestSubTypeExt: any = [
    {id: 1, title: 'IPV'},
    {id: 2, title: 'Habilidades Agile'}
];

export const MethodType: any = {
    1: 'Personality',
    2: 'Skill'
};

export const MethodTypeExt: any = [
    {id: 1, title: 'Personalidad'},
    {id: 2, title: 'Habilidad'}
];

export const TestExecutionStatus: any = {
    0: 'PENDING',
    1: 'STARTED',
    2: 'FINISHED',
    3: 'ASSESSED'
};
