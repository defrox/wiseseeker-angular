import {PersonalityTrait} from './trait';

export class Personality {
    id: number | string;
    traits: PersonalityTrait;
}

export const PersonalitySkills: any = [
    {id: 1, text: 'OPEN'},
    {id: 2, text: 'CLOSED'},
    {id: 3, text: 'RESPONSIBLE'},
    {id: 4, text: 'IMPULSIVE'},
    {id: 5, text: 'SOCIABLE'},
    {id: 6, text: 'RESERVED'},
    {id: 7, text: 'FRIENDLY'},
    {id: 8, text: 'CRITIC'},
    {id: 9, text: 'BALANCED'},
    {id: 10, text: 'UNSTABLE'}
];

export const PersonalityRelevances: any = [
    {id: 1, text: 'NOTHING'},
    {id: 2, text: 'LITTLE'},
    {id: 3, text: 'SOME'},
    {id: 4, text: 'MUCH'}
];
