export enum Group {
    User = 'User',
    Guest = 'Guest',
    Admin = 'Admin',
    Recruiter = 'Recruiter',
    Candidate = 'Candidate',
    External = 'External'
}
