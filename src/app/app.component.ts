import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';
import {Router, Event, NavigationStart} from '@angular/router';
import {DOCUMENT} from '@angular/common';
import {Platform} from '@angular/cdk/platform';
import {TranslateService} from '@ngx-translate/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {CookieService} from 'ngx-cookie-service';

import {FuseConfigService} from '@fuse/services/config.service';
import {FuseNavigationService} from '@fuse/components/navigation/navigation.service';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {FuseSplashScreenService} from '@fuse/services/splash-screen.service';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {TranslatePipe} from '@ngx-translate/core';
import {AuthenticationService, AppConfigService, NotificationService} from 'app/_services';
import {Group, User} from 'app/_models';
import * as _ from 'lodash/fp';

import {navigation} from 'app/navigation/navigation';
import {appConfig as defaultConfig} from 'app/app.config';
import {locale as navigationEnglish} from 'app/navigation/i18n/en';
import {locale as navigationSpanish} from 'app/navigation/i18n/es';
import {fuseAnimations} from '../@fuse/animations';

@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    providers: [TranslatePipe]
})
export class AppComponent implements OnInit, OnDestroy {
    fuseConfig: any;
    appConfig: any;
    navigation: any;
    shortcutItems: any[];
    currentUser: User;
    notifications: any;
    pageTitle: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {DOCUMENT} document
     * @param {AppConfigService} _appConfigService
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseNavigationService} _fuseNavigationService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {FuseSplashScreenService} _fuseSplashScreenService
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     * @param {TranslatePipe} _translatePipe
     * @param {Platform} _platform
     * @param {Title} _titleService
     * @param {TranslateService} _translateService
     * @param {Router} _router
     * @param {CookieService} _cookieService
     * @param {NotificationService} _notificationService
     * @param {AuthenticationService} _authenticationService
     */
    constructor(
        @Inject(DOCUMENT) private document: any,
        private _appConfigService: AppConfigService,
        private _fuseConfigService: FuseConfigService,
        private _fuseNavigationService: FuseNavigationService,
        private _fuseSidebarService: FuseSidebarService,
        private _fuseSplashScreenService: FuseSplashScreenService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _translateService: TranslateService,
        private _translatePipe: TranslatePipe,
        private _platform: Platform,
        private _titleService: Title,
        private _router: Router,
        private _cookieService: CookieService,
        private _notificationService: NotificationService,
        private _authenticationService: AuthenticationService
    ) {
        // Get current user
        this._authenticationService.currentUser.subscribe(x => this.currentUser = x);

        // Get default navigation
        this.navigation = navigation;

        // Get default app config
        this.appConfig = defaultConfig;

        // Register the navigation to the service
        this._fuseNavigationService.register('main', this.navigation);

        // Set the main navigation as our current navigation
        this._fuseNavigationService.setCurrentNavigation('main');

        // Add languages
        this._translateService.addLangs(this.appConfig.installedLangs);

        // Set the default language
        this._translateService.setDefaultLang(this.appConfig.defaultLang);

        // Set the navigation translations
        this._fuseTranslationLoaderService.loadTranslations(navigationEnglish, navigationSpanish);

        // Use a language
        if (this._translateService.getBrowserLang() && this._translateService.getLangs().includes(this._translateService.getBrowserLang())) {
            this._translateService.use(this._translateService.getBrowserLang());
        } else {
            this._translateService.use(this._translateService.getDefaultLang());
        }

        // Set the default shortcut items for toolbar
        this.shortcutItems = [];

        /**
         * ----------------------------------------------------------------------------------------------------
         * ngxTranslate Fix Start
         * ----------------------------------------------------------------------------------------------------
         */

        /**
         * If you are using a language other than the default one, i.e. Turkish in this case,
         * you may encounter an issue where some of the components are not actually being
         * translated when your app first initialized.
         *
         * This is related to ngxTranslate module and below there is a temporary fix while we
         * are moving the multi language implementation over to the Angular's core language
         * service.
         **/

        // Set the default language to 'en' and then back to 'tr'.
        // '.use' cannot be used here as ngxTranslate won't switch to a language that's already
        // been selected and there is no way to force it, so we overcome the issue by switching
        // the default language back and forth.

        setTimeout(() => {
            this._translateService.setDefaultLang('en');
            this._translateService.setDefaultLang(this._translateService.getDefaultLang());
        });

        /**
         * ----------------------------------------------------------------------------------------------------
         * ngxTranslate Fix End
         * ----------------------------------------------------------------------------------------------------
         */

        // Add is-mobile class to the body if the platform is mobile
        if (this._platform.ANDROID || this._platform.IOS) {
            this.document.body.classList.add('is-mobile');
        }

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Remove shortcuts on toolbar
        if (!this._cookieService.check('FUSE2.shortcuts')) {
            this._cookieService.set('FUSE2.shortcuts', JSON.stringify(this.shortcutItems));
        }
        // Subscribe to config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((config) => {

                this.fuseConfig = config;

                // Boxed
                if (this.fuseConfig.layout.width === 'boxed') {
                    this.document.body.classList.add('boxed');
                } else {
                    this.document.body.classList.remove('boxed');
                }

                // Color theme - Use normal for loop for IE11 compatibility
                for (let i = 0; i < this.document.body.classList.length; i++) {
                    const className = this.document.body.classList[i];

                    if (className.startsWith('theme-')) {
                        this.document.body.classList.remove(className);
                    }
                }

                this.document.body.classList.add(this.fuseConfig.colorTheme);
            });

        this._appConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((config) => {
                this.appConfig = config;
                this.pageTitle = this.appConfig.appName;
                this._titleService.setTitle(this.pageTitle);
            });

        this._notificationService.onNotificationChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((notifications) => {
                this.notifications = notifications;
                this._notificationService.setCandidateNavigationBadges(notifications);
            });

        // Get current navigation
        // this._fuseNavigationService.onNavigationChanged
        //     .subscribe(() => {
        //         this._setNavigation();
        //     });

        // Subscribe to router changes
        this._router.events.subscribe((event: Event) => {
            if (event instanceof NavigationStart) {
                this._setNavigation(navigation, event);
                this._titleService.setTitle(this.pageTitle);
                this._notificationService.onNotificationChanged
                    .pipe(takeUntil(this._unsubscribeAll))
                    .subscribe((notifications) => {
                        this.notifications = notifications;
                        this._notificationService.setCandidateNavigationBadges(notifications);
                    });
            }
        });
        // Update the navigation menu
        this._setNavigation();
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Set visible navigation menu depending on user group
     *
     * @private
     */
    private _setNavigation(currentNav: any = navigation, event: any | null = null): any {
        const newNav = [];
        for (const currentNavItem of currentNav) {
            let currentId: any;
            let currentGroup = [Group.Guest];
            if (currentNavItem.children) {
                currentNavItem.children = this._setNavigation(currentNavItem.children, event);
            }
            if (currentNavItem.url) {
                // TODO: try to check auth guard for group by url
            }
            if (currentNavItem.url && event && event.url && currentNavItem.url.includes(event.url)) {
                this.pageTitle = this._translatePipe.transform(currentNavItem.translate) + ' - ' + this.appConfig.appName;
            } else {
                this.pageTitle = this.appConfig.appName;
            }
            currentId = currentNavItem.id.split('.');
            const currentNavGroup = Group[_.capitalize(currentId[0])];
            if (this.currentUser && this.currentUser.groups) {
                currentGroup = this.appConfig.appSettings.externalSite && this.currentUser.groups.includes(Group.Candidate) && this.currentUser.groups.length === 1 ? [Group.External] : this.currentUser.groups;
            }
            if (currentGroup.includes(Group.Admin)) {
                this._fuseNavigationService.updateNavigationItem(currentNavItem.id, {
                    hidden: false
                });
            } else if (!currentGroup.includes(currentNavGroup) && currentId.length > 1) {
                this._fuseNavigationService.updateNavigationItem(currentNavItem.id, {
                    hidden: true
                });
            } else {
                this._fuseNavigationService.updateNavigationItem(currentNavItem.id, {
                    hidden: false
                });
            }
            newNav.push(currentNavItem);
        }
        return newNav;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle sidebar open
     *
     * @param key
     */
    toggleSidebarOpen(key): void {
        this._fuseSidebarService.getSidebar(key).toggleOpen();
    }

    /**
     * Logout
     */
    logout(): any {
        this._authenticationService.logout();
        // localStorage.removeItem('currentUser');
        this._router.navigate(['/']);
    }

}
