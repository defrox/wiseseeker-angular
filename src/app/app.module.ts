import {LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule, Title} from '@angular/platform-browser';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ReactiveFormsModule} from '@angular/forms';
import {MatMomentDateModule} from '@angular/material-moment-adapter';
import {TranslateModule} from '@ngx-translate/core';
import {DeviceDetectorModule} from 'ngx-device-detector';
import 'hammerjs';

import {FuseModule} from '@fuse/fuse.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule} from '@fuse/components';

import {fuseConfig} from 'app/fuse-config';

import {AppConfigModule} from 'app/_helpers/app-config.module';
import {appConfig} from 'app/app.config';

import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';
import {AngularFireAuthModule} from 'angularfire2/auth';

// CoundDown
import {CountdownModule} from 'ngx-countdown';
// import {CountdownConfig} from 'ngx-countdown/src/countdown.config';
// HighCharts
import {ChartModule, HIGHCHARTS_MODULES} from 'angular-highcharts';
import * as more from 'highcharts/highcharts-more.src';
import * as solidGauge from 'highcharts/modules/solid-gauge.src';

// Import angular-fusioncharts
import {FusionChartsModule} from 'angular-fusioncharts';

// Import FusionCharts library and chart modules
import * as FusionCharts from 'fusioncharts';
import * as Widgets from 'fusioncharts/fusioncharts.widgets';

import * as FusionTheme from 'fusioncharts/themes/fusioncharts.theme.fusion';

// Pass the fusioncharts library and chart modules
FusionChartsModule.fcRoot(FusionCharts, Widgets, FusionTheme);

// used to create fake backend
import {fakeBackendProvider} from 'app/_helpers';
import {FakeDbService} from 'app/fake-db/fake-db.service';
import {InMemoryWebApiModule} from 'angular-in-memory-web-api';

import {environment} from 'environments/environment';
import {AppComponent} from 'app/app.component';
import {LayoutModule} from 'app/layout/layout.module';
import {AppRouting} from 'app/app.routing';
import {BlobErrorInterceptor, JwtInterceptor, ErrorInterceptor, ApiInterceptor, DefroxModule, MaterialModule} from 'app/_helpers';
import {CacheInterceptor} from 'app/_helpers';


export function highChartsModules(): any {
    // apply Highcharts Modules to this array
    return [more, solidGauge];
}


@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        ReactiveFormsModule,
        DeviceDetectorModule.forRoot(),

        TranslateModule.forRoot(),

        // Material moment date module
        MatMomentDateModule,

        // Material
        MaterialModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // HighCharts
        ChartModule,

        // CountDown
        CountdownModule,

        // Fusion Charts
        FusionChartsModule,

        // FireBase Modules
        AngularFireModule.initializeApp(environment.firebaseConfig),
        AngularFireDatabaseModule,
        AngularFireAuthModule,

        // Fake Backend
        InMemoryWebApiModule.forRoot(FakeDbService, {
            delay: 500,
            passThruUnknownUrl: true
        }),

        // App Config
        AppConfigModule.forRoot(appConfig),

        // Defrox
        DefroxModule,

        // Layout
        LayoutModule,

        // App routing
        AppRouting
    ],
    providers: [
        Title,
        {provide: LOCALE_ID, useValue: 'es' },
        {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: BlobErrorInterceptor, multi: true},
        // provider used to create fake backend
        FakeDbService,
        fakeBackendProvider,
        // this must be the last interceptor
        {provide: HTTP_INTERCEPTORS, useClass: CacheInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: ApiInterceptor, multi: true},
        {provide: HIGHCHARTS_MODULES, useFactory: highChartsModules}
    ],
    bootstrap: [
        AppComponent
    ]
})

export class AppModule {
}
