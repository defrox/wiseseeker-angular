import {interval, Observable} from 'rxjs';
import {map} from 'rxjs/operators';

export function startTimer(timeLeft: number, timeTotal: number, delay: number = 100, currScope = null): any {
    return interval(delay).pipe(
        map(() => {
            timeLeft = timeLeft - (delay / 1000);
            /*if (timeLeft <= 60) {
                this.timerProgressColor = 'alert';
                this._changeDetectorRef.detectChanges();
            } else if (timeLeft <= 10) {
                this.timerProgressColor = 'warn';
                this._changeDetectorRef.detectChanges();
            }*/
            currScope.timeLeft = timeLeft;
            return (timeTotal - timeLeft) * 100 / timeTotal;
        })
    );
}
