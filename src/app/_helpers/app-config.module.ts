import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {APP_CONFIG} from 'app/_services/config.service';

@NgModule()
export class AppConfigModule {
    constructor(@Optional() @SkipSelf() parentModule: AppConfigModule) {
        if (parentModule) {
            throw new Error('AppConfigModule is already loaded. Import it in the AppModule only!');
        }
    }

    static forRoot(config): ModuleWithProviders {
        return {
            ngModule: AppConfigModule,
            providers: [
                {
                    provide: APP_CONFIG,
                    useValue: config
                }
            ]
        };
    }
}
