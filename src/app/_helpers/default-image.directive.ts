import {Directive, Input, HostBinding, HostListener} from '@angular/core';

@Directive({
    selector: 'img[default]'
})
export class DefaultImageDirective {
    @Input() @HostBinding() src: string;
    @Input() default: string;

    @HostBinding('class') className;

    @HostListener('error') onError(): void {
        this.src = this.default;
    }

    @HostListener('load') onLoad(): void {
        this.className = 'image-loaded';
    }

    @HostListener('click') onClick(): void {
        console.log(this.default);
    }

}
