import {Directive, Input, ElementRef, Renderer2, HostListener, OnInit} from '@angular/core';

@Directive({
    selector: '[colorByValue]'
})
export class ColorByValueDirective implements OnInit {
    constructor(public el: ElementRef, public renderer: Renderer2) {
        this.renderer.addClass(this.el.nativeElement, 'light-blue-fg');
    }
    @Input() colorByValue: number;
    @Input() maxValue: number;

    ngOnInit(): void {
        this.changeColorByValueClass();
    }

    private changeColorByValueClass(): void {
        let colorByValueClass = 'light-blue-fg';
        const ratio = +this.colorByValue * 100 / +this.maxValue;
        if (ratio > 80) {
            colorByValueClass = 'green-fg';
        } else if (ratio > 60) {
            colorByValueClass = 'lime-fg';
        } else if (ratio > 40) {
            colorByValueClass = 'amber-fg';
        } else if (ratio > 20) {
            colorByValueClass = 'orange-fg';
        } else {
            colorByValueClass = 'red-fg';
        }
        this.renderer.addClass(this.el.nativeElement, colorByValueClass);
    }

}
