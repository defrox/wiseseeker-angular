﻿import {Injectable} from '@angular/core';
import {HttpRequest, HttpResponse, HttpHandler, HttpEvent, HttpInterceptor, HTTP_INTERCEPTORS} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {delay, mergeMap, materialize, dematerialize} from 'rxjs/operators';
import {User, Group} from 'app/_models';
import {UserApiService} from 'app/_services';


@Injectable()
export class FakeBackendInterceptor implements HttpInterceptor {

    users: User[] = JSON.parse(localStorage.getItem('users'));

    constructor(
        private _userService: UserApiService
    ) {
        // this._userService.getAll().subscribe((users: any) => this.users = users);
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const authHeader = request.headers.get('Authorization');
        const isLoggedIn = authHeader && authHeader.startsWith('Bearer fake-jwt-token');
        const groupString = isLoggedIn && authHeader.split('.')[1];
        const group = groupString ? Group[groupString] : null;

        // wrap in delayed observable to simulate server api call
        return of(null).pipe(mergeMap(() => {
            // get rest
            if (((request.url.includes('/candidate-') && !request.url.includes('/candidate-personality') && !request.url.includes('/candidate-dashboard') && !request.url.includes('/candidate-jobs') && !request.url.includes('/candidate-job-categories')) ||
                    (request.url.includes('/recruiter-') && !request.url.includes('/recruiter-dashboard')) || request.url.includes('/job-') || request.url.includes('/knowledge-x')) && request.method === 'GET') {
                // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
                if (request.headers.get('Authorization') !== '') {
                    return of(new HttpResponse({status: 200, body: this.users}));
                } else {
                    // return 401 not authorised if token is null or invalid
                    return throwError({status: 401, error: {message: 'Unauthorised'}});
                }
            }

            // pass through any requests not handled above
            return next.handle(request);

        }))

        // call materialize and dematerialize to ensure delay even if an error is thrown (https://github.com/Reactive-Extensions/RxJS/issues/648)
            .pipe(materialize())
            .pipe(delay(500))
            .pipe(dematerialize());
    }
}

export let fakeBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HTTP_INTERCEPTORS,
    useClass: FakeBackendInterceptor,
    multi: true
};
