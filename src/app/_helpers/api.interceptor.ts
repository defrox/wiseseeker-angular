import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from 'environments/environment';
import {FakeDbService} from 'app/fake-db/fake-db.service';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class ApiInterceptor implements HttpInterceptor {
    apiURL: string = environment.apiConfig.url + environment.apiConfig.version;

    constructor(
        private _fakeDbService: FakeDbService,
        private _translateService: TranslateService,
        ) {
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        // Remove for production
        const fakes = this._fakeDbService.createDb();
        const model = request.url.replace('api/', '').split('/');

        // Add api url from environment file and headers
        if (!fakes.hasOwnProperty(model[0])) {
            if (request.url.startsWith('api/render/pdf') && !request.url.startsWith(this.apiURL)) {
                const newUrl = this.apiURL + request.url.replace('api/', '/');
                request = request.clone({
                    url: newUrl,
                    setHeaders: {
                        'Accept': 'application/pdf',
                        'Content-Type': 'application/pdf',
                        'Accept-Language': this._translateService.currentLang
                    },
                    responseType: 'blob'
                });
            } else if (request.url.startsWith('api/') && !request.url.startsWith(this.apiURL)) {
                const newUrl = this.apiURL + request.url.replace('api/', '/');
                request = request.clone({
                    url: newUrl,
                    setHeaders: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                        'Accept-Language': this._translateService.currentLang
                    }
                });
            }
        }
        return next.handle(request);
    }
}
