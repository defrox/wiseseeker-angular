import {NgModule} from '@angular/core';
import {DefaultImageDirective} from './default-image.directive';
import {DisableControlDirective} from './disable-control.directive';
import {ColorByValueDirective} from './color-by-value.directive';
import {NgStringPipesModule} from 'ngx-pipes';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {GaugeChartModule} from 'angular-gauge-chart';
import {ExternalLinkDirective} from './external-link.directive';
import {SafeHtmlPipe} from './safe-html.pipe';
import {FormatStringPipe} from './format-string.pipe';
import {NgbProgressbarModule} from '@ng-bootstrap/ng-bootstrap';
import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {far} from '@fortawesome/free-regular-svg-icons';
import {fab} from '@fortawesome/free-brands-svg-icons';

@NgModule({
    imports: [
        FontAwesomeModule,
        NgStringPipesModule,
        GaugeChartModule,
        NgbProgressbarModule
    ],
    declarations: [
        DefaultImageDirective,
        DisableControlDirective,
        ExternalLinkDirective,
        ColorByValueDirective,
        FormatStringPipe,
        SafeHtmlPipe
    ],
    exports: [
        DefaultImageDirective,
        DisableControlDirective,
        ExternalLinkDirective,
        ColorByValueDirective,
        NgStringPipesModule,
        FontAwesomeModule,
        GaugeChartModule,
        SafeHtmlPipe,
        FormatStringPipe,
        NgbProgressbarModule
    ]
})
export class DefroxModule {
    constructor() {
        // Add an icon to the library for convenient access in other components
        library.add(fas, far, fab);
    }
}
