import {Injectable} from '@angular/core';
import {HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse} from '@angular/common/http';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {retry, catchError, filter, take, switchMap} from 'rxjs/operators';
import {AuthenticationService, SnackAlertService} from 'app/_services';
import {environment} from 'environments/environment';
import {TranslateService} from '@ngx-translate/core';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';

// Import the locale files
import {locale as english} from 'app/main/i18n/en';
import {locale as spanish} from 'app/main/i18n/es';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
    isRefreshingToken = false;
    tokenSubject: BehaviorSubject<string> = new BehaviorSubject<string>(null);

    constructor(
        private _translationLoader: FuseTranslationLoaderService,
        private _translateService: TranslateService,
        private _authenticationService: AuthenticationService,
        private _snackAlertService: SnackAlertService
    ) {
        this._translationLoader.loadTranslations(spanish, english);
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        return next.handle(request).pipe(retry(1), catchError((err: HttpErrorResponse) => {
            if (err.status === 401 && err.error && (err.error.hasOwnProperty('non_field_errors') && err['error']['non_field_errors'][0] === 'User doesn\'t exist.' ||
                err.error.hasOwnProperty('detail') && err.error['detail'] === 'Invalid signature.') ||
                (err.status === 400 && err.error && (err.error.error === 'invalid_grant' ||
                    (err.error.hasOwnProperty('non_field_errors') && err['error']['non_field_errors'][0] === 'Signature has expired.')))) {
                    // if (err && err.status === 400 && err.error && err.error.error === 'invalid_grant') {
                    // If we get a 400 and the error message is 'invalid_grant', the token is no longer valid so logout.
                    this.logoutUser();
            }
            if (err.status === 401 && this._authenticationService.currentUserValue) {
                this.handle401Error(request, next);
            }
            let errorMessage = '';
            if (err.error && err.error.hasOwnProperty('detail')) {
                // django
                errorMessage = `Error: ${err['error']['detail']}`;
            } else if (err.error && err.status === 400) {
                // django
                errorMessage = this._translateService.instant('MAIN.ERRORS.ERRORS_OCCURRED_TEXT') + ':<br/><br/>';
                if (err.error instanceof Array) {
                    for (const item of err.error) {
                        for (const element of Object.keys(item)) {
                            errorMessage += element + ': ' + item[element] + '<br/>';
                        }
                    }
                } else {
                    for (const item of Object.keys(err.error)) {
                        errorMessage += item + ': ' + err.error[item] + '<br/>';
                    }
                }
            } else if (err.error && err.error.hasOwnProperty('non_field_errors')) {
                // django
                errorMessage = `Error: ${err['error']['non_field_errors'][0]}`;
            } else if (err.error instanceof ErrorEvent) {
                // client-side error
                errorMessage = `Error: ${err.error.message}`;
            } else if (err.error && err.status && err.statusText && err.message) {
                // server-side error
                errorMessage = `Error Code: ${err.status} ${err.statusText}` + '\n' + `Message: ${err.message}`;
            } else {
                // server-side error
                errorMessage = `${err.status} ${err.statusText}<br/>${err.message}`;
            }

            if (environment.enableDebug) {
                console.log(err);
            }

            this._snackAlertService.error(errorMessage);
            return throwError(err);
        }));
    }

    handle401Error(req: HttpRequest<any>, next: HttpHandler): any {
        if (!this.isRefreshingToken) {
            this.isRefreshingToken = true;

            // Reset here so that the following requests wait until the token
            // comes back from the refreshToken call.
            this.tokenSubject.next(null);

            return this._authenticationService.refreshToken()
                .switchMap((newToken: string) => {
                    if (newToken) {
                        this.tokenSubject.next(newToken);
                        return next.handle(this.addToken(req, newToken));
                    }

                    // If we don't get a new token, we are in trouble so logout.
                    return this.logoutUser();
                })
                .catch( err => {
                    // If there is an exception calling 'refreshToken', bad news so logout.
                    return this.logoutUser();
                })
                .finally(() => {
                    this.isRefreshingToken = false;
                });
        } else {
            return this.tokenSubject.pipe(
                filter(token => token != null),
                take(1),
                switchMap(token => {
                    this._authenticationService.updateUserToken(token);
                    return next.handle(this.addToken(req, token));
                })
            );
        }
    }

    handle400Error(err): any {
        if (err && err.status === 400 && err.error && (err.error.error === 'invalid_grant' || (err.error.hasOwnProperty('non_field_errors') && err['error']['non_field_errors'][0] === 'Signature has expired.'))) {
        // if (err && err.status === 400 && err.error && err.error.error === 'invalid_grant') {
            // If we get a 400 and the error message is 'invalid_grant', the token is no longer valid so logout.
            return this.logoutUser();
        }
        return throwError(err);
    }
    
    logoutUser(): void {
        this._snackAlertService.error('Session expired');
        this._authenticationService.logout();
        location.reload(true);
    }

    addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
        return req.clone({ setHeaders: { Authorization: 'JWT ' + token }});
    }
}
