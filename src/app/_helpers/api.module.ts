import {NgModule} from '@angular/core';
import {
    AnswerApiService,
    AssessmentApiService,
    Big5QuestionApiService,
    EvaluationApiService,
    IpvQuestionApiService,
    KnowledgeApiService,
    MethodApiService,
    QuestionApiService,
    RoleApiService,
    SoftSkillApiService,
    TestExecutionApiService,
    TestTypeApiService,
    TestApiService,
    UserApiService
} from 'app/_services';

@NgModule({
    providers: [
        AnswerApiService,
        AssessmentApiService,
        Big5QuestionApiService,
        EvaluationApiService,
        IpvQuestionApiService,
        KnowledgeApiService,
        MethodApiService,
        QuestionApiService,
        RoleApiService,
        SoftSkillApiService,
        TestExecutionApiService,
        TestTypeApiService,
        TestApiService,
        UserApiService
    ]
})
export class ApiModule {
}
