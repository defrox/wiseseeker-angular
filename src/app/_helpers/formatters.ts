export function radarTooltipFormatter(): string {
    return this.x + ' <b> ' + this.y + ' </b>';
}

export function centeredGaugeFormatterOld(): number | string {
    return Math.abs((this.y - 50) * 2);
}

export function centeredGaugeFormatter(): number | string {
    return Math.abs(this.y);
}

export function formatValueCenteredGauge(value): number | string {
    return Math.abs((value - 20) * 1.25);
}

export function roundValue(): number | string {
    return +this.y.toFixed(0);
}
