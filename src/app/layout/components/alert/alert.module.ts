import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {FuseSharedModule} from '@fuse/shared.module';
import {AlertComponent} from 'app/layout/components/alert/alert.component';

@NgModule({
    declarations: [
        AlertComponent
    ],
    imports: [
        RouterModule,
        FuseSharedModule
    ],
    exports: [
        AlertComponent
    ]
})
export class AlertModule {
}
