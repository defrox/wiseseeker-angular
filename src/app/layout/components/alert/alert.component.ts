﻿import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';
import {AlertService} from 'app/_services';

@Component({
    selector: 'alert',
    templateUrl: 'alert.component.html'
})

export class AlertComponent implements OnInit, OnDestroy {
    private subscription: Subscription;
    alertMessage: any;

    constructor(private _alertService: AlertService) {
    }

    ngOnInit() {
        this.subscription = this._alertService.getAlertMessage().subscribe(alertMessage => {
            this.alertMessage = alertMessage;
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
