import {Component, OnDestroy, OnInit, QueryList, ViewChildren, ViewEncapsulation} from '@angular/core';
import {NotificationService} from 'app/_services';
import {Event, NavigationEnd, NavigationStart, Router} from '@angular/router';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {QuickPanelCloserDirective} from 'app/layout/components/quick-panel/quick-panel-closer.directive';

// Import the locale files
import {locale as english} from 'app/main/i18n/en';
import {locale as spanish} from 'app/main/i18n/es';
import {TranslateService} from '@ngx-translate/core';

@Component({
    selector     : 'quick-panel',
    templateUrl  : './quick-panel.component.html',
    styleUrls    : ['./quick-panel.component.scss'],
    providers    : [QuickPanelCloserDirective],
    encapsulation: ViewEncapsulation.None
})
export class QuickPanelComponent implements OnInit, OnDestroy {
    date: Date;
    events: any[];
    notes: any[];
    settings: any;
    notifications: any;
    locale = 'en';
    notificationsSubscription: Subscription;

    // Private
    private _unsubscribeAll: Subject<any>;

    @ViewChildren(QuickPanelCloserDirective)
    quickPanelCloserDirective: QueryList<QuickPanelCloserDirective>;

    /**
     * Constructor
     */
    constructor(
        private _quickPanelCloserDirective: QuickPanelCloserDirective,
        private _notificationsService: NotificationService,
        private _router: Router,
        private _translationLoader: FuseTranslationLoaderService,
        private _translateService: TranslateService
    )
    {
        // Set the defaults
        this.date = new Date();
        this.settings = {
            notify: true,
            cloud : false,
            retro : true
        };
        this.notificationsSubscription = this._notificationsService.onNotificationChanged.subscribe(notifications => {
            this.notifications = notifications;
            this.notifications = this._notificationsService.getCandidateNotificationsBadges(notifications);
        });

        // Subscribe to router changes
        this._router.events.subscribe((event: Event) => {
            if (event instanceof NavigationStart) {
                this._notificationsService.reloadUserNotifications();
            }
        });

        this._translationLoader.loadTranslations(english, spanish);
        this.locale = this._translateService.getDefaultLang();
        this._unsubscribeAll = new Subject();

    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this._notificationsService.onNotificationChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(notifications => {
                this.notifications = notifications;
                this.notifications = this._notificationsService.getCandidateNotificationsBadges(notifications);
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
