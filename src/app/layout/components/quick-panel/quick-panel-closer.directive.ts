import {Directive, HostListener} from '@angular/core';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';

@Directive({
    selector: 'a:[routerLink]'
})
export class QuickPanelCloserDirective {
    constructor (
        private _fuseSidebarService: FuseSidebarService
    ) {}
    @HostListener('click')
    closeQuickPanel(): void {
        this._fuseSidebarService.getSidebar('quickPanel').close();
    }
}
