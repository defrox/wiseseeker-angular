import {NgModule} from '@angular/core';
import {MatDividerModule, MatListModule, MatSlideToggleModule, MatButtonModule} from '@angular/material';
import {RouterModule} from '@angular/router';
import {NgStringPipesModule} from 'ngx-pipes';
import {FuseSharedModule} from '@fuse/shared.module';
import {TranslateModule} from '@ngx-translate/core';
import {QuickPanelComponent} from 'app/layout/components/quick-panel/quick-panel.component';
import {QuickPanelCloserDirective} from 'app/layout/components/quick-panel/quick-panel-closer.directive';
import {registerLocaleData} from '@angular/common';
import localeSpanish from '@angular/common/locales/es';

registerLocaleData(localeSpanish);

@NgModule({
    declarations: [
        QuickPanelComponent,
        QuickPanelCloserDirective
    ],
    imports: [
        RouterModule,
        MatDividerModule,
        MatListModule,
        MatSlideToggleModule,
        MatButtonModule,
        TranslateModule,
        NgStringPipesModule,
        FuseSharedModule,
    ],
    exports: [
        QuickPanelComponent,
        QuickPanelCloserDirective
    ]
})
export class QuickPanelModule {
}
