import { NgModule } from '@angular/core';
import { LoaderOverlayComponent } from 'app/layout/components/loader-overlay/loader-overlay.component';

@NgModule({
    declarations: [
        LoaderOverlayComponent
    ],
    imports     : [],
    exports     : [
        LoaderOverlayComponent
    ]
})
export class LoaderOverlayModule
{}
