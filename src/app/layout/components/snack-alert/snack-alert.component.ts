﻿import {Component, Input, OnInit, OnDestroy, ViewEncapsulation, Inject} from '@angular/core';
import {Subscription} from 'rxjs';
import {SnackAlertService} from 'app/_services';
import {MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MAT_SNACK_BAR_DATA} from '@angular/material';

@Component({
    selector: 'snack-alert',
    templateUrl: './snack-alert.component.html'
})

export class SnackAlertComponent implements OnInit, OnDestroy {
    private subscription: Subscription;
    private snackRef: any;
    // snackMessage: any;
    private _snackMessage: any;
    private _snackTypes = {
        'error': 'snack-alert-danger',
        'info': 'snack-alert-info',
        'warning': 'snack-alert-warning',
        'success': 'snack-alert-success',
        'primary': 'snack-alert-primary',
        'secondary': 'snack-alert-secondary',
        'dark': 'snack-alert-dark',
        'light': 'snack-alert-light'
    };
    // Snack Bar Configuration
    private _snackConfig = {
        duration: 5000,
        horizontalPosition: <MatSnackBarHorizontalPosition>'center',
        verticalPosition: <MatSnackBarVerticalPosition>'top'
    };

    constructor(
        private _snackAlertService: SnackAlertService,
        public snackBar: MatSnackBar
    ) {
    }

    ngOnInit(): void {
        this.subscription = this._snackAlertService.getSnackAlertMessage().subscribe(snackMessage => {
            this.snackMessage = snackMessage;
        });
    }

    ngOnDestroy(): void {
        this.subscription.unsubscribe();
    }

    openSnackBarOld(snackMessage: string, action: string, type: string): void {
        this.snackBar.open(snackMessage, action, {
            duration: this._snackConfig.duration,
            panelClass: [type],
            horizontalPosition: this._snackConfig.horizontalPosition,
            verticalPosition: this._snackConfig.verticalPosition
        });
    }

    openSnackBar(snackMessage: string, action: string, type: string): void {
        this.snackRef = this.snackBar.openFromComponent(SnackAlertContainerComponent, {
            duration: this._snackConfig.duration,
            panelClass: [type],
            horizontalPosition: this._snackConfig.horizontalPosition,
            verticalPosition: this._snackConfig.verticalPosition,
            data: {
                contents: snackMessage,
                action: action,
                type: type
            }
        });
        this.snackRef.instance.snackRefContainerComponent = this.snackRef;
    }

    @Input()
    set snackMessage(snackMessage: any) {
        this._snackMessage = snackMessage;
        if (snackMessage) {
            this.openSnackBar(snackMessage.text, 'X', this._snackTypes[snackMessage.type]);
        }
    }

    get snackMessage(): any {
        return this._snackMessage;
    }
}


@Component({
    selector: 'snack-alert-container',
    templateUrl: './snack-alert-container.component.html',
    styleUrls: ['./snack-alert.component.scss'],
    encapsulation: ViewEncapsulation.None,
})
export class SnackAlertContainerComponent {
    constructor(
        @Inject(MAT_SNACK_BAR_DATA) public data: any
    ) {  }

    public snackRefContainerComponent: any;

    public dismissSnackbar(): void {
        this.snackRefContainerComponent.dismiss();
    }
}
