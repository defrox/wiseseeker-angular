import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {FuseSharedModule} from '@fuse/shared.module';
import {SnackAlertComponent, SnackAlertContainerComponent} from 'app/layout/components/snack-alert/snack-alert.component';

@NgModule({
    declarations: [
        SnackAlertComponent,
        SnackAlertContainerComponent
    ],
    imports: [
        RouterModule,
        FuseSharedModule
    ],
    exports: [
        SnackAlertComponent
    ],
    entryComponents: [SnackAlertContainerComponent],
})
export class SnackAlertModule {
}
