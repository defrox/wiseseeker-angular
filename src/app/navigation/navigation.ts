import {FuseNavigation} from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id: 'recruiter.group',
        title: 'Recruiter',
        translate: 'NAV.RECRUITER',
        type: 'group',
        children: [
            {
                id: 'recruiter.dashboard',
                title: 'Dashboard',
                translate: 'NAV.DASHBOARD.TITLE',
                type: 'item',
                icon: 'dashboard',
                url: '/recruiter/dashboard'
            },
            /*{
                id: 'recruiter.database',
                title: 'Database',
                translate: 'NAV.DATABASE.TITLE',
                type: 'item',
                icon: 'contacts',
                url: '/recruiter/database'
            },*/
            {
                id: 'recruiter.talent',
                title: 'Talent',
                translate: 'NAV.TALENT.TITLE',
                type: 'item',
                icon: 'assistant',
                url: '/recruiter/talent'
            },
            {
                id: 'recruiter.jobs',
                title: 'Jobs',
                translate: 'NAV.JOBS.TITLE_2',
                type: 'item',
                icon: 'business_center',
                url: '/recruiter/jobs'
            },
            {
                id: 'recruiter.roles',
                title: 'Roles',
                translate: 'NAV.ROLES.TITLE',
                type: 'item',
                icon: 'person_pin_circle',
                url: '/recruiter/roles'
            },
            {
                id: 'recruiter.logout',
                title: 'Sign Out',
                translate: 'NAV.LOGOUT.TITLE',
                type: 'item',
                icon: 'exit_to_app',
                url: '/auth/logout'
            }
        ]
    },
    {
        id: 'candidate.group',
        title: 'Candidate',
        translate: 'NAV.CANDIDATE',
        type: 'group',
        children: [
            {
                id: 'candidate.dashboard',
                title: 'Dashboard',
                translate: 'NAV.DASHBOARD.TITLE',
                type: 'item',
                icon: 'dashboard',
                url: '/candidate/dashboard'
            },
            /*{
                id: 'candidate.profile',
                title: 'User Data',
                translate: 'NAV.PROFILE.TITLE',
                type: 'item',
                icon: 'person',
                url: '/candidate/profile'
            },*/
            {
                id: 'candidate.personality',
                title: 'Personality',
                translate: 'NAV.PERSONALITY.TITLE',
                type: 'item',
                icon: 'insert_emoticon',
                url: '/candidate/personality'
            },
            {
                id: 'candidate.skills',
                title: 'Skills',
                translate: 'NAV.SKILLS.TITLE',
                type: 'item',
                icon: 'verified_user',
                // url: '/candidate/skills'
                url: '/candidate/expertise'
            },
            /*{
                id: 'candidate.expertise',
                title: 'Digital Expertise',
                translate: 'NAV.EXPERTISE.TITLE',
                type: 'item',
                icon: 'important_devices',
                url: '/candidate/expertise'
            },
            {
                id: 'candidate.roles',
                title: 'Roles',
                translate: 'NAV.ROLES.TITLE',
                type: 'item',
                icon: 'person_pin_circle',
                url: '/candidate/roles'
            },*/
            {
                id: 'candidate.knowledge',
                title: 'Knowledge',
                translate: 'NAV.KNOWLEDGE.TITLE',
                type: 'item',
                icon: 'school',
                url: '/candidate/knowledge'
            },
            /*{
                id: 'candidate.experience',
                title: 'Experience',
                translate: 'NAV.EXPERIENCE.TITLE',
                type: 'item',
                icon: 'library_books',
                url: '/candidate/experience'
            },*/
            {
                id: 'candidate.jobs',
                title: 'Jobs',
                translate: 'NAV.JOBS.TITLE',
                type: 'item',
                icon: 'business_center',
                url: '/candidate/jobs'
            },
            /*{
                id: 'candidate.todo',
                title: 'Todo',
                translate: 'NAV.TODO.TITLE',
                type: 'item',
                icon: 'check_box',
                url: '/candidate/todo/all'
            },*/
            {
                id: 'candidate.logout',
                title: 'Sign Out',
                translate: 'NAV.LOGOUT.TITLE',
                type: 'item',
                icon: 'exit_to_app',
                url: '/auth/logout'
            }
        ]
    },
    {
        id: 'external.group',
        title: 'Candidate',
        translate: 'NAV.CANDIDATE',
        type: 'group',
        children: [
            {
                id: 'external.dashboard',
                title: 'Dashboard',
                translate: 'NAV.DASHBOARD.TITLE',
                type: 'item',
                icon: 'dashboard',
                url: '/candidate/dashboard'
            },
            /*{
                id: 'candidate.profile',
                title: 'User Data',
                translate: 'NAV.PROFILE.TITLE',
                type: 'item',
                icon: 'person',
                url: '/candidate/profile'
            },*/
            {
                id: 'external.personality',
                title: 'Personality',
                translate: 'NAV.PERSONALITY.TITLE',
                type: 'item',
                icon: 'insert_emoticon',
                url: '/candidate/personality'
            },
            {
                id: 'external.skills',
                title: 'Skills',
                translate: 'NAV.SKILLS.TITLE',
                type: 'item',
                icon: 'verified_user',
                // url: '/candidate/skills'
                url: '/candidate/expertise'
            },
            /*{
                id: 'external.expertise',
                title: 'Digital Expertise',
                translate: 'NAV.EXPERTISE.TITLE',
                type: 'item',
                icon: 'important_devices',
                url: '/candidate/expertise'
            },
            {
                   id: 'candidate.roles',
                   title: 'Roles',
                   translate: 'NAV.ROLES.TITLE',
                   type: 'item',
                   icon: 'person_pin_circle',
                   url: '/candidate/roles'
               },*/
            {
                id: 'external.knowledge',
                title: 'Knowledge',
                translate: 'NAV.KNOWLEDGE.TITLE',
                type: 'item',
                icon: 'school',
                url: '/candidate/knowledge'
            },
            /*{
                id: 'candidate.experience',
                title: 'Experience',
                translate: 'NAV.EXPERIENCE.TITLE',
                type: 'item',
                icon: 'library_books',
                url: '/candidate/experience'
            },
            {
                id: 'external.jobs',
                title: 'Jobs',
                translate: 'NAV.JOBS.TITLE',
                type: 'item',
                icon: 'business_center',
                url: '/candidate/jobs'
            },
            {
                id: 'candidate.todo',
                title: 'Todo',
                translate: 'NAV.TODO.TITLE',
                type: 'item',
                icon: 'check_box',
                url: '/candidate/todo/all'
            },*/
            {
                id: 'external.logout',
                title: 'Sign Out',
                translate: 'NAV.LOGOUT.TITLE',
                type: 'item',
                icon: 'exit_to_app',
                url: '/auth/logout'
            }
        ]
    },
    {
        id: 'admin.group',
        title: 'Administrator',
        translate: 'NAV.ADMINISTRATOR',
        type: 'group',
        hidden: false,
        children: [
            {
                id: 'admin',
                title: 'Admin',
                translate: 'NAV.ADMIN.TITLE',
                type: 'item',
                icon: 'home',
                url: '/admin'
            },
            /*{
                id: 'academy',
                title: 'Academy',
                translate: 'NAV.ACADEMY.TITLE',
                type: 'item',
                icon: 'school',
                url: '/courses'
            },
            {
                id: 'login',
                title: 'Login',
                translate: 'NAV.LOGIN.TITLE',
                type: 'item',
                icon: 'person',
                url: '/auth/login',
                badge: {
                    title: '25',
                    translate: 'NAV.SAMPLE.BADGE',
                    bg: '#F44336',
                    fg: '#FFFFFF'
                }
            },
            {
                id: 'academy',
                title: 'Academy',
                translate: 'NAV.ACADEMY.TITLE',
                type: 'item',
                icon: 'school',
                url: '/courses'
            },
            {
                id: 'sample',
                title: 'Sample',
                translate: 'NAV.SAMPLE.TITLE',
                type: 'item',
                icon: 'layers',
                url: '/sample',
                badge: {
                    title: '25',
                    translate: 'NAV.SAMPLE.BADGE',
                    bg: '#F44336',
                    fg: '#FFFFFF'
                }
            },*/
            {
                id: 'admin.logout',
                title: 'Sign Out',
                translate: 'NAV.LOGOUT.TITLE',
                type: 'item',
                icon: 'exit_to_app',
                url: '/auth/logout'
            }
        ]
    }
];
