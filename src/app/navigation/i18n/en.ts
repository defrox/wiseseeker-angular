export const locale = {
    lang: 'en',
    data: {
        'NAV': {
            'RECRUITER': 'Recruiter',
            'CANDIDATE': 'Candidate',
            'ADMINISTRATOR': 'Administrator',
            'DASHBOARD'        : {
                'TITLE': 'Dashboard'
            },
            'DATABASE'        : {
                'TITLE': 'Database'
            },
            'TALENT'        : {
                'TITLE': 'Talent'
            },
            'SKILLS'        : {
                'TITLE': 'Skills'
            },
            'EXPERTISE'        : {
                'TITLE': 'Digital Expertise'
            },
            'JOBS'        : {
                'TITLE': 'Open processes',
                'TITLE_2': 'Jobs'
            },
            'LOGOUT'        : {
                'TITLE': 'Sign Out'
            },
            'PROFILE'        : {
                'TITLE': 'User Data'
            },
            'PERSONALITY'        : {
                'TITLE': 'Personality'
            },
            'EXPERIENCE'        : {
                'TITLE': 'Experience'
            },
            'KNOWLEDGE'        : {
                'TITLE': 'Knowledge'
            },
            'ROLES'        : {
                'TITLE': 'Roles'
            },
            'SAMPLE'        : {
                'TITLE': 'Sample',
                'BADGE': '25'
            },
            'LOGIN'        : {
                'TITLE': 'Login'
            },
            'ADMIN'        : {
                'TITLE': 'Admin'
            },
            'ACADEMY'        : {
                'TITLE': 'Academy'
            },
            'TODO'        : {
                'TITLE': 'Todo'
            }
        }
    }
};
