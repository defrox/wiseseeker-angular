export const locale = {
    lang: 'es',
    data: {
        'NAV': {
            'RECRUITER': 'Reclutador',
            'CANDIDATE': 'Candidato',
            'ADMINISTRATOR': 'Administrador',
            'DASHBOARD'        : {
                'TITLE': 'Escritorio'
            },
            'DATABASE'        : {
                'TITLE': 'Base de datos'
            },
            'TALENT'        : {
                'TITLE': 'Talento'
            },
            'SKILLS'        : {
                'TITLE': 'Habilidades'
            },
            'EXPERTISE'        : {
                'TITLE': 'Habilidades Agile'
            },
            'JOBS'        : {
                'TITLE': 'Procesos Abiertos',
                'TITLE_2': 'Selección'
            },
            'LOGOUT'        : {
                'TITLE': 'Cerrar Sesión'
            },
            'PROFILE'        : {
                'TITLE': 'Datos de Usuario'
            },
            'PERSONALITY'        : {
                'TITLE': 'Personalidad'
            },
            'EXPERIENCE'        : {
                'TITLE': 'Experiencia'
            },
            'KNOWLEDGE'        : {
                'TITLE': 'Conocimientos'
            },
            'ROLES'        : {
                'TITLE': 'Roles'
            },
            'SAMPLE'        : {
                'TITLE': 'Muestra',
                'BADGE': '25'
            },
            'LOGIN'        : {
                'TITLE': 'Iniciar Sesión'
            },
            'ADMIN'        : {
                'TITLE': 'Admin'
            },
            'ACADEMY'        : {
                'TITLE': 'Academia'
            },
            'TODO'        : {
                'TITLE': 'Tareas'
            }
        }
    }
};
