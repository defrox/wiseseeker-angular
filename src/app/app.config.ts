import {AppConfig} from 'app/_models/app-config';
import {centeredGaugeFormatter, radarTooltipFormatter, roundValue} from 'app/_helpers/formatters';
import {environment} from 'environments/environment';

/**
 * Default App Configuration
 *
 * You can edit these options to change the default options. All these options also can be
 * changed per component basis or by the environment file.
 */

export const appConfig: AppConfig = {
    logo: {
        icon: 'assets/images/logos/Icon Wise Seeker.png',
        iconInvert: 'assets/images/logos/Icon Wise Seeker white.png',
        iconBig: 'assets/images/logos/Icon Wise Seeker.png',
        small: 'assets/images/logos/Icon Wise Seeker.png',
        medium: 'assets/images/logos/Icon Wise Seeker.png',
        big: 'assets/images/logos/Icon Wise Seeker.png',
        small_var1: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
        medium_var1: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
        big_var1: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
        small_var2 : 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
        medium_var2: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
        big_var2: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png'
    },
    appName: 'The Wise Seeker',
    installedLangs: ['es'],
    defaultLang: 'es',
    appSettings: {
        anonymousSite: true,
        externalSite: false,
        enableExternalAutoApply: false,
        enableUserRegistration: false,
        enableExternalUserRegistration: false,
        autoLoginOnRegister: false,
    },
    timerConfig: {
        demand: true,
        leftTime: 1800,
        template: '$!h!:$!m!:$!s!',
        notify: [60, 10]
    },
    solidGaugeOptions: {
        chart: {
            type: 'solidgauge',
            height: '100%',
            width: 350,
            backgroundColor: 'transparent'
        },
        credits: {enabled: false},
        title: {
            text: 'Title',
            y: 300,
            style: {'font-family': 'Muli, Helvetica Neue, Arial, sans-serif', 'font-size': '38px'}
        },
        pane: {
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: 'white',
                innerRadius: '60%',
                outerRadius: '90%',
                shape: 'arc',
                borderWidth: 0,
            }
        },
        tooltip: {
            enabled: false
        },
        yAxis: {
            stops: [
                [0.33, 'red'],
                [0.66, 'yellow'],
                [1, 'green']
            ],
            length: 5,
            lineWidth: 0,
            minorTicks: false,
            tickAmount: 0,
            tickColor: 'transparent',
            labels: {
                enabled: false,
            },
            min: 0,
            max: 100,
            plotBands: [
                { from: 0, to: 0, label: {text: 'Start', x: 75 , y: 20, align: 'left', verticalAlign: 'bottom', textAlign: 'center', style: {'font-family': 'Muli, Helvetica Neue, Arial, sans-serif', 'font-size': '16px'}}},
                { from: 0, to: 0, label: {text: 'Finish', x: 157, y: 20, align: 'right', verticalAlign: 'bottom', textAlign: 'center', style: {'font-family': 'Muli, Helvetica Neue, Arial, sans-serif', 'font-size': '16px'}}},
                { from: 0, to: 33, color: 'red', outerRadius: '132'},
                { from: 33, to: 66, color: 'yellow', outerRadius: '132'},
                { from: 66, to: 100, color: 'green', outerRadius: '132'},
            ]
        },
        plotOptions: {
            solidgauge: {
                dataLabels: {
                    style: {'fontSize': '36px', 'font-family': 'Muli, Helvetica Neue, Arial, sans-serif', 'fontWeight': 'light'},
                    y: -50,
                    borderWidth: 0
                }
            }
        },
        series: [
            {
                data: [100]
            }
        ]
    },
    solidGaugeOptionsCenter: {
        chart: {
            type: 'solidgauge',
            height: '100%',
            width: 350,
            backgroundColor: 'transparent'
        },
        credits: {enabled: false},
        title: {
            text: 'Title',
            y: 250,
            style: {'font-family': 'Muli, Helvetica Neue, Arial, sans-serif', 'font-size': '36px'}
        },
        pane: {
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: 'white',
                innerRadius: '60%',
                outerRadius: '90%',
                shape: 'arc',
                borderWidth: 0,
            }
        },
        tooltip: {
            enabled: false
        },
        yAxis: {
            stops: [
                [0.5, 'red'],
                [1, 'green']
            ],
            length: 5,
            lineWidth: 0,
            minorTicks: false,
            tickAmount: 0,
            tickColor: 'transparent',
            labels: {
                enabled: false,
            },
            min: 0,
            max: 100,
            plotBands: [
                { from: 0, to: 50, color: 'red', outerRadius: '132'},
                { from: 50, to: 100, color: 'green', outerRadius: '132'},
            ]
        },
        plotOptions: {
            solidgauge: {
                threshold: 50,
                dataLabels: {
                    style: {'fontSize': '36px', 'font-family': 'Muli, Helvetica Neue, Arial, sans-serif', 'fontWeight': 'light'},
                    y: -50,
                    borderWidth: 0,
                    formatter: centeredGaugeFormatter,
                }
            }
        },
        series: [
            {
                data: [100]
            }
        ]
    },
    solidGaugeOptionsCenterColors: [
        {min: '#b0bec5', max: '#546e7a'},
        {min: '#bcaaa4', max: '#6d4c41'},
        {min: '#c5e1a5', max: '#7cb342'},
        {min: '#ffcc80', max: '#fb8c00'},
        {min: '#81d4fa', max: '#039be5'},
    ],
    solidDonutOptions: {
        chart: {
            type: 'solidgauge',
            height: '100%',
            backgroundColor: 'transparent'
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Title',
            y: 30,
            style: {'font-family': 'Muli, Helvetica Neue, Arial, sans-serif', 'font-size': '38px'}
        },
        pane: {
            startAngle: 0,
            endAngle: 360,
            background: {
                backgroundColor: '#dedede',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc',
                borderWidth: 0,
            }
        },
        tooltip: {
            enabled: false
        },
        yAxis: {
            stops: [
                [0.25, 'red'],
                [0.5, 'orange'],
                [0.75, 'yellow'],
                [1, 'green']
            ],
            lineWidth: 0,
            minorTicks: false,
            tickAmount: 0,
            tickColor: 'transparent',
            labels: {
                enabled: false,
            },
            min: 0,
            max: 100,
            plotBands: [
                { from: 0, to: 25, color: 'red', innerRadius: '107.5%', outerRadius: '115%'},
                { from: 25, to: 50, color: 'orange', innerRadius: '107.5%', outerRadius: '115%'},
                { from: 50, to: 75, color: 'yellow', innerRadius: '107.5%', outerRadius: '115%'},
                { from: 75, to: 100, color: 'green', innerRadius: '107.5%', outerRadius: '115%'},
            ]
        },
        plotOptions: {
            solidgauge: {
                dataLabels: {
                    style: {'fontSize': '72px', 'font-family': 'Muli, Helvetica Neue, Arial, sans-serif', 'fontWeight': 'light'},
                    y: -50,
                    borderWidth: 0,
                    formatter: roundValue,
                }
            }
        },
        series: [
            {
                data: [100]
            }
        ]
    },
    solidDonut2Options:  {
        chart: {
            type: 'solidgauge',
            height: '100%',
            backgroundColor: 'transparent'
        },
        credits: {
            enabled: false
        },
        title: {
            text: 'Title',
            y: 30,
            style: {'font-family': 'Muli, Helvetica Neue, Arial, sans-serif', 'font-size': '38px'}
        },
        pane: {
            startAngle: 0,
            endAngle: 360,
            background: {
                backgroundColor: '#dedede',
                innerRadius: '85%',
                outerRadius: '100%',
                shape: 'arc',
                borderWidth: 0,
            }
        },
        tooltip: {
            enabled: false
        },
        yAxis: {
            stops: [
                [1, 'green']
            ],
            lineWidth: 0,
            minorTicks: false,
            tickAmount: 0,
            tickColor: 'transparent',
            labels: {
                enabled: false,
            },
            min: 0,
            max: 100,
        },
        plotOptions: {
            solidgauge: {
                dataLabels: {
                    style: {'fontSize': '72px', 'font-family': 'Muli, Helvetica Neue, Arial, sans-serif', 'fontWeight': 'light'},
                    y: -50,
                    borderWidth: 0,
                    formatter: roundValue,
                }
            }
        },
        series: [
            {
                data: [{y: 20, innerRadius: '85%'}]
            }
        ]
    },
    solidDonutOptionsColors: {
        '1': {min: '#c5e1a5', max: '#7cb342', middle: '#9ccc65'},
        '2': {min: '#ffcc80', max: '#fb8c00', middle: '#ffa726'},
        '3': {min: '#81d4fa', max: '#039be5', middle: '#29b6f6'},
        '4': {min: '#b39ddb', max: '#5e35b1', middle: '#7e57c2'},
        '5': {min: '#ef9a9a', max: '#e53935', middle: '#ef5350'},
        '6': {min: '#80cbc4', max: '#00897b', middle: '#26a69a'},
        '7': {min: '#fff59d', max: '#ffee58', middle: '#fdd835'},
        '8': {min: '#ffab91', max: '#f4511e', middle: '#ff7043'},
        '9': {min: '#bcaaa4', max: '#6d4c41', middle: '#8d6e63'},
        '0': {min: '#b0bec5', max: '#546e7a', middle: '#78909c'},
    },
    spiderWebOptions:  {
        chart: {
            polar: true,
            type: 'line',
            backgroundColor: 'transparent',
            scrollablePlotArea: { minHeight: 768, minWidth: 768, scrollPositionX: 0.5, scrollPositionY: 0.5 }
        },
        title: {
            text: 'Title',
        },
        pane: {
            size: '90%'
        },
        xAxis: {
            categories: ['Cat1', 'Cat2', 'Cat3', 'Cat4', 'Cat5', 'Cat6'],
            labels: {
                reserveSpace: true,
                // align: 'center',
                style: {
                    /*whiteSpace: 'nowrap',
                    textOverflow: 'ellipsis',*/
                    fontSize: '14px',
                }
            },
            tickmarkPlacement: 'on',
            lineWidth: 0
        },
        yAxis: {
            gridLineInterpolation: 'polygon',
            tickInterval: 50,
            minorTickInterval: 25,
            lineWidth: 0,
            min: 0,
            max: 100
        },
        legend: {
            enabled: false
        },
        tooltip: {
            formatter: radarTooltipFormatter
        },
        credits: {
            enabled: false
        },
        responsive: {
            rules: [{
                condition: {
                    maxWidth: 750
                },
                chartOptions: {
                    legend: {
                        align: 'center',
                        verticalAlign: 'bottom'
                    },
                    pane: {
                        size: '70%'
                    },
                    yAxis: {
                        labels: {
                            enabled: false,
                        }
                    }
                }
            }]
        },
        series: [
            {
                name: 'Serie',
                color: '#88a682',
                type: 'area',
                data: [10, 20, 30, 40, 50, 60],
                pointPlacement: 'on'
            }
        ]
    },
};

Object.assign(appConfig, appConfig, environment.appConfig);
