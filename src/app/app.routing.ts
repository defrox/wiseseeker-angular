import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AuthenticationModule, AdminModule, ErrorsModule, CandidateModule, RecruiterModule, DashboardModule} from 'app/main';

const appRoutes: Routes = [
    {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
    {path: 'candidate', loadChildren: 'app/main/candidate/candidate.module#CandidateModule'},
    {path: 'recruiter', loadChildren: 'app/main/recruiter/recruiter.module#RecruiterModule'},
    {path: 'admin', loadChildren: 'app/main/admin/admin.module#AdminModule'},
    {path: 'dashboard', loadChildren: 'app/main/dashboard/dashboard.module#DashboardModule'},

    // Authentication pages
    {path: 'auth', loadChildren: 'app/main/authentication/authentication.module#AuthenticationModule'},

    // Error pages
    {path: 'error', loadChildren: 'app/main/errors/errors.module#ErrorsModule'},
    // otherwise redirect to 404
    {path: '**', redirectTo: '/error/404', pathMatch: 'full'}
];

@NgModule({
    imports: [AuthenticationModule, AdminModule, ErrorsModule, CandidateModule, RecruiterModule, DashboardModule, RouterModule.forRoot(appRoutes, {scrollPositionRestoration: 'enabled'})],
    exports: [RouterModule]
})
export class AppRouting {
}
