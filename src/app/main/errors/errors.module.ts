import {NgModule} from '@angular/core';
import {ErrorsRouting} from 'app/main/errors/errors.routing';

@NgModule({
    imports: [
        ErrorsRouting
    ]
})
export class ErrorsModule {
}
