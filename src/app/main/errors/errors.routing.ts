import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {Error401Component} from 'app/main/errors/401/error-401.component';
import {Error403Component} from 'app/main/errors/403/error-403.component';
import {Error404Component} from 'app/main/errors/404/error-404.component';
import {Error500Component} from 'app/main/errors/500/error-500.component';
import {MaterialModule} from 'app/_helpers/material.module';
import {FuseSharedModule} from '@fuse/shared.module';

const appRoutes: Routes = [
    {path: 'error', children: [
        {path: '', redirectTo: '404', pathMatch: 'full'},
        {path: '401', component: Error401Component},
        {path: '403', component: Error403Component},
        {path: '404', component: Error404Component},
        {path: '500', component: Error500Component},
        // otherwise redirect to 404
        {path: '**', redirectTo: '/error/404', pathMatch: 'full'}
    ]}
];

@NgModule({
    declarations: [
        Error401Component,
        Error403Component,
        Error404Component,
        Error500Component
    ],
    imports: [
        RouterModule.forChild(appRoutes),
        MaterialModule,
        FuseSharedModule
    ],
    exports: [RouterModule]
})
export class ErrorsRouting {
}
