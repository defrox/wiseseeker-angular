﻿import {Component, OnDestroy} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {User} from 'app/_models';
import {AuthenticationService} from 'app/_services';

@Component({templateUrl: 'admin.component.html'})
export class AdminComponent implements OnDestroy {
    currentUser: User;
    currentUserSubscription: Subscription;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _authenticationService: AuthenticationService,
    ) {
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
        this._unsubscribeAll = new Subject();
    }

    ngOnDestroy() {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}
