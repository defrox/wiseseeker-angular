import {NgModule} from '@angular/core';
import {AdminRouting} from 'app/main/admin/admin.routing';

@NgModule({
    imports: [
        AdminRouting
    ]
})
export class AdminModule {
}
