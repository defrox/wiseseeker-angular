import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MaterialModule} from 'app/_helpers/material.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {AdminComponent} from 'app/main/admin';
import {AuthGuard} from 'app/_guards';
import {Group} from 'app/_models';

const appRoutes: Routes = [
    {path: 'admin', children: [
        {path: '', pathMatch: 'full', component: AdminComponent, canActivate: [AuthGuard], data: {groups: [Group.Admin]}},
        // otherwise redirect to 404
        {path: '**', redirectTo: '/error/404', pathMatch: 'full'}
    ]}
];

@NgModule({
    declarations: [
        AdminComponent
    ],
    imports: [
        RouterModule.forChild(appRoutes),
        MaterialModule,
        FuseSharedModule
    ],
    exports: [RouterModule]
})
export class AdminRouting {
}
