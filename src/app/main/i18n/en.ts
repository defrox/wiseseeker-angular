export const locale = {
    lang: 'en',
    data: {
        'MAIN': {
            'ANONYMOUS': 'Anonymous',
            'ALL_SG_F': 'All',
            'ALL_SG_M': 'All',
            'ALL_PL_F': 'All',
            'ALL_PL_M': 'All',
            'CATEGORY': 'Category',
            'TYPE': 'Type',
            'CONGRATULATIONS': 'Congratulations!',
            'DESCRIPTION': 'Description',
            'TITLE': 'Title',
            'ACTIVE': 'Active',
            'TESTS': 'Tests',
            'USERS': 'Users',
            'NOTHING_HERE': 'You have nothing here!',
            'ROWS_PER_PAGE': 'Rows per page:',
            'TODAY': 'Today',
            'VS': 'vs',
            'SEARCH_FOR': 'Search for a talent',
            'FILTER_BY': 'Filter by',
            'NO_RESULTS': 'No results',
            'ENTER_KEYWORD': 'Enter a keyword...',
            'DATE_ACTIVE_FROM': 'Active from',
            'DATE_ACTIVE_TO': 'Active to',
            'FROM': 'From',
            'TO': 'To',
            'YEARS': 'years',
            'MONTHS': 'months',
            'DAYS': 'days',
            'OF_PROGRESS': 'of',
            'NS/NC': 'NS/NC',
            'REQUIRED': 'Required',
            'NOT_REQUIRED': 'Not Required',
            'STATUS': 'Status',
            'APPLIED': 'Applied',
            'NOT_APPLIED': 'Not applied',
            'ROLE': 'Rol',
            'POSITION': 'Position',
            'DATE_FROM': 'From',
            'DATE_TO': 'To',
            'ENTERPRISE': 'Company',
            'LOCATION': 'Location',
            'CITY': 'City',
            'COUNTRY': 'Country',
            'PSICOLOGICAL_PROFILE': {
                'TITLE': 'Perfil Psicológico',
            },
            'ERRORS': {
                'ERRORS_OCCURRED_TEXT': 'Se han producido los siguientes errores',
                'UNDEFINED_ERROR': 'Se ha producido un error. Si el error persiste contacte al administrador.',
            },
            'TRAITS': {
                'TRAIT': 'Trait',
                'TRAITS': 'Traits',
                'OPENNESS': {
                    'TITLE': 'Openess',
                    'TITLES': {
                        'GOOD': 'Sociable',
                        'BAD': 'Introvertido'
                    },
                    'EXCERPT': {
                        'GOOD': 'Curioso, imaginativo, múltiples intereses',
                        'BAD': 'Práctico, convencional, prefiere la rutina'
                    }
                },
                'CONSCIENTIOUSNESS': {
                    'TITLE': 'Conscientiousness',
                    'TITLES': {
                        'GOOD': 'Sociable',
                        'BAD': 'Introvertido'
                    },
                    'EXCERPT': {
                        'GOOD': 'Trabajador, organizado, cunmplidor',
                        'BAD': 'Impulsivo, desorganizado, genial'
                    }
                },
                'EXTRAVERSION': {
                    'TITLE': 'Extroversion',
                    'TITLES': {
                        'GOOD': 'Sociable',
                        'BAD': 'Introvertido'
                    },
                    'EXCERPT': {
                        'GOOD': 'Hablador,  sociable, abierto, afectivo',
                        'BAD': 'Callado, reservado, sobrio,  retraído'
                    }
                },
                'AGREEABLENESS': {
                    'TITLE': 'Agreeableness',
                    'TITLES': {
                        'GOOD': 'Amable',
                        'BAD': 'Crítico'
                    },
                    'EXCERPT': {
                        'GOOD': 'Cooperativo, bondadoso, indulgente',
                        'BAD': 'Crítico, suspicaz, autónomo, despiadado'
                    }
                },
                'NEUROTICISM': {
                    'TITLE': 'Neuroticism',
                    'TITLES': {
                        'GOOD': 'Equilibrado',
                        'BAD': 'Intranquilo'
                    },
                    'EXCERPT': {
                        'GOOD': 'Calmado, tranquilo, equilibrado',
                        'BAD': 'Ansioso, tenso, excitable, intranquilo'
                    }
                },
            },
            'NOTIFICATIONS': {
                'TITLE': 'Notifications',
                'TEXT': 'Pending Tasks',
                'NO_PENDING_TASKS': 'Congrats! You have no pending tasks'
            },
            'PERSONALITY': {
                'TITLE': 'Personality',
                'TEXT': 'You have pending tasks',
                'MUST_DO_TEST': 'You must pass the Big 5 test',
            },
            'SKILLS': {
                'TITLE': 'Skills',
                'TEXT': 'You have pending tasks',
                'MUST_DO_TEST': 'You must pass the tests:',
                'DGV': 'APTITUD COMERCIAL',
                'VR': 'RECEPTIVIDAD',
                'VA': 'AGRESIVIDAD',
            },
            'ROLES': {
                'TITLE': 'Roles',
                'TEXT': 'You have pending tasks',
                'MUST_DO_TESTS': 'You must pass the tests:',
                'TYPES': {
                    'DEFAULT': 'Defecto',
                    'CUSTOM': 'Personalizado',
                },
            },
            'KNOWLEDGE': {
                'TITLE': 'Roles',
                'TEXT': 'You have pending tasks',
                'MUST_DO_TESTS': 'You must pass the tests:',
                'TYPES': {
                    'INTERNAL': 'Interno',
                    'EXTERNAL': 'Externo',
                },
            },
            'EXPERIENCE': {
                'TITLE': 'Experience',
                'TEXT': 'You have pending tasks',
                'MUST_FILL': 'You must fill your experience.',
            },
            'PROFILE': {
                'TITLE': 'Profile',
                'TEXT': 'You have pending task',
                'MUST_FILL': 'You must fill your profile.',
            },
            'JOBS': {
                'TITLE': 'Jobs',
                'TEXT': 'You have pending task',
                'MUST_APPLY': 'You have no job applications yet.',
                'PERSONALITY_RELEVANCE': {
                    '1': 'Nada',
                    '2': 'Poco',
                    '3': 'Algo',
                    '4': 'Mucho',
                },
                'PERSONALITY_SKILL': {
                    '1': 'Abierto',
                    '2': 'Cerrado',
                    '3': 'Responsable',
                    '4': 'Impulsivo',
                    '5': 'Sociable',
                    '6': 'Introvertido',
                    '7': 'Amable',
                    '8': 'Crítico',
                    '9': 'Equilibrado',
                    '10': 'Intranquilo',
                },
            },
            'BUTTON': {
                'ADD': 'Add',
                'ADD_NEW': 'Add New',
                'COMPLETE': 'Complete',
                'ADD_EXPERIENCE': 'Add experience',
                'FINISH_EXPERIENCE': 'Finish experience',
                'ADD_KNOWLEDGE': 'Add a knowledge',
                'FINISH_KNOWLEDGE': 'Finish knowledge',
                'ADD_JOB': 'Add job',
                'FINISH_JOB': 'Finish job',
                'ADD_MORE': 'Add more',
                'FINISH': 'Finish',
                'CLOSE': 'Close',
                'ADD_SKILL': 'Add skill',
                'FINISH_SKILL': 'Finish skill',
                'ADD_ROLE': 'Add role',
                'FINISH_ROLE': 'Finish role',
                'ADD_TALENT': 'Add talent',
                'ADD_PROCESS': 'Add process',
                'FINISH_TALENT': 'Finish talent',
                'APPLY_TO_JOB': 'Apply to a job',
                'APPLY': 'Apply',
                'BACK': 'Back',
                'BACK_TO_DASH': 'Back to dashboard',
                'EDIT': 'Edit',
                'NEXT': 'Next',
                'PREVIOUS': 'Previous',
                'DELETE': 'Delete',
                'CANCEL': 'Cancel',
                'EDIT_PROFILE': 'Edit profile',
                'VIEW_PROFILE': 'View profile',
                'GO_BACK': 'Go Back',
                'LOGOUT': 'Logout',
                'MY_PROFILE': 'My Profile',
                'FINISH_TEST': 'Finish test',
                'PASS_TEST': 'Pass the test',
                'REPEAT_TEST': 'Repeat the test',
                'REPEAT_EVALUATION': 'Repeat the evaluation',
                'FINISH_EVALUATION': 'Finish the evaluation',
                'TAKE_EVALUATION': 'Take evaluation',
                'START': 'Start',
                'REPEAT': 'Repeat',
                'VIEW': 'View',
                'RESULTS': 'Results',
                'VIEW_EDIT': 'View/Edit',
                'VIEW_REPORT': 'View Report',
                'DOWNLOAD_REPORT': 'Download report',
                'VIEW_TALENTS': 'View talents',
                'VIEW_JOBS': 'View jobs',
                'VIEW_PROCESSES': 'View processes',
                'CONTINUE': 'Continue',
                'START_AGAIN': 'Start again',
                'SUBMIT': 'Submit',
                'SAVE': 'Save',
                'UPDATE': 'Update',
                'HELP': 'Help',
                'REGISTER': 'Register',
                'CHANGE_PASSWORD': 'Change password',
                'DELETE_JOB_APPLICATION': 'Delete job application',
                'REPORT': 'Report',
            },
            'PAGINATOR': {
                'ITEMS_PER_PAGE_LABEL': 'Items per page:',
                'NEXT_PAGE_LABEL': 'Next page',
                'PREVIOUS_PAGE_LABEL': 'Previous page',
                'FIRST_PAGE_LABEL': 'First page',
                'LAST_PAGE_LABEL': 'Last page',
                'RANGE_PAGE_LABEL_1': '0 of {{length}}',
                'RANGE_PAGE_LABEL_2': '{{startIndex}} - {{endIndex}} of {{length}}'
            },
            'COLUMN': {
                'ICON': 'Icon',
                'TITLE': 'Title',
                'NO_USERS': 'No. Users',
                'NO_APPLICATIONS': 'No. Applications',
                'ACTIONS': '',
                'TEST_TYPE': 'Type',
                'USER': 'User',
                'USERNAME': 'Username',
                'FIRST_NAME': 'First name',
                'LAST_NAME': 'Last name',
                'EMAIL': 'Email',
                'KTI': 'KTI',
                'STATUS': 'Estado',
                'FROM': 'Desde',
                'TO': 'Hasta',
                'COMPANY': 'Empresa',
                'POSITION': 'Cargo',
                'KNOWLEDGE': 'Knowledge',
                'LEVEL': 'Level',
                'RELEVANCE': 'Relevance',
            },
            'FORMS': {
                'TITLE': 'Title',
                'NAME': 'Name',
                'DESCRIPTION': 'Description',
                'DATE_FROM': 'Date from',
                'DATE_TO': 'Date to',
                'STARTS': 'Starts',
                'ENDS': 'Ends',
                'SELECT': 'Select...',
                'NONE': 'None',
                'FILL_A_DATE': 'Fill a date',
                'CHOOSE_A_FILE': 'Choose a file',
                'NO_FILE_CHOSEN': 'No file chosen',
                'ERRORS': {
                    'FIELD_REQUIRED_F': 'Field {{field}} is required!',
                    'FIELD_REQUIRED': 'Field is required!',
                    'NAME_REQUIRED': 'Name is required!',
                    'FIELD_NUMBER_VALUES': 'Valor entre {{minimo}} y {{maximo}}',
                }
            },
            'TEST_STATUS': {
                'PENDING': 'Not started',
                'STARTED': 'Started',
                'FINISHED': 'Finished',
                'ASSESSED': 'Assessed',
                'REQUESTED': 'Requested',
                'CONFIRMED': 'Confirmed',
                'EXECUTED': 'Executed',
            },
            'TIMER': {
                'DIALOG_TITLE_TIME_OVER': 'Time Over',
                'DIALOG_TEXT_TIME_OVER': 'Your time is over. The results have been saved.',
                '10_SECONDS_LEFT': 'You only have 10 seconds left.',
                '60_SECONDS_LEFT': 'You have less 60 seconds left.',
                'TIME_OVER': 'Your time is over. The results have been saved.',
            },
            'JOB_APPLICATION_STATUS': {
                '0': 'Not applied',
                '1': 'Requested',
                '2': 'Confirmed',
                '3': 'Executed',
                '4': 'Assessed',
            },
        }
    }
};
