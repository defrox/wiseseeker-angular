import {NgModule} from '@angular/core';
import {RecruiterRouting} from 'app/main/recruiter/recruiter.routing';
import {MatPaginatorIntl} from '@angular/material';
import {PaginatorI18n} from 'app/_helpers';
import {TranslateService} from '@ngx-translate/core';

@NgModule({
    imports: [
        RecruiterRouting
    ],
    providers: [
        {provide: MatPaginatorIntl, deps: [TranslateService], useFactory: (translateService: TranslateService) => new PaginatorI18n(translateService).getPaginatorIntl()},
    ]
})
export class RecruiterModule {
}
