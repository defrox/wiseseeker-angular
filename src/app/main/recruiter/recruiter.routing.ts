import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RecruiterDashboardComponent, RecruiterDashboardJobDialogComponent, RecruiterDashboardService, RecruiterDashboardTalentDialogComponent} from 'app/main/recruiter/dashboard';
import {RecruiterDatabaseComponent, RecruiterDatabaseService} from 'app/main/recruiter/database';
import {RecruiterJobsModule} from 'app/main/recruiter/jobs/jobs.module';
import {RecruiterRolesModule} from 'app/main/recruiter/roles/roles.module';
import {RecruiterTalentModule} from 'app/main/recruiter/talent/talent.module';
import {MaterialModule} from 'app/_helpers/material.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseSidebarModule, FuseWidgetModule} from '@fuse/components';
import {AuthGuard} from 'app/_guards';
import {Group} from 'app/_models';
import {TranslateModule} from '@ngx-translate/core';
import {ChartsModule} from 'ng2-charts';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {DefroxModule} from 'app/_helpers';
import {ChartModule} from 'angular-highcharts';

const appRoutes: Routes = [
    {path: 'recruiter', pathMatch: 'prefix', children: [
        {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
        {path: 'dashboard', component: RecruiterDashboardComponent, resolve: {jobs: RecruiterDashboardService}, canActivate: [AuthGuard], data: {groups: [Group.Recruiter]}},
        {path: 'database', component: RecruiterDatabaseComponent, resolve: {jobs: RecruiterDatabaseService}, canActivate: [AuthGuard], data: {groups: [Group.Recruiter]}},
        {path: 'jobs', loadChildren: 'app/main/recruiter/jobs/jobs.module#RecruiterJobsModule'},
        {path: 'roles', loadChildren: 'app/main/recruiter/roles/roles.module#RecruiterRolesModule'},
        {path: 'talent', loadChildren: 'app/main/recruiter/talent/talent.module#RecruiterTalentModule'},
        // otherwise redirect to 404
        {path: '**', redirectTo: '/error/404', pathMatch: 'full'}
    ]}
];

@NgModule({
    declarations: [
        RecruiterDashboardComponent,
        RecruiterDashboardJobDialogComponent,
        RecruiterDashboardTalentDialogComponent,
        RecruiterDatabaseComponent,
    ],
    imports: [
        RouterModule.forChild(appRoutes),
        RecruiterJobsModule,
        RecruiterRolesModule,
        RecruiterTalentModule,
        MaterialModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseWidgetModule,
        TranslateModule,
        ChartsModule,
        NgxChartsModule,
        FontAwesomeModule,
        DefroxModule,
        ChartModule
    ],
    providers: [
        RecruiterDashboardService,
        RecruiterDatabaseService,
        AuthGuard
    ],
    entryComponents: [RecruiterDashboardJobDialogComponent, RecruiterDashboardTalentDialogComponent],
    exports: [RouterModule]
})
export class RecruiterRouting {
}
