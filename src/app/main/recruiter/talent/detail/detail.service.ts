import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {EvaluationApiService, UserApiService} from 'app/_services';
import {TestExecutionStatus} from 'app/_models';
import {TranslateService} from '@ngx-translate/core';
import * as _ from 'lodash';

@Injectable()
export class RecruiterTalentDetailService implements Resolve<any> {
    onTalentChanged: BehaviorSubject<any>;
    talent: any;

    /**
     * Constructor
     *
     * @param {TranslateService} _translateService
     * @param {UserApiService} _userApiService
     * @param {EvaluationApiService} _evaluationApiService
     */
    constructor(
        private _translateService: TranslateService,
        private _userApiService: UserApiService,
        private _evaluationApiService: EvaluationApiService,
    ) {
        // Set the defaults
        this.onTalentChanged = new BehaviorSubject({});
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getTalent(route.params.talentId)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get talent
     *
     * @returns {Promise<any>}
     */
    getTalent(id): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._evaluationApiService.get(id)
                .subscribe((response: any) => {
                    this.talent = response;
                    this.onTalentChanged.next(this.talent);
                    resolve(this.talent);
                }, reject);
        });
    }

    /**
     * Delete talent
     *
     * @returns {Promise<any>}
     */
    deleteTalent(id): Promise<any>
    {
        return this._evaluationApiService.delete(id).toPromise();
    }

    /**
     * Format talent object
     *
     * @param talent
     * @return {any[]}
     */
    async formatTalent(talent: any, anonymous = false): Promise<any[]>
    {
        const result = _.cloneDeep(talent);
        const users = [];
        const executions = [];
        const data = [];
        const widget = [];
        const status_counter = {
            'PENDING': 0,
            'STARTED': 0,
            'FINISHED': 0,
            'ASSESSED': 0
        };
        const columns = [];
        const columnsTitles = {};
        columns.push('actions');
        columns.push('user');
        for (const test of talent.tests) {
            columns.push('test' + test.id);
            if (test.executions) {
                if (test.executions.id) {
                    executions[test.executions.id] = test.executions;
                }
                for (const execution of test.executions) {
                    if (execution.id) {
                        executions[execution.id] = execution;
                    }
                }
            }
        }
        for (const user of talent.users) {
            await this._userApiService.getUser(user).toPromise().then(res => {
                users[user] = res;
                const tests = {};
                columnsTitles['actions'] = '';
                columnsTitles['user'] = this._translateService.instant('MAIN.COLUMN.USER');
                // const row = {user: {name: this._translateService.instant('MAIN.COLUMN.USER'), value: res.lastName + ', ' + res.firstName}};
                const visible_name = res.last_name ? res.last_name + ', ' + res.first_name : res.first_name;
                const row = anonymous ? {user: res.username, actions: null} : {user: visible_name, actions: res.id};
                for (const test of talent.tests) {
                    let status = executions.filter( item => item.user === user && item.test.id === test.id).map(item => item.status)[0];
                    const execution = executions.filter( item => item.user === user && item.test.id === test.id).map(item => item)[0];
                    status = status ? status : 0;
                    const status_value = status ? TestExecutionStatus[status]  : 'PENDING';
                    tests[test.id] = {
                        id: test.id,
                        title: test.title,
                        status: {
                            id: status,
                            value: status_value
                        }
                    };
                    columnsTitles['test' + test.id] = test.title;
                    // row['test' + test.id] = {name: test.title, value: status ? TestExecutionStatus[status] : 'PENDING'};
                    row['test' + test.id] = {
                        'text': status_value,
                        'progress': execution && execution.progress ? execution.progress : null,
                        'marks': execution && execution.marks ? execution.marks : null
                    };
                    if (status_value === 'FINISHED' || status_value === 'ASSESSED') {
                        status_counter['FINISHED']++;
                    } else {
                        status_counter[status_value]++;
                    }
                }
                data.push(row);
                for (const item of Object.keys(status_counter)) {
                    if (item !== 'ASSESSED') {
                        const element = {
                            name    : this._translateService.instant('RECRUITER.COMMON.TEST_STATUS.' + item),
                            value   : status_counter[item] * 100 / (talent.users.length * talent.tests.length),
                        };
                        widget.push(element);
                    }
                }
            });
        }
        result.table = {data: data, columns: columns, columnsTitles: columnsTitles, widget: widget};
        return result;
    }

}
