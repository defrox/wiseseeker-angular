import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, QueryList, ViewChildren, ViewChild, ViewEncapsulation, Inject} from '@angular/core';
import {merge, Observable, Subject, of} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {Router} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
// import {slideInDownAnimation } from '@covalent/core/common';
import {TdDataTableSortingOrder, TdDataTableService, TdDataTableComponent, ITdDataTableSortChangeEvent, ITdDataTableColumn} from '@covalent/core/data-table';
import {IPageChangeEvent, TdPagingBarComponent} from '@covalent/core/paging';
import {TdDialogService} from '@covalent/core/dialogs';
import {RecruiterTalentEditorDialogComponent} from '../editor';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {TranslatePipe} from '@ngx-translate/core';
import {RecruiterTalentDetailService} from './detail.service';
import {LoaderOverlayService, SnackAlertService, PdfApiService, AppConfigService} from 'app/_services';
import {DialogData} from 'app/_models';

// Import the locale files
import {locale as english} from 'app/main/recruiter/i18n/en';
import {locale as spanish} from 'app/main/recruiter/i18n/es';


@Component({
    selector: 'recruiter-talent-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class RecruiterTalentDetailComponent implements OnInit, OnDestroy {
    appConfig: any;
    usersTestsTableDisplayedColumns: string[] = ['actions', 'user', 'test'];
    usersTestsTableColumnsTitles: any;
    talent: any;
    widget: any;
    usersTestsTable = new MatTableDataSource<any>();
    usersTestsTableData: any[];
    usersTestsTableColumns: ITdDataTableColumn[] = [
        { name: 'actions', label: '', width: 36, sortable: false, filter: false},
        { name: 'user', label: 'User', width: {min: 100}},
        { name: 'test', label: 'Test', nested: true, width: {min: 100}}
    ];
    usersTestsTableFilteredData: any[];
    usersTestsTableIsLoaded = false;
    data: any[];
    clickable = false;
    resizableColumns = true;
    sortable = true;
    filterColumn = true;
    filteredTotal: number ;
    searchTerm = '';
    fromRow = 1;
    currentPage = 1;
    pageSize = 10;
    sortBy = 'actions';
    sortOrder: TdDataTableSortingOrder = TdDataTableSortingOrder.Descending;

    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(TdPagingBarComponent) pagingBar: TdPagingBarComponent;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {AppConfigService} _appConfigService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslatePipe} _translatePipe
     * @param {RecruiterTalentDetailService} _recruiterTalentDetailService
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {SnackAlertService} _snackAlertService
     * @param {PdfApiService} _pdfApiService
     * @param {MatDialog} _dialog
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {TdDataTableService} _dataTableService
     * @param {Router} _router
     */
    constructor(
        private _appConfigService: AppConfigService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translatePipe: TranslatePipe,
        private _recruiterTalentDetailService: RecruiterTalentDetailService,
        private _loaderOverlay: LoaderOverlayService,
        private _snackAlertService: SnackAlertService,
        private _pdfApiService: PdfApiService,
        private _dialog: MatDialog,
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseSidebarService: FuseSidebarService,
        private _dataTableService: TdDataTableService,
        private _router: Router

    ) {
        // Set the private defaults
        this._translationLoader.loadTranslations(spanish, english);
        this._appConfigService.config.subscribe(config => {
            this.appConfig = config;
        });
        this._unsubscribeAll = new Subject();
        this._loaderOverlay.show();
        // this.usersTestsTable = new MatTableDataSource();
        this.sort = new MatSort();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // TODO: use wrapper table https://github.com/angular/material2/blob/master/src/material-examples/table-wrapped/

        // If the user changes the sort order, reset back to the first page.
        // this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);
        // Subscribe to talent
        this._recruiterTalentDetailService.onTalentChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(talent => {
                this.talent = talent;
                    this._recruiterTalentDetailService.formatTalent(talent, this.appConfig.appSettings.anonymousSite).then(res => {
                        this.talent = res;
                        if (this.talent.table) {
                            this.usersTestsTable = this.usersTestsTableData = this.usersTestsTableFilteredData = this.talent.table.data;
                            this.usersTestsTableColumnsTitles = this.talent.table.columnsTitles;
                            this.usersTestsTableDisplayedColumns = this.talent.table.columns;
                            this.usersTestsTableColumns = [];
                            for (const column of this.talent.table.columns) {
                                if (column === 'actions') {
                                    this.usersTestsTableColumns.push({name: column, label: this.talent.table.columnsTitles[column], width: 36, sortable: false, filter: false});
                                } else if (column !== 'user') {
                                    this.usersTestsTableColumns.push({name: column, label: this.talent.table.columnsTitles[column], width: {min: 100}, filter: false, nested: true});
                                } else {
                                    this.usersTestsTableColumns.push({name: column, label: this.talent.table.columnsTitles[column], width: {min: 100}});
                                }
                            }
                            if (this.talent.table.widget) {
                                this.widget = this.talent.table.widget.slice(-3);
                            }
                        }
                        this._loaderOverlay.hide();
                        this.usersTestsTableIsLoaded = true;
                        this.filter();
                    }).catch(err => {
                        // do something in case of error
                        this._loaderOverlay.hide();
                        this.usersTestsTableIsLoaded = false;
                });
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * get user pdf report
     */
    getUserPdf(userId: number): void {
        this._loaderOverlay.show();
        this._pdfApiService.getUserPdf(+userId).subscribe(data => {
            this._downloadFile(data, userId);
            this._loaderOverlay.hide();
        }, err => this._loaderOverlay.hide());
    }

    deleteTalent(talentId): void {
        const dialogRef = this._dialog.open(RecruiterTalentDetailDialogComponent);
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // delete confirmed
                this._loaderOverlay.show();
                this._recruiterTalentDetailService.deleteTalent(talentId).then( () => {
                    this._router.navigate([`/recruiter/talent`]).then(() => {
                        this._loaderOverlay.hide();
                        this._snackAlertService.success(this._translatePipe.transform('RECRUITER.TALENT.DIALOG.DELETED_SUCCESSFULLY'));
                    });
                });
            } else {
                // cancel delete
                this._loaderOverlay.hide();
            }
        });
    }

    showResults(event, testId): void {
        // TODO: finish the feature to show test results on modal window
        const test = null;
        const dialogRef = this._dialog.open(RecruiterTalentEditorDialogComponent, {
            data: {test: test}
        });
    }

    applyFilter(filterValue: string): void {
        const data = this.usersTestsTable.data;
        // this.usersTestsTable = new MatTableDataSource<any>(data);
        this.usersTestsTable.filter = filterValue.trim().toLowerCase();

        if (this.paginator) {
            this.paginator.firstPage();
        }
    }

    sortTable(sortEvent: ITdDataTableSortChangeEvent): void {
        this.sortBy = sortEvent.name;
        this.sortOrder = sortEvent.order === 'DESC' ? TdDataTableSortingOrder.Ascending : TdDataTableSortingOrder.Descending;
        this.filter();
    }

    search(searchTerm: string): void {
        this.searchTerm = searchTerm;
        this.pagingBar.navigateToPage(1);
        this.filter();
    }

    page(pagingEvent: IPageChangeEvent): void {
        this.fromRow = pagingEvent.fromRow;
        this.currentPage = pagingEvent.page;
        this.pageSize = pagingEvent.pageSize;
        this.filter();
    }

    async filter(): Promise<void> {
        let newData: any[] = this.usersTestsTableData;
        let excludedColumns: string[] = await this.usersTestsTableColumns
            .filter((column: ITdDataTableColumn) => {
                return ((column.filter === undefined && column.hidden === true) ||
                    (column.filter !== undefined && column.filter === false));
            }).map((column: ITdDataTableColumn) => {
                return column.name;
            });
        newData = await this._dataTableService.filterData(newData, this.searchTerm, true, excludedColumns);
        this.filteredTotal = newData.length;
        if (this.sortBy === 'actions' || this.sortBy === 'user') {
            newData = await this._dataTableService.sortData(newData, this.sortBy, this.sortOrder);
        } else {
            newData = await this.sortTests(newData, this.sortBy, 'text', this.sortOrder);
            newData = await this.sortTests(newData, this.sortBy, 'progress', this.sortOrder);
            newData = await this.sortTests(newData, this.sortBy, 'marks', this.sortOrder);
        }
        newData = await this._dataTableService.pageData(newData, this.fromRow, this.currentPage * this.pageSize);
        this.usersTestsTableFilteredData = newData;
    }

    async sortTests(sortData, sortBy, sortField, sortOrder): Promise<any[]> {
        sortData = Array.from(sortData); // Change the array reference to trigger OnPush
        sortData = sortData.sort((a: any, b: any) => {
            let direction = 0;
            if (sortOrder === TdDataTableSortingOrder.Descending) {
                direction = 1;
            } else if (sortOrder === TdDataTableSortingOrder.Ascending) {
                direction = -1;
            }
            if (a[sortBy][sortField] < b[sortBy][sortField]) {
                return direction;
            } else if (a[sortBy][sortField] > b[sortBy][sortField]) {
                return -direction;
            } else {
                return direction;
            }
        });
        return sortData;
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    _downloadFile(data: any, userId: number): any {
        const blob = new Blob([data], { type: 'application/pdf' });
        const url = window.URL.createObjectURL(blob);
        const anchor = document.createElement('a');
        anchor.download = `report${userId}.pdf`;
        anchor.href = url;
        document.body.appendChild(anchor);
        anchor.click();
        document.body.removeChild(anchor);
    }
}


@Component({
    selector: 'recruiter-talent-detail-dialog',
    templateUrl: './detail.dialog.component.html',
    styleUrls: ['./detail.component.scss'],
})
export class RecruiterTalentDetailDialogComponent {
    constructor(
        private _dialogRef: MatDialogRef<RecruiterTalentDetailDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
        _dialogRef.disableClose = true;
    }
}
