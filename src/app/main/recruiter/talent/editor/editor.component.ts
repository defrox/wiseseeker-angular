import {Component, OnDestroy, OnInit, ViewEncapsulation, Inject} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {fuseAnimations} from '@fuse/animations';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {RecruiterTalentEditorService} from './editor.service';
import {AppConfigService, LoaderOverlayService, SnackAlertService, TestApiService, UserApiService} from 'app/_services';
import {TranslatePipe} from '@ngx-translate/core';
import {DialogData} from 'app/_models';
import * as _ from 'lodash';

// Import the locale files
import {locale as english} from 'app/main/recruiter/i18n/en';
import {locale as spanish} from 'app/main/recruiter/i18n/es';


@Component({
    selector: 'recruiter-talent-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class RecruiterTalentEditorComponent implements OnInit, OnDestroy {
    appConfig: any;
    animationDirection: 'left' | 'right' | 'none';
    skillsTests: any[];
    skillsTest: any;
    skillsSelected = false;
    skillsSelectedTests: any[] = [];
    skillsSelectedItem: any;
    personalityTests: any[];
    personalityTest: any;
    personalitySelected = false;
    personalitySelectedTests: any[] = [];
    personalitySelectedItem: any;
    rolesTests: any[];
    rolesSelected: any[] = [];
    knowledgeTests: any[];
    knowledgeSelected = false;
    knowledgeSelectedTests: any[] = [];
    knowledgeSelectedItem: any;
    testsSelected: any[] = [];
    usersList: any[];
    filteredUsersList: any[];
    usersSelected: any[];
    talent: any;
    talentId: string | number | null;
    talentSlug: string | null;
    form: FormGroup;
    submitted = false;
    icon = null;
    image = null;
    searchTerm: string;

    // Horizontal Stepper
    horizontalStepperStep1: FormGroup;
    horizontalStepperStep2: FormGroup;
    horizontalStepperStep3: FormGroup;
    horizontalStepperStep4: FormGroup;
    horizontalStepperStep5: FormGroup;
    horizontalStepperStep6: FormGroup;
    horizontalStepperStep7: FormGroup;
    horizontalStepperStep8: FormGroup;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {AppConfigService} _appConfigService
     * @param {RecruiterTalentEditorService} _recruiterTalentEditorService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslatePipe} _translatePipe
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {SnackAlertService} _snackAlertService
     * @param {TestApiService} _testApiService
     * @param {UserApiService} _userApiService
     * @param {FormBuilder} _formBuilder
     * @param {MatDialog} _dialog
     * @param {ActivatedRoute} _route
     * @param {Router} _router
     */
    constructor(
        private _appConfigService: AppConfigService,
        private _recruiterTalentEditorService: RecruiterTalentEditorService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translatePipe: TranslatePipe,
        private _loaderOverlay: LoaderOverlayService,
        private _snackAlertService: SnackAlertService,
        private _testApiService: TestApiService,
        private _userApiService: UserApiService,
        private _formBuilder: FormBuilder,
        private _dialog: MatDialog,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        // Set the defaults
        this._loaderOverlay.show();
        this._appConfigService.config.subscribe(config => { this.appConfig = config; });
        this.animationDirection = 'none';
        this.searchTerm = '';
        this.skillsTests = this.personalityTests = this.rolesTests = this.knowledgeTests = this.usersSelected = this.testsSelected = [];
        this._translationLoader.loadTranslations(spanish, english);
        this._route.paramMap.subscribe(params => {
            this.talentId = params.get('talentId');
            this.talentSlug = params.get('talentSlug');
        });

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to all talent
        this._recruiterTalentEditorService.onTalentChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(talent => {
                this.talent = talent;
                if (talent.id && talent.id > 0) {
                    this.usersSelected = talent.users;
                    this.testsSelected = talent.tests.map(item => item.id);
                }
                this._loaderOverlay.hide();
            });

        // Get all tests
        this._testApiService.getAll()
            .subscribe(tests => {
                this.knowledgeTests = tests.filter( test => +test.type === 1);
                this.knowledgeSelectedTests = this.testsSelected.filter(item => this.filterTests(this.knowledgeTests, item));
                // this.knowledgeSelected = this.testsSelected.filter(item => item === +this.knowledgeTests[0].id).length > 0;
                this.knowledgeSelected = this.knowledgeSelectedTests.length > 0;
                this.knowledgeSelectedItem = this.knowledgeSelectedTests.length > 0 ? this.knowledgeSelectedTests[0] : null;
                this.rolesTests = tests.filter( test => +test.type === 2);
                this.rolesSelected = this.testsSelected.filter(item => this.filterTests(this.rolesTests, item));
                this.skillsTests = tests.filter( test => +test.type === 3);
                this.skillsTest = this.skillsTests[0];
                this.skillsSelected = this.testsSelected.filter(item => item === +this.skillsTests[0].id).length > 0;
                this.skillsSelectedTests = this.testsSelected.filter(item => this.filterTests(this.skillsTests, item));
                this.skillsSelectedItem = this.skillsSelectedTests.length > 0 ? this.skillsSelectedTests[0] : null;
                this.personalityTests = tests.filter( test => +test.type === 4);
                this.personalityTest = this.personalityTests[0];
                this.personalitySelected = this.testsSelected.filter(item => item === +this.personalityTests[0].id).length > 0;
                this.personalitySelectedTests = this.testsSelected.filter(item => this.filterTests(this.personalityTests, item));
                this.personalitySelectedItem = this.personalitySelectedTests.length > 0 ? this.personalitySelectedTests[0] : null;
                // this.horizontalStepperStep5.get('personality').reset(this.personalitySelected);
                // this.horizontalStepperStep5.get('skills').reset(this.skillsSelected);
                // this.horizontalStepperStep5.get('knowledge').reset(this.knowledgeSelected);
                // this.horizontalStepperStep5.get('knowledgeSel').reset(this.knowledgeSelectedItem);
                this.horizontalStepperStep6.get('skillsSel').reset(this.skillsSelectedItem);
                this.horizontalStepperStep7.get('personalitySel').reset(this.personalitySelectedItem);
                this.horizontalStepperStep8.get('knowledgeSel').reset(this.knowledgeSelectedItem);
            });

        // Get all users
        this._userApiService.getAll()
            .subscribe(users => {
                this.usersList = this.filteredUsersList = users;
            });

        // Horizontal Stepper form steps
        this.horizontalStepperStep1 = this._formBuilder.group({
            title : [this.talent.title, Validators.required],
            description: [this.talent.description]
        });

        this.horizontalStepperStep2 = this._formBuilder.group({
            active_from: [this.talent.active_from, Validators.required],
            active_to: [this.talent.active_to, Validators.required]
        });

        /*this.horizontalStepperStep3 = this._formBuilder.group({
            icon      : [''],
            image     : ['']
        });*/

        this.horizontalStepperStep4 = this._formBuilder.group({
            file      : [''],
            users     : [this.usersSelected]
        });

        this.horizontalStepperStep5 = this._formBuilder.group({
            // skills          : [this.skillsSelected],
            // skillsSel       : [this.testsSelected],
            // personality     : {value: this.personalitySelected, disabled: true},
            // personalitySel  : {value: this.testsSelected, disabled: true},
            knowledge       : {value: this.knowledgeSelected},
            knowledgeSel    : {value: this.knowledgeSelectedTests}
        });

        this.horizontalStepperStep6 = this._formBuilder.group({
            // skills          : {value: this.skillsSelected},
            skillsSel       : {value: this.skillsSelectedItem},
            // personality     : {value: this.personalitySelected, disabled: true},
            // personalitySel  : {value: this.testsSelected, disabled: true},
            // knowledge       : {value: this.knowledgeSelected, disabled: true},
            // knowledgeSel    : {value: this.knowledgeSelectedTests, disabled: true}
        });

        this.horizontalStepperStep7 = this._formBuilder.group({
            // skills          : [this.skillsSelected],
            // skillsSel       : [this.testsSelected],
            // personality     : {value: this.testsSelected},
            personalitySel  : {value: this.personalitySelectedItem},
            // knowledge       : {value: this.knowledgeSelected, disabled: true},
            // knowledgeSel    : {value: this.knowledgeSelectedTests, disabled: true}
        });

        this.horizontalStepperStep8 = this._formBuilder.group({
            // skills          : [this.skillsSelected],
            // skillsSel       : [this.testsSelected],
            // personality     : {value: this.personalitySelected, disabled: true},
            // personalitySel  : {value: this.testsSelected, disabled: true},
            // knowledge       : {value: this.testsSelected},
            knowledgeSel    : {value: this.knowledgeSelectedItem}
        });

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._recruiterTalentEditorService.resetTalent();
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    csv2js(csvText, separator = ',', line = '\r\n'): any {
        let lines = csvText.split(line);
        let result = [];
        let headers = lines[0].split(separator);
        for (let i = 1; i < lines.length; i++) {
            let obj = {};
            let currentline = lines[i].split(separator);
            for (let j = 0; j < headers.length; j++) {
                if (currentline[j].trim().substr(0, 1) === '[' && currentline[j].trim().substr(-1, 1) === ']') {
                    obj[headers[j]] = JSON.parse(currentline[j]);
                } else {
                    obj[headers[j]] = currentline[j];
                }
            }
            result.push(obj);
        }
        return result;
    }

    /**
     * Filter talents by term
     */
    filterUsersByTerm(): void {
        const searchTerm = this.searchTerm.toLowerCase();

        // Search
        if (searchTerm === '') {
            this.filteredUsersList = this.usersList;
        } else {
            this.filteredUsersList = this.usersList.filter((user) => {
                return (user.first_name.toLowerCase().includes(searchTerm) ||
                        user.last_name.toLowerCase().includes(searchTerm) ||
                        user.username.toLowerCase().includes(searchTerm) ||
                        user.directorate.toLowerCase().includes(searchTerm) ||
                        user.division.toLowerCase().includes(searchTerm) ||
                        user.enterprise.toLowerCase().includes(searchTerm) ||
                        user.city.toLowerCase().includes(searchTerm) ||
                        user.management.toLowerCase().includes(searchTerm));
            });
        }
    }

    /**
     * Upload users list file
     *
     * @param event
     * @returns {void}
     */
    processUserList(event): void {
        const reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsText(file);
            reader.onload = () => {
                this._loaderOverlay.show();
                const users = this.csv2js(reader.result, ';');
                // No dialog, add users always
                this._recruiterTalentEditorService.addUsers(users).then( newUsers => {
                    this.filteredUsersList = this.usersList = [...this.usersList, ...newUsers];
                    this.usersSelected = [...this.usersSelected, ...newUsers.map(item => item.id)];
                    this.horizontalStepperStep4.get('users').reset(this.usersSelected);
                    this.horizontalStepperStep4.controls['file'].setValue(null);
                    this._loaderOverlay.hide();
                    this._snackAlertService.success(this._translatePipe.transform('RECRUITER.TALENT.DIALOG.USERS_ADDED_SUCCESSFULLY'));
                }).catch( err => {
                    this.horizontalStepperStep4.controls['file'].setValue(null);
                    this._loaderOverlay.hide();
                    // throw new Error(err);
                });
                this.filterUsersByTerm();

                /*const dialogRef = this._dialog.open(RecruiterTalentEditorDialogComponent, {
                    data: {users: users}
                });
                dialogRef.afterClosed().subscribe(result => {
                    if (result) {
                        // add users confirmed
                        this._loaderOverlay.show();
                        this._recruiterTalentEditorService.addUsers(users).then( newUsers => {
                            this._loaderOverlay.hide();
                            this._snackAlertService.success(this._translatePipe.transform('RECRUITER.TALENT.DIALOG.USERS_ADDED_SUCCESSFULLY'));
                            this.usersList = [...this.usersList, ...newUsers];
                            this.usersSelected = [...this.usersSelected, ...newUsers.map(item => item.id)];
                            this.horizontalStepperStep4.get('users').reset(this.usersSelected);
                            this.horizontalStepperStep4.controls['file'].setValue(null);
                        }).catch( err => {
                            this.horizontalStepperStep4.controls['file'].setValue(null);
                            this._loaderOverlay.hide();
                            // throw new Error(err);
                        });
                    } else {
                        // cancel add users
                        this.horizontalStepperStep4.controls['file'].setValue(null);
                        this._loaderOverlay.hide();
                    }
                });*/
            };
        }
    }

    /**
     * Upload image file
     *
     * @param event
     * @returns {void}
     */
    uploadImage(event): void {
        const reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.image = {src: reader.result};
                // TODO: upload image through API
            };
        }
    }

    /**
     * Upload icon file
     *
     * @param event
     * @returns {void}
     */
    uploadIcon(event): void {
        const reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.icon = {src: reader.result};
                // TODO: upload image through API
            };
        }
    }

    /**
     * Filter tests
     *
     * @param testArray
     * @param testId
     * @returns {boolean}
     */
    filterTests(testArray: any[], testId: number): boolean {
        let result = false;
        for (const item of testArray) {
            if (+item.id === testId) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Selected Tests merger
     */
    fuseTests(personality: boolean = false, skills: boolean = false, knowledge: any = false): any[]
    {
        let testsSelected = [];
        if (skills) {
            testsSelected = [this.skillsTest.id];
        } else {
            if (knowledge) {
                if (this.knowledgeSelectedTests instanceof Array && this.knowledgeSelectedTests.length > 0) {
                    testsSelected = this.knowledgeSelectedTests;
                } else if (!(this.knowledgeSelectedTests instanceof Array)) {
                    testsSelected = [this.knowledgeSelectedTests];
                }
            }
            if (personality) {
                testsSelected.push(this.personalityTest.id);
            }
        }
        return testsSelected;
    }

    togglePersonality(): void {
        if (this.horizontalStepperStep5.value.skills) {
            this.skillsSelected = true;
            this.skillsSelectedTests = [];
            this.personalitySelected = false;
            this.personalitySelectedTests = [];
            this.knowledgeSelected = false;
            this.knowledgeSelectedTests = [];
        } else {
            this.skillsSelected = false;
        }
        this.horizontalStepperStep5.get('personality').reset(this.personalitySelected);
        // this.horizontalStepperStep5.get('personalitySel').reset(this.personalitySelectedTests);
        this.horizontalStepperStep5.get('skills').reset(this.skillsSelected);
        // this.horizontalStepperStep5.get('skillsSel').reset(this.skillsSelectedTests);
        this.horizontalStepperStep5.get('knowledge').reset(this.knowledgeSelected);
        this.horizontalStepperStep5.get('knowledgeSel').reset(this.knowledgeSelectedTests);
    }

    /**
     * Selected Tests merger
     */
    mergeTests(): void
    {
        // this.testsSelected = [...this.personalitySelectedTests, ...this.skillsSelectedTests, ...this.rolesSelected, ...this.knowledgeSelectedTests];
        this.testsSelected = [];
        if (this.personalitySelectedItem) {
            this.testsSelected.push(this.personalitySelectedItem);
        }
        if (this.skillsSelectedItem) {
            this.testsSelected.push(this.skillsSelectedItem);
        }
        if (this.knowledgeSelectedItem) {
            this.testsSelected.push(this.knowledgeSelectedItem);
        }
    }

    /**
     * Submit talent
     */
    submitTalent(): void
    {
        if (!this.submitted) {
            this.submitted = true;
            this._loaderOverlay.show();
            const result = _.cloneDeep(this.talent);
            result.title = this.horizontalStepperStep1.value.title;
            result.description = this.horizontalStepperStep1.value.description;
            result.active_to = this.horizontalStepperStep2.value.active_to;
            result.active_from = this.horizontalStepperStep2.value.active_from;
            // result.image = this.horizontalStepperStep3.value.image;
            // result.icon = this.horizontalStepperStep3.value.icon;
            result.users = this.usersSelected;
            result.tests = null;
            // result.test_ids = this.fuseTests(this.horizontalStepperStep5.value.personality, this.horizontalStepperStep5.value.skills, this.horizontalStepperStep5.value.knowledge);
            result.test_ids = this.testsSelected;
            // console.log(result);
            this._recruiterTalentEditorService.saveTalent(result).then(res => {
                this.talent = res;
                this._router.navigate([`/recruiter/talent`]).then(() => {
                    this._loaderOverlay.hide();
                    this._snackAlertService.success(this._translatePipe.transform('RECRUITER.DASHBOARD.TALENT.SAVED_SUCCESSFULLY'));
                });
            }).catch(err => {
                // do something in case of error
                this._loaderOverlay.hide();
                this.submitted = false;
                // throw new Error(err);
            });
        }
    }
}


@Component({
    selector: 'recruiter-talent-editor-dialog',
    templateUrl: './editor.dialog.component.html',
    styleUrls: ['./editor.component.scss'],
})
export class RecruiterTalentEditorDialogComponent {
    usersColumns: string[] = ['username', 'first_name', 'last_name', 'email'];

    constructor(
        private _dialogRef: MatDialogRef<RecruiterTalentEditorDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
        ) {
        _dialogRef.disableClose = true;
    }
}
