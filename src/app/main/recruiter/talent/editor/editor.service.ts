import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {EvaluationApiService, UserApiService} from 'app/_services';


@Injectable()
export class RecruiterTalentEditorService implements Resolve<any> {
    onTalentChanged: BehaviorSubject<any>;
    newUsersList: BehaviorSubject<any>;
    usersAdded: any[];

    /**
     * Constructor
     *
     * @param {EvaluationApiService} _evaluationApiService
     * @param {UserApiService} _userApiService
     */
    constructor(
        private _evaluationApiService: EvaluationApiService,
        private _userApiService: UserApiService,
    ) {
        // Set the defaults
        this.onTalentChanged = new BehaviorSubject({});
        this.newUsersList = new BehaviorSubject([]);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getTalent(route.params.talentId)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get talent
     *
     * @param {number} id
     * @returns {Promise<any>}
     */
    getTalent(id): Promise<any>
    {
        if (id > 0) {
            return new Promise((resolve, reject) => {
                this._evaluationApiService.get(id)
                    .subscribe((response: any) => {
                        this.onTalentChanged.next(response);
                        resolve(response);
                    }, reject);
            });
        } else {
            return new Promise(resolve => {
                resolve({title: null, description: null, active_from: null, active_to: null, tests: null, users: null});
            });
        }
    }

    /**
     * Save talent
     *
     * @param {any} talent
     * @returns {Promise<any>}
     */
    saveTalent(talent: any): Promise<any>
    {
        return new Promise((resolve, reject) => {
            if (talent.id && talent.id > 0) {
                this._evaluationApiService.update(talent)
                    .subscribe((response: any) => {
                        this.onTalentChanged.next(response);
                        resolve(response);
                    }, reject);
            } else {
                this._evaluationApiService.add(talent)
                    .subscribe((response: any) => {
                        this.onTalentChanged.next(response);
                        resolve(response);
                    }, reject);
            }
        });
    }

    /**
     * Add users
     *
     * @param {any[]} users
     * @returns {Promise<any>}
     */
    addUsers(users: any[]): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._userApiService.addUsers(users)
                .toPromise().then((response: any) => {
                    this.usersAdded = response;
                    this.newUsersList.next(this.usersAdded);
                    resolve(this.usersAdded);
                }, reject);
        });
    }

    /**
     * Reset talent
     *
     * @returns {void}
     */
    resetTalent(): void
    {
        this.onTalentChanged.next({});
    }

    public set newUsers(value) {
        this.newUsersList.next(value);
    }

    public get newUsers(): any | Observable<any> {
        return this.newUsersList.asObservable();
    }

}
