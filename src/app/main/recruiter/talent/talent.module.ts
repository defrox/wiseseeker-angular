import {NgModule} from '@angular/core';
import {RecruiterTalentRouting} from 'app/main/recruiter/talent/talent.routing';


@NgModule({
    imports: [
        RecruiterTalentRouting
    ]
})
export class RecruiterTalentModule {
}
