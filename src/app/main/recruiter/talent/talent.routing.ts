import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RecruiterTalentHomeComponent, RecruiterTalentHomeService, RecruiterTalentHomeDialogComponent} from 'app/main/recruiter/talent/home';
import {RecruiterTalentDetailComponent, RecruiterTalentDetailDialogComponent, RecruiterTalentDetailService} from 'app/main/recruiter/talent/detail';
import {RecruiterTalentEditorComponent, RecruiterTalentEditorDialogComponent, RecruiterTalentEditorService} from 'app/main/recruiter/talent/editor';
import {MaterialModule} from 'app/_helpers/material.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseSidebarModule, FuseWidgetModule} from '@fuse/components';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {TranslateModule} from '@ngx-translate/core';
import {ChartsModule} from 'ng2-charts';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {AuthGuard} from 'app/_guards';
import {DefroxModule} from 'app/_helpers';
import {Group} from 'app/_models';
import {ChartModule} from 'angular-highcharts';
import {CovalentSearchModule} from '@covalent/core/search';
import {CovalentDataTableModule} from '@covalent/core/data-table';
import {CovalentPagingModule} from '@covalent/core/paging';

const appRoutes: Routes = [
    {path: 'recruiter/talent', pathMatch: 'prefix', children: [
        {path: '', redirectTo: 'home', pathMatch: 'full'},
        {path: 'home', component: RecruiterTalentHomeComponent, resolve: {home: RecruiterTalentHomeService}, canActivate: [AuthGuard], data: {groups: [Group.Recruiter]}},
        {path: 'detail/:talentId/:talentSlug', component: RecruiterTalentDetailComponent, resolve: {talent: RecruiterTalentDetailService}, canActivate: [AuthGuard], data: {groups: [Group.Recruiter]}},
        {path: 'editor/:talentId/:talentSlug', component: RecruiterTalentEditorComponent, resolve: {talent: RecruiterTalentEditorService}, canActivate: [AuthGuard], data: {groups: [Group.Recruiter]}},
        // otherwise redirect to 404
        {path: '**', pathMatch: 'full', redirectTo: '/error/404'},
    ]}
];

@NgModule({
    declarations: [
        RecruiterTalentHomeComponent,
        RecruiterTalentHomeDialogComponent,
        RecruiterTalentDetailComponent,
        RecruiterTalentDetailDialogComponent,
        RecruiterTalentEditorComponent,
        RecruiterTalentEditorDialogComponent
    ],
    imports: [
        RouterModule.forChild(appRoutes),
        MaterialModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseWidgetModule,
        TranslateModule,
        ChartsModule,
        NgxChartsModule,
        FontAwesomeModule,
        DefroxModule,
        ChartModule,
        CovalentSearchModule,
        CovalentDataTableModule,
        CovalentPagingModule
    ],
    providers: [
        RecruiterTalentHomeService,
        RecruiterTalentDetailService,
        RecruiterTalentEditorService,
        AuthGuard
    ],
    entryComponents: [RecruiterTalentDetailDialogComponent, RecruiterTalentEditorDialogComponent, RecruiterTalentHomeDialogComponent],
    exports: [RouterModule]
})
export class RecruiterTalentRouting {
}
