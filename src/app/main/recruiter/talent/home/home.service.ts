import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {EvaluationApiService} from 'app/_services';

@Injectable()
export class RecruiterTalentHomeService implements Resolve<any> {
    onCategoriesChanged: BehaviorSubject<any>;
    onTalentsChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {EvaluationApiService} _evaluationApiService
     */
    constructor(
        private _evaluationApiService: EvaluationApiService
    ) {
        // Set the defaults
        this.onCategoriesChanged = new BehaviorSubject({});
        this.onTalentsChanged = new BehaviorSubject({});
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                // this.getCategories(),
                this.getTalents()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get categories
     *
     * @returns {Promise<any>}
     */
/*    getCategories(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._evaluationApiService.getCategories()
                .subscribe((response: any) => {
                    this.onCategoriesChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }*/

    /**
     * Get talents
     *
     * @returns {Promise<any>}
     */
    getTalents(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._evaluationApiService.getAll()
                .subscribe((response: any) => {
                    this.onTalentsChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Delete talent
     *
     * @returns {Promise<any>}
     */
    deleteTalent(id): Promise<any>
    {
        return this._evaluationApiService.delete(id).toPromise();
    }

}
