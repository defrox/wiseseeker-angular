import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {TranslatePipe} from '@ngx-translate/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {RecruiterTalentHomeService} from './home.service';
import {DialogData} from 'app/_models';
import {LoaderOverlayService, SnackAlertService} from 'app/_services';

// Import the locale files
import {locale as english} from 'app/main/recruiter/i18n/en';
import {locale as spanish} from 'app/main/recruiter/i18n/es';

@Component({
    selector: 'recruiter-talent-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class RecruiterTalentHomeComponent implements OnInit, OnDestroy {
    categories: any[];
    talents: any[];
    talentsFilteredByCategory: any[];
    filteredTalents: any[];
    currentCategory: string;
    searchTerm: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MatDialog} _dialog
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {SnackAlertService} _snackAlertService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslatePipe} _translatePipe
     * @param {RecruiterTalentHomeService} _recruiterTalentHomeService
     */
    constructor(
        private _dialog: MatDialog,
        private _loaderOverlay: LoaderOverlayService,
        private _snackAlertService: SnackAlertService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translatePipe: TranslatePipe,
        private _recruiterTalentHomeService: RecruiterTalentHomeService
    ) {
        // Set the defaults
        this.currentCategory = 'all';
        this.searchTerm = '';

        // Set the private defaults
        this._translationLoader.loadTranslations(spanish, english);
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to categories
        /*this._recruiterTalentHomeService.onCategoriesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(categories => {
                this.categories = categories;
            });*/

        // Subscribe to talents
        this._recruiterTalentHomeService.onTalentsChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(talents => {
                this.filteredTalents = this.talentsFilteredByCategory = this.talents = talents;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Filter talents by category
     */
    filterTalentsByCategory(): void {
        // Filter
        if (this.currentCategory === 'all') {
            this.talentsFilteredByCategory = this.talents;
            this.filteredTalents = this.talents;
        } else {
            this.talentsFilteredByCategory = this.talents.filter((talent) => {
                return talent.category === this.currentCategory;
            });

            this.filteredTalents = [...this.talentsFilteredByCategory];

        }

        // Re-filter by search term
        this.filterTalentsByTerm();
    }

    /**
     * Filter talents by term
     */
    filterTalentsByTerm(): void {
        const searchTerm = this.searchTerm.toLowerCase();

        // Search
        if (searchTerm === '') {
            this.filteredTalents = this.talentsFilteredByCategory;
        } else {
            this.filteredTalents = this.talentsFilteredByCategory.filter((talent) => {
                return (talent.title.toLowerCase().includes(searchTerm) || talent.description.toLowerCase().includes(searchTerm));
            });
        }
    }

    deleteTalent(jobId): void {
        const dialogRef = this._dialog.open(RecruiterTalentHomeDialogComponent);
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // delete confirmed
                this._loaderOverlay.show();
                this._recruiterTalentHomeService.deleteTalent(jobId).then( () => {
                    this._recruiterTalentHomeService.getTalents().then(() => {
                        this._loaderOverlay.hide();
                        this._snackAlertService.success(this._translatePipe.transform('RECRUITER.TALENT.DIALOG.DELETED_SUCCESSFULLY'));
                    });
                });
            } else {
                // cancel delete
                this._loaderOverlay.hide();
            }
        });
    }

}

@Component({
    selector: 'recruiter-talent-home-dialog',
    templateUrl: './home.dialog.component.html',
    styleUrls: ['./home.component.scss'],
})
export class RecruiterTalentHomeDialogComponent {
    constructor(
        private _dialogRef: MatDialogRef<RecruiterTalentHomeDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
        _dialogRef.disableClose = true;
    }
}
