import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RecruiterRolesHomeComponent, RecruiterRolesHomeService, RecruiterRolesHomeDialogComponent} from 'app/main/recruiter/roles/home';
import {RecruiterRolesDetailComponent, RecruiterRolesDetailDialogComponent, RecruiterRolesDetailService} from 'app/main/recruiter/roles/detail';
import {RecruiterRolesEditorComponent, RecruiterRolesEditorDialogComponent, RecruiterRolesEditorService} from 'app/main/recruiter/roles/editor';
import {MaterialModule} from 'app/_helpers/material.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseSidebarModule, FuseWidgetModule} from '@fuse/components';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {TranslateModule} from '@ngx-translate/core';
import {ChartsModule} from 'ng2-charts';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {AuthGuard} from 'app/_guards';
import {DefroxModule} from 'app/_helpers';
import {Group} from 'app/_models';
import {ChartModule} from 'angular-highcharts';

const appRoutes: Routes = [
    {path: 'recruiter/roles', pathMatch: 'prefix', children: [
        {path: '', redirectTo: 'home', pathMatch: 'full'},
        {path: 'home', component: RecruiterRolesHomeComponent, resolve: {home: RecruiterRolesHomeService}, canActivate: [AuthGuard], data: {groups: [Group.Recruiter]}},
        {path: 'detail/:roleId/:roleSlug', component: RecruiterRolesDetailComponent, resolve: {role: RecruiterRolesDetailService}, canActivate: [AuthGuard], data: {groups: [Group.Recruiter]}},
        {path: 'editor/:roleId/:roleSlug', component: RecruiterRolesEditorComponent, resolve: {role: RecruiterRolesEditorService}, canActivate: [AuthGuard], data: {groups: [Group.Recruiter]}},
        // otherwise redirect to 404
        {path: '**', pathMatch: 'full', redirectTo: '/error/404'},
    ]}
];

@NgModule({
    declarations: [
        RecruiterRolesHomeComponent,
        RecruiterRolesHomeDialogComponent,
        RecruiterRolesDetailComponent,
        RecruiterRolesDetailDialogComponent,
        RecruiterRolesEditorComponent,
        RecruiterRolesEditorDialogComponent
    ],
    imports: [
        RouterModule.forChild(appRoutes),
        MaterialModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseWidgetModule,
        TranslateModule,
        ChartsModule,
        NgxChartsModule,
        FontAwesomeModule,
        DefroxModule,
        ChartModule
    ],
    providers: [
        RecruiterRolesHomeService,
        RecruiterRolesDetailService,
        RecruiterRolesEditorService,
        AuthGuard
    ],
    entryComponents: [RecruiterRolesDetailDialogComponent, RecruiterRolesEditorDialogComponent, RecruiterRolesHomeDialogComponent],
    exports: [RouterModule]
})
export class RecruiterRolesRouting {
}
