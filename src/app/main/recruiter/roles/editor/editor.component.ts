import {Component, OnDestroy, ChangeDetectorRef, OnInit, ViewEncapsulation, ViewChildren, QueryList, ViewChild, Inject, Optional} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {fuseAnimations} from '@fuse/animations';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {RecruiterRolesEditorService} from './editor.service';
import {LoaderOverlayService, SnackAlertService, KnowledgeApiService, RoleApiService} from 'app/_services';
import {TranslatePipe} from '@ngx-translate/core';
import {Knowledge, Role, RoleComponent} from 'app/_models';
import {MatTable, MatTableDataSource} from '@angular/material/table';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import * as _ from 'lodash';

// Import the locale files
import {locale as english} from 'app/main/recruiter/i18n/en';
import {locale as spanish} from 'app/main/recruiter/i18n/es';


@Component({
    selector: 'recruiter-roles-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class RecruiterRolesEditorComponent implements OnInit,  OnDestroy {
    animationDirection: 'left' | 'right' | 'none';
    knowledgeTests: Knowledge[];
    knowledgeTestsSelect: Knowledge[];
    roleComponentsDisplayedColumns: string[] = ['knowledge', 'relevance', 'actions'];
    roleComponentsTable = new MatTableDataSource<any>();
    roleComponentsTotalRelevance = 0;
    roleComponents: RoleComponent[];
    emptyComponent = {
        knowledge: null,
        relevance: ''
    };
    role: Role;
    roleId: string | number | null;
    roleSlug: string | null;
    form: FormGroup;
    submitted = false;
    icon = null;
    image = null;

    // Horizontal Stepper
    horizontalStepperStep1: FormGroup;
    horizontalStepperStep2: FormGroup;
    horizontalStepperStep3: FormGroup;
    horizontalStepperStep4: FormGroup;

    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;
    @ViewChild(MatTable) componentsTable: MatTable<any>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {RecruiterRolesEditorService} _recruiterRolesEditorService
     * @param {RoleApiService} _roleApiService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslatePipe} _translatePipe
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {SnackAlertService} _snackAlertService
     * @param {KnowledgeApiService} _knowledgeApiService
     * @param {FormBuilder} _formBuilder
     * @param {MatDialog} _dialog
     * @param {ChangeDetectorRef} _changeDetector
     * @param {ActivatedRoute} _route
     * @param {Router} _router
     */
    constructor(
        private _recruiterRolesEditorService: RecruiterRolesEditorService,
        private _roleApiService: RoleApiService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translatePipe: TranslatePipe,
        private _loaderOverlay: LoaderOverlayService,
        private _snackAlertService: SnackAlertService,
        private _knowledgeApiService: KnowledgeApiService,
        private _formBuilder: FormBuilder,
        private _dialog: MatDialog,
        private _changeDetector: ChangeDetectorRef,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        // Set the defaults
        this._loaderOverlay.show();
        this.animationDirection = 'none';
        this.knowledgeTests = this.roleComponents = [];
        this._translationLoader.loadTranslations(spanish, english);
        this._route.paramMap.subscribe(params => {
            this.roleId = params.get('roleId');
            this.roleSlug = params.get('roleSlug');
        });

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to all role
        this._recruiterRolesEditorService.onRoleChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(role => {
                this.role = role;
                if (role.components && role.components.length > 0) {
                    this.roleComponents = _.cloneDeep(role.components);
                    this._updateTotalRelevance();
                }
                this._loaderOverlay.hide();
            });

        // Get all knowledge
        this._knowledgeApiService.getAll()
            .subscribe(tests => {
                this.knowledgeTests = tests;
            });

        // Horizontal Stepper form steps
        this.horizontalStepperStep1 = this._formBuilder.group({
            title : [this.role.title, Validators.required],
            description: [this.role.description]
        });

        /*this.horizontalStepperStep2 = this._formBuilder.group({
            active_from: [this.role.active_from, Validators.required],
            active_to: [this.role.active_to, Validators.required]
        });*/

        /*this.horizontalStepperStep3 = this._formBuilder.group({
            icon      : [''],
            image     : ['']
        });*/

        this.horizontalStepperStep4 = this._formBuilder.group({});
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._recruiterRolesEditorService.resetRole();
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Upload image file
     *
     * @param event
     * @returns {void}
     */
    uploadImage(event): void {
        const reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.image = {src: reader.result};
                // TODO: upload image through API
            };
        }
    }

    /**
     * Upload icon file
     *
     * @param event
     * @returns {void}
     */
    uploadIcon(event): void {
        const reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.icon = {src: reader.result};
                // TODO: upload image through API
            };
        }
    }

    /**
     * Submit role
     */
    submitRole(): void
    {
        if (!this.submitted) {
            this.submitted = true;
            this._loaderOverlay.show();
            const result = _.cloneDeep(this.role);
            result.title = this.horizontalStepperStep1.value.title;
            result.description = this.horizontalStepperStep1.value.description;
            // result.active_to = this.horizontalStepperStep2.value.active_to;
            // result.active_from = this.horizontalStepperStep2.value.active_from;
            // result.image = this.horizontalStepperStep3.value.image;
            // result.icon = this.horizontalStepperStep3.value.icon;
            result.components = this.roleComponents.map(item => { return {knowledge: item.knowledge, knowledge_id: item.knowledge.id, level: item.level, relevance: item.relevance}; } );
            result.tests = null;
            // console.log(result);
            this._recruiterRolesEditorService.saveRole(result).then(res => {
                this.role = res;
                this._router.navigate([`/recruiter/roles`]).then(() => {
                    this._loaderOverlay.hide();
                    this._snackAlertService.success(this._translatePipe.transform('RECRUITER.ROLES.EDITOR.SAVED_SUCCESSFULLY'));
                });
            }).catch(err => {
                // do something in case of error
                this._loaderOverlay.hide();
                this.submitted = false;
                // throw new Error(err);
            });
        }
    }

    /**
     * Edit component
     */
    updateComponent(component, index): void
    {
        this.roleComponents[index] = component;
        this._updateComponents();
    }

    /**
     * Delete component
     */
    deleteComponent(component): void
    {
        this.roleComponents = this.roleComponents.filter(obj => obj !== component);
        this._updateComponents();
    }

    /**
     * Add component
     */
    addComponent(component): void
    {
        this.roleComponents.push(component);
        this._updateComponents();
    }

    /**
     * Open dialog
     *
     * @param action
     * @param component
     */
    openDialog(action, component): void {
        let newComponent = _.cloneDeep(component);
        this._updateComponents();
        if (action === 'UPDATE') {
            this.knowledgeTestsSelect.push(newComponent.knowledge);
        }
        if (action === 'ADD') {
            newComponent = _.cloneDeep(this.emptyComponent);
        }
        if (action === 'DELETE') {
            newComponent = component;
        }
        const dialogRef = this._dialog.open(RecruiterRolesEditorDialogComponent, {
            data: {
                action: action,
                component: newComponent,
                index: this.roleComponents.indexOf(component),
                knowledgeTests: this.knowledgeTestsSelect,
                totalRelevance: this.roleComponentsTotalRelevance
            }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result.event === 'ADD') {
                this.addComponent(result.data.component);
            } else if (result.event === 'UPDATE') {
                this.updateComponent(result.data.component, result.data.index);
            } else if (result.event === 'DELETE') {
                this.deleteComponent(result.data.component);
            }
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private _getFirst(array, n = 1): any | null {
        return n > 0 && array.length > n ? array.slice(0, n) : array;
    }

    /**
     * Filter Components
     *
     * @param testArray
     * @param testId
     * @returns {boolean}
     */
    private _filterComponents(testArray: any[], testId: number): boolean {
        let result = false;
        for (const item of testArray) {
            if (+item.knowledge.id === testId) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Update components
     */
    private _updateComponents(): void {
        if (this.componentsTable) {
            this.componentsTable.renderRows();
        }
        this._updateTotalRelevance();
        this.knowledgeTestsSelect = this.knowledgeTests.filter(item => !this._filterComponents(this.roleComponents, +item.id));
    }

    /**
     * Update total relevance
     */
    private _updateTotalRelevance(): void {
        this.roleComponentsTotalRelevance = this.roleComponents.reduce((accumulator, currentValue) => accumulator + currentValue.relevance, 0);
    }

}


@Component({
    selector: 'recruiter-roles-editor-dialog',
    templateUrl: './editor.dialog.component.html',
    styleUrls: ['./editor.component.scss'],
})
export class RecruiterRolesEditorDialogComponent {
    action: string;
    local_data: any;
    editorForm: FormGroup;
    maxRelevance = 100;

    constructor(
        private _dialogRef: MatDialogRef<RecruiterRolesEditorDialogComponent>,
        private _formBuilder: FormBuilder,
        @Optional() @Inject(MAT_DIALOG_DATA) public data: any
    ) {
        _dialogRef.disableClose = true;
        this.local_data = {...data};
        this.action = this.local_data.action;
        this.maxRelevance = 100 - (this.local_data.totalRelevance - this.local_data.component.relevance);
        this.editorForm = this._formBuilder.group({
            knowledge : [this.local_data.component.knowledge, Validators.required],
            // level: [this.local_data.component.level, [Validators.required, Validators.min(1), Validators.max(5)]],
            relevance: [this.local_data.component.relevance, [Validators.required, Validators.min(1), Validators.max(this.maxRelevance)]]
        });

    }

    doAction(): void {
        this._dialogRef.close({event: this.action, data: this.local_data});
    }

    closeDialog(): void {
        this._dialogRef.close({event: 'CANCEL'});
    }

    selectKnowledge(event): void {
        this.editorForm.controls['knowledge'].setValue(event.value);
    }

}
