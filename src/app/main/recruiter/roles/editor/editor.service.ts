import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {RoleApiService} from 'app/_services';


@Injectable()
export class RecruiterRolesEditorService implements Resolve<any> {
    onRoleChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {RoleApiService} _roleApiService
     */
    constructor(
        private _roleApiService: RoleApiService,
    ) {
        // Set the defaults
        this.onRoleChanged = new BehaviorSubject({});
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getRole(route.params.roleId)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get role
     *
     * @param {number} id
     * @returns {Promise<any>}
     */
    getRole(id): Promise<any>
    {
        if (id > 0) {
            return new Promise((resolve, reject) => {
                this._roleApiService.get(id)
                    .subscribe((response: any) => {
                        this.onRoleChanged.next(response);
                        resolve(response);
                    }, reject);
            });
        } else {
            return new Promise(resolve => {
                resolve({title: null, description: null, image: null, icon: null, components: null});
            });
        }
    }

    /**
     * Save role
     *
     * @param {any} role
     * @returns {Promise<any>}
     */
    saveRole(role: any): Promise<any>
    {
        return new Promise((resolve, reject) => {
            if (role.id && role.id > 0) {
                this._roleApiService.update(role)
                    .subscribe((response: any) => {
                        this.onRoleChanged.next(response);
                        resolve(response);
                    }, reject);
            } else {
                this._roleApiService.add(role)
                    .subscribe((response: any) => {
                        this.onRoleChanged.next(response);
                        resolve(response);
                    }, reject);
            }
        });
    }

    /**
     * Reset role
     *
     * @returns {void}
     */
    resetRole(): void
    {
        this.onRoleChanged.next({});
    }

}
