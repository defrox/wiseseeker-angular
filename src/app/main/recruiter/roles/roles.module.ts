import {NgModule} from '@angular/core';
import {RecruiterRolesRouting} from 'app/main/recruiter/roles/roles.routing';


@NgModule({
    imports: [
        RecruiterRolesRouting
    ]
})
export class RecruiterRolesModule {
}
