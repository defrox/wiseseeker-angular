import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {RoleApiService, UserApiService, TestApiService, SoftSkillApiService} from 'app/_services';
import {TranslateService} from '@ngx-translate/core';
import * as _ from 'lodash';

@Injectable()
export class RecruiterRolesDetailService implements Resolve<any> {
    onRoleChanged: BehaviorSubject<any>;
    role: any;

    /**
     * Constructor
     *
     * @param {TranslateService} _translateService
     * @param {UserApiService} _userApiService
     * @param {RoleApiService} _roleApiService
     * @param {TestApiService} _testApiService
     * @param {SoftSkillApiService} _softSkillApiService
     */
    constructor(
        private _translateService: TranslateService,
        private _userApiService: UserApiService,
        private _roleApiService: RoleApiService,
        private _testApiService: TestApiService,
        private _softSkillApiService: SoftSkillApiService,
    ) {
        // Set the defaults
        this.onRoleChanged = new BehaviorSubject({});
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getRole(route.params.roleId)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get role
     *
     * @returns {Promise<any>}
     */
    getRole(id): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._roleApiService.get(id)
                .subscribe((response: any) => {
                    this._formatRole(response).then(res => {
                        this.role = res;
                        this.onRoleChanged.next(this.role);
                        resolve(this.role);
                    });
                }, reject);
        });
    }

    /**
     * Delete role
     *
     * @returns {Promise<any>}
     */
    deleteRole(id): Promise<any>
    {
        return this._roleApiService.delete(id).toPromise();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Format role object
     *
     * @param role
     * @return {Promise<>ny[]>}
     */
    async _formatRole(role: any): Promise<any[]>
    {
        const result = _.cloneDeep(role);
        if (result.test_personality) {
            await this._testApiService.get(role.test_personality).toPromise().then(res => result.test_personality = res);
        }
        if (result.test) {
            await this._testApiService.get(role.test).toPromise().then(res => result.test = res);
        }
        /*const skills = [];
        if (role.skills && role.skills.length > 0) {
            for (const skill of role.skills) {
                this._softSkillApiService.get(skill).toPromise().then(res => skills.push(res));
            }
        }
        result.skills = skills;*/
        const applications = [];
        if (role.applications && role.applications.length > 0) {
            for (const application of role.applications) {
                const item = _.cloneDeep(application);
                await this._userApiService.getById(application.user).toPromise().then(res => {
                    item.user = res;
                    item.actions = res.id;
                    applications.push(item);
                });
            }
        }
        result.applications = applications;
        return result;
    }

}
