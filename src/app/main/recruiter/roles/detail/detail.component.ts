import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, QueryList, ViewChildren, ViewChild, ViewEncapsulation} from '@angular/core';
import {merge, Observable, Subject, of} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {Router} from '@angular/router';
import {MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {TranslatePipe} from '@ngx-translate/core';
import {RecruiterRolesDetailService} from './detail.service';
import {AppConfigService, LoaderOverlayService, SnackAlertService} from 'app/_services';

// Import the locale files
import {locale as english} from 'app/main/recruiter/i18n/en';
import {locale as spanish} from 'app/main/recruiter/i18n/es';


@Component({
    selector: 'recruiter-roles-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class RecruiterRolesDetailComponent implements OnInit, OnDestroy, AfterViewInit {
    appConfig: any;
    roleComponentsDisplayedColumns: string[] = ['knowledge', 'relevance'];
    role: any;
    roleComponentsTable = new MatTableDataSource<any>();

    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {AppConfigService} _appConfigService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslatePipe} _translatePipe
     * @param {RecruiterRolesDetailService} _recruiterRolesDetailService
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {SnackAlertService} _snackAlertService
     * @param {MatDialog} _dialog
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {Router} _router
     */
    constructor(
        private _appConfigService: AppConfigService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translatePipe: TranslatePipe,
        private _recruiterRolesDetailService: RecruiterRolesDetailService,
        private _loaderOverlay: LoaderOverlayService,
        private _snackAlertService: SnackAlertService,
        private _dialog: MatDialog,
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseSidebarService: FuseSidebarService,
        private _router: Router

    ) {
        // Set the private defaults
        this._translationLoader.loadTranslations(spanish, english);
        this._appConfigService.config.subscribe(config => {
            this.appConfig = config;
        });
        this._unsubscribeAll = new Subject();
        this.sort = new MatSort();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // TODO: use wrapper table https://github.com/angular/material2/blob/master/src/material-examples/table-wrapped/

        // If the user changes the sort order, reset back to the first page.
        // this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        // Subscribe to role
        this._recruiterRolesDetailService.onRoleChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(role => {
                this.role = role;
            });
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void {}

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    deleteRole(roleId): void {
        const dialogRef = this._dialog.open(RecruiterRolesDetailDialogComponent);
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // delete confirmed
                this._loaderOverlay.show();
                this._recruiterRolesDetailService.deleteRole(roleId).then( () => {
                    this._router.navigate([`/recruiter/roles`]).then(() => {
                        this._loaderOverlay.hide();
                        this._snackAlertService.success(this._translatePipe.transform('RECRUITER.ROLES.DIALOG.DELETED_SUCCESSFULLY'));
                    });
                });
            } else {
                // cancel delete
                this._loaderOverlay.hide();
            }
        });
    }

    applyFilter(filterValue: string): void {
        // const data = this.roleComponentsTable.data;
        // this.roleComponentsTable = new MatTableDataSource<any>(data);
        this.roleComponentsTable.filter = filterValue.trim().toLowerCase();
        if (this.paginator) {
            this.paginator.firstPage();
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

}


@Component({
    selector: 'recruiter-roles-detail-dialog',
    templateUrl: './detail.dialog.component.html',
    styleUrls: ['./detail.component.scss'],
})
export class RecruiterRolesDetailDialogComponent {
    constructor(private _dialogRef: MatDialogRef<RecruiterRolesDetailDialogComponent>) {
        _dialogRef.disableClose = true;
    }
}
