import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {RoleApiService, RoleCategoryApiService} from 'app/_services';

@Injectable()
export class RecruiterRolesHomeService implements Resolve<any> {
    onCategoriesChanged: BehaviorSubject<any>;
    onRolesChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {RoleCategoryApiService} _rolesCategoryApiService
     * @param {RoleApiService} _roleApiService
     */
    constructor(
        private _rolesCategoryApiService: RoleCategoryApiService,
        private _roleApiService: RoleApiService
    ) {
        // Set the defaults
        this.onCategoriesChanged = new BehaviorSubject({});
        this.onRolesChanged = new BehaviorSubject({});
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getCategories(),
                this.getRoles()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get categories
     *
     * @returns {Promise<any>}
     */
    getCategories(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._rolesCategoryApiService.getAll()
                .subscribe((response: any) => {
                    this.onCategoriesChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Get roles
     *
     * @returns {Promise<any>}
     */
    getRoles(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._roleApiService.getAll()
                .subscribe((response: any) => {
                    this.onRolesChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Delete role
     *
     * @returns {Promise<any>}
     */
    deleteRole(id): Promise<any>
    {
        return this._roleApiService.delete(id).toPromise();
    }

}
