import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {TranslatePipe} from '@ngx-translate/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {RecruiterRolesHomeService} from './home.service';
import {DialogData} from 'app/_models';
import {LoaderOverlayService, SnackAlertService} from 'app/_services';

// Import the locale files
import {locale as english} from 'app/main/recruiter/i18n/en';
import {locale as spanish} from 'app/main/recruiter/i18n/es';

@Component({
    selector: 'recruiter-roles-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class RecruiterRolesHomeComponent implements OnInit, OnDestroy {
    categories: any[];
    types = [{title: 'MAIN.ROLES.TYPES.DEFAULT', type: 'default'}, {title: 'MAIN.ROLES.TYPES.CUSTOM', type: 'custom'}];
    roles: any[];
    rolesFilteredByCategory: any[];
    filteredRoles: any[];
    currentCategory: string;
    currentType: string;
    searchTerm: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MatDialog} _dialog
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {SnackAlertService} _snackAlertService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslatePipe} _translatePipe
     * @param {RecruiterRolesHomeService} _recruiterRolesHomeService
     */
    constructor(
        private _dialog: MatDialog,
        private _loaderOverlay: LoaderOverlayService,
        private _snackAlertService: SnackAlertService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translatePipe: TranslatePipe,
        private _recruiterRolesHomeService: RecruiterRolesHomeService
    ) {
        // Set the defaults
        this.currentCategory = this.currentType = 'all';
        this.searchTerm = '';

        // Set the private defaults
        this._translationLoader.loadTranslations(spanish, english);
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to categories
        this._recruiterRolesHomeService.onCategoriesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(categories => {
                this.categories = categories;
            });

        // Subscribe to roles
        this._recruiterRolesHomeService.onRolesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(roles => {
                this.filteredRoles = this.rolesFilteredByCategory = this.roles = roles;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Filter roles by category
     */
    filterRolesByCategory(): void {
        // Filter
        if (this.currentCategory === 'all') {
            this.rolesFilteredByCategory = this.roles;
            this.filteredRoles = this.roles;
        } else {
            this.rolesFilteredByCategory = this.roles.filter((knowledge) => {
                return knowledge.category === this.currentCategory;
            });
            this.filteredRoles = [...this.rolesFilteredByCategory];
        }
        if (this.currentType === 'all') {
            this.filteredRoles = this.rolesFilteredByCategory;
        } else {
            this.rolesFilteredByCategory = this.rolesFilteredByCategory.filter((knowledge) => {
                const isCustom = knowledge.tests && knowledge.tests.length > 0 && knowledge.tests[0].hasOwnProperty('custom');
                return (knowledge.custom && this.currentType === 'custom') || (!knowledge.custom && this.currentType === 'default');
            });
            this.filteredRoles = [...this.rolesFilteredByCategory];
        }
        // Re-filter by search term
        this.filterRolesByTerm();
    }

    /**
     * Filter roles by term
     */
    filterRolesByTerm(): void {
        const searchTerm = this.searchTerm.toLowerCase();

        // Search
        if (searchTerm === '') {
            this.filteredRoles = this.rolesFilteredByCategory;
        } else {
            this.filteredRoles = this.rolesFilteredByCategory.filter((role) => {
                return (role.title.toLowerCase().includes(searchTerm) || role.description.toLowerCase().includes(searchTerm));
            });
        }
    }

    deleteRole(roleId): void {
        const dialogRef = this._dialog.open(RecruiterRolesHomeDialogComponent);
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // delete confirmed
                this._loaderOverlay.show();
                this._recruiterRolesHomeService.deleteRole(roleId).then( () => {
                    this._recruiterRolesHomeService.getRoles().then(() => {
                        this._loaderOverlay.hide();
                        this._snackAlertService.success(this._translatePipe.transform('RECRUITER.ROLES.DIALOG.DELETED_SUCCESSFULLY'));
                    });
                });
            } else {
                // cancel delete
                this._loaderOverlay.hide();
            }
        });
    }

}

@Component({
    selector: 'recruiter-roles-home-dialog',
    templateUrl: './home.dialog.component.html',
    styleUrls: ['./home.component.scss'],
})
export class RecruiterRolesHomeDialogComponent {
    constructor(
        private _dialogRef: MatDialogRef<RecruiterRolesHomeDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
        _dialogRef.disableClose = true;
    }
}
