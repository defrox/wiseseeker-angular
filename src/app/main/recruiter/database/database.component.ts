import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
// import {RecruiterDatabaseService} from './database.service';

@Component({
    selector: 'recruiter-database',
    templateUrl: './database.component.html',
    styleUrls: ['./database.component.scss'],
    animations: fuseAnimations
})
export class RecruiterDatabaseComponent implements OnInit, OnDestroy {
    // categories: any[];

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {RecruiterDatabaseService} _recruiterDatabaseService
     */
    constructor(
        // private _recruiterDatabaseService: RecruiterDatabaseService
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

}
