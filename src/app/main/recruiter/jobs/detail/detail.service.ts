import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {JobApiService, UserApiService, TestApiService, SoftSkillApiService} from 'app/_services';
import {TranslateService} from '@ngx-translate/core';
import * as _ from 'lodash';

@Injectable()
export class RecruiterJobsDetailService implements Resolve<any> {
    onJobChanged: BehaviorSubject<any>;
    job: any;

    /**
     * Constructor
     *
     * @param {TranslateService} _translateService
     * @param {UserApiService} _userApiService
     * @param {JobApiService} _jobApiService
     * @param {TestApiService} _testApiService
     * @param {SoftSkillApiService} _softSkillApiService
     */
    constructor(
        private _translateService: TranslateService,
        private _userApiService: UserApiService,
        private _jobApiService: JobApiService,
        private _testApiService: TestApiService,
        private _softSkillApiService: SoftSkillApiService,
    ) {
        // Set the defaults
        this.onJobChanged = new BehaviorSubject({});
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getJob(route.params.jobId)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get job
     *
     * @returns {Promise<any>}
     */
    getJob(id): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._jobApiService.get(id)
                .subscribe((response: any) => {
                    this._formatJob(response).then(res => {
                        this.job = res;
                        this.onJobChanged.next(this.job);
                        resolve(this.job);
                    });
                }, reject);
        });
    }

    /**
     * Delete job
     *
     * @returns {Promise<any>}
     */
    deleteJob(id): Promise<any>
    {
        return this._jobApiService.delete(id).toPromise();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Format job object
     *
     * @param job
     * @return {Promise<>ny[]>}
     */
    async _formatJob(job: any): Promise<any[]>
    {
        const result = _.cloneDeep(job);
        if (result.test_personality) {
            await this._testApiService.get(job.test_personality).toPromise().then(res => result.test_personality = res);
        }
        if (result.test) {
            await this._testApiService.get(job.test).toPromise().then(res => result.test = res);
        }
        /*const skills = [];
        if (job.skills && job.skills.length > 0) {
            for (const skill of job.skills) {
                this._softSkillApiService.get(skill).toPromise().then(res => skills.push(res));
            }
        }
        result.skills = skills;*/
        const applications = [];
        if (job.applications && job.applications.length > 0) {
            for (const application of job.applications) {
                const item = _.cloneDeep(application);
                await this._userApiService.getById(application.user).toPromise().then(res => {
                    item.user = res;
                    item.actions = res.id;
                    applications.push(item);
                });
            }
        }
        result.applications = applications;
        return result;
    }

}
