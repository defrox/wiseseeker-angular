import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, QueryList, ViewChildren, ViewChild, ViewEncapsulation} from '@angular/core';
import {merge, Observable, Subject, of} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {Router} from '@angular/router';
import {MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {TranslatePipe} from '@ngx-translate/core';
import {RecruiterJobsDetailService} from './detail.service';
import {AppConfigService, LoaderOverlayService, PdfApiService, SnackAlertService} from 'app/_services';

// Import the locale files
import {locale as english} from 'app/main/recruiter/i18n/en';
import {locale as spanish} from 'app/main/recruiter/i18n/es';


@Component({
    selector: 'recruiter-jobs-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class RecruiterJobsDetailComponent implements OnInit, OnDestroy, AfterViewInit {
    appConfig: any;
    jobApplicationsDisplayedColumns: string[] = ['actions', 'user', 'status', 'kti'];
    job: any;
    anonymous = true;
    jobApplicationsTable = new MatTableDataSource<any>();

    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {AppConfigService} _appConfigService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslatePipe} _translatePipe
     * @param {RecruiterJobsDetailService} _recruiterJobsDetailService
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {SnackAlertService} _snackAlertService
     * @param {PdfApiService} _pdfApiService
     * @param {MatDialog} _dialog
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {Router} _router
     */
    constructor(
        private _appConfigService: AppConfigService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translatePipe: TranslatePipe,
        private _recruiterJobsDetailService: RecruiterJobsDetailService,
        private _loaderOverlay: LoaderOverlayService,
        private _snackAlertService: SnackAlertService,
        private _pdfApiService: PdfApiService,
        private _dialog: MatDialog,
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseSidebarService: FuseSidebarService,
        private _router: Router

    ) {
        // Set the private defaults
        this._translationLoader.loadTranslations(spanish, english);
        this._appConfigService.config.subscribe(config => {
            this.appConfig = config;
            if (this.appConfig.appSettings.anonymousSite) {
                this.jobApplicationsDisplayedColumns = ['user', 'status', 'kti'];
            }
        });
        this._unsubscribeAll = new Subject();
        this.sort = new MatSort();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // TODO: use wrapper table https://github.com/angular/material2/blob/master/src/material-examples/table-wrapped/

        // If the user changes the sort order, reset back to the first page.
        // this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        // Subscribe to job
        this._recruiterJobsDetailService.onJobChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(job => {
                this.job = job;
            });
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void {}

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    /**
     * get user pdf report
     */
    getUserPdf(userId: number): void {
        this._loaderOverlay.show();
        this._pdfApiService.getUserPdf(+userId).subscribe(data => {
            this._downloadFile(data, userId);
            this._loaderOverlay.hide();
        }, err => this._loaderOverlay.hide());
    }

    /**
     * get anonymous pdf report
     */
    getAnonymousPdf(userId: number): void {
        this._loaderOverlay.show();
        this._pdfApiService.getAnonymousPdf(+userId).subscribe(data => {
            this._downloadFile(data, 'Anonymous');
            this._loaderOverlay.hide();
        }, err => this._loaderOverlay.hide());
    }

    deleteJob(jobId): void {
        const dialogRef = this._dialog.open(RecruiterJobsDetailDialogComponent);
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // delete confirmed
                this._loaderOverlay.show();
                this._recruiterJobsDetailService.deleteJob(jobId).then( () => {
                    this._router.navigate([`/recruiter/jobs`]).then(() => {
                        this._loaderOverlay.hide();
                        this._snackAlertService.success(this._translatePipe.transform('RECRUITER.JOBS.DIALOG.DELETED_SUCCESSFULLY'));
                    });
                });
            } else {
                // cancel delete
                this._loaderOverlay.hide();
            }
        });
    }

    applyFilter(filterValue: string): void {
        // const data = this.jobApplicationsTable.data;
        // this.jobApplicationsTable = new MatTableDataSource<any>(data);
        this.jobApplicationsTable.filter = filterValue.trim().toLowerCase();
        if (this.paginator) {
            this.paginator.firstPage();
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    _downloadFile(data: any, userId: number | string): any {
        const blob = new Blob([data], { type: 'application/pdf' });
        const url = window.URL.createObjectURL(blob);
        const anchor = document.createElement('a');
        anchor.download = `report${userId}.pdf`;
        anchor.href = url;
        document.body.appendChild(anchor);
        anchor.click();
        document.body.removeChild(anchor);
    }
}


@Component({
    selector: 'recruiter-jobs-detail-dialog',
    templateUrl: './detail.dialog.component.html',
    styleUrls: ['./detail.component.scss'],
})
export class RecruiterJobsDetailDialogComponent {
    constructor(private _dialogRef: MatDialogRef<RecruiterJobsDetailDialogComponent>) {
        _dialogRef.disableClose = true;
    }
}
