import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RecruiterJobsHomeComponent, RecruiterJobsHomeService, RecruiterJobsHomeDialogComponent} from 'app/main/recruiter/jobs/home';
import {RecruiterJobsDetailComponent, RecruiterJobsDetailDialogComponent, RecruiterJobsDetailService} from 'app/main/recruiter/jobs/detail';
import {RecruiterJobsEditorComponent, RecruiterJobsEditorDialogComponent, RecruiterJobsEditorService} from 'app/main/recruiter/jobs/editor';
import {MaterialModule} from 'app/_helpers/material.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseSidebarModule, FuseWidgetModule} from '@fuse/components';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {TranslateModule} from '@ngx-translate/core';
import {ChartsModule} from 'ng2-charts';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {AuthGuard} from 'app/_guards';
import {DefroxModule} from 'app/_helpers';
import {Group} from 'app/_models';
import {ChartModule} from 'angular-highcharts';

const appRoutes: Routes = [
    {path: 'recruiter/jobs', pathMatch: 'prefix', children: [
        {path: '', redirectTo: 'home', pathMatch: 'full'},
        {path: 'home', component: RecruiterJobsHomeComponent, resolve: {home: RecruiterJobsHomeService}, canActivate: [AuthGuard], data: {groups: [Group.Recruiter]}},
        {path: 'detail/:jobId/:jobSlug', component: RecruiterJobsDetailComponent, resolve: {job: RecruiterJobsDetailService}, canActivate: [AuthGuard], data: {groups: [Group.Recruiter]}},
        {path: 'editor/:jobId/:jobSlug', component: RecruiterJobsEditorComponent, resolve: {job: RecruiterJobsEditorService}, canActivate: [AuthGuard], data: {groups: [Group.Recruiter]}},
        // otherwise redirect to 404
        {path: '**', pathMatch: 'full', redirectTo: '/error/404'},
    ]}
];

@NgModule({
    declarations: [
        RecruiterJobsHomeComponent,
        RecruiterJobsHomeDialogComponent,
        RecruiterJobsDetailComponent,
        RecruiterJobsDetailDialogComponent,
        RecruiterJobsEditorComponent,
        RecruiterJobsEditorDialogComponent
    ],
    imports: [
        RouterModule.forChild(appRoutes),
        MaterialModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseWidgetModule,
        TranslateModule,
        ChartsModule,
        NgxChartsModule,
        FontAwesomeModule,
        DefroxModule,
        ChartModule
    ],
    providers: [
        RecruiterJobsHomeService,
        RecruiterJobsDetailService,
        RecruiterJobsEditorService,
        AuthGuard
    ],
    entryComponents: [RecruiterJobsDetailDialogComponent, RecruiterJobsEditorDialogComponent, RecruiterJobsHomeDialogComponent],
    exports: [RouterModule]
})
export class RecruiterJobsRouting {
}
