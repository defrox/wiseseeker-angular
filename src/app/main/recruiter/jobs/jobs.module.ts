import {NgModule} from '@angular/core';
import {RecruiterJobsRouting} from 'app/main/recruiter/jobs/jobs.routing';


@NgModule({
    imports: [
        RecruiterJobsRouting
    ]
})
export class RecruiterJobsModule {
}
