import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {TranslatePipe} from '@ngx-translate/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {RecruiterJobsHomeService} from './home.service';
import {DialogData} from 'app/_models';
import {LoaderOverlayService, SnackAlertService} from 'app/_services';

// Import the locale files
import {locale as english} from 'app/main/recruiter/i18n/en';
import {locale as spanish} from 'app/main/recruiter/i18n/es';

@Component({
    selector: 'recruiter-jobs-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class RecruiterJobsHomeComponent implements OnInit, OnDestroy {
    categories: any[];
    jobs: any[];
    jobsFilteredByCategory: any[];
    filteredJobs: any[];
    currentCategory: string;
    searchTerm: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MatDialog} _dialog
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {SnackAlertService} _snackAlertService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslatePipe} _translatePipe
     * @param {RecruiterJobsHomeService} _recruiterJobsHomeService
     */
    constructor(
        private _dialog: MatDialog,
        private _loaderOverlay: LoaderOverlayService,
        private _snackAlertService: SnackAlertService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translatePipe: TranslatePipe,
        private _recruiterJobsHomeService: RecruiterJobsHomeService
    ) {
        // Set the defaults
        this.currentCategory = 'all';
        this.searchTerm = '';

        // Set the private defaults
        this._translationLoader.loadTranslations(spanish, english);
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to categories
        /*this._recruiterJobsHomeService.onCategoriesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(categories => {
                this.categories = categories;
            });*/

        // Subscribe to jobs
        this._recruiterJobsHomeService.onJobsChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(jobs => {
                this.filteredJobs = this.jobsFilteredByCategory = this.jobs = jobs;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Filter jobs by category
     */
    filterJobsByCategory(): void {
        // Filter
        if (this.currentCategory === 'all') {
            this.jobsFilteredByCategory = this.jobs;
            this.filteredJobs = this.jobs;
        } else {
            this.jobsFilteredByCategory = this.jobs.filter((job) => {
                return job.category === this.currentCategory;
            });

            this.filteredJobs = [...this.jobsFilteredByCategory];

        }

        // Re-filter by search term
        this.filterJobsByTerm();
    }

    /**
     * Filter jobs by term
     */
    filterJobsByTerm(): void {
        const searchTerm = this.searchTerm.toLowerCase();

        // Search
        if (searchTerm === '') {
            this.filteredJobs = this.jobsFilteredByCategory;
        } else {
            this.filteredJobs = this.jobsFilteredByCategory.filter((job) => {
                return (job.title.toLowerCase().includes(searchTerm) || job.description.toLowerCase().includes(searchTerm));
            });
        }
    }

    deleteJob(jobId): void {
        const dialogRef = this._dialog.open(RecruiterJobsHomeDialogComponent);
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // delete confirmed
                this._loaderOverlay.show();
                this._recruiterJobsHomeService.deleteJob(jobId).then( () => {
                    this._recruiterJobsHomeService.getJobs().then(() => {
                        this._loaderOverlay.hide();
                        this._snackAlertService.success(this._translatePipe.transform('RECRUITER.JOBS.DIALOG.DELETED_SUCCESSFULLY'));
                    });
                });
            } else {
                // cancel delete
                this._loaderOverlay.hide();
            }
        });
    }

}

@Component({
    selector: 'recruiter-jobs-home-dialog',
    templateUrl: './home.dialog.component.html',
    styleUrls: ['./home.component.scss'],
})
export class RecruiterJobsHomeDialogComponent {
    constructor(
        private _dialogRef: MatDialogRef<RecruiterJobsHomeDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
        _dialogRef.disableClose = true;
    }
}
