import {Component, OnDestroy, ChangeDetectorRef, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {fuseAnimations} from '@fuse/animations';
import {MatDialog, MatDialogRef} from '@angular/material';
import {RecruiterJobsEditorService} from './editor.service';
import {LoaderOverlayService, SnackAlertService, TestApiService, JobSkillApiService} from 'app/_services';
import {PersonalityRelevances, JobSkill} from 'app/_models';
import {TranslatePipe} from '@ngx-translate/core';
import * as _ from 'lodash';

// Import the locale files
import {locale as english} from 'app/main/recruiter/i18n/en';
import {locale as spanish} from 'app/main/recruiter/i18n/es';


@Component({
    selector: 'recruiter-jobs-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class RecruiterJobsEditorComponent implements OnInit, OnDestroy {
    animationDirection: 'left' | 'right' | 'none';
    personalityRelevances: any[];
    personalitySkills: any[];
    personalitySkillsGroups: any[];
    personalitySkillsGroupsLoaded = false;
    personalityTests: any[];
    personalityTest: any;
    personalitySelected = true;
    rolesTests: any[];
    job: any;
    jobId: string | number | null;
    jobSlug: string | null;
    form: FormGroup;
    submitted = false;
    icon = null;
    image = null;

    // Horizontal Stepper
    horizontalStepperStep1: FormGroup;
    horizontalStepperStep2: FormGroup;
    horizontalStepperStep3: FormGroup;
    horizontalStepperStep4: FormGroup;
    horizontalStepperStep5: FormGroup;
    horizontalStepperStep6: FormGroup;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {RecruiterJobsEditorService} _recruiterJobsEditorService
     * @param {JobSkillApiService} _jobSkillApiService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslatePipe} _translatePipe
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {SnackAlertService} _snackAlertService
     * @param {TestApiService} _testApiService
     * @param {FormBuilder} _formBuilder
     * @param {MatDialog} _dialog
     * @param {ChangeDetectorRef} _changeDetector
     * @param {ActivatedRoute} _route
     * @param {Router} _router
     */
    constructor(
        private _recruiterJobsEditorService: RecruiterJobsEditorService,
        private _jobSkillApiService: JobSkillApiService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translatePipe: TranslatePipe,
        private _loaderOverlay: LoaderOverlayService,
        private _snackAlertService: SnackAlertService,
        private _testApiService: TestApiService,
        private _formBuilder: FormBuilder,
        private _dialog: MatDialog,
        private _changeDetector: ChangeDetectorRef,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        // Set the defaults
        this._loaderOverlay.show();
        this.animationDirection = 'none';
        this.personalityRelevances = PersonalityRelevances;
        this.personalityTests = this.rolesTests = [];
        this._translationLoader.loadTranslations(spanish, english);
        this._route.paramMap.subscribe(params => {
            this.jobId = params.get('jobId');
            this.jobSlug = params.get('jobSlug');
        });

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to all job
        this._recruiterJobsEditorService.onJobChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(job => {
                this.job = job;
                this._loaderOverlay.hide();
                // this.personalitySelected = this.job.test_personality !== null;
            });

        // Get all tests
        this._testApiService.getAll()
            .subscribe(tests => {
                this.rolesTests = tests.filter( skill => +skill.type === 2);
                this.personalityTests = tests.filter( skill => +skill.type === 4);
                this.personalityTest = this.personalityTests[0].id;
            });

        // Horizontal Stepper form steps
        this.horizontalStepperStep1 = this._formBuilder.group({
            title : [this.job.title, Validators.required],
            description: [this.job.description]
        });

        this.horizontalStepperStep2 = this._formBuilder.group({
            active_from: [this.job.active_from, Validators.required],
            active_to: [this.job.active_to, Validators.required]
        });

        /*this.horizontalStepperStep3 = this._formBuilder.group({
            icon      : [''],
            image     : ['']
        });*/

        this.horizontalStepperStep6 = this._formBuilder.group({
            roles     : [this.job.test, Validators.required]
        });

        this.horizontalStepperStep4 = this._formBuilder.group({
            personality            : [this.personalitySelected],
            personality_relevance  : [this.job.personality_relevance],
            personality_skills     : [this.job.skills],
        });

        this._jobSkillApiService.getAll().subscribe(res => {
            this.personalitySkills = res;
            this.personalitySkillsGroups = this._formatSkillGroups(res);
            this.horizontalStepperStep4.setControl('personality_relevance', this._formBuilder.control(this.job.personality_relevance ? this.job.personality_relevance : 1, Validators.required));
            for (const group of this.personalitySkillsGroups) {
                if (group && group.name) {
                    this.horizontalStepperStep4.addControl(
                        'personality_skill_' + group.name.toLowerCase(),
                        this._formBuilder.control(this.job && this.job.skills && this.job.skills.filter(item => this._filterSkills(group.skills, item)) > 0 ? +this.job.skills.filter(item => this._filterSkills(group.skills, item)) : 0)
                    );
                }
            }
            this.personalitySkillsGroupsLoaded = true;
            if (!this.personalitySelected) {
                this.resetPersonality();
            }
        });
        this._changeDetector.detectChanges();

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._recruiterJobsEditorService.resetJob();
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Upload image file
     *
     * @param event
     * @returns {void}
     */
    uploadImage(event): void {
        const reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.image = {src: reader.result};
                // TODO: upload image through API
            };
        }
    }

    /**
     * Upload icon file
     *
     * @param event
     * @returns {void}
     */
    uploadIcon(event): void {
        const reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.icon = {src: reader.result};
                // TODO: upload image through API
            };
        }
    }

    /**
     * Submit job
     */
    submitJob(): void
    {
        if (!this.submitted) {
            this.submitted = true;
            this._loaderOverlay.show();
            const result = _.cloneDeep(this.job);
            result.title = this.horizontalStepperStep1.value.title;
            result.description = this.horizontalStepperStep1.value.description;
            result.active_to = this.horizontalStepperStep2.value.active_to;
            result.active_from = this.horizontalStepperStep2.value.active_from;
            // result.image = this.horizontalStepperStep3.value.image;
            // result.icon = this.horizontalStepperStep3.value.icon;
            result.skills = [];
            for (const group of this.personalitySkillsGroups) {
                if (group && group.name) {
                    if (this.horizontalStepperStep4.value['personality_skill_' + group.name.toLowerCase()] && this.horizontalStepperStep4.value['personality_skill_' + group.name.toLowerCase()] > 0) {
                        result.skills.push(this.horizontalStepperStep4.value['personality_skill_' + group.name.toLowerCase()]);
                    }
                }
            }
            /*if (result.skills.length === 0) {
                result.skills = [1];
            }*/
            result.test_personality = this.personalitySelected ? this.personalityTest : null;
            result.personality_relevance = this.horizontalStepperStep4.value.personality_relevance ? this.horizontalStepperStep4.value.personality_relevance : 1;
            // result.personality_relevance = this.horizontalStepperStep4.value.personality_relevance;
            // result.skills = this.horizontalStepperStep5.value.personality_skills;
            /*result.test_personality = this.horizontalStepperStep4.value.test_personality;
            result.test = this.horizontalStepperStep6.value.roles;*/
            // console.log(result);
            this._recruiterJobsEditorService.saveJob(result).then(res => {
                this.job = res;
                this._router.navigate([`/recruiter/jobs`]).then(() => {
                    this._loaderOverlay.hide();
                    this._snackAlertService.success(this._translatePipe.transform('RECRUITER.DASHBOARD.JOBS.SAVED_SUCCESSFULLY'));
                });
            }).catch(err => {
                // do something in case of error
                this._loaderOverlay.hide();
                this.submitted = false;
                // throw new Error(err);
            });
        }
    }

    resetPersonality(): void {
        this.horizontalStepperStep4.get('personality_skills').reset(null);
        this.horizontalStepperStep4.get('personality_relevance').reset(1);
        for (const group of this.personalitySkillsGroups) {
            if (group && group.name) {
                if (this.horizontalStepperStep4.get(['personality_skill_' + group.name.toLowerCase()])) {
                    this.horizontalStepperStep4.get('personality_skill_' + group.name.toLowerCase()).reset(0);
                }
            }
        }
    }

    togglePersonality(): void {
        if (this.horizontalStepperStep4.value.personality) {
            this.personalitySelected = true;
        } else {
            this.personalitySelected = false;
        }
        this.resetPersonality();
        this.horizontalStepperStep4.get('personality').reset(this.personalitySelected);
    }

    private  _formatSkillGroups(skills): any[] {
        let result = [];
        if (skills && skills.length > 0) {
            for (let skill of skills) {
                if (skill.skill) {
                    const tmpSkills = result[skill.skill.id] && result[skill.skill.id].skills ? result[skill.skill.id].skills : [];
                    // console.log(skill);
                    result[skill.skill.id] = {id: skill.skill.id, name: skill.skill.description, skills: [...tmpSkills, ...[skill]]};
                }
            }
        }
        return result;
    }

    private _getFirst(array, n = 1): any | null {
        return n > 0 && array.length > n ? array.slice(0, n) : array;
    }

    /**
     * Filter tests
     *
     * @param testArray
     * @param testId
     * @returns {boolean}
     */
    private _filterTests(testArray: any[], testId: number): boolean {
        let result = false;
        for (const item of testArray) {
            if (+item.id === testId) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Filter skills
     *
     * @param skillsArray
     * @param skillId
     * @returns {boolean}
     */
    private _filterSkills(skillsArray: any[], skillId: number): boolean {
        let result = false;
        for (const item of skillsArray) {
            if (+item.id === skillId) {
                result = true;
            }
        }
        return result;
    }

}


@Component({
    selector: 'recruiter-jobs-editor-dialog',
    templateUrl: './editor.dialog.component.html',
    styleUrls: ['./editor.component.scss'],
})
export class RecruiterJobsEditorDialogComponent {
    constructor(private _dialogRef: MatDialogRef<RecruiterJobsEditorDialogComponent>) {
        _dialogRef.disableClose = true;
    }
}
