import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {JobApiService} from 'app/_services';


@Injectable()
export class RecruiterJobsEditorService implements Resolve<any> {
    onJobChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {JobApiService} _jobApiService
     */
    constructor(
        private _jobApiService: JobApiService,
    ) {
        // Set the defaults
        this.onJobChanged = new BehaviorSubject({});
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getJob(route.params.jobId)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get job
     *
     * @param {number} id
     * @returns {Promise<any>}
     */
    getJob(id): Promise<any>
    {
        if (id > 0) {
            return new Promise((resolve, reject) => {
                this._jobApiService.get(id)
                    .subscribe((response: any) => {
                        this.onJobChanged.next(response);
                        resolve(response);
                    }, reject);
            });
        } else {
            return new Promise(resolve => {
                resolve({title: null, description: null, active_from: null, active_to: null, personality_relevance: null, test_personality: null, test: null, skills: null});
            });
        }
    }

    /**
     * Save job
     *
     * @param {any} job
     * @returns {Promise<any>}
     */
    saveJob(job: any): Promise<any>
    {
        return new Promise((resolve, reject) => {
            if (job.id && job.id > 0) {
                this._jobApiService.update(job)
                    .subscribe((response: any) => {
                        this.onJobChanged.next(response);
                        resolve(response);
                    }, reject);
            } else {
                this._jobApiService.add(job)
                    .subscribe((response: any) => {
                        this.onJobChanged.next(response);
                        resolve(response);
                    }, reject);
            }
        });
    }

    /**
     * Reset job
     *
     * @returns {void}
     */
    resetJob(): void
    {
        this.onJobChanged.next({});
    }

}
