﻿import {Component, OnInit, OnDestroy, ViewEncapsulation, Inject} from '@angular/core';
import {Router} from '@angular/router';
import {Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {TranslatePipe, TranslateService} from '@ngx-translate/core';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {RecruiterDashboardService} from './dashboard.service';
import {AppConfigService, AuthenticationService, LoaderOverlayService, NotificationService, SnackAlertService} from 'app/_services';
import {DialogData, User} from 'app/_models';

// Import the locale files
import {locale as english} from 'app/main/recruiter/i18n/en';
import {locale as spanish} from 'app/main/recruiter/i18n/es';

@Component({
    selector: 'recruiter-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class RecruiterDashboardComponent implements OnInit, OnDestroy {
    appConfig: any;
    dashboard: any;
    talents: any;
    talentsDisplayedColumns: string[] = ['title', 'users', 'actions'];
    jobs: any;
    jobsDisplayedColumns: string[] = ['title', 'applications', 'actions'];
    currentUser: User;
    currentUserSubscription: Subscription;
    notifications: any;
    notificationsBadges: any;
    notificationsSubscription: Subscription;
    widgets = {};

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _authenticationService: AuthenticationService,
        private _recruiterDashboardService: RecruiterDashboardService,
        private _snackAlertService: SnackAlertService,
        private _notificationsService: NotificationService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translateService: TranslateService,
        private _translatePipe: TranslatePipe,
        private _appConfigService: AppConfigService,
        private _dialog: MatDialog,
        private _loaderOverlay: LoaderOverlayService,
        private _router: Router
    ) {
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
        this._appConfigService.config.subscribe(config => { this.appConfig = config; });
        this._translationLoader.loadTranslations(spanish, english);
        this.notificationsSubscription = this._notificationsService.onNotificationChanged.subscribe(notifications => {
            this.notifications = notifications;
            this.notificationsBadges = this._notificationsService.getCandidateNotificationsBadges(notifications);
            if (this._router.url.startsWith('/recruiter/dashboard')) {
                this._alertNotifications(this.notificationsBadges);
            }
        });
        this._unsubscribeAll = new Subject();
    }

    ngOnInit(): void {
        // Subscribe to Notifications
        this._notificationsService.onNotificationChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(notifications => {
                this.notifications = notifications;
                this.notificationsBadges = this._notificationsService.getCandidateNotificationsBadges(notifications);
            });

        // Subscribe to Talents
        this._recruiterDashboardService.onTalentsChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(talents => {
                this.talents = talents;
            });

        // Subscribe to Talents
        this._recruiterDashboardService.onJobsChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(jobs => {
                this.jobs = jobs;
            });
    }

    ngOnDestroy(): void {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    private _alertNotifications(notifications: any): void {
        if (notifications.dashboard_todo) {
            this._snackAlertService.primary(this._translatePipe.transform('RECRUITER.DASHBOARD.PENDING_TASKS_MSG').replace('{tasks}', notifications.dashboard_todo));
        }
    }

    deleteJob(jobId): void {
        const dialogRef = this._dialog.open(RecruiterDashboardJobDialogComponent);
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // delete confirmed
                this._loaderOverlay.show();
                this._recruiterDashboardService.deleteJob(jobId).then( () => {
                    this._recruiterDashboardService.getJobs().then(() => {
                        this._loaderOverlay.hide();
                        this._snackAlertService.success(this._translatePipe.transform('RECRUITER.JOBS.DIALOG.DELETED_SUCCESSFULLY'));
                    });
                });
            } else {
                // cancel delete
                this._loaderOverlay.hide();
            }
        });
    }

    deleteTalent(talentId): void {
        const dialogRef = this._dialog.open(RecruiterDashboardTalentDialogComponent);
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // delete confirmed
                this._loaderOverlay.show();
                this._recruiterDashboardService.deleteTalent(talentId).then( () => {
                    this._recruiterDashboardService.getTalents().then(() => {
                        this._loaderOverlay.hide();
                        this._snackAlertService.success(this._translatePipe.transform('RECRUITER.TALENT.DIALOG.DELETED_SUCCESSFULLY'));
                    });
                });
            } else {
                // cancel delete
                this._loaderOverlay.hide();
            }
        });
    }

}

@Component({
    selector: 'recruiter-dashboard-job-dialog',
    templateUrl: './dashboard.job.dialog.component.html',
    styleUrls: ['./dashboard.component.scss'],
})
export class RecruiterDashboardJobDialogComponent {
    constructor(
        private _dialogRef: MatDialogRef<RecruiterDashboardJobDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
        _dialogRef.disableClose = true;
    }
}

@Component({
    selector: 'recruiter-dashboard-talent-dialog',
    templateUrl: './dashboard.talent.dialog.component.html',
    styleUrls: ['./dashboard.component.scss'],
})
export class RecruiterDashboardTalentDialogComponent {
    constructor(
        private _dialogRef: MatDialogRef<RecruiterDashboardTalentDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
        _dialogRef.disableClose = true;
    }
}
