import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {EvaluationApiService, JobApiService} from 'app/_services';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable()
export class RecruiterDashboardService implements Resolve<any> {
    onDashboardChanged: BehaviorSubject<any>;
    onTalentsChanged: BehaviorSubject<any>;
    onJobsChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {EvaluationApiService} _evaluationApiService
     * @param {JobApiService} _jobApiService
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _evaluationApiService: EvaluationApiService,
        private _jobApiService: JobApiService,
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onDashboardChanged = new BehaviorSubject({});
        this.onTalentsChanged = new BehaviorSubject({});
        this.onJobsChanged = new BehaviorSubject({});
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                // this.getDashboard(),
                this.getTalents(),
                this.getJobs()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get dashboard
     *
     * @returns {Promise<any>}
     */
    /*getDashboard(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._httpClient.get('api/recruiter-dashboard')
                .subscribe((response: any) => {
                    this.onDashboardChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }*/

    /**
     * Get talents
     *
     * @returns {Promise<any>}
     */
    getTalents(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._evaluationApiService.getAll()
                .subscribe((response: any) => {
                    this.onTalentsChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Get jobs
     *
     * @returns {Promise<any>}
     */
    getJobs(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._jobApiService.getAll()
                .subscribe((response: any) => {
                    this.onJobsChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Delete talent
     *
     * @returns {Promise<any>}
     */
    deleteTalent(id): Promise<any>
    {
        return this._evaluationApiService.delete(id).toPromise();
    }

    /**
     * Delete job
     *
     * @returns {Promise<any>}
     */
    deleteJob(id): Promise<any>
    {
        return this._jobApiService.delete(id).toPromise();
    }

}
