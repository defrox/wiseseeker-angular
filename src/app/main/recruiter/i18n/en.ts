import {locale as main_locale} from 'app/main/i18n/en';

export const locale = {
    lang: 'en',
    data: {
        'RECRUITER': {
            'DASHBOARD': {
                'PENDING_TASKS_MSG': 'You have {tasks} pending tasks.',
                'JOBS': {
                    'TITLE': 'Jobs',
                    'DATE_APPLIED': 'Date applied:',
                    'SAVED_SUCCESSFULLY': 'Job saved successfully!',
                },
                'TALENT': {
                    'TITLE': 'Talent',
                    'SAVED_SUCCESSFULLY': 'Talent saved successfully!',
                },
                'NONE_YET': 'None yet'
            },
            'TALENT': {
                'HOME': {
                    'WELCOME_TEXT': 'WELCOME TO TALENT',
                    'WELCOME_SUBTEXT': 'Review talents.',
                },
                'DIALOG': {
                    'DELETE_TITLE': 'Confirm deletion',
                    'DELETE_TEXT': 'Are you sure you want to delete this talent? You will not be able to restore the data once deleted.',
                    'DELETED_SUCCESSFULLY': 'Talent deleted successfully!',
                    'ADD_USERS_TITLE': 'Confirmar añadir usuarios',
                    'ADD_USERS_TEXT': '¿Está seguro de querer añadir todos los usuarios de la lista a la base de datos?',
                    'USERS_ADDED_SUCCESSFULLY': '¡Usuarios añadidos con éxito!',
                },
                'DETAIL': {
                    'TESTS_BY_STATUS': 'Tests por estado',
                },
                'EDITOR': {
                    'NEW_TALENT': 'Nuevo talento',
                    'STEP_1_LABEL': 'Nombre y descripción',
                    'STEP_1_TEXT_1': 'Dale un nombre a tu proceso de evaluación del talento',
                    'STEP_1_TEXT_2': 'Dale una descripción',
                    'STEP_2_LABEL': 'Inicio y Fin',
                    'STEP_3_TEXT': 'Selecciona los empleados a los que quieras evaluar en este proceso',
                    'STEP_3_LABEL': 'Empleados',
                    'STEP_4_TEXT': 'Selecciona los empleados a los que quieras evaluar en este proceso',
                    'STEP_4_LABEL': 'Empleados',
                    'STEP_5_TEXT': 'Selecciona los tests psicológicos que quieras evaluar',
                    'STEP_5_LABEL': 'Personalidad',
                    'STEP_6_TEXT': 'Selecciona las habilidades que quieres evaluar',
                    'STEP_6_LABEL': 'Habilidades',
                    'STEP_7_TEXT': 'Selecciona los roles que quieres evaluar',
                    'STEP_7_LABEL': 'Roles',
                    'STEP_8_TEXT': 'Selecciona los conocimientos que quieres evaluar',
                    'STEP_8_LABEL': 'Conocimientos',
                    'STEP_FINAL_TEXT': 'Se ha creado un nuevo proceso de evaluación del talento'
                }
            },
            'JOBS': {
                'HOME': {
                    'WELCOME_TEXT': 'WELCOME TO JOBS',
                    'WELCOME_SUBTEXT': 'Review jobs.',
                },
                'DETAIL': {
                    'PERSONALITY_RELEVANCE_TITLE': 'Relevancia de la personalidad',
                    'PERSONALITY_TEST_TITLE': 'Test de personalidad',
                    'TEST_TITLE': 'Roles',
                    'SKILLS_TITLE': 'Habilidades',
                    'APPLICATIONS_TITLE': 'Applications',
                },
                'DIALOG': {
                    'DELETE_TITLE': 'Confirm deletion',
                    'DELETE_TEXT': 'Are you sure you want to delete this job? You will not be able to restore the data once deleted.',
                    'DELETED_SUCCESSFULLY': 'Job deleted successfully!',
                    'ADDED_SUCCESSFULLY': 'Job added successfully!',
                },
                'EDITOR': {
                    'NEW_JOB': 'Nuevo empleo',
                    'STEP_1_LABEL': 'Nombre y descripción',
                    'STEP_1_TEXT_1': 'Dale un nombre al empleo',
                    'STEP_1_TEXT_2': 'Dale una descripción',
                    'STEP_2_LABEL': 'Inicio y Fin',
                    'STEP_3_LABEL': 'Imagen e Icono',
                    'STEP_3_TEXT_A': 'Selecciona la imagen del proceso',
                    'STEP_3_LABEL_A': 'Imagen',
                    'STEP_3_TEXT_B': 'Selecciona el icono del proceso',
                    'STEP_3_LABEL_B': 'Icono',
                    'STEP_4_LABEL': 'Personalidad',
                    'STEP_4_TEXT_A': 'Elige la relevancia que tiene la personalidad en este empleo',
                    'STEP_4_LABEL_A': 'Relevancia de la Personalidad',
                    'STEP_4_TEXT_B': 'Elige los rasgos de personalidad para este empleo',
                    'STEP_4_LABEL_B': 'Rasgos de Personalidad',
                    'STEP_5_TEXT': 'Elige el test de personalidad para este empleo',
                    'STEP_5_LABEL': 'Test Personalidad',
                    'STEP_6_TEXT': 'Selecciona los roles para este empleo',
                    'STEP_6_LABEL': 'Roles',
                    'STEP_FINAL_TEXT': 'Se ha creado un nuevo empleo'
                }
            },
            'ROLES': {
                'HOME': {
                    'WELCOME_TEXT': 'WELCOME TO USER ROLES',
                    'WELCOME_SUBTEXT': 'Review user roles.',
                },
                'DETAIL': {
                    'KNOWLEDGE_TITLE': 'Knowledge',
                    'COMPONENTS_TITLE': 'Components',
                },
                'DIALOG': {
                    'DELETE_TITLE': 'Confirm deletion',
                    'DELETE_TEXT': 'Are you sure you want to delete this role? You will not be able to restore the data once deleted.',
                    'DELETED_SUCCESSFULLY': 'Role deleted successfully!',
                    'ADDED_SUCCESSFULLY': 'Role added successfully!',
                },
                'EDITOR': {
                    'NEW_ROLE': 'Nuevo rol',
                    'STEP_1_LABEL': 'Nombre y descripción',
                    'STEP_1_TEXT_1': 'Dale un nombre al role',
                    'STEP_1_TEXT_2': 'Dale una descripción',
                    'STEP_2_LABEL': 'Inicio y Fin',
                    'STEP_3_LABEL': 'Imagen e Icono',
                    'STEP_3_TEXT_A': 'Selecciona la imagen del proceso',
                    'STEP_3_LABEL_A': 'Imagen',
                    'STEP_3_TEXT_B': 'Selecciona el icono del proceso',
                    'STEP_3_LABEL_B': 'Icono',
                    'STEP_4_LABEL': 'Componentes',
                    'STEP_4_LABEL_TOTAL': 'Relevancia total',
                    'STEP_FINAL_TEXT': 'Se ha creado un nuevo rol',
                    'ERROR_FULL_PERCENTAGE': 'El total debe ser 100%. Modifique los valores hasta alcanzar este valor.',
                    'SAVED_SUCCESSFULLY': '¡Rol de usuario guardado con éxito!',
                    'DIALOG': {
                        'TITLE_ADD': 'Añadir nuevo conocimiento',
                        'TITLE_UPDATE': 'Editar conocimiento',
                        'TITLE_DELETE': 'Confirmar eliminación',
                        'CONFIRM_DELETE': '¿Está seguro de querer borrar este conocimiento? No se podrá recuperar la información una vez eliminado.',
                    },
                }
            },
            'COMMON': {
                'TYPE_HERE_YOUR_ANSWER': 'Type here your answer',
                'TEST_STATUS': {
                    'PENDING': 'Not started',
                    'STARTED': 'Started',
                    'FINISHED': 'Finished',
                    'ASSESSED': 'Finished',
                    'REQUESTED': 'Requested',
                    'CONFIRMED': 'Confirmed',
                    'EXECUTED': 'Executed',
                },
            },
        }
    }
};

Object.assign(locale.data, locale.data, main_locale.data);
