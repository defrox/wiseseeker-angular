import {locale as main_locale} from 'app/main/i18n/es';

export const locale = {
    lang: 'es',
    data: {
        'RECRUITER': {
            'DASHBOARD': {
                'PENDING_TASKS_MSG': 'Tienes {tasks} tareas pendientes.',
                'OPEN_PROCESSES_TOTAL': 'procesos abiertos',
                'JOBS': {
                    'TITLE': 'Procesos de Selección',
                    'OPEN_PROCESSES': 'Procesos de Selección Abiertos',
                    'DATE_APPLIED': 'Fecha aplicación:',
                    'SAVED_SUCCESSFULLY': '¡Procesos de Selección guardado con éxito!',
                },
                'TALENT': {
                    'TITLE': 'Procesos de Talento',
                    'OPEN_PROCESSES': 'Procesos de Talento Abiertos',
                    'SAVED_SUCCESSFULLY': '¡Talento guardado con éxito!',
                },
                'NONE_YET': 'Ninguno aun'
            },
            'TALENT': {
                'HOME': {
                    'WELCOME_TEXT': 'PROCESOS DE TALENTO',
                    'WELCOME_SUBTEXT': 'Procesos Abiertos de Evaluación del Talento',
                },
                'DIALOG': {
                    'DELETE_TITLE': 'Confirmar eliminación',
                    'DELETE_TEXT': '¿Está seguro de querer borrar este talento? No se podrá recuperar la información una vez eliminado.',
                    'DELETED_SUCCESSFULLY': '¡Talento eliminado con éxito!',
                    'ADD_USERS_TITLE': 'Confirmar añadir usuarios',
                    'ADD_USERS_TEXT': '¿Está seguro de querer añadir todos los usuarios de la lista a la base de datos?',
                    'USERS_ADDED_SUCCESSFULLY': '¡Usuarios añadidos con éxito!',
                },
                'DETAIL': {
                    'TESTS_BY_STATUS': 'Tests por estado',
                },
                'EDITOR': {
                    'NEW_TALENT': 'Nuevo talento',
                    'STEP_1_LABEL': 'Nombre y descripción',
                    'STEP_1_TEXT_1': 'Dale un nombre a tu proceso de evaluación del talento',
                    'STEP_1_TEXT_2': 'Dale una descripción',
                    'STEP_2_LABEL': 'Inicio y Fin',
                    'STEP_3_LABEL': 'Imagen e Icono',
                    'STEP_3_TEXT_A': 'Selecciona la imagen del proceso',
                    'STEP_3_LABEL_A': 'Imagen',
                    'STEP_3_TEXT_B': 'Selecciona el icono del proceso',
                    'STEP_3_LABEL_B': 'Icono',
                    'STEP_4_TEXT': 'Selecciona los empleados a los que quieras evaluar en este proceso',
                    'STEP_4_LABEL': 'Empleados a evaluar',
                    'STEP_5_TEXT': 'Selecciona los tests psicológicos que quieras evaluar',
                    'STEP_5_LABEL': 'Tipo de Evaluación',
                    'STEP_5_LABEL_A': 'Personalidad',
                    'STEP_5_TEXT_A': 'Evaluar la personalidad (Test Big Five).',
                    'STEP_5_LABEL_B': 'Habilidades',
                    'STEP_5_TEXT_B': 'Evaluar las habilidades de venta (Test IPV) Nota: habilitando la opción deshabilita el resto.',
                    'STEP_5_TEXT_C': 'Selecciona los conocimientos que quieres evaluar',
                    'STEP_5_LABEL_C': 'Conocimientos',
                    'STEP_5_TEXT_D': 'Habilita la evaluación de conocimientos',
                    'STEP_FINAL_TEXT': 'Se ha creado un nuevo proceso de evaluación del talento'
                }
            },
            'JOBS': {
                'HOME': {
                    'WELCOME_TEXT': 'EMPLEOS',
                    'WELCOME_SUBTEXT': 'Procesos de Selección Abiertos',
                },
                'DETAIL': {
                    'PERSONALITY_RELEVANCE_TITLE': 'Relevancia de la personalidad',
                    'PERSONALITY_TEST_TITLE': 'Test de personalidad',
                    'TEST_TITLE': 'Roles',
                    'SKILLS_TITLE': 'Habilidades',
                    'APPLICATIONS_TITLE': 'Aplicaciones',
                },
                'DIALOG': {
                    'DELETE_TITLE': 'Confirmar eliminación',
                    'DELETE_TEXT': '¿Está seguro de querer borrar este empleo? No se podrá recuperar la información una vez eliminado.',
                    'DELETED_SUCCESSFULLY': '¡Empleo eliminado con éxito!',
                    'ADDED_SUCCESSFULLY': '¡Empleo añadido con éxito!',
                },
                'EDITOR': {
                    'NEW_JOB': 'Nuevo empleo',
                    'STEP_1_LABEL': 'Nombre y descripción',
                    'STEP_1_TEXT_1': 'Dale un nombre al empleo',
                    'STEP_1_TEXT_2': 'Dale una descripción',
                    'STEP_2_LABEL': 'Inicio y Fin',
                    'STEP_3_LABEL': 'Imagen e Icono',
                    'STEP_3_TEXT_A': 'Selecciona la imagen del proceso',
                    'STEP_3_LABEL_A': 'Imagen',
                    'STEP_3_TEXT_B': 'Selecciona el icono del proceso',
                    'STEP_3_LABEL_B': 'Icono',
                    'STEP_4_LABEL': 'Personalidad',
                    'STEP_4_TEXT_A': 'Elige la relevancia que tiene la personalidad en este empleo',
                    'STEP_4_LABEL_A': 'Relevancia de la Personalidad',
                    'STEP_4_TEXT_B': 'Elige los rasgos de personalidad para este empleo',
                    'STEP_4_LABEL_B': 'Rasgos de Personalidad',
                    'STEP_4_TEXT_C': 'Elige si se debe evaluar la personalidad para este empleo',
                    'STEP_4_LABEL_C': 'Evaluar la Personalidad',
                    'STEP_5_TEXT': 'Elige si se añade el test de personalidad para este empleo',
                    'STEP_5_LABEL': 'Test Personalidad',
                    'STEP_6_TEXT': 'Selecciona el rol para este empleo',
                    'STEP_6_LABEL': 'Rol',
                    'STEP_FINAL_TEXT': 'Se ha creado un nuevo empleo'
                }
            },
            'ROLES': {
                'HOME': {
                    'WELCOME_TEXT': 'ROLES DE USUARIO',
                    'WELCOME_SUBTEXT': 'Procesos de Rol de Usuario',
                },
                'DETAIL': {
                    'KNOWLEDGE_TITLE': 'Conocimientos',
                    'COMPONENTS_TITLE': 'Componentes',
                },
                'DIALOG': {
                    'DELETE_TITLE': 'Confirmar eliminación',
                    'DELETE_TEXT': '¿Está seguro de querer borrar este rol? No se podrá recuperar la información una vez eliminado.',
                    'DELETED_SUCCESSFULLY': '¡Rol eliminado con éxito!',
                    'ADDED_SUCCESSFULLY': '¡Rol añadido con éxito!',
                },
                'EDITOR': {
                    'NEW_ROLE': 'Nuevo rol de usuario',
                    'STEP_1_LABEL': 'Nombre y descripción',
                    'STEP_1_TEXT_1': 'Dale un nombre al rol',
                    'STEP_1_TEXT_2': 'Dale una descripción',
                    'STEP_2_LABEL': 'Inicio y Fin',
                    'STEP_3_LABEL': 'Imagen e Icono',
                    'STEP_3_TEXT_A': 'Selecciona la imagen del rol',
                    'STEP_3_LABEL_A': 'Imagen',
                    'STEP_3_TEXT_B': 'Selecciona el icono del rol',
                    'STEP_3_LABEL_B': 'Icono',
                    'STEP_4_LABEL': 'Componentes',
                    'STEP_4_LABEL_TOTAL': 'Relevancia total',
                    'STEP_FINAL_TEXT': 'Se ha creado un nuevo rol',
                    'ERROR_FULL_PERCENTAGE': 'El total debe ser 100%. Modifique los valores hasta alcanzar este valor.',
                    'SAVED_SUCCESSFULLY': '¡Rol de usuario guardado con éxito!',
                    'DIALOG': {
                        'TITLE_ADD': 'Añadir nuevo conocimiento',
                        'TITLE_UPDATE': 'Editar conocimiento',
                        'TITLE_DELETE': 'Confirmar eliminación',
                        'CONFIRM_DELETE': '¿Está seguro de querer borrar este conocimiento? No se podrá recuperar la información una vez eliminado.',
                    },
                }
            },
            'COMMON': {
                'TYPE_HERE_YOUR_ANSWER': 'Escriba aquí su respuesta',
                'TEST_STATUS': {
                    'PENDING': 'No iniciado',
                    'STARTED': 'Iniciado',
                    'FINISHED': 'Finalizado',
                    'ASSESSED': 'Finalizado',
                    'REQUESTED': 'Solicitado',
                    'CONFIRMED': 'Confirmado',
                    'EXECUTED': 'Ejecutado',
                },
            },
        }
    }
};

Object.assign(locale.data, locale.data, main_locale.data);
