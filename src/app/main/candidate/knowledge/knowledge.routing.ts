import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {KnowledgeStartComponent} from 'app/main/candidate/knowledge/start';
import {KnowledgeResultComponent, KnowledgeResultService} from 'app/main/candidate/knowledge/result';
import {KnowledgeQuestionnaireComponent, KnowledgeQuestionnaireService, KnowledgeQuestionnaireDialogComponent} from 'app/main/candidate/knowledge/questionnaire';
import {CandidateKnowledgeListComponent, CandidateKnowledgeListDialogComponent, CandidateKnowledgeListService} from 'app/main/candidate/knowledge/list';
import {CandidateKnowledgeDetailComponent, CandidateKnowledgeDetailService} from 'app/main/candidate/knowledge/detail';
import {KnowledgeHomeComponent, KnowledgeHomeDialogComponent, KnowledgeHomeService} from 'app/main/candidate/knowledge/home';
import {MaterialModule} from 'app/_helpers/material.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseSidebarModule, FuseWidgetModule} from '@fuse/components';
import {TranslateModule} from '@ngx-translate/core';
import {ChartsModule} from 'ng2-charts';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {DefroxModule} from 'app/_helpers';
import {AuthGuard, ExitTestGuard} from 'app/_guards';
import {Group} from 'app/_models';
import {CountdownModule} from 'ngx-countdown';

const appRoutes: Routes = [
    {path: 'candidate/knowledge', children: [
        {path: '', component: KnowledgeHomeComponent, resolve: {home: KnowledgeHomeService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'start', component: KnowledgeStartComponent, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'list', component: CandidateKnowledgeListComponent, resolve: {list: CandidateKnowledgeListService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: ':knowledgeId/:knowledgeSlug', component: CandidateKnowledgeDetailComponent, resolve: {knowledge: CandidateKnowledgeDetailService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'questionnaire/:knowledgeId/:testId/:knowledgeSlug', component: KnowledgeQuestionnaireComponent, resolve: {questionnaire: KnowledgeQuestionnaireService}, canDeactivate: [ExitTestGuard], canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'result/:knowledgeId/:testId/:knowledgeSlug', component: KnowledgeResultComponent, resolve: {result: KnowledgeResultService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        // otherwise redirect to 404
        {path: '**', redirectTo: '/error/404', pathMatch: 'full'}
    ]}
];

@NgModule({
    declarations: [
        KnowledgeStartComponent,
        KnowledgeQuestionnaireComponent,
        KnowledgeResultComponent,
        KnowledgeHomeComponent,
        KnowledgeHomeDialogComponent,
        CandidateKnowledgeListComponent,
        CandidateKnowledgeListDialogComponent,
        CandidateKnowledgeDetailComponent,
        KnowledgeQuestionnaireDialogComponent,
        ],
    imports: [
        RouterModule.forChild(appRoutes),
        MaterialModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseWidgetModule,
        TranslateModule,
        ChartsModule,
        CountdownModule,
        DefroxModule,
        NgxChartsModule
    ],
    providers: [
        KnowledgeQuestionnaireService,
        KnowledgeResultService,
        KnowledgeHomeService,
        CandidateKnowledgeListService,
        CandidateKnowledgeDetailService,
        AuthGuard,
        ExitTestGuard
    ],
    entryComponents: [CandidateKnowledgeListDialogComponent, KnowledgeHomeDialogComponent, KnowledgeQuestionnaireDialogComponent],
    exports: [RouterModule]
})
export class KnowledgeRouting { }
