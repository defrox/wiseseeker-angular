import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {AuthenticationService, KnowledgeApiService, TestExecutionApiService} from 'app/_services';
import {User} from 'app/_models';

@Injectable()
export class CandidateKnowledgeDetailService implements Resolve<any> {
    onKnowledgeChanged: BehaviorSubject<any>;
    currentUserSubscription: Subscription;
    currentUser: User;
    executions: any;
    knowledge: any;

    /**
     * Constructor
     *
     * @param {KnowledgeApiService} _knowledgeApiService
     * @param {TestExecutionApiService} _testExecutionApiService
     * @param {AuthenticationService} _authenticationService
     */
    constructor(
        private _knowledgeApiService: KnowledgeApiService,
        private _testExecutionApiService: TestExecutionApiService,
        private _authenticationService: AuthenticationService
    ) {
        // Set the defaults
        this.onKnowledgeChanged = new BehaviorSubject({});
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getKnowledge(route.params.knowledgeId)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get knowledge
     *
     * @returns {Promise<any>}
     */
    getKnowledge(id): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._knowledgeApiService.get(id)
                .subscribe((knowledge: any) => {
                    this.knowledge = knowledge;
                    this._testExecutionApiService.getByUser(+this.currentUser.id)
                        .subscribe((executions: any) => {
                            this.executions = executions;
                            for (const execution of executions) {
                                if (this._filterByTestId(this.knowledge.tests, execution.test.id)) {
                                    this.knowledge.execution = execution;
                                }
                            }
                        });
                    this.onKnowledgeChanged.next(this.knowledge);
                    resolve(this.knowledge);
                }, reject);
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private _filterByTestId(item: any, testId: number): any {
        return item.indexOf(testId) !== -1;
    }
}
