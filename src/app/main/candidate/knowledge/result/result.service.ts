import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {AuthenticationService, KnowledgeApiService, TestExecutionApiService} from 'app/_services';
import {Knowledge, User} from 'app/_models';

@Injectable()
export class KnowledgeResultService implements Resolve<any> {
    onQuestionnaireChanged: BehaviorSubject<any>;
    onResultChanged: BehaviorSubject<any>;
    onKnowledgeChanged: BehaviorSubject<any>;
    onTestExecutionChanged: BehaviorSubject<any>;
    onCurrentExecutionChanged: BehaviorSubject<any>;
    currentUserSubscription: Subscription;
    currentUser: User;
    knowledge: Knowledge;
    lastExecution: any;
    currentExecution: any;
    result: any;

    /**
     * Constructor
     *
     * @param {KnowledgeApiService} _knowledgeApiService
     * @param {TestExecutionApiService} _testExecutionApiService
     * @param {AuthenticationService} _authenticationService
     */
    constructor(
        private _knowledgeApiService: KnowledgeApiService,
        private _testExecutionApiService: TestExecutionApiService,
        private _authenticationService: AuthenticationService
    ) {
        // Set the defaults
        this.onResultChanged = new BehaviorSubject({});
        this.onTestExecutionChanged = new BehaviorSubject({});
        this.onCurrentExecutionChanged = new BehaviorSubject({});
        this.onKnowledgeChanged = new BehaviorSubject({});
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {
            Promise.all([
                this.getAllTestExecutions(route.params.testId, [2, 3]),
                this.getKnowledgeDetails(route.params.knowledgeId)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get result
     *
     * @param userId
     * @param testId
     * @returns {Promise<any>}
     */
    getResult(userId, testId): Observable<any> | Promise<any>
    {
/*        return new Promise((resolve, reject) => {
            this._httpClient.get<any>('api/knowledge/result/' + userId)
                .subscribe((response: any) => {
                    // this.result = response;
                    // this.onResultChanged.next(this.result);
                    resolve(this.onResultChanged);
                }, error => {
                    resolve(this.onResultChanged);
                });
        });*/
        return Promise.resolve(this.onResultChanged);
    }

    /**
     * Get all test executions
     *
     * @param testId
     * @param statusIds
     * @returns {Promise<any>}
     */
    getAllTestExecutions(testId: number, statusIds: number[]): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testExecutionApiService.getByUserAndTest(+this.currentUser.id, testId)
                .subscribe((executions: any) => {
                    // this.lastExecution = executions.filter(item => this._filterByTestIdAndStatus(item, testId, statusIds));
                    this.lastExecution = executions;
                    this.onTestExecutionChanged.next(this.lastExecution);
                    resolve(this.lastExecution);
                }, reject);
        });
    }

    /**
     * Get knowledge details
     *
     * @param knowledgeId
     * @returns {Observable<any>}
     */
    getKnowledgeDetails(knowledgeId: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._knowledgeApiService.get(knowledgeId)
                .subscribe((knowledge: any) => {
                    this.knowledge = knowledge;
                    this.onKnowledgeChanged.next(this.knowledge);
                    resolve(this.knowledge);
                }, reject);
        });
    }

    /**
     * Set Knowledge Results
     *
     * @param results
     */
    setKnowledgeResults(results): void {
        this.onResultChanged.next(results);
    }

    /**
     * Get Knowledge Results
     *
     * @returns {Observable<any>}
     */
    getKnowledgeResults(): Observable<any> {
        return this.onResultChanged.asObservable();
    }

    /**
     * Reset Knowledge Results
     */
    resetKnowledgeResults(): void {
        this.onResultChanged.next(null);
    }

    /**
     * Set Current Execution
     *
     * @param execution
     */
    setCurrentExecution(execution): void {
        this.onCurrentExecutionChanged.next(execution);
    }

    /**
     * Get Current Execution
     *
     * @returns {Observable<any>}
     */
    getCurrentExecution(): Observable<any> {
        return this.onCurrentExecutionChanged.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private handleError(error: any): any {
        return throwError(error);
    }


}
