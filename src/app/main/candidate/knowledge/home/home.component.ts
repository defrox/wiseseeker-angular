import {Component, OnInit, OnDestroy, ViewEncapsulation, Inject} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {TranslatePipe} from '@ngx-translate/core';
import {KnowledgeHomeService} from './home.service';
import {LoaderOverlayService, SnackAlertService} from 'app/_services';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {DialogData} from 'app/_models';
import {FuseProgressBarService} from '@fuse/components/progress-bar/progress-bar.service';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';
import {Router} from '@angular/router';

@Component({
    selector: 'candidate-knowledge-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class KnowledgeHomeComponent implements OnInit, OnDestroy {
    userKnowledge: any[];
    categories: any[] = [];
    types = [{title: 'MAIN.KNOWLEDGE.TYPES.INTERNAL', type: 'int'}, {title: 'MAIN.KNOWLEDGE.TYPES.EXTERNAL', type: 'ext'}];
    knowledgeFilteredByCategory: any[];
    filteredKnowledge: any[];
    currentCategory: string;
    currentType: string;
    searchTerm: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {MatDialog} _dialog
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {FuseProgressBarService} _fuseProgressBar
     * @param {SnackAlertService} _snackAlertService
     * @param {Router} _router
     * @param {KnowledgeHomeService} _knowledgeHomeService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslatePipe} _translatePipe
     */
    constructor(
        private _dialog: MatDialog,
        private _loaderOverlay: LoaderOverlayService,
        private _fuseProgressBar: FuseProgressBarService,
        private _snackAlertService: SnackAlertService,
        private _router: Router,
        private _knowledgeHomeService: KnowledgeHomeService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translatePipe: TranslatePipe,
    ) {
        // Set the defaults
        this.currentCategory = this.currentType = 'all';
        this.searchTerm = '';

        // Set the private defaults
        this._translationLoader.loadTranslations(spanish, english);
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to categories
        this._knowledgeHomeService.onKnowledgeCategoriesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(categories => {
                this.categories = categories;
            });

        // Subscribe to user knowledge
        this._knowledgeHomeService.onKnowledgeChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(knowledge => {
                this.filteredKnowledge = this.knowledgeFilteredByCategory = this.userKnowledge = knowledge;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Filter knowledge by category
     */
    filterKnowledgeByCategory(): void {
        // Filter
        if (this.currentCategory === 'all') {
            this.knowledgeFilteredByCategory = this.userKnowledge;
            this.filteredKnowledge = this.userKnowledge;
        } else {
            this.knowledgeFilteredByCategory = this.userKnowledge.filter((knowledge) => {
                return knowledge.category === this.currentCategory;
            });
            this.filteredKnowledge = [...this.knowledgeFilteredByCategory];
        }
        if (this.currentType === 'all') {
            this.filteredKnowledge = this.knowledgeFilteredByCategory;
        } else {
            this.knowledgeFilteredByCategory = this.knowledgeFilteredByCategory.filter((knowledge) => {
                const isExternal = knowledge.tests && knowledge.tests.length > 0 && knowledge.tests[0].hasOwnProperty('external');
                return (knowledge.external && this.currentType === 'ext') || (!knowledge.external && this.currentType === 'int');
            });
            this.filteredKnowledge = [...this.knowledgeFilteredByCategory];
        }

        // Re-filter by search term
        this.filterKnowledgeByTerm();
    }

    /**
     * Filter knowledge by term
     */
    filterKnowledgeByTerm(): void {
        const searchTerm = this.searchTerm.toLowerCase();

        // Search
        if (searchTerm === '') {
            this.filteredKnowledge = this.knowledgeFilteredByCategory;
        } else {
            this.filteredKnowledge = this.knowledgeFilteredByCategory.filter((knowledge) => {
                return knowledge.title.toLowerCase().includes(searchTerm);
                // return (knowledge.title.toLowerCase().includes(searchTerm) || knowledge.description.toLowerCase().includes(searchTerm));
            });
        }
    }

    openExternalTest(testId: number): void {
        const execution = {'test_id': testId};
        const dialogRef = this._dialog.open(KnowledgeHomeDialogComponent, {
            data: {externalOpened: true}
        });
        this._knowledgeHomeService.newTestExecution(execution).then(res => {
            if (res.external && res.external.url && res.external.url !== '') {
                window.open(res.external.url, '_blank');
                dialogRef.afterClosed().subscribe(() => {
                    this._router.navigate(['/candidate/dashboard']).then();
                    // this._loaderOverlay.show();
                    // this._knowledgeHomeService.getAllKnowledgeExecutions().then(() => this._loaderOverlay.hide());
                });
            } else {
                dialogRef.close();
                this._snackAlertService.error(this._translatePipe.transform('MAIN.ERRORS.UNDEFINED_ERROR'));
            }
        }, err => {
            dialogRef.close();
            this._snackAlertService.error(this._translatePipe.transform('MAIN.ERRORS.UNDEFINED_ERROR'));
        });
    }

    openExternal(url: string): void {
        window.open(url, '_blank');
    }

    openSnackInfo(event: any): void {
        const message = event.target.closest('.mat-button').getAttribute('data-message');
        this._snackAlertService.info(message);
        this._fuseProgressBar.show();
        this._knowledgeHomeService.getAllKnowledgeExecutions().then(() => this._fuseProgressBar.hide());
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------
}


@Component({
    selector: 'candidate-knowledge-home-dialog',
    templateUrl: './home.dialog.component.html',
    styleUrls: ['./home.component.scss'],
})
export class KnowledgeHomeDialogComponent {
    constructor(
        private _dialogRef: MatDialogRef<KnowledgeHomeDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
        _dialogRef.disableClose = true;
    }
}
