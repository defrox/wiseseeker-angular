import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {AuthenticationService, KnowledgeApiService, KnowledgeCategoryApiService, TestApiService, TestExecutionApiService} from 'app/_services';
import {Knowledge, TestExecution, User} from 'app/_models';
import {injectTemplateRef} from '@angular/core/src/render3/view_engine_compatibility';

@Injectable()
export class KnowledgeHomeService implements Resolve<any> {
    onKnowledgeChanged: BehaviorSubject<any>;
    onKnowledgeCategoriesChanged: BehaviorSubject<any>;
    executions: any;
    knowledge: any;
    knowledgeExecutions: Knowledge[];
    tests: any;
    currentUserSubscription: Subscription;
    currentUser: User;

    /**
     * Constructor
     *
     * @param {KnowledgeApiService} _knowledgeApiService
     * @param {KnowledgeCategoryApiService} _knowledgeCategoryApiService
     * @param {TestApiService} _testApiService
     * @param {TestExecutionApiService} _testExecutionApiService
     * @param {AuthenticationService} _authenticationService
     */
    constructor(
        private _knowledgeApiService: KnowledgeApiService,
        private _knowledgeCategoryApiService: KnowledgeCategoryApiService,
        private _testApiService: TestApiService,
        private _testExecutionApiService: TestExecutionApiService,
        private _authenticationService: AuthenticationService
    ) {
        // Set the defaults
        this.onKnowledgeChanged = new BehaviorSubject({});
        this.onKnowledgeCategoriesChanged = new BehaviorSubject({});
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {
            Promise.all([
                this.getAllKnowledgeExecutions(),
                this.getAllKnowledgeCategories()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get all knowledge executions
     *
     * @returns {Promise<any>}
     */
    getAllKnowledgeExecutions(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testExecutionApiService.getByUser(+this.currentUser.id)
                .subscribe((executions: any) => {
                    // console.log(executions);
                    this.executions = executions;
                    this.getAllKnowledge(+this.currentUser.id).then(
                        knowledge => {
                            this.knowledge = knowledge;
                            this.knowledgeExecutions = [];
                            for (const execution of executions) {
                                const tmpKnowledge = this.knowledge.filter(item => this._filterByTestId(item.tests, execution.test.id));
                                if (tmpKnowledge.length > 0) {
                                    tmpKnowledge[0].execution = execution;
                                    this.knowledgeExecutions = [...this.knowledgeExecutions, ...tmpKnowledge[0]];
                                }
                            }
                            // console.log(this.knowledgeExecutions);
                            // this.testExecution = executions.filter(item => this._filterByTestId(item, testId));
                            this.onKnowledgeChanged.next(this.knowledgeExecutions);
                            resolve(this.knowledgeExecutions);
                        });
                }, reject);
        });
    }

    /**
     * Get all knowledge categories
     *
     * @returns {Promise<any>}
     */
    getAllKnowledgeCategories(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._knowledgeCategoryApiService.getAll()
                .subscribe((categories: any) => {
                    this.onKnowledgeCategoriesChanged.next(categories);
                    resolve(categories);
                }, reject);
        });
    }

    /**
     * Get all knowledge
     *
     * @param userId
     * @returns {Promise<any>}
     */
    getAllKnowledge(userId: number): Promise<any>
    {
        // return this._knowledgeApiService.getAll().toPromise().then(res => this._randomExternal(res));
        return this._knowledgeApiService.getAll().toPromise().then(res => res);
    }

    /**
     * Get knowledge
     *
     * @param userId
     * @returns {Promise<any>}
     */
    getAllTests(userId: number): Promise<any>
    {
        return this._testApiService.getAll().toPromise().then(res => res);
    }

    /**
     * Set Knowledge
     *
     * @param homes
     */
    setKnowledge(homes): void {
        this.onKnowledgeChanged.next(homes);
    }

    /**
     * Get Knowledge
     *
     * @returns {Observable<any>}
     */
    getKnowledge(): Observable<any> {
        return this.onKnowledgeChanged.asObservable();
    }

    /**
     * Reset Knowledge
     */
    resetKnowledge(): void {
        this.onKnowledgeChanged.next(null);
    }

    /**
     * Get all knowledge categories
     *
     * @returns {Promise<any>}
     */
    getKnowledgeCategories(): Promise<any>
    {
        return this._knowledgeCategoryApiService.getAll().toPromise().then(res => res);
    }

    /**
     * Get knowledge categori bi id
     *
     * @returns {Promise<any>}
     */
    getKnowledgeCategory(categoryId: number): Promise<any>
    {
        return this._knowledgeCategoryApiService.get(categoryId).toPromise().then(res => res);
    }

    /**
     * New test execution
     *
     * @param execution
     */
    newTestExecution(execution: TestExecution): any
    {
        return this._testExecutionApiService.add(execution).toPromise().then(res => res);
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private _filterByTestId(item: any, testId: number): any {
        return item.filter(obj => obj.id === testId).length > 0;
        // return item.indexOf(testId) !== -1;
    }

    private _getFirst(array, n = 1): any | null {
        return n > 0 && array.length > n ? array.slice(0, n) : array;
    }

    private handleError(error: any): any {
        return throwError(error);
    }

    /**
     * Randomize external
     *
     * @param knowledge
     * @returns {Promise<any>}
     */
    private _randomExternal(knowledge: any): any
    {
        return knowledge.map(item => {
            const tests = [];
            let newTest = {external: null};
            if (item.tests && item.tests.length > 0) {
                item.tests[0].external = null;
                newTest = item.tests[0];
            }
            tests.push(newTest);
            console.log(tests);
            if (Math.random() >= 0.5) {
                tests[0].external = {
                    image: 'assets/images/logos/devskiller_logo_white.png',
                    url: 'https://devskiller.com/coding-tests/',
                    status: Math.random() >= 0.5
                };
            }
            item.tests = tests;
            return item;
        });
    }
}
