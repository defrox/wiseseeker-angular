import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {AuthenticationService, KnowledgeApiService, KnowledgeCategoryApiService, TestExecutionApiService} from 'app/_services';
import {Knowledge, User} from 'app/_models';

@Injectable()
export class CandidateKnowledgeListService implements Resolve<any> {
    onAllKnowledgeChanged: BehaviorSubject<any>;
    onKnowledgeCategoriesChanged: BehaviorSubject<any>;
    currentUserSubscription: Subscription;
    currentUser: User;
    knowledgeExecutions: Knowledge[];
    executions: any;
    knowledge: any;


    /**
     * Constructor
     *
     * @param {KnowledgeApiService} _knowledgeApiService
     * @param {KnowledgeCategoryApiService} _knowledgeCategoryApiService
     * @param {TestExecutionApiService} _testExecutionApiService
     * @param {AuthenticationService} _authenticationService
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _knowledgeApiService: KnowledgeApiService,
        private _knowledgeCategoryApiService: KnowledgeCategoryApiService,
        private _testExecutionApiService: TestExecutionApiService,
        private _authenticationService: AuthenticationService,
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onAllKnowledgeChanged = new BehaviorSubject({});
        this.onKnowledgeCategoriesChanged = new BehaviorSubject({});
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getAllKnowledge(),
                this.getAllKnowledgeCategories()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get all knowledge
     *
     * @returns {Promise<any>}
     */
    getAllKnowledge(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._knowledgeApiService.getAll()
                .subscribe((knowledge: any) => {
                    this.knowledge = knowledge;
                    // this.knowledge = this._randomExternal(knowledge);
                    this._testExecutionApiService.getByUser(+this.currentUser.id)
                        .subscribe((executions: any) => {
                            this.executions = executions;
                            this.knowledgeExecutions = [];
                            for (const execution of executions) {
                                const tmpKnowledge = this.knowledge.filter(item => this._filterByTestId(item.tests, execution.test.id));
                                if (tmpKnowledge.length > 0) {
                                    tmpKnowledge[0].execution = execution;
                                    this.knowledgeExecutions = [...this.knowledgeExecutions, ...tmpKnowledge[0]];
                                }
                            }
                        });
                    this.onAllKnowledgeChanged.next(this.knowledge);
                    resolve(this.knowledge);
                }, reject);
        });
    }

    /**
     * Get all knowledge categories
     *
     * @returns {Promise<any>}
     */
    getAllKnowledgeCategories(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._knowledgeCategoryApiService.getAll()
                .subscribe((categories: any) => {
                    this.onKnowledgeCategoriesChanged.next(categories);
                    resolve(categories);
                }, reject);
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private _filterByTestId(item: any, testId: number): any {
        return item.filter(obj => obj.id === testId).length > 0;
        // return item.indexOf(testId) !== -1;
    }

    /**
     * Randomize external
     *
     * @param knowledge
     * @returns {Promise<any>}
     */
    _randomExternal(knowledge: any): any
    {
        return knowledge.map(item => {
            const tests = [];
            let newTest = {external: null};
            if (item.tests && item.tests.length > 0) {
                item.tests[0].external = null;
                newTest = item.tests[0];
            }
            tests.push(newTest);
            console.log(tests);
            if (Math.random() >= 0.5) {
                tests[0].external = {
                    image: 'assets/images/logos/devskiller_logo_white.png',
                    url: 'https://devskiller.com/coding-tests/',
                    status: Math.random() >= 0.5
                };
            }
            item.tests = tests;
            return item;
        });
    }
}
