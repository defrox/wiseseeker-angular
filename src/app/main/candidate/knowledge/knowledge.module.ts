import {NgModule} from '@angular/core';
import {KnowledgeRouting} from 'app/main/candidate/knowledge/knowledge.routing';
import {ApiModule, DefroxModule} from 'app/_helpers';

@NgModule({
    declarations: [],
    imports: [
        ApiModule,
        DefroxModule,
        KnowledgeRouting
    ]
})
export class KnowledgeModule {
}
