import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {AuthenticationService, DashboardApiService, EvaluationApiService, ExperienceApiService, JobApiService} from 'app/_services';
import {Dashboard, User} from 'app/_models';

@Injectable()
export class CandidateDashboardService implements Resolve<any> {
    onDashboardChanged: BehaviorSubject<any>;
    onPersonalityChanged: BehaviorSubject<any>;
    currentUserSubscription: Subscription;
    currentUser: User;
    dashboard: Dashboard;
    widgets: any[];

    /**
     * Constructor
     *
     * @param {DashboardApiService} _dashboardApiService
     * @param {EvaluationApiService} _evaluationApiService
     * @param {ExperienceApiService} _experienceApiService
     * @param {JobApiService} _jobApiService
     * @param {AuthenticationService} _authenticationService
     */
    constructor(
        private _dashboardApiService: DashboardApiService,
        private _evaluationApiService: EvaluationApiService,
        private _experienceApiService: ExperienceApiService,
        private _jobApiService: JobApiService,
        private _authenticationService: AuthenticationService
) {
        // Set the defaults
        this.onDashboardChanged = new BehaviorSubject({});
        this.onPersonalityChanged = new BehaviorSubject({});
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getNewDashboard(),
                this.getDashboard(),
                this.getEvaluations()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get dashboard
     *
     * @returns {Promise<any>}
     */
    getDashboard(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._dashboardApiService.getNewDash()
                .subscribe((response: any) => {
                    this.dashboard = response[0];
                    /*this._experienceApiService.getAllByUser().toPromise().then(
                        res => {
                            let years = 0;
                            if (res && res.length > 0) {
                                for (const company of res) {
                                    const from = new Date(company.working_from);
                                    const to = new Date(company.working_to);
                                    years += Math.abs((to.getTime() - from.getTime()) / (1000 * 60 * 60 * 24 * 365));
                                }
                            }
                            this.dashboard.experience = {companies: res && res.length > 0 ? res.length : 0, years: Math.round(years) };
                            this.onDashboardChanged.next(this.dashboard);
                            resolve(this.dashboard);
                        }
                    );*/
                    /*this._jobApiService.getAll().toPromise().then(
                        res => {
                            this.dashboard.jobs = res;
                            this.onDashboardChanged.next(this.dashboard);
                            resolve(this.dashboard);
                        }
                    );*/
                    resolve(this.dashboard);
                    this.onDashboardChanged.next(this.dashboard);
                }, reject);
        });
    }

    /**
     * Get new dashboard
     *
     * @returns {Promise<any>}
     */
    getNewDashboard(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._dashboardApiService.get()
                .subscribe((response: any) => {
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Get evaluations
     *
     * @returns {Promise<any>}
     */
    getEvaluations(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._evaluationApiService.getAll()
                .subscribe((response: any) => {
                    this.widgets = response;
                    resolve(response);
                }, reject);
        });
    }

}
