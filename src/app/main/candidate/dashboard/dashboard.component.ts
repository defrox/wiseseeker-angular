﻿import {Component, OnInit, OnDestroy, ViewEncapsulation, ViewChildren, QueryList} from '@angular/core';
import {Router} from '@angular/router';
import {Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {Chart} from 'angular-highcharts';
import {Group, User} from 'app/_models';
import {AppConfigService, AuthenticationService, NotificationService, SnackAlertService} from 'app/_services';
import {CandidateDashboardService} from './dashboard.service';
import {TranslatePipe, TranslateService} from '@ngx-translate/core';
import {appConfig} from 'app/app.config';
import {formatValueCenteredGauge} from 'app/_helpers/formatters';
import * as _ from 'lodash';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'candidate-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class CandidateDashboardComponent implements OnInit, OnDestroy {
    appConfig = appConfig;
    groupCandidate: Group = Group.Candidate;
    groupExternal: Group = Group.External;
    dashboard: any;
    personality: any;
    skills: any;
    knowledge: any;
    experience: any;
    role: any;
    jobs: any;
    currentUser: User;
    currentUserSubscription: Subscription;
    notifications: any;
    notificationsBadges: any;
    notificationsSubscription: Subscription;
    widgets = {};

    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _authenticationService: AuthenticationService,
        private _candidateDashboardService: CandidateDashboardService,
        private _snackAlertService: SnackAlertService,
        private _notificationsService: NotificationService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translateService: TranslateService,
        private _translatePipe: TranslatePipe,
        private _appConfigService: AppConfigService,
        private _router: Router
    ) {
        // Set the private defaults
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
        this._appConfigService.config.subscribe(config => { this.appConfig = config; });
        this._translationLoader.loadTranslations(spanish, english);
        this._notificationsService.resetUserNotifications();
        this.notificationsSubscription = this._notificationsService.onNotificationChanged.subscribe(notifications => {
            this.notifications = notifications;
            this.notificationsBadges = this._notificationsService.getCandidateNotificationsBadges(notifications);
            if (this._router.url.startsWith('/candidate/dashboard')) {
                this._alertNotifications(this.notificationsBadges);
            }
        });
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.personality = null;
        // Subscribe to Dashboard
        this._candidateDashboardService.onDashboardChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(dashboard => {
                // console.log(dashboard);
                this.dashboard = dashboard;
                this.personality = this.dashboard.big5 && this.dashboard.big5.assessed ? this.dashboard.big5.assessed : null;
                this.widgets['personality'] = this._formatPersonality(this.personality);
                // this.skills = this.dashboard.ipv && this.dashboard.ipv.assessed && this.dashboard.ipv.assessed.length > 0 ? this.dashboard.ipv.assessed : null;
                this.skills = this.dashboard.ipv && this.dashboard.ipv.length > 0 ? this.dashboard.ipv : null;
                // this.knowledge = this.dashboard.knowledge && this.dashboard.knowledge.assessed && this.dashboard.knowledge.assessed.length > 0 ? this.dashboard.knowledge.assessed : null;
                this.knowledge = this.dashboard.knowledge && this.dashboard.knowledge.length > 0 ? this.dashboard.knowledge : null;
                this.experience = this.dashboard.experience ? this.dashboard.experience : null;
                this.jobs = this.dashboard.job && this.dashboard.job.length > 0 ? this.dashboard.job : null;
                if (this.dashboard.role && this.dashboard.role.assessed && this.dashboard.role.assessed.length > 0) {
                    this.role = this.dashboard.role;
                    this.widgets['role'] = this._formatRole(this.role.assessed, this.role.test.title);
                }
            });

        // Subscribe to Notifications
        this._notificationsService.onNotificationChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(notifications => {
                this.notifications = notifications;
                this.notificationsBadges = this._notificationsService.getCandidateNotificationsBadges(notifications);
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
        this.personality = null;
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    _formatPersonality(data): any {
        const widgets = {};
        if (data) {
            for (const item of Object.keys(data)) {
                const option = _.cloneDeep(this.appConfig.solidGaugeOptionsCenter);
                const colors = this.appConfig.solidGaugeOptionsCenterColors;
                option.title.text = this._translateService.instant('MAIN.TRAITS.' + data[item].skill.description.toUpperCase() + '.TITLE').toUpperCase();
                option.yAxis.max = data[item].maximum;
                option.yAxis.min = data[item].minimum ? data[item].minimum : 0;
                option.yAxis.stops = [
                    [0, colors[item].min],
                    [0.5, colors[item].min],
                    [0.5, colors[item].max],
                    [1, colors[item].max]
                ];
                option.plotOptions.solidgauge.threshold = (option.yAxis.max - option.yAxis.min) / 2 + option.yAxis.min;
                // option.plotOptions.solidgauge.formatter = function (): number { return (this.y - option.plotOptions.solidgauge.threshold) * 2; };
                option.series = [{data: [data[item].punctuation]}];
                for (const band of Object.keys(option.yAxis.plotBands)) {
                    if (+band === 0) {
                        option.yAxis.plotBands[band].from = option.yAxis.min;
                        option.yAxis.plotBands[band].to = option.plotOptions.solidgauge.threshold;
                        option.yAxis.plotBands[band].color = colors[item].min;
                    }
                    if (+band === 1) {
                        option.yAxis.plotBands[band].from = option.plotOptions.solidgauge.threshold;
                        option.yAxis.plotBands[band].to = option.yAxis.max;
                        option.yAxis.plotBands[band].color = colors[item].max;
                    }
                }
                widgets[item] = new Chart(option as any);
            }
        }
        return widgets;
    }

    _formatRole(data, title): any {
        let widgets: any;
        if (data  && title) {
            for (const item of Object.keys(data)) {
                let option = _.cloneDeep(this.appConfig.solidGaugeOptions);
                option.title.text = title.replace('Test Role', '');
                option.yAxis.max = data[item].maximum;
                option.series = [{data: [data[item].punctuation]}];
                for (const band of Object.keys(option.yAxis.plotBands)) {
                    if (+band === 0) {
                        option.yAxis.plotBands[band].label.text = 0;
                    }
                    if (+band === 1) {
                        option.yAxis.plotBands[band].label.text = data[item].maximum;
                    }
                    if (+band === 2) {
                        option.yAxis.plotBands[band].to = data[item].maximum * 0.33;
                    }
                    if (+band === 3) {
                        option.yAxis.plotBands[band].from = data[item].maximum * 0.33;
                        option.yAxis.plotBands[band].to = data[item].maximum * 0.66;
                    }
                    if (+band === 4) {
                        option.yAxis.plotBands[band].from = data[item].maximum * 0.66;
                    }
                }
                widgets = new Chart(option as any);
            }
        }
        return widgets;
    }

    private _alertNotifications(notifications: any): void {
        if (notifications.dashboard_todo) {
            this._snackAlertService.primary(this._translatePipe.transform('CANDIDATE.DASHBOARD.PENDING_TASKS_MSG').replace('{tasks}', notifications.dashboard_todo));
        }
    }
}
