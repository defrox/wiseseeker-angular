import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {TodoComponent} from './todo.component';
import {TodoService} from './todo.service';
import {TodoMainSidebarComponent} from 'app/main/candidate/todo/sidebars/main/main-sidebar.component';
import {TodoListItemComponent} from 'app/main/candidate/todo/todo-list/todo-list-item/todo-list-item.component';
import {TodoListComponent} from 'app/main/candidate/todo/todo-list/todo-list.component';
import {TodoDetailsComponent} from 'app/main/candidate/todo/todo-details/todo-details.component';
import {MaterialModule} from 'app/_helpers/material.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseSidebarModule, FuseWidgetModule} from '@fuse/components';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {TranslateModule} from '@ngx-translate/core';
import {ChartsModule} from 'ng2-charts';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {NgxDnDModule} from '@swimlane/ngx-dnd';
import {FusionChartsModule} from 'angular-fusioncharts';
import {AuthGuard} from 'app/_guards';
import {Group} from 'app/_models';
import {DefroxModule} from 'app/_helpers';

const appRoutes: Routes = [
    {path: 'candidate/todo', pathMatch: 'prefix', children: [
        {path: 'all', component: TodoComponent, resolve: {todo: TodoService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate]}},
        {path: 'all/:todoId', component: TodoComponent, resolve: {todo: TodoService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate]}},
        {path: 'tag/:tagHandle', component: TodoComponent, resolve: {todo: TodoService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate]}},
        {path: 'tag/:tagHandle/:todoId', component: TodoComponent, resolve: {todo: TodoService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate]}},
        {path: 'filter/:filterHandle', component: TodoComponent, resolve: {todo: TodoService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate]}},
        {path: 'filter/:filterHandle/:todoId', component: TodoComponent, resolve: {todo: TodoService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate]}},
        // otherwise redirect to 404
        {path: '**', pathMatch: 'full', redirectTo: '/error/404'},
    ]}
];

@NgModule({
    declarations: [
        TodoComponent,
        TodoMainSidebarComponent,
        TodoListItemComponent,
        TodoListComponent,
        TodoDetailsComponent
    ],
    imports: [
        RouterModule.forChild(appRoutes),
        MaterialModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseWidgetModule,
        TranslateModule,
        ChartsModule,
        NgxChartsModule,
        FontAwesomeModule,
        FusionChartsModule,
        DefroxModule,
        NgxDnDModule
    ],
    providers: [
        TodoService,
        AuthGuard
    ],
    exports: [RouterModule]
})
export class TodoRouting {
}
