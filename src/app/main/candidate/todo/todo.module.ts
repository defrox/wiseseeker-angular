import {NgModule} from '@angular/core';
import {TodoRouting} from 'app/main/candidate/todo/todo.routing';

@NgModule({
    declarations: [],
    imports: [
        TodoRouting
    ]
})
export class TodoModule {
}
