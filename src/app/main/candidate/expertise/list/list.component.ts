import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {CandidateExpertiseListService} from './list.service';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'candidate-expertise-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss'],
    animations: fuseAnimations
})
export class CandidateExpertiseListComponent implements OnInit, OnDestroy {
    filteredExpertise: any[];
    expertiseFilteredByCategory: any[];
    allExpertise: any[];
    categories: any[] = [];
    currentCategory: string;
    searchTerm: string;
    
    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {CandidateExpertiseListService} _candidateExpertiseListService
     */
    constructor(
        private _translationLoader: FuseTranslationLoaderService,
        private _candidateExpertiseListService: CandidateExpertiseListService
    ) {
        // Set the defaults
        this.currentCategory = 'all';
        this.searchTerm = '';

        // Set the private defaults
        this._translationLoader.loadTranslations(spanish, english);
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to categories
        this._candidateExpertiseListService.onExpertiseCategoriesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(categories => {
                this.categories = categories;
            });

        // Subscribe to all expertise
        this._candidateExpertiseListService.onAllExpertiseChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(expertise => {
                this.filteredExpertise = this.expertiseFilteredByCategory = this.allExpertise = expertise;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Filter expertise by category
     */
    filterExpertiseByCategory(): void {
        // Filter
        if (this.currentCategory === 'all') {
            this.expertiseFilteredByCategory = this.allExpertise;
            this.filteredExpertise = this.allExpertise;
        } else {
            this.expertiseFilteredByCategory = this.allExpertise.filter((expertise) => {
                return expertise.subtype === this.currentCategory;
            });
            this.filteredExpertise = [...this.expertiseFilteredByCategory];
        }
        // Re-filter by search term
        this.filterExpertiseByTerm();
    }

    /**
     * Filter expertise by term
     */
    filterExpertiseByTerm(): void {
        const searchTerm = this.searchTerm.toLowerCase();
        // Search
        if (searchTerm === '') {
            this.filteredExpertise = this.expertiseFilteredByCategory;
        } else {
            this.filteredExpertise = this.expertiseFilteredByCategory.filter((expertise) => {
                return expertise.title.toLowerCase().includes(searchTerm);
                // return (expertise.title.toLowerCase().includes(searchTerm) || expertise.description.toLowerCase().includes(searchTerm));
            });
        }
    }

}
