import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {AuthenticationService, TestApiService, TestExecutionApiService} from 'app/_services';
import {TestSubTypeExt, TestExecution, User} from 'app/_models';

@Injectable()
export class CandidateExpertiseListService implements Resolve<any> {
    onAllExpertiseChanged: BehaviorSubject<any>;
    onExpertiseCategoriesChanged: BehaviorSubject<any>;
    currentUserSubscription: Subscription;
    currentUser: User;
    expertiseExecutions: TestExecution[];
    executions: any;
    expertise: any;


    /**
     * Constructor
     *
     * @param {TestApiService} _testApiService
     * @param {TestExecutionApiService} _testExecutionApiService
     * @param {AuthenticationService} _authenticationService
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _testApiService: TestApiService,
        private _testExecutionApiService: TestExecutionApiService,
        private _authenticationService: AuthenticationService,
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onAllExpertiseChanged = new BehaviorSubject({});
        this.onExpertiseCategoriesChanged = new BehaviorSubject({});
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getAllExpertise(),
                this.getAllExpertiseCategories()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get all expertise
     *
     * @returns {Promise<any>}
     */
    getAllExpertise(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testApiService.getFiltered({type: 3})
                .subscribe((expertise: any) => {
                    this.expertise = expertise;
                    this._testExecutionApiService.getByUser(+this.currentUser.id)
                        .subscribe((executions: any) => {
                            this.executions = executions;
                            this.expertiseExecutions = [];
                            for (const execution of executions) {
                                const tmpExpertise = this.expertise.filter(item => item.id === execution.test.id);
                                if (tmpExpertise.length > 0) {
                                    tmpExpertise[0].execution = execution;
                                    this.expertiseExecutions = [...this.expertiseExecutions, ...tmpExpertise[0]];
                                }
                            }
                        });
                    this.onAllExpertiseChanged.next(this.expertise);
                    resolve(this.expertise);
                }, reject);
        });
    }

    /**
     * Get all expertise categories
     *
     * @returns {Promise<any>}
     */
    getAllExpertiseCategories(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            const categories = TestSubTypeExt;
            this.onExpertiseCategoriesChanged.next(categories);
            resolve(categories);
            /*this._expertiseCategoryApiService.getAll()
                .subscribe((categories: any) => {
                    this.onExpertiseCategoriesChanged.next(categories);
                    resolve(categories);
                }, reject);*/
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private _filterByTestId(item: any, testId: number): any {
        return item.indexOf(testId) !== -1;
    }
}
