import {NgModule} from '@angular/core';
import {ExpertiseRouting} from 'app/main/candidate/expertise/expertise.routing';
import {ApiModule, DefroxModule} from 'app/_helpers';

@NgModule({
    declarations: [],
    imports: [
        ApiModule,
        DefroxModule,
        ExpertiseRouting
    ]
})
export class ExpertiseModule {
}
