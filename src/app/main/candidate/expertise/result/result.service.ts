import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {AuthenticationService, TestApiService, TestExecutionApiService} from 'app/_services';
import {Expertise, User} from 'app/_models';

@Injectable()
export class ExpertiseResultService implements Resolve<any> {
    onQuestionnaireChanged: BehaviorSubject<any>;
    onResultChanged: BehaviorSubject<any>;
    onExpertiseChanged: BehaviorSubject<any>;
    onTestExecutionChanged: BehaviorSubject<any>;
    onCurrentExecutionChanged: BehaviorSubject<any>;
    currentUserSubscription: Subscription;
    currentUser: User;
    expertise: Expertise;
    lastExecution: any;
    currentExecution: any;
    result: any;

    /**
     * Constructor
     *
     * @param {TestApiService} _testApiService
     * @param {TestExecutionApiService} _testExecutionApiService
     * @param {AuthenticationService} _authenticationService
     */
    constructor(
        private _testApiService: TestApiService,
        private _testExecutionApiService: TestExecutionApiService,
        private _authenticationService: AuthenticationService
    ) {
        // Set the defaults
        this.onResultChanged = new BehaviorSubject({});
        this.onTestExecutionChanged = new BehaviorSubject({});
        this.onCurrentExecutionChanged = new BehaviorSubject({});
        this.onExpertiseChanged = new BehaviorSubject({});
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {
            Promise.all([
                this.getAllTestExecutions(route.params.expertiseId, [2, 3]),
                this.getExpertiseDetails(route.params.expertiseId)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get result
     *
     * @param userId
     * @param testId
     * @returns {Promise<any>}
     */
    getResult(userId, testId): Observable<any> | Promise<any>
    {
/*        return new Promise((resolve, reject) => {
            this._httpClient.get<any>('api/expertise/result/' + userId)
                .subscribe((response: any) => {
                    // this.result = response;
                    // this.onResultChanged.next(this.result);
                    resolve(this.onResultChanged);
                }, error => {
                    resolve(this.onResultChanged);
                });
        });*/
        return Promise.resolve(this.onResultChanged);
    }

    /**
     * Get all test executions
     *
     * @param testId
     * @param statusIds
     * @returns {Promise<any>}
     */
    getAllTestExecutions(testId: number, statusIds: number[]): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testExecutionApiService.getByUserAndTest(+this.currentUser.id, testId)
                .subscribe((executions: any) => {
                    // this.lastExecution = executions.filter(item => this._filterByTestIdAndStatus(item, testId, statusIds));
                    this.lastExecution = executions;
                    this.onTestExecutionChanged.next(this.lastExecution);
                    resolve(this.lastExecution);
                }, reject);
        });
    }

    /**
     * Get expertise details
     *
     * @param expertiseId
     * @returns {Observable<any>}
     */
    getExpertiseDetails(expertiseId: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testApiService.get(expertiseId)
                .subscribe((expertise: any) => {
                    this.expertise = expertise;
                    this.onExpertiseChanged.next(this.expertise);
                    resolve(this.expertise);
                }, reject);
        });
    }

    /**
     * Set Expertise Results
     *
     * @param results
     */
    setExpertiseResults(results): void {
        this.onResultChanged.next(results);
    }

    /**
     * Get Expertise Results
     *
     * @returns {Observable<any>}
     */
    getExpertiseResults(): Observable<any> {
        return this.onResultChanged.asObservable();
    }

    /**
     * Reset Expertise Results
     */
    resetExpertiseResults(): void {
        this.onResultChanged.next(null);
    }

    /**
     * Set Current Execution
     *
     * @param execution
     */
    setCurrentExecution(execution): void {
        this.onCurrentExecutionChanged.next(execution);
    }

    /**
     * Get Current Execution
     *
     * @returns {Observable<any>}
     */
    getCurrentExecution(): Observable<any> {
        return this.onCurrentExecutionChanged.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private handleError(error: any): any {
        return throwError(error);
    }


}
