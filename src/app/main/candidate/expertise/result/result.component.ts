import {Component, OnInit, OnDestroy, ViewEncapsulation} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {Expertise, TestExecution, User} from 'app/_models';
import {AppConfigService, AuthenticationService, NotificationService} from 'app/_services';
import {ExpertiseResultService} from './result.service';
import * as _ from 'lodash';
import {Chart} from 'angular-highcharts';
import {TranslateService} from '@ngx-translate/core';
import {centeredGaugeFormatter, radarTooltipFormatter} from 'app/_helpers/formatters';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'expertise-result',
    templateUrl: './result.component.html',
    styleUrls: ['./result.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ExpertiseResultComponent implements OnInit, OnDestroy {
    appConfig: any;
    currentUser: User;
    currentUserSubscription: Subscription;
    expertise: Expertise;
    result: any;
    execution: any;
    currentExecution: any;
    assessment: any;
    widgets: any;
    spiderWeb: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {AppConfigService} _appConfigService
     * @param {ExpertiseResultService} _expertiseResultService
     * @param {AuthenticationService} _authenticationService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslateService} _translateService
     * @param {NotificationService} _notificationsService
     */
    constructor(
        private _appConfigService: AppConfigService,
        private _expertiseResultService: ExpertiseResultService,
        private _authenticationService: AuthenticationService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translateService: TranslateService,
        private _notificationsService: NotificationService
) {
        // Set the private defaults
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
        this._appConfigService.config.subscribe(config => { this.appConfig = config; });
        this._translationLoader.loadTranslations(spanish, english);
        this._notificationsService.resetUserNotifications();
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to expertise
        this._expertiseResultService.onExpertiseChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(expertise => {
                this.expertise = expertise;
            });
        
        // Subscribe to executions
        this._expertiseResultService.onTestExecutionChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(execution => {
                this.execution = this._getLastAssessedExecution(execution);
                this.assessment = this._getAssessment(this.execution);
                this.widgets = this._formatWidgets(this.assessment);
                this.spiderWeb = this._formatWidgets(this.assessment, true)[0];
            });

        // Subscribe to result
        this._expertiseResultService.onCurrentExecutionChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(execution => {
                this.currentExecution = execution;
                if (this.currentExecution && this.currentExecution.id && (!(this.execution && this.execution.id) || (this.currentExecution.id !== this.execution.id))) {
                    this.execution = execution;
                    this.assessment = this._getAssessment(this.execution);
                    this.widgets = this._formatWidgets(this.assessment);
                    this.spiderWeb = this._formatWidgets(this.assessment, true)[0];
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();

        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get last assessed execution
     *
     * @param executions
     * @returns {execution}
     */
    _getLastAssessedExecution(executions: TestExecution[] = []): TestExecution | null {
        if (executions.length === 0) {
            return;
        }
        return executions.filter(item => +item.status === 3)
            .sort((a, b) => {
                a = a.created;
                b = b.created;
                if (a > b) {
                    return -1;
                } else if (a < b) {
                    return 1;
                }
                return 0;
            })[0];
    }

    /**
     * Get execution assessment
     *
     * @param execution
     * @returns {execution}
     */
    _getAssessment(execution: TestExecution = null): TestExecution | null {
        if (!execution || !execution.assessed || execution.assessed.length === 0) {
            return;
        } else if (execution.assessed.length === 1) {
            return execution.assessed[0];
        } else {
            return execution.assessed;
        }
    }

    _formatWidgets(data, spiderWeb = false): any {
        const widgets = [];
        if (data) {
            if (!(data.length > 0)) {
                data = [data];
            }
            const optionWeb = _.cloneDeep(this.appConfig.spiderWebOptions);
            optionWeb.xAxis.categories = [];
            optionWeb.series[0].data = [];
            let itemCounter = 0;
            // Loop through main skills
            for (const item of data) {
                // Loop through children skills
                itemCounter ++;
                if (itemCounter > 9) {
                    itemCounter = 0;
                }
                const childObj = [];
                let childCounter = 0;
                for (const subItem of item.children) {
                    childCounter ++;
                    if (spiderWeb) {
                        optionWeb.title.text = item.skill.description;
                        optionWeb.yAxis.max = item.maximum;
                        optionWeb.yAxis.tickInterval = item.maximum * 0.5;
                        optionWeb.yAxis.minorTickInterval = item.maximum * 0.25;
                        optionWeb.title.text = null;
                        optionWeb.xAxis.categories.push('<span class="spider-bullet" style="color:' + this.appConfig.solidDonutOptionsColors[itemCounter].middle + '">●</span> <span class="spider-title-text" style="color:' + this.appConfig.solidDonutOptionsColors[itemCounter].middle + '">' + subItem.skill.title + '</span>');
                        optionWeb.xAxis.className = 'spider-title';
                        optionWeb.series[0].data.push(item.punctuation);
                        optionWeb.tooltip.formatter = radarTooltipFormatter;
                        optionWeb.chart.scrollablePlotArea = { minHeight: 1024, minWidth: 1024, scrollPositionX: 0.5, scrollPositionY: 0.5 };
                        optionWeb.responsive.rules = [{
                            condition: {
                                maxWidth: 1000
                            },
                            chartOptions: {
                                legend: {
                                    align: 'center',
                                    verticalAlign: 'bottom'
                                },
                                pane: {
                                    size: '70%'
                                },
                                yAxis: {
                                    labels: {
                                        enabled: false,
                                    }
                                }
                            }
                        }];
                    } else {
                        const subOption = _.cloneDeep(this.appConfig.solidDonut2Options);
                        const scaledChildValues = this._scaleConverter(subItem.punctuation, subItem.minimum, subItem.maximum);
                        // subOption.title.text = this._translateService.instant('MAIN.SKILLS.' + subItem.skill.title);
                        subOption.title = null;
                        subOption.yAxis.max = scaledChildValues.max;
                        subOption.series.data = [];
                        subOption.series = [{data: [{y: scaledChildValues.value, innerRadius: '85%'}]}];
                        // subOption.plotOptions.solidgauge.formatter = function (): number { return (this.y.toFixed(0)); };
                        subOption.yAxis.stops = [
                            [0.5, this.appConfig.solidDonutOptionsColors[itemCounter].min],
                            [1, this.appConfig.solidDonutOptionsColors[itemCounter].max]
                        ];
                        childObj.push({
                            skill: subItem.skill,
                            chart: new Chart(subOption as any)
                        });
                    }
                }
                const option = _.cloneDeep(this.appConfig.solidDonutOptions);
                const scaledValues = this._scaleConverter(item.punctuation, item.minimum, item.maximum);
                // option.title.text = this._translateService.instant('MAIN.SKILLS.' + item.skill.title);
                option.title = null;
                option.yAxis.max = scaledValues.max;
                option.series.data = [];
                option.series = [{data: [scaledValues.value]}];
                // option.plotOptions.solidgauge.formatter = function (): number { return (this.y.toFixed(0)); };
                option.yAxis.stops = [
                    [0.5, this.appConfig.solidDonutOptionsColors[itemCounter].min],
                    [0.75, this.appConfig.solidDonutOptionsColors[itemCounter].middle],
                    [1, this.appConfig.solidDonutOptionsColors[itemCounter].max]
                ];
                option.yAxis.plotBands = [
                    { from: 0, to: (option.yAxis.max * 0.5), color: this.appConfig.solidDonutOptionsColors[itemCounter].min, innerRadius: '107.5%', outerRadius: '115%'},
                    { from: (option.yAxis.max * 0.5), to: (option.yAxis.max * 0.75), color: this.appConfig.solidDonutOptionsColors[itemCounter].middle, innerRadius: '107.5%', outerRadius: '115%'},
                    { from: (option.yAxis.max * 0.75), to: option.yAxis.max, color: this.appConfig.solidDonutOptionsColors[itemCounter].max, innerRadius: '107.5%', outerRadius: '115%'},
                ];
                if (!spiderWeb) {
                    widgets.push({
                        skill: item.skill,
                        children: childObj,
                        chart: new Chart(option as any)
                    });
                }
            }
            if (spiderWeb) {
                optionWeb.title = null;
                widgets.push({
                    skill: {id: 0},
                    chart: new Chart(optionWeb as any)
                });
            }
        }
        return widgets;
    }

    _scaleConverter(value = 0, min = 0, max = 10): any {
        const range = max - min;
        if (range > 100) {
            return {
                value: ((value - min) * 100 ) / range,
                min: 0,
                max: 100,
            };
        }
        return {
            value: value - min,
            min: 0,
            max: max - min,
        };
    }
}
