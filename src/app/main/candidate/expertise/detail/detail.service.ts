import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {AuthenticationService, TestApiService, TestExecutionApiService} from 'app/_services';
import {User} from 'app/_models';

@Injectable()
export class CandidateExpertiseDetailService implements Resolve<any> {
    onExpertiseChanged: BehaviorSubject<any>;
    currentUserSubscription: Subscription;
    currentUser: User;
    executions: any;
    expertise: any;

    /**
     * Constructor
     *
     * @param {TestApiService} _testApiService
     * @param {TestExecutionApiService} _testExecutionApiService
     * @param {AuthenticationService} _authenticationService
     */
    constructor(
        private _testApiService: TestApiService,
        private _testExecutionApiService: TestExecutionApiService,
        private _authenticationService: AuthenticationService
    ) {
        // Set the defaults
        this.onExpertiseChanged = new BehaviorSubject({});
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getExpertise(route.params.expertiseId)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get expertise
     *
     * @returns {Promise<any>}
     */
    getExpertise(id): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testApiService.get(id)
                .subscribe((expertise: any) => {
                    this.expertise = expertise;
                    this._testExecutionApiService.getByUser(+this.currentUser.id)
                        .subscribe((executions: any) => {
                            this.executions = executions;
                            for (const execution of executions) {
                                if (this._filterByTestId(this.expertise.id, execution.test.id)) {
                                    this.expertise.execution = execution;
                                }
                            }
                        });
                    this.onExpertiseChanged.next(this.expertise);
                    resolve(this.expertise);
                }, reject);
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private _filterByTestId(item: any, testId: number): any {
        return item.indexOf(testId) !== -1;
    }
}
