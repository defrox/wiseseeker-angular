import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ExpertiseStartComponent} from 'app/main/candidate/expertise/start';
import {ExpertiseResultComponent, ExpertiseResultService} from 'app/main/candidate/expertise/result';
import {ExpertiseQuestionnaireComponent, ExpertiseQuestionnaireService, ExpertiseQuestionnaireDialogComponent} from 'app/main/candidate/expertise/questionnaire';
import {CandidateExpertiseListComponent, CandidateExpertiseListService} from 'app/main/candidate/expertise/list';
import {CandidateExpertiseDetailComponent, CandidateExpertiseDetailService} from 'app/main/candidate/expertise/detail';
import {ExpertiseHomeComponent, ExpertiseHomeService} from 'app/main/candidate/expertise/home';
import {MaterialModule} from 'app/_helpers/material.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseSidebarModule, FuseWidgetModule} from '@fuse/components';
import {TranslateModule} from '@ngx-translate/core';
import {ChartModule} from 'angular-highcharts';
import {DefroxModule} from 'app/_helpers';
import {AuthGuard} from 'app/_guards';
import {Group} from 'app/_models';

const appRoutes: Routes = [
    {path: 'candidate/expertise', children: [
        {path: '', component: ExpertiseHomeComponent, resolve: {home: ExpertiseHomeService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'start', component: ExpertiseStartComponent, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'list', component: CandidateExpertiseListComponent, resolve: {list: CandidateExpertiseListService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: ':expertiseId/:expertiseSlug', component: CandidateExpertiseDetailComponent, resolve: {expertise: CandidateExpertiseDetailService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'questionnaire/:expertiseId/:expertiseSlug', component: ExpertiseQuestionnaireComponent, resolve: {questionnaire: ExpertiseQuestionnaireService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'result/:expertiseId/:expertiseSlug', component: ExpertiseResultComponent, resolve: {result: ExpertiseResultService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        // otherwise redirect to 404
        {path: '**', redirectTo: '/error/404', pathMatch: 'full'}
    ]}
];

@NgModule({
    declarations: [
        ExpertiseStartComponent,
        ExpertiseQuestionnaireComponent,
        ExpertiseResultComponent,
        ExpertiseHomeComponent,
        CandidateExpertiseListComponent,
        CandidateExpertiseDetailComponent,
        ExpertiseQuestionnaireDialogComponent,
        ],
    imports: [
        RouterModule.forChild(appRoutes),
        MaterialModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseWidgetModule,
        TranslateModule,
        ChartModule,
        DefroxModule
    ],
    providers: [
        ExpertiseQuestionnaireService,
        ExpertiseResultService,
        ExpertiseHomeService,
        CandidateExpertiseListService,
        CandidateExpertiseDetailService,
        AuthGuard
    ],
    entryComponents: [ExpertiseQuestionnaireDialogComponent],
    exports: [RouterModule]
})
export class ExpertiseRouting { }
