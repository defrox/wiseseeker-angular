import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {AuthenticationService, TestApiService, TestExecutionApiService} from 'app/_services';
import {TestSubTypeExt, TestExecution, User} from 'app/_models';

@Injectable()
export class ExpertiseHomeService implements Resolve<any> {
    onExpertiseChanged: BehaviorSubject<any>;
    onExpertiseCategoriesChanged: BehaviorSubject<any>;
    executions: any;
    expertise: any;
    expertiseExecutions: TestExecution[];
    tests: any;
    currentUserSubscription: Subscription;
    currentUser: User;

    /**
     * Constructor
     *
     * @param {TestApiService} _testApiService
     * @param {TestExecutionApiService} _testExecutionApiService
     * @param {AuthenticationService} _authenticationService
     */
    constructor(
        private _testApiService: TestApiService,
        private _testExecutionApiService: TestExecutionApiService,
        private _authenticationService: AuthenticationService
    ) {
        // Set the defaults
        this.onExpertiseChanged = new BehaviorSubject({});
        this.onExpertiseCategoriesChanged = new BehaviorSubject({});
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {
            Promise.all([
                this.getAllExpertiseExecutions(+this.currentUser.id),
                this.getAllExpertiseCategories()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get all expertise executions
     *
     * @param testId
     * @returns {Promise<any>}
     */
    getAllExpertiseExecutions(testId: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testExecutionApiService.getByUser(+this.currentUser.id)
                .subscribe((executions: any) => {
                    // console.log(executions);
                    this.executions = executions;
                    this.getAllExpertise(+this.currentUser.id).then(
                        expertise => {
                            this.expertise = expertise;
                            this.expertiseExecutions = [];
                            for (const execution of executions) {
                                const tmpExpertise = this.expertise.filter(item => item.id === execution.test.id);
                                if (tmpExpertise.length > 0) {
                                    tmpExpertise[0].execution = execution;
                                    this.expertiseExecutions = [...this.expertiseExecutions, ...tmpExpertise[0]];
                                }
                            }
                            // console.log(this.expertiseExecutions);
                            // this.testExecution = executions.filter(item => this._filterByTestId(item, testId));
                            this.onExpertiseChanged.next(this.expertiseExecutions);
                            resolve(this.expertiseExecutions);
                        });
                }, reject);
        });
    }

    /**
     * Get all expertise categories
     *
     * @returns {Promise<any>}
     */
    getAllExpertiseCategories(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            const categories = TestSubTypeExt;
            this.onExpertiseCategoriesChanged.next(categories);
            resolve(categories);
            /*this._expertiseCategoryApiService.getAll()
                .subscribe((categories: any) => {
                    this.onExpertiseCategoriesChanged.next(categories);
                    resolve(categories);
                }, reject);*/
        });
    }

    /**
     * Get all expertise
     *
     * @param userId
     * @returns {Promise<any>}
     */
    getAllExpertise(userId: number): Promise<any>
    {
        return this._testApiService.getFiltered({type: 3}).toPromise().then(res => res);
    }

    /**
     * Get expertise
     *
     * @param userId
     * @returns {Promise<any>}
     */
    getAllTests(userId: number): Promise<any>
    {
        return this._testApiService.getAll().toPromise().then(res => res);
    }

    /**
     * Set Expertise
     *
     * @param homes
     */
    setExpertise(homes): void {
        this.onExpertiseChanged.next(homes);
    }

    /**
     * Get Expertise
     *
     * @returns {Observable<any>}
     */
    getExpertise(): Observable<any> {
        return this.onExpertiseChanged.asObservable();
    }

    /**
     * Reset Expertise
     */
    resetExpertise(): void {
        this.onExpertiseChanged.next(null);
    }

    /**
     * Get all expertise categories
     *
     * @returns {Promise<any>}
     */
    /*getExpertiseCategories(): Promise<any>
    {
        return this._expertiseCategoryApiService.getAll().toPromise().then(res => res);
    }*/

    /**
     * Get expertise categori bi id
     *
     * @returns {Promise<any>}
     */
    /*getExpertiseCategory(categoryId: number): Promise<any>
    {
        return this._expertiseCategoryApiService.get(categoryId).toPromise().then(res => res);
    }*/

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private _filterByTestId(item: any, testId: number): any {
        return item.indexOf(testId) !== -1;
    }

    private _getFirst(array, n = 1): any | null {
        return n > 0 && array.length > n ? array.slice(0, n) : array;
    }

    private handleError(error: any): any {
        return throwError(error);
    }
}
