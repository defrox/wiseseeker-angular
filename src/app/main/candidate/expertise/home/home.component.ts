import {Component, OnInit, OnDestroy, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {ExpertiseHomeService} from './home.service';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'candidate-expertise-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ExpertiseHomeComponent implements OnInit, OnDestroy {
    userExpertise: any[];
    categories: any[] = [];
    expertiseFilteredByCategory: any[];
    filteredExpertise: any[];
    currentCategory: string;
    searchTerm: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {ExpertiseHomeService} _expertiseHomeService
     * @param {FuseTranslationLoaderService} _translationLoader
     */
    constructor(
        private _expertiseHomeService: ExpertiseHomeService,
        private _translationLoader: FuseTranslationLoaderService,
    ) {
        // Set the defaults
        this.currentCategory = 'all';
        this.searchTerm = '';

        // Set the private defaults
        this._translationLoader.loadTranslations(spanish, english);
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to categories
        this._expertiseHomeService.onExpertiseCategoriesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(categories => {
                this.categories = categories;
            });

        // Subscribe to user expertise
        this._expertiseHomeService.onExpertiseChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(expertise => {
                this.filteredExpertise = this.expertiseFilteredByCategory = this.userExpertise = expertise;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Filter expertise by category
     */
    filterExpertiseByCategory(): void {
        // Filter
        if (this.currentCategory === 'all') {
            this.expertiseFilteredByCategory = this.userExpertise;
            this.filteredExpertise = this.userExpertise;
        } else {
            this.expertiseFilteredByCategory = this.userExpertise.filter((expertise) => {
                return expertise.subtype === this.currentCategory;
            });

            this.filteredExpertise = [...this.expertiseFilteredByCategory];

        }

        // Re-filter by search term
        this.filterExpertiseByTerm();
    }

    /**
     * Filter expertise by term
     */
    filterExpertiseByTerm(): void {
        const searchTerm = this.searchTerm.toLowerCase();

        // Search
        if (searchTerm === '') {
            this.filteredExpertise = this.expertiseFilteredByCategory;
        } else {
            this.filteredExpertise = this.expertiseFilteredByCategory.filter((expertise) => {
                return expertise.title.toLowerCase().includes(searchTerm);
                // return (expertise.title.toLowerCase().includes(searchTerm) || expertise.description.toLowerCase().includes(searchTerm));
            });
        }
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------
}
