import {AfterViewInit, ChangeDetectorRef, Component, HostListener, OnDestroy, OnInit, QueryList, ViewChildren, ViewEncapsulation, Inject} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {ActivatedRoute, Router} from '@angular/router';
import {ExpertiseQuestionnaireService} from './questionnaire.service';
import {ExpertiseResultService} from 'app/main/candidate/expertise/result';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Answer, TestExecution, DialogData, Question} from 'app/_models';
import {LoaderOverlayService, NotificationService} from 'app/_services';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'candidate-expertise-questionnaire',
    templateUrl: './questionnaire.component.html',
    styleUrls: ['./questionnaire.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class ExpertiseQuestionnaireComponent implements OnInit, OnDestroy, AfterViewInit {
    animationDirection: 'left' | 'right' | 'none';
    questionnaire: any;
    currentStep: number;
    testId: string | number | null;
    expertiseId: string | number | null;
    expertiseSlug: string | null;
    previousExecutions: any | null;
    lastResult: any | null;
    lastAttempt: any | null;
    submitted = false;
    questionnaireReady = false;

    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;

    // Private
    private _unsubscribeAll: Subject<any>;

    @HostListener('document:keydown', ['$event'])
    handleKeyboardEvent($event: KeyboardEvent): void {
        if ($event.key === 'f' && $event.ctrlKey && $event.altKey) {
            this.currentStep = this.questionnaire.questions.length - 1;
        }
    }

    /**
     * Constructor
     *
     * @param {ExpertiseQuestionnaireService} _expertiseQuestionnaireService
     * @param {ExpertiseResultService} _expertiseResultService
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {NotificationService} _notificationsService
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {MatDialog} _dialog
     * @param {ActivatedRoute} _route
     * @param {Router} _router
     */
    constructor(
        private _expertiseQuestionnaireService: ExpertiseQuestionnaireService,
        private _expertiseResultService: ExpertiseResultService,
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseSidebarService: FuseSidebarService,
        private _translationLoader: FuseTranslationLoaderService,
        private _notificationsService: NotificationService,
        private _loaderOverlay: LoaderOverlayService,
        private _dialog: MatDialog,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        // Set the defaults
        // this._loaderOverlay.show();
        this._cleanUp();
        this.animationDirection = 'none';
        this.currentStep = 0;
        this._translationLoader.loadTranslations(spanish, english);
        this._route.paramMap.subscribe(params => {
            this.expertiseId = params.get('expertiseId');
            this.expertiseSlug = params.get('expertiseSlug');
            this.testId = this.expertiseId;
        });
        this._expertiseQuestionnaireService.getAllTestExecutions(+this.testId)
            .then(executions => {
                if (executions.length > 0) {
                    this.previousExecutions = executions;
                    const lastFinishedExecution = this._getLastFinishedExecution(executions);
                    if (lastFinishedExecution) {
                        this.lastResult = lastFinishedExecution.answers;
                    }
                    const lastUnfinishedExecution = this._getLastUnfinishedExecution(executions);
                    if (lastUnfinishedExecution) {
                        this.lastAttempt = lastUnfinishedExecution.answers;
                    }
                    this._openRetakeExecutionDialog();
                } else {
                    this._newExecutionTest(+this.testId);
                }
            });

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to questionnaires
        this._expertiseQuestionnaireService.onExpertiseChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe( expertise => {
                this.questionnaire = expertise;
                this.questionnaire.execution = {id: 0};
            });

        // Subscribe to questionnaires
        this._expertiseQuestionnaireService.onQuestionnaireChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(questions => {
                this.questionnaire.questions = questions.questions;
            });

        // Subscribe to results
        this._expertiseResultService.onResultChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(results => {
                this.questionnaire.results = results;
            });
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void {}

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._cleanUp();
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this._notificationsService.resetUserNotifications();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Clean up results
     *
     * @returns {void}
     */
    _cleanUp(): void {
        this.questionnaire = null;
        this.previousExecutions = null;
        this.lastResult = null;
        this.lastAttempt = null;
        // this._expertiseResultService.resetExpertiseResults();
    }

    /**
     * Format results
     *
     * @param questions
     * @returns {any}
     */
    _formatResults(questions: Question[] = []): any {
        if (questions.length === 0) {
            return;
        }
        const result = [];
        for (const question of questions) {
            if (+question.type === 3) {
                result[question.id] = [];
                if (question.answers && question.answers.length > 0) {
                    for (const questionAnswer of question.answers){
                        result[question.id][questionAnswer.id] = false;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Format execution
     *
     * @param answers
     * @returns {any}
     */
    _formatExecution(answers: Answer[] = []): any {
        if (answers.length === 0) {
            return;
        }
        const result = [];
        for (const question of this.questionnaire.questions) {
            if (+question.type === 3) {
                result[question.id] = [];
                if (question.answers && question.answers.length > 0) {
                    for (const questionAnswer of question.answers){
                        result[question.id][questionAnswer.id] = false;
                    }
                }
            }
        }
        for (const answer of answers) {
            const answerType = this._getTypeOfQuestion(answer.question, this.questionnaire.questions);
            if (+answerType === 3) {
                for (const item of answer.answers) {
                    result[answer.question][item] = true;
                }
            } else  {
                for (const item of answer.answers) {
                    result[answer.question] = +item;
                }
            }
        }
        return result;
    }

    /**
     * Get last unfinished execution
     *
     * @param executions
     * @returns {execution}
     */
    _getLastUnfinishedExecution(executions: TestExecution[] = []): TestExecution | null {
        if (executions.length === 0) {
            return;
        }
        return executions.filter(item => +item.status === 1)
            .sort((a, b) => {
                a = a.created;
                b = b.created;
                if (a > b) {
                    return -1;
                } else if (a < b) {
                    return 1;
                }
                return 0;
            })[0];
    }

    /**
     * Get last finished execution
     *
     * @param executions
     * @returns {execution}
     */
    _getLastFinishedExecution(executions: TestExecution[] = []): TestExecution | null {
        if (executions.length === 0) {
            return;
        }
        return executions.filter(item => +item.status === 2 || +item.status === 3)
            .sort((a, b) => {
                a = a.created;
                b = b.created;
                if (a > b) {
                    return -1;
                } else if (a < b) {
                    return 1;
                }
                return 0;
            })[0];
    }

    /**
     * Open retake execution dialog
     *
     * @returns {void}
     */
    _openRetakeExecutionDialog(): void {
        this._loaderOverlay.hide();
        const dialogRef = this._dialog.open(ExpertiseQuestionnaireDialogComponent, {
            data: {lastResult: this.lastResult, lastAttempt: this.lastAttempt}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result === 'cancel') {
                // Cancel retake
                this._loaderOverlay.show();
                this._router.navigate([`/candidate/expertise/result/${this.expertiseId}/${this.expertiseSlug}`]).then(() => this._loaderOverlay.hide());
            } else if (result) {
                // Continue last attempt
                this.questionnaire.results = this._formatResults(this.questionnaire.questions);
                this.questionnaireReady = true;
                if (this.lastAttempt && this.lastAttempt.length > 0) {
                    this.questionnaire.results = this._formatExecution(this.lastAttempt);
                }
                this.questionnaire.execution = this.previousExecutions[0];
            } else {
                // Start again
                this._loaderOverlay.show();
                this._deletePreviousExecutions(this.previousExecutions);
                this._newExecutionTest(+this.testId);
            }
        });
    }

    /**
     * Creates a new test execution
     *
     * @param testId
     * @returns {void}
     */
    _newExecutionTest(testId: number): void {
        this.questionnaire.results = this._formatResults(this.questionnaire.questions);
        this.questionnaireReady = true;
        const execution = {'test_id': testId};
        this._expertiseQuestionnaireService.newTestExecution(execution).then(
            res => {
                this.questionnaire.execution = res;
                this.questionnaire.results = this._formatResults(this.questionnaire.questions);
                this.questionnaireReady = true;
            }).then(() => this._loaderOverlay.hide());
    }

    /**
     * Deletes all previous test executions
     *
     * @param executions
     * @returns {void}
     */
    _deletePreviousExecutions(executions: TestExecution[] = []): void {
        for (const execution of executions) {
            this._expertiseQuestionnaireService.deleteTestExecution(execution);
        }
    }

    /**
     * Get type of question
     *
     * @param questionId
     * @param questions
     * @returns {number}
     */
    private _getTypeOfQuestion(questionId: number, questions: any[]): number {
        for (const question of questions) {
            if (+question.id === questionId) {
                return question.type;
            }
        }
        return;
    }

    /**
     * Filters by status id
     *
     * @param item
     * @param statusId
     * @returns {boolean}
     */
    private _filterByStatus(item: any, statusId: number): boolean {
        return +item.status === +statusId;
    }

    /**
     * Filters by statuses id
     *
     * @param item
     * @param statusIds
     * @returns {boolean}
     */
    private _filterByStatuses(item: any, statusIds: number[]): any {
        return statusIds.indexOf(item.status) !== -1;
    }

    /**
     * Converts array of 1 item to singleton
     *
     * @param array
     * @returns {void}
     */
    private _singletonize(array: any): any {
        if (array && array.length > 0) {
            for (const item of array) {
                return item;
            }
        } else {
            return;
        }
    }
    
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Go to step
     *
     * @param step
     */
    gotoStep(step): void {
        // Decide the animation direction
        this.animationDirection = this.currentStep < step ? 'left' : 'right';

        // Run change detection so the change
        // in the animation direction registered
        this._changeDetectorRef.detectChanges();

        // Set the current step
        this.currentStep = step;
    }

    /**
     * Go to next step
     */
    gotoNextStep(): void {
        if (this.currentStep === this.questionnaire.questions.length - 1) {
            return;
        }

        // Set the animation direction
        this.animationDirection = 'left';

        // Run change detection so the change
        // in the animation direction registered
        this._changeDetectorRef.detectChanges();

        // Increase the current step
        this.currentStep++;
    }

    /**
     * Go to previous step
     */
    gotoPreviousStep(): void {
        if (this.currentStep === 0) {
            return;
        }

        // Set the animation direction
        this.animationDirection = 'right';

        // Run change detection so the change
        // in the animation direction registered
        this._changeDetectorRef.detectChanges();

        // Decrease the current step
        this.currentStep--;
    }

    /**
     * Submit the results
     *
     * @param execution
     */
    submitExecution(execution): void {
        if (!this.submitted) {
            this.submitted = true;
            this._loaderOverlay.show();
            const executionCopy = Object.assign({}, execution);
            executionCopy.status = 2;
            this._expertiseQuestionnaireService.submitTestExecution(executionCopy).then(res => {
                this._expertiseResultService.setCurrentExecution(res);
                this._router.navigate([`/candidate/expertise/result/${this.expertiseId}/${this.expertiseSlug}`]).then(() => this._loaderOverlay.hide());
            }).catch( err => {
                // do something in case of error
                this._loaderOverlay.hide();
                this.submitted = false;
                throw new Error(err);
            });
        }
    }

    /**
     * Save the answer(s)
     *
     * @param answer
     * @param type
     * @param event
     */
    saveAnswer(answer: any, type = 1, event: any = null): void {
        if (+type === 3) { // If is checkbox then format, else do nothing
            let newAnswer = [];
            Object.entries(this.questionnaire.results[answer.question]).forEach(([key, value]) => {
                if (value) {
                    newAnswer = [+key, ...newAnswer];
                }
            });
            answer.answers = newAnswer;
        }
        this._expertiseQuestionnaireService.saveTestAnswer(answer);
    }

    /**
     * Toggle the sidebar
     *
     * @param name
     */
    toggleSidebar(name): void {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }
}

@Component({
    selector: 'candidate-expertise-questionnaire-dialog',
    templateUrl: './questionnaire.dialog.component.html',
    styleUrls: ['./questionnaire.component.scss'],
})
export class ExpertiseQuestionnaireDialogComponent {
    constructor(
        private _dialogRef: MatDialogRef<ExpertiseQuestionnaireDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
        _dialogRef.disableClose = true;
    }
}
