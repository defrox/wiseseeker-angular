import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {AuthenticationService, QuestionApiService, TestApiService, TestExecutionApiService, AnswerApiService} from 'app/_services';
import {Answer, TestExecution, User} from 'app/_models';

@Injectable()
export class ExpertiseQuestionnaireService implements Resolve<any> {
    onQuestionnaireChanged: BehaviorSubject<any>;
    onExpertiseChanged: BehaviorSubject<any>;
    onTestExecutionChanged: BehaviorSubject<any>;
    questionnaire: any;
    expertise: any;
    testExecution: any;
    currentUserSubscription: Subscription;
    currentUser: User;

    /**
     * Constructor
     *
     * @param {TestApiService} _testApiService
     * @param {QuestionApiService} _questionApiService
     * @param {AnswerApiService} _answerApiService
     * @param {TestExecutionApiService} _testExecutionApiService
     * @param {AuthenticationService} _authenticationService
     */
    constructor(
        private _testApiService: TestApiService,
        private _questionApiService: QuestionApiService,
        private _answerApiService: AnswerApiService,
        private _testExecutionApiService: TestExecutionApiService,
        private _authenticationService: AuthenticationService
    ) {
        // Set the defaults
        this.onQuestionnaireChanged = new BehaviorSubject({});
        this.onExpertiseChanged = new BehaviorSubject({});
        this.onTestExecutionChanged = new BehaviorSubject({});
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {
            Promise.all([
                this.getExpertiseDetails(route.params.expertiseId),
                this.getQuestionnaire(route.params.expertiseId)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get expertise questionnaire
     *
     * @param testId
     * @returns {Promise<any>}
     */
    getQuestionnaire(testId: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testApiService.get(testId)
                .subscribe((questionnaire: any) => {
                    this.questionnaire = questionnaire;
                    this.onQuestionnaireChanged.next(this.questionnaire);
                    resolve(this.questionnaire);
                }, reject);
        });
    }

    /**
     * Get expertise questions
     *
     * @param testId
     * @returns {Promise<any>}
     */
    getAllQuestions(testId: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._questionApiService.getAll()
                .subscribe((questionnaire: any) => {
                    this.questionnaire = questionnaire.filter(item => this._filterQuestionsById(item, testId));
                    this.onQuestionnaireChanged.next(this.questionnaire);
                    resolve(this.questionnaire);
                }, reject);
        });
    }

    /**
     * Get Expertise details
     *
     * @param expertiseId
     * @returns {Observable<any>}
     */
    getExpertiseDetails(expertiseId: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            if (expertiseId === 0) {
                resolve(null);
            } else {
                this._testApiService.get(expertiseId)
                    .subscribe((expertise: any) => {
                        this.expertise = expertise;
                        this.onExpertiseChanged.next(this.expertise);
                        resolve(this.expertise);
                    }, reject);
            }
        });
    }

    /**
     * Get all test executions
     *
     * @param testId
     * @returns {Promise<any>}
     */
    getAllTestExecutions(testId: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testExecutionApiService.getByUserAndTest(+this.currentUser.id, testId)
                .subscribe((executions: any) => {
                    this.testExecution = executions;
                    // this.testExecution = executions.filter(item => this._filterByTestId(item, testId));
                    this.onTestExecutionChanged.next(this.testExecution);
                    resolve(this.testExecution);
                }, reject);
        });
    }

    /**
     * Get test execution by id
     *
     * @param executionId
     * @returns {Observable<any>}
     */
    getTestExecution(executionId: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testExecutionApiService.get(executionId)
                .subscribe((execution: any) => {
                    this.testExecution = execution;
                    this.onTestExecutionChanged.next(this.testExecution);
                    resolve(this.testExecution);
                }, reject);
        });
    }

    /**
     * Save Test answer
     *
     * @param answer
     */
    saveTestAnswer(answer: Answer): void
    {
        this._answerApiService.add(answer).toPromise().then();
    }

    /**
     * New test execution
     *
     * @param execution
     */
    newTestExecution(execution: TestExecution): any
    {
        return this._testExecutionApiService.add(execution).toPromise().then(res => res);
    }

    /**
     * Deletes test executions
     *
     * @param execution
     */
    deleteTestExecution(execution: TestExecution): void
    {
        this._testExecutionApiService.delete(execution).toPromise().then();
    }

    /**
     * Submit test executions
     *
     * @param execution
     */
    submitTestExecution(execution: TestExecution): any
    {
        return this._testExecutionApiService.update(execution).toPromise().then(res => res);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private _filterQuestionsById(item: any, filteredId: number): any {
        return +item.expertise.id === +filteredId;
    }

    private _filterByExecutionId(item: any, filteredId: number): any {
        return +item.execution.id === +filteredId;
    }

    private _filterByTestIdAndStatus(item: any, testId: number, statusIds: number[]): any {
        return +item.test.id === +testId && statusIds.indexOf(item.status) !== -1;
    }

    private _filterByStatus(item: any, statusIds: number[]): any {
        return statusIds.indexOf(item.status) !== -1;
    }

    private _filterByTestId(item: any, filteredId: number): any {
        return +item.test.id === +filteredId;
    }

    private _filterById(item: any, filteredId: number): any {
        return +item.id === +filteredId;
    }

    private handleError(error: any): any {
        return throwError(error);
    }
}
