import {NgModule} from '@angular/core';
import {CandidateRouting} from 'app/main/candidate/candidate.routing';
import {WindowRef} from 'app/_helpers';

@NgModule({
    imports: [
        CandidateRouting
    ],
    providers: [
        WindowRef
    ]
})
export class CandidateModule {
}
