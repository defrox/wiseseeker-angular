import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {ExperienceApiService, UserApiService} from 'app/_services';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class CandidateExperienceDetailService implements Resolve<any> {
    onExperienceChanged: BehaviorSubject<any>;
    experience: any;

    /**
     * Constructor
     *
     * @param {TranslateService} _translateService
     * @param {UserApiService} _userApiService
     * @param {ExperienceApiService} _experienceApiService
     */
    constructor(
        private _translateService: TranslateService,
        private _userApiService: UserApiService,
        private _experienceApiService: ExperienceApiService
    ) {
        // Set the defaults
        this.onExperienceChanged = new BehaviorSubject({});
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getExperience(route.params.experienceId)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get experience
     *
     * @returns {Promise<any>}
     */
    getExperience(id): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._experienceApiService.get(id)
                .subscribe((response: any) => {
                    this.experience = response;
                    this.onExperienceChanged.next(this.experience);
                    resolve(this.experience);
                }, reject);
        });
    }

    /**
     * Delete experience
     *
     * @returns {Promise<any>}
     */
    deleteExperience(id): Promise<any>
    {
        return this._experienceApiService.delete(id).toPromise();
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

}
