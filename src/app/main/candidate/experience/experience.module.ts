import {NgModule} from '@angular/core';
import {ExperienceRouting} from 'app/main/candidate/experience/experience.routing';

@NgModule({
    declarations: [],
    imports: [
        ExperienceRouting
    ]
})
export class ExperienceModule {
}
