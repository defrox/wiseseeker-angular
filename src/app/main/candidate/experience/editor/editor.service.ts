import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {ExperienceApiService} from 'app/_services';

@Injectable()
export class CandidateExperienceEditorService implements Resolve<any> {
    onExperienceChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {ExperienceApiService} _experienceApiService
     */
    constructor(
        private _experienceApiService: ExperienceApiService
    ) {
        // Set the defaults
        this.onExperienceChanged = new BehaviorSubject({});
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getExperience(route.params.experienceId)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get experience
     *
     * @param experienceId
     * @returns {Promise<any>}
     */
    getExperience(experienceId): Promise<any> {
        if (experienceId > 0) {
            return new Promise((resolve, reject) => {
                this._experienceApiService.get(experienceId)
                    .subscribe((response: any) => {
                        this.onExperienceChanged.next(response);
                        resolve(response);
                    }, reject);
            });
        } else {
            return new Promise(resolve => {
                resolve({title: null, description: null, working_from: null, working_to: null, rol: null, city: null, country: null, enterprise: null});
            });
        }
    }

    /**
     * Save experience
     *
     * @param {any} experience
     * @returns {Promise<any>}
     */
    saveExperience(experience: any): Promise<any>
    {
        return new Promise((resolve, reject) => {
            if (experience.id && experience.id > 0) {
                this._experienceApiService.update(experience)
                    .subscribe((response: any) => {
                        this.onExperienceChanged.next(response);
                        resolve(response);
                    }, reject);
            } else {
                this._experienceApiService.add(experience)
                    .subscribe((response: any) => {
                        this.onExperienceChanged.next(response);
                        resolve(response);
                    }, reject);
            }
        });
    }

    /**
     * Reset experience
     *
     * @returns {void}
     */
    resetExperience(): void
    {
        this.onExperienceChanged.next({});
    }
}
