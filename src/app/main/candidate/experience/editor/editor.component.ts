import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, QueryList, ViewChildren, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {TranslatePipe} from '@ngx-translate/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {CandidateExperienceEditorService} from './editor.service';
import {AuthenticationService, LoaderOverlayService, NotificationService, SnackAlertService} from 'app/_services';
import * as _ from 'lodash';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'candidate-experience-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class CandidateExperienceEditorComponent implements OnInit, OnDestroy, AfterViewInit {
    animationDirection: 'left' | 'right' | 'none';
    currentUser;
    experience: any;
    experienceId: number | string;
    experienceEditor: FormGroup;
    submitted = false;

    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslatePipe} _translatePipe
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {SnackAlertService} _snackAlertService
     * @param {CandidateExperienceEditorService} _candidateExperienceService
     * @param {FormBuilder} _formBuilder
     * @param {ActivatedRoute} _route
     * @param {Router} _router
     * @param {AuthenticationService} _authenticationService
     * @param {NotificationService} _notificationsService
     * @param {ChangeDetectorRef} _changeDetectorRef
     */
    constructor(
        private _translationLoader: FuseTranslationLoaderService,
        private _translatePipe: TranslatePipe,
        private _loaderOverlay: LoaderOverlayService,
        private _snackAlertService: SnackAlertService,
        private _candidateExperienceService: CandidateExperienceEditorService,
        private _formBuilder: FormBuilder,
        private _route: ActivatedRoute,
        private _router: Router,
        private _authenticationService: AuthenticationService,
        private _notificationsService: NotificationService,
        private _changeDetectorRef: ChangeDetectorRef
    ) {
        // Set the defaults
        this._loaderOverlay.show();
        this.animationDirection = 'none';
        this._route.paramMap.subscribe(params => {
            this.experienceId = params.get('experienceId');
        });

        // Set the private defaults
        this.currentUser = this._authenticationService.currentUserValue;
        this._translationLoader.loadTranslations(spanish, english);
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to experiences
        this._candidateExperienceService.onExperienceChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(experience => {
                this.experience = experience;
                this._loaderOverlay.hide();
            });

        // Editor reactive form
        this.experienceEditor = this._formBuilder.group({
            title: [this.experience.title, Validators.required],
            role: [this.experience.rol],
            description: [this.experience.description, Validators.required],
            enterprise: [this.experience.enterprise, Validators.required],
            city: [this.experience.city, Validators.required],
            country: [this.experience.country, Validators.required],
            working_from: [this.experience.working_from, Validators.required],
            working_to: [this.experience.working_to, Validators.required],
        });

    }

    /**
     * After view init
     */
    ngAfterViewInit(): void {}

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._candidateExperienceService.resetExperience();
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Submit experience
     */
    submitExperience(): void
    {
        if (!this.submitted) {
            this.submitted = true;
            this._loaderOverlay.show();
            const result = _.cloneDeep(this.experience);
            result.title = this.experienceEditor.value.title;
            result.description = this.experienceEditor.value.description;
            result.working_from = this.experienceEditor.value.working_from;
            result.working_to = this.experienceEditor.value.working_to;
            // result.image = this.horizontalStepperStep3.value.image;
            // result.icon = this.horizontalStepperStep3.value.icon;
            result.rol = this.experienceEditor.value.role;
            result.enterprise = this.experienceEditor.value.enterprise;
            result.city = this.experienceEditor.value.city;
            result.country = this.experienceEditor.value.country;
            result.user = this.currentUser.id;
            this._candidateExperienceService.saveExperience(result).then(res => {
                this.experience = res;
                this._router.navigate([`/candidate/profile`]).then(() => {
                    this._loaderOverlay.hide();
                    this._snackAlertService.success(this._translatePipe.transform('CANDIDATE.EXPERIENCE.DIALOG.SAVED_SUCCESSFULLY'));
                    this._notificationsService.resetUserNotifications();
                });
            }).catch(err => {
                // do something in case of error
                this._loaderOverlay.hide();
                this.submitted = false;
                throw new Error(err);
            });
        }
    }

}
