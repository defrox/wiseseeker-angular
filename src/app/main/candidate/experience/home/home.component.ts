import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {CandidateExperienceHomeService} from './home.service';
import {TranslateService} from '@ngx-translate/core';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'candidate-experience-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    animations: fuseAnimations
})
export class CandidateExperienceHomeComponent implements OnInit, OnDestroy {
    categories: any[];
    experience: any[];
    experienceFilteredByCategory: any[];
    filteredExperience: any[];
    currentCategory: string;
    searchTerm: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {CandidateExperienceHomeService} _candidateExperienceHomeService
     * @param {TranslateService} _translateService
     */
    constructor(
        private _translationLoader: FuseTranslationLoaderService,
        private _candidateExperienceHomeService: CandidateExperienceHomeService,
        private _translateService: TranslateService
    ) {
        // Set the defaults
        this.currentCategory = 'all';
        this.searchTerm = '';

        // Set the private defaults
        this._translationLoader.loadTranslations(spanish, english);
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to categories
        this._candidateExperienceHomeService.onCategoriesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(categories => {
                this.categories = categories;
                // TODO: get categories from roles
            });

        // Subscribe to experience
        this._candidateExperienceHomeService.onExperienceChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(experience => {
                this.filteredExperience = this.experienceFilteredByCategory = this.experience = experience;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Filter experience by category
     */
    filterExperienceHomeByCategory(): void {
        // Filter
        if (this.currentCategory === 'all') {
            this.experienceFilteredByCategory = this.experience;
            this.filteredExperience = this.experience;
        } else {
            this.experienceFilteredByCategory = this.experience.filter((experience) => {
                return experience.category === this.currentCategory;
            });

            this.filteredExperience = [...this.experienceFilteredByCategory];

        }

        // Re-filter by search term
        this.filterExperienceHomeByTerm();
    }

    /**
     * Filter experience by term
     */
    filterExperienceHomeByTerm(): void {
        const searchTerm = this.searchTerm.toLowerCase();

        // Search
        if (searchTerm === '') {
            this.filteredExperience = this.experienceFilteredByCategory;
        } else {
            this.filteredExperience = this.experienceFilteredByCategory.filter((experience) => {
                return (experience.title.toLowerCase().includes(searchTerm) || experience.description.toLowerCase().includes(searchTerm) || experience.enterprise.toLowerCase().includes(searchTerm) || experience.rol.toLowerCase().includes(searchTerm));
            });
        }
    }

    getExperienceTime(date_from: string, date_to: string): string {
        const months = Math.round(Math.abs(new Date(date_to).getTime() - new Date(date_from).getTime()) / ((1000 * 60 * 60 * 24 * 365) / 12));
        const years = Math.floor(months / 12);
        const meses = months.toString();
        return `${years} ${this._translateService.instant('MAIN.YEARS')} ${months - (years * 12)} ${this._translateService.instant('MAIN.MONTHS')}`;
    }
}
