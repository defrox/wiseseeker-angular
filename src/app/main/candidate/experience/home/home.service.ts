import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {ExperienceApiService} from 'app/_services';

@Injectable()
export class CandidateExperienceHomeService implements Resolve<any> {
    onCategoriesChanged: BehaviorSubject<any>;
    onExperienceChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {ExperienceApiService} _experienceApiService
     */
    constructor(
        private _experienceApiService: ExperienceApiService
    ) {
        // Set the defaults
        this.onCategoriesChanged = new BehaviorSubject({});
        this.onExperienceChanged = new BehaviorSubject({});
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                // this.getCategories(),
                this.getExperienceList()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get categories
     *
     * @returns {Promise<any>}
     */
/*    getCategories(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._experienceApiService.getCategories()
                .subscribe((response: any) => {
                    this.onCategoriesChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }*/

    /**
     * Get courses
     *
     * @returns {Promise<any>}
     */
    getExperienceList(): Promise<any> {
        return new Promise((resolve, reject) => {
            this._experienceApiService.getAllByUser()
                .subscribe((response: any) => {
                    this.onExperienceChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }

}
