import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CandidateExperienceEditorComponent, CandidateExperienceEditorService} from 'app/main/candidate/experience/editor';
import {CandidateExperienceHomeComponent, CandidateExperienceHomeService} from 'app/main/candidate/experience/home';
import {CandidateExperienceDetailComponent, CandidateExperienceDetailService, CandidateExperienceDetailDialogComponent} from 'app/main/candidate/experience/detail';
import {MaterialModule} from 'app/_helpers/material.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseSidebarModule, FuseWidgetModule} from '@fuse/components';
import {TranslateModule} from '@ngx-translate/core';
import {DefroxModule} from 'app/_helpers';
import {AuthGuard} from 'app/_guards';
import {Group} from 'app/_models';

const appRoutes: Routes = [
    {path: 'candidate/experience', children: [
        {path: '', component: CandidateExperienceHomeComponent, resolve: {home: CandidateExperienceHomeService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate]}},
        {path: 'editor/:experienceId', component: CandidateExperienceEditorComponent, resolve: {experience: CandidateExperienceEditorService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate]}},
        {path: 'detail/:experienceId', component: CandidateExperienceDetailComponent, resolve: {experience: CandidateExperienceDetailService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate]}},
        // otherwise redirect to 404
        {path: '**', redirectTo: '/error/404', pathMatch: 'full'}
    ]}
];

@NgModule({
    declarations: [
        CandidateExperienceEditorComponent,
        CandidateExperienceHomeComponent,
        CandidateExperienceDetailComponent,
        CandidateExperienceDetailDialogComponent
        ],
    imports: [
        RouterModule.forChild(appRoutes),
        MaterialModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseWidgetModule,
        TranslateModule,
        DefroxModule
    ],
    providers: [
        CandidateExperienceEditorService,
        CandidateExperienceHomeService,
        CandidateExperienceDetailService,
        AuthGuard
    ],
    entryComponents: [CandidateExperienceDetailDialogComponent],
    exports: [RouterModule]
})
export class ExperienceRouting { }
