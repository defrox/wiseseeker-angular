import {NgModule} from '@angular/core';
import {SkillsRouting} from 'app/main/candidate/skills/skills.routing';

@NgModule({
    declarations: [],
    imports: [
        SkillsRouting
    ]
})
export class SkillsModule {
}
