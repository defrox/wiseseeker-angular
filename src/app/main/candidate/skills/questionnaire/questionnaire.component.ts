import {ElementRef, HostListener, ViewChild, ChangeDetectorRef, Component, OnDestroy, OnInit, QueryList, ViewChildren, ViewEncapsulation, Inject} from '@angular/core';
import {DeviceDetectorService} from 'ngx-device-detector';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {Router, ActivatedRoute} from '@angular/router';
import {SkillsQuestionnaireService} from './questionnaire.service';
import {SkillsResultService} from 'app/main/candidate/skills/result';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Answer, TestExecution, DialogData} from 'app/_models';
import {LoaderOverlayService, NotificationService} from 'app/_services';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'candidate-skills-questionnaire',
    templateUrl: './questionnaire.component.html',
    styleUrls: ['./questionnaire.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class SkillsQuestionnaireComponent implements OnInit, OnDestroy {
    animationDirection: 'left' | 'right' | 'none';
    questionnaire: any;
    currentStep: number;
    previousExecutions: any | null;
    lastResult: any | null;
    lastAttempt: any | null;
    submitted = false;
    testId: string | number | null;
    testSlug: string | null;
    // testId = 1;

    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild('questionnaireContent')
    myScrollContainer: ElementRef;

    // Private
    private _unsubscribeAll: Subject<any>;

    @HostListener('document:keydown', ['$event'])
    handleKeyboardEvent($event: KeyboardEvent): void {
        if ($event.key === 'f' && $event.ctrlKey && $event.altKey) {
            this.currentStep = this.questionnaire.questions.length - 1;
        }
    }

    /**
     * Constructor
     *
     * @param {SkillsQuestionnaireService} _skillsQuestionnaireService
     * @param {SkillsResultService} _skillsResultService
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {DeviceDetectorService} _deviceService
     * @param {NotificationService} _notificationsService
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {MatDialog} _dialog
     * @param {ActivatedRoute} _route
     * @param {Router} _router
     */
    constructor(
        private _skillsQuestionnaireService: SkillsQuestionnaireService,
        private _skillsResultService: SkillsResultService,
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseSidebarService: FuseSidebarService,
        private _deviceService: DeviceDetectorService,
        private _translationLoader: FuseTranslationLoaderService,
        private _notificationsService: NotificationService,
        private _loaderOverlay: LoaderOverlayService,
        private _dialog: MatDialog,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        // Set the defaults
        this._loaderOverlay.show();
        this._cleanUp();
        this.animationDirection = 'none';
        this.currentStep = 0;
        this._translationLoader.loadTranslations(spanish, english);
        this._route.paramMap.subscribe(params => {
            this.testId = params.get('testId') ? params.get('testId') : 1;
            this.testSlug = params.get('testSlug') ? params.get('testSlug') : 'ipv';
            this._skillsQuestionnaireService.getAllTestExecutions(+this.testId)
                .then(executions => {
                    if (executions.length > 0) {
                        this.previousExecutions = executions;
                        const lastFinishedExecution = this._getLastFinishedExecution(executions);
                        if (lastFinishedExecution) {
                            this.lastResult = this._formatExecution(lastFinishedExecution.answers);
                        }
                        const lastUnfinishedExecution = this._getLastUnfinishedExecution(executions);
                        if (lastUnfinishedExecution) {
                            this.lastAttempt = this._formatExecution(lastUnfinishedExecution.answers);
                        }
                        this._openRetakeExecutionDialog();
                    } else {
                        this._newExecutionTest(+this.testId);
                    }
                    this.scrollToTest();
                });
        });

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to questionnaires
        this._skillsQuestionnaireService.onQuestionnaireChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(questions => {
                this.questionnaire = questions;
            });

        // Subscribe to results
        this._skillsResultService.onResultChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(results => {
                this.questionnaire.results = results;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._cleanUp();
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this._notificationsService.resetUserNotifications();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Clean up results
     *
     * @returns {void}
     */
    _cleanUp(): void {
        this.questionnaire = null;
        this.previousExecutions = null;
        this.lastResult = null;
        this.lastAttempt = null;
        // this._skillsResultService.resetSkillsResults();
    }

    /**
     * Format execution
     *
     * @param answers
     * @returns {any}
     */
    _formatExecution(answers: Answer[] = []): any {
        if (answers.length === 0) {
            return;
        }
        const result = [];
        for (const answer of answers) {
            if (answer.answers && answer.answers.length > 0) {
                for (const item of answer.answers) {
                    result[answer.question] = item;
                }
            }
        }
        return result;
    }

    /**
     * Get last unfinished execution
     *
     * @param executions
     * @returns {execution}
     */
    _getLastUnfinishedExecution(executions: TestExecution[] = []): TestExecution | null {
        if (executions.length === 0) {
            return;
        }
        return executions.filter(item => +item.status === 1)
            .sort((a, b) => {
                a = a.created;
                b = b.created;
                if (a > b) {
                    return -1;
                } else if (a < b) {
                    return 1;
                }
                return 0;
            })[0];
    }

    /**
     * Get last finished execution
     *
     * @param executions
     * @returns {execution}
     */
    _getLastFinishedExecution(executions: TestExecution[] = []): TestExecution | null {
        if (executions.length === 0) {
            return;
        }
        return executions.filter(item => +item.status === 2 || +item.status === 3)
            .sort((a, b) => {
                a = a.created;
                b = b.created;
                if (a > b) {
                    return -1;
                } else if (a < b) {
                    return 1;
                }
                return 0;
            })[0];
    }

    /**
     * Open retake execution dialog
     *
     * @returns {void}
     */
    _openRetakeExecutionDialog(): void {
        this._loaderOverlay.hide();
        const dialogRef = this._dialog.open(SkillsQuestionnaireDialogComponent, {
            data: {lastResult: this.lastResult, lastAttempt: this.lastAttempt}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result === 'cancel') {
                // Cancel retake
                this._loaderOverlay.show();
                this._router.navigate([`/candidate/skills/result/${this.testId}/${this.testSlug}`]).then(() => this._loaderOverlay.hide());
            } else if (result) {
                // Continue last attempt
                if (this.lastAttempt && this.lastAttempt.length > 0) {
                    this.questionnaire.results = this.lastAttempt;
                }
                this.questionnaire.execution = this.previousExecutions[0];
            } else {
                // Start again
                this._loaderOverlay.show();
                this._deletePreviousExecutions(this.previousExecutions);
                this._newExecutionTest(+this.testId);
            }
        });
    }

    /**
     * Creates a new test execution
     *
     * @param testId
     * @returns {void}
     */
    _newExecutionTest(testId: number): void {
        const execution = {'test_id': testId};
        this._skillsQuestionnaireService.newTestExecution(execution).then(
            res => {
                this.questionnaire.execution = res;
            }).then(() => this._loaderOverlay.hide());
    }

    /**
     * Deletes all previous test executions
     *
     * @param executions
     * @returns {void}
     */
    _deletePreviousExecutions(executions: TestExecution[] = []): void {
        for (const execution of executions) {
            this._skillsQuestionnaireService.deleteTestExecution(execution);
        }
    }

    /**
     * Filters by status id
     *
     * @param item
     * @param statusId
     * @returns {boolean}
     */
    private _filterByStatus(item: any, statusId: number): boolean {
        return +item.status === +statusId;
    }

    /**
     * Filters by statuses id
     *
     * @param item
     * @param statusIds
     * @returns {boolean}
     */
    private _filterByStatuses(item: any, statusIds: number[]): any {
        return statusIds.indexOf(item.status) !== -1;
    }

    /**
     * Converts array of 1 item to singleton
     *
     * @param array
     * @returns {void}
     */
    private _singletonize(array: any): any {
        if (array && array.length > 0) {
            for (const item of array) {
                return item;
            }
        } else {
            return;
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Go to step
     *
     * @param step
     */
    gotoStep(step): void {
        // Decide the animation direction
        this.animationDirection = this.currentStep < step ? 'left' : 'right';

        // Run change detection so the change
        // in the animation direction registered
        this._changeDetectorRef.detectChanges();

        // Set the current step
        this.currentStep = step;
    }

    /**
     * Go to next step
     */
    gotoNextStep(): void {
        if (this.currentStep === this.questionnaire.questions.length - 1) {
            return;
        }

        // Set the animation direction
        this.animationDirection = 'left';

        // Run change detection so the change
        // in the animation direction registered
        this._changeDetectorRef.detectChanges();

        // Increase the current step
        this.currentStep++;
    }

    /**
     * Go to previous step
     */
    gotoPreviousStep(): void {
        if (this.currentStep === 0) {
            return;
        }

        // Set the animation direction
        this.animationDirection = 'right';

        // Run change detection so the change
        // in the animation direction registered
        this._changeDetectorRef.detectChanges();

        // Decrease the current step
        this.currentStep--;
    }


    scrollToTest(): void {
        try {
            const deviceInfo = this._deviceService.getDeviceInfo();
            // console.log(deviceInfo);
            if (deviceInfo.browser !== 'IE') {
                setTimeout(() => {
                    this.myScrollContainer.nativeElement.scrollIntoView(false);
                });
            }
        } catch (err) { console.log(err); }
    }
    /**
     * Submit the results
     *
     * @param execution
     */
    submitExecution(execution): void {
        if (!this.submitted) {
            this.submitted = true;
            this._loaderOverlay.show();
            const executionCopy = Object.assign({}, execution);
            executionCopy.status = 2;
            this._skillsQuestionnaireService.submitTestExecution(executionCopy).then(res => {
                this._skillsResultService.setSkillsResults(res);
                this._router.navigate([`/candidate/skills/result/${this.testId}/${this.testSlug}`]).then(() => this._loaderOverlay.hide());
            }).catch( err => {
                // do something in case of error
                this._loaderOverlay.hide();
                this.submitted = false;
                throw new Error(err);
            });
        }
    }

    /**
     * Save the answer(s)
     *
     * @param answer
     */
    saveAnswer(answer: Answer | Answer[]): void {
        this._skillsQuestionnaireService.saveSkillAnswer(answer);
    }

    /**
     * Toggle the sidebar
     *
     * @param name
     */
    toggleSidebar(name): void {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }
}

@Component({
    selector: 'candidate-skills-questionnaire-dialog',
    templateUrl: './questionnaire.dialog.component.html',
    styleUrls: ['./questionnaire.component.scss'],
})
export class SkillsQuestionnaireDialogComponent {
    constructor(
        private _dialogRef: MatDialogRef<SkillsQuestionnaireDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
        _dialogRef.disableClose = true;
    }
}
