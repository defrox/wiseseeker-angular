import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {AnswerApiService, AuthenticationService, IpvQuestionApiService, TestExecutionApiService, TestApiService} from 'app/_services';
import {Answer, TestExecution, User} from 'app/_models';

@Injectable()
export class SkillsQuestionnaireService implements Resolve<any> {
    onQuestionnaireChanged: BehaviorSubject<any>;
    onTestExecutionChanged: BehaviorSubject<any>;
    questionnaire: any;
    testExecution: any;
    currentUserSubscription: Subscription;
    currentUser: User;
    testId: 1;

    /**
     * Constructor
     *
     * @param {IpvQuestionApiService} _ipvQuestionApiService
     * @param {TestExecutionApiService} _testExecutionApiService
     * @param {TestApiService} _testApiService
     * @param {AnswerApiService} _answerApiService
     * @param {AuthenticationService} _authenticationService
     */
    constructor(
        private _ipvQuestionApiService: IpvQuestionApiService,
        private _testExecutionApiService: TestExecutionApiService,
        private _testApiService: TestApiService,
        private _answerApiService: AnswerApiService,
        private _authenticationService: AuthenticationService
    ) {
        // Set the defaults
        this.onQuestionnaireChanged = new BehaviorSubject({});
        this.onTestExecutionChanged = new BehaviorSubject({});
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getQuestionnaire(route.params.testId ? route.params.testId : this.testId)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get IPV questionnaire
     *
     * @param testId
     * @returns {Promise<any>}
     */
    getQuestionnaire(testId: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testApiService.get(testId)
                .subscribe((questionnaire: any) => {
                    this.questionnaire = questionnaire;
                    this.onQuestionnaireChanged.next(this.questionnaire);
                    resolve(this.questionnaire);
                }, reject);
        });
    }

    /**
     * Get IPV question
     *
     * @param questionId
     * @returns {Observable<any>}
     */
    getSkillQuestion(questionId: number): Observable<any>
    {
        return this._ipvQuestionApiService.get(questionId).catch(this.handleError);
    }

    /**
     * Get all test executions
     *
     * @param testId
     * @returns {Promise<any>}
     */
    getAllTestExecutions(testId: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testExecutionApiService.getByUserAndTest(+this.currentUser.id, testId)
                .subscribe((executions: any) => {
                    this.testExecution = executions;
                    // this.testExecution = executions.filter(item => this._filterByTestId(item, testId));
                    this.onTestExecutionChanged.next(this.testExecution);
                    resolve(this.testExecution);
                }, reject);
        });
    }

    /**
     * Get test execution by id
     *
     * @param executionId
     * @returns {Observable<any>}
     */
    getTestExecution(executionId: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testExecutionApiService.get(executionId)
                .subscribe((execution: any) => {
                    this.testExecution = execution;
                    this.onTestExecutionChanged.next(this.testExecution);
                    resolve(this.testExecution);
                }, reject);
        });
    }

    /**
     * Save IPV answer
     *
     * @param answer
     */
    saveSkillAnswer(answer: Answer): void
    {
        this._answerApiService.add(answer).toPromise().then();
    }

    /**
     * New test execution
     *
     * @param execution
     */
    newTestExecution(execution: TestExecution): any
    {
        return this._testExecutionApiService.add(execution).toPromise().then(res => res);
    }

    /**
     * Deletes test executions
     *
     * @param execution
     */
    deleteTestExecution(execution: TestExecution): void
    {
        this._testExecutionApiService.delete(execution).toPromise().then();
    }

    /**
     * Submit test executions
     *
     * @param execution
     */
    submitTestExecution(execution: TestExecution): any
    {
        return this._testExecutionApiService.update(execution).toPromise().then(res => res);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private _filterByExecutionId(item: any, filteredId: number): any {
        return +item.execution.id === +filteredId;
    }

    private _filterByTestIdAndStatus(item: any, testId: number, statusIds: number[]): any {
        return +item.test.id === +testId && statusIds.indexOf(item.status) !== -1;
    }

    private _filterByStatus(item: any, statusIds: number[]): any {
        return statusIds.indexOf(item.status) !== -1;
    }

    private _filterByTestId(item: any, filteredId: number): any {
        return +item.test.id === +filteredId;
    }

    private _filterById(item: any, filteredId: number): any {
        return +item.id === +filteredId;
    }

    private handleError(error: any): any {
        return throwError(error);
    }
}
