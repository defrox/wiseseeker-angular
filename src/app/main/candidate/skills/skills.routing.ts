import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {SkillsStartComponent} from 'app/main/candidate/skills/start';
import {SkillsResultComponent, SkillsResultService} from 'app/main/candidate/skills/result';
import {SkillsQuestionnaireComponent, SkillsQuestionnaireDialogComponent, SkillsQuestionnaireService} from 'app/main/candidate/skills/questionnaire';
import {SkillsHomeComponent, SkillsHomeService} from 'app/main/candidate/skills/home';
import {MaterialModule} from 'app/_helpers/material.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseSidebarModule, FuseWidgetModule} from '@fuse/components';
import {TranslateModule} from '@ngx-translate/core';
import {ChartModule} from 'angular-highcharts';
import {DefroxModule} from 'app/_helpers';
import {AuthGuard} from 'app/_guards';
import {Group} from 'app/_models';

const appRoutes: Routes = [
    {path: 'candidate/skills', children: [
        {path: '', component: SkillsHomeComponent, resolve: {home: SkillsHomeService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'start', component: SkillsStartComponent, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'start/:testId/:testSlug', component: SkillsStartComponent, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'questionnaire', component: SkillsQuestionnaireComponent, resolve: {questionnaire: SkillsQuestionnaireService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'questionnaire/:testId/:testSlug', component: SkillsQuestionnaireComponent, resolve: {questionnaire: SkillsQuestionnaireService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'result', component: SkillsResultComponent, resolve: {result: SkillsResultService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'result/:testId/:testSlug', component: SkillsResultComponent, resolve: {result: SkillsResultService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        // otherwise redirect to 404
        {path: '**', redirectTo: '/error/404', pathMatch: 'full'}
    ]}
];

@NgModule({
    declarations: [
        SkillsStartComponent,
        SkillsQuestionnaireComponent,
        SkillsResultComponent,
        SkillsHomeComponent,
        SkillsQuestionnaireDialogComponent,
        ],
    imports: [
        RouterModule.forChild(appRoutes),
        MaterialModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseWidgetModule,
        TranslateModule,
        ChartModule,
        DefroxModule
    ],
    providers: [
        SkillsQuestionnaireService,
        SkillsResultService,
        SkillsHomeService,
        AuthGuard
    ],
    entryComponents: [SkillsQuestionnaireDialogComponent],
    exports: [RouterModule]
})
export class SkillsRouting { }
