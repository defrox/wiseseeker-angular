import {Component, OnInit, OnDestroy, ViewEncapsulation} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {TranslateService} from '@ngx-translate/core';
import {User, TestExecution} from 'app/_models';
import {AppConfigService, AuthenticationService, NotificationService} from 'app/_services';
import {SkillsResultService} from './result.service';
import {Chart} from 'angular-highcharts';
import {centeredGaugeFormatter, radarTooltipFormatter} from 'app/_helpers/formatters';
import * as _ from 'lodash';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'skills-result',
    templateUrl: './result.component.html',
    styleUrls: ['./result.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class SkillsResultComponent implements OnInit, OnDestroy {
    appConfig: any;
    currentUser: User;
    currentUserSubscription: Subscription;
    skillsResults: any;
    lastExecution: any;
    currentExecution: any;
    assessment: any;
    widgets: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {SkillsResultService} _skillsResultService
     * @param {AuthenticationService} _authenticationService
     * @param {AppConfigService} _appConfigService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslateService} _translateService
     * @param {NotificationService} _notificationsService
     */
    constructor(
        private _skillsResultService: SkillsResultService,
        private _authenticationService: AuthenticationService,
        private _appConfigService: AppConfigService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translateService: TranslateService,
        private _notificationsService: NotificationService
    ) {
        // Set the private defaults
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
        this._appConfigService.config.subscribe(config => { this.appConfig = config; });
        this._translationLoader.loadTranslations(spanish, english);
        this._notificationsService.resetUserNotifications();
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to personality
        this._skillsResultService.onTestExecutionChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(executions => {
                this.lastExecution = this._getLastAssessedExecution(executions);
                this.assessment = this._getAssessment(this.lastExecution);
                this.widgets = this._formatWidgets(this.assessment);
            });

        // Subscribe to result
        this._skillsResultService.onCurrentExecutionChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(execution => {
                this.currentExecution = execution;
                if ((!this.lastExecution && this.currentExecution.id) || (this.currentExecution.id && this.currentExecution.id !== this.lastExecution.id)) {
                    this._skillsResultService.getTestExecution(this.currentExecution.id)
                        .then(updatedExecution => {
                            if (updatedExecution.status === 3) {
                                this.lastExecution = updatedExecution;
                                this.assessment = this._getAssessment(this.lastExecution);
                                this.widgets = this._formatWidgets(this.assessment);
                            }
                        });
                }
            });

        this._skillsResultService.getSkillsResults()
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(result => {
                if (result && result.id) {
                    this.skillsResults = result;
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();

        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get last assessed execution
     *
     * @param executions
     * @returns {execution}
     */
    _getLastAssessedExecution(executions: TestExecution[] = []): TestExecution | null {
        if (executions.length === 0) {
            return;
        }
        return executions.filter(item => +item.status === 3)
            .sort((a, b) => {
                a = a.created;
                b = b.created;
                if (a > b) {
                    return -1;
                } else if (a < b) {
                    return 1;
                }
                return 0;
            })[0];
    }

    /**
     * Get execution assessment
     *
     * @param execution
     * @returns {execution}
     */
    _getAssessment(execution: TestExecution = null): TestExecution | null {
        if (!execution || execution.assessed.length === 0) {
            return;
        }
        return execution.assessed;
    }

    _formatWidgets(data): any {
        const widgets = [];
        if (data) {
            const optionWeb = _.cloneDeep(this.appConfig.spiderWebOptions);
            let itemCounter = 0;
            let parentCounter = 0;
            let lastParentId = 0;
            optionWeb.xAxis.categories = [];
            optionWeb.series[0].data = [];
            for (const item of data) {
                itemCounter ++;
                if (itemCounter > 9) {
                    itemCounter = 0;
                }
                let option = _.cloneDeep(this.appConfig.solidDonutOptions);
                if (item.skill.order === 2 || item.skill.order === 3) {
                    option = _.cloneDeep(this.appConfig.solidDonut2Options);
                }
                option.title.text = this._translateService.instant('MAIN.SKILLS.' + item.skill.title);
                option.title = null;
                option.yAxis.max = item.maximum;
                option.series.data = [];
                optionWeb.title.text = item.skill.description;
                optionWeb.yAxis.max = item.maximum;
                optionWeb.yAxis.tickInterval = item.maximum * 0.5;
                optionWeb.yAxis.minorTickInterval = item.maximum * 0.25;
                if (item.skill.order === 1) {
                    itemCounter = 0;
                    option.series = [{data: [item.punctuation]}];
                    option.yAxis.stops = [
                        [0.5, this.appConfig.solidDonutOptionsColors[item.skill.order].min],
                        [0.75, this.appConfig.solidDonutOptionsColors[item.skill.order].middle],
                        [1, this.appConfig.solidDonutOptionsColors[item.skill.order].max]
                    ];
                    option.yAxis.plotBands = [
                        { from: 0, to: (item.maximum * 0.5), color: this.appConfig.solidDonutOptionsColors[1].min, innerRadius: '107.5%', outerRadius: '115%'},
                        { from: (item.maximum * 0.5), to: (item.maximum * 0.75), color: this.appConfig.solidDonutOptionsColors[1].middle, innerRadius: '107.5%', outerRadius: '115%'},
                        { from: (item.maximum * 0.75), to: item.maximum, color: this.appConfig.solidDonutOptionsColors[1].max, innerRadius: '107.5%', outerRadius: '115%'},
                    ];
                    widgets.push({
                        skill: item.skill,
                        chart: new Chart(option as any)
                    });
                } else if (item.skill.order === 2 || item.skill.order === 3) {
                    itemCounter = 0;
                    option.series = [{data: [{y: item.punctuation, innerRadius: '85%'}]}];
                    option.yAxis.stops = [
                        [0.5, this.appConfig.solidDonutOptionsColors[item.skill.order].min],
                        [1, this.appConfig.solidDonutOptionsColors[item.skill.order].max]
                    ];
                    widgets.push({
                        skill: item.skill,
                        chart: new Chart(option as any)
                    });
                } else {
                    if (item.parent_id && item.parent_id > 0 && item.parent_id !== lastParentId) {
                        parentCounter++;
                        lastParentId = item.parent_id;
                    } else { // Remove
                        if (itemCounter === 1) {
                            parentCounter = 0;
                        } else if (itemCounter > 1 && itemCounter <= 5) {
                            parentCounter = 3;
                        } else {
                            parentCounter = 2;
                        }
                        // Remove EOF
                    }
                    const parentColor = parentCounter >= 0 ? parentCounter : itemCounter;
                    optionWeb.title.text = this._translateService.instant('MAIN.PSICOLOGICAL_PROFILE.TITLE');
                    optionWeb.xAxis.categories.push('<span class="spider-bullet" style="color:' + this.appConfig.solidDonutOptionsColors[parentColor].middle + '">●</span> <span class="spider-title-text" style="color:' + this.appConfig.solidDonutOptionsColors[parentColor].middle + '">' + item.skill.description + '</span>');
                    // optionWeb.xAxis.categories.push(item.skill.description);
                    optionWeb.xAxis.className = 'spider-title';
                    optionWeb.series[0].data.push(item.punctuation);
                    optionWeb.tooltip.formatter = radarTooltipFormatter;
                    // console.log(optionWeb);
                }
            }
            optionWeb.title = null;
            widgets.push({
                skill: {id: 4, order: 4},
                chart: new Chart(optionWeb as any)
            });
        }
        return widgets;
    }
}
