import {Component, OnInit, OnDestroy, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {TestExecution, User} from 'app/_models';
import {Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {AuthenticationService} from 'app/_services';
import {ActivatedRoute} from '@angular/router';
import {SkillsHomeService} from './home.service';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'candidate-skills-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class SkillsHomeComponent {
    testId: string | number | null;
    testSlug: string | null;
    currentUser: User;
    currentUserSubscription: Subscription;
    lastExecution: any;
    // testId = 1;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {SkillsResultService} skillsHomeService
     * @param {AuthenticationService} _authenticationService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {ActivatedRoute} _route
     * @param {Router} _router
     */
    constructor(
        private skillsHomeService: SkillsHomeService,
        private _authenticationService: AuthenticationService,
        private _translationLoader: FuseTranslationLoaderService,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        // Set the private defaults
        this._unsubscribeAll = new Subject();
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
        this._translationLoader.loadTranslations(spanish, english);
        this._route.paramMap.subscribe(params => {
            this.testId = params.get('testId') ? params.get('testId') : 1;
            this.testSlug = params.get('testSlug') ? params.get('testSlug') : 'ipv';
            this.skillsHomeService.getAllTestExecutions(+this.testId, [2, 3])
                .then(executions => {
                    this.lastExecution = this._getLastAssessedExecution(executions);
                    if (this.lastExecution) {
                        this._router.navigate([`/candidate/skills/result/${this.testId}/${this.testSlug}`]);
                    }
                });
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to last execution
        this.skillsHomeService.onTestExecutionChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(executions => {
                this.lastExecution = this._getLastAssessedExecution(executions);
                if (this.lastExecution) {
                    this._router.navigate([`/candidate/skills/result/${this.testId}/${this.testSlug}`]);
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();

        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get last assessed execution
     *
     * @param executions
     * @returns {execution}
     */
    _getLastAssessedExecution(executions: TestExecution[] = []): TestExecution | null {
        if (executions.length === 0) {
            return;
        }
        return executions.filter(item => +item.status === 3)
            .sort((a, b) => {
                a = a.created;
                b = b.created;
                if (a > b) {
                    return -1;
                } else if (a < b) {
                    return 1;
                }
                return 0;
            })[0];
    }
}
