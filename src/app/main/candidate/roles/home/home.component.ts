import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {Role} from 'app/_models';
import {Subject} from 'rxjs';
import {RolesHomeService} from './home.service';
import {takeUntil} from 'rxjs/operators';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'candidate-roles-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class RolesHomeComponent implements OnInit, OnDestroy {
    userRoles: Role[];
    categories: any[] = [];
    types = [{title: 'MAIN.ROLES.TYPES.DEFAULT', type: 'default'}, {title: 'MAIN.ROLES.TYPES.CUSTOM', type: 'custom'}];
    rolesFilteredByCategory: any[];
    filteredRoles: any[];
    currentCategory: string;
    currentType: string;
    searchTerm: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {RolesHomeService} _rolesHomeService
     * @param {FuseTranslationLoaderService} _translationLoader
     */
    constructor(
        private _rolesHomeService: RolesHomeService,
        private _translationLoader: FuseTranslationLoaderService
    ) {
        // Set the defaults
        this.currentCategory = this.currentType = 'all';
        this.searchTerm = '';

        // Set the private defaults
        this._translationLoader.loadTranslations(spanish, english);
        this._unsubscribeAll = new Subject();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to categories
        this._rolesHomeService.onRolesCategoriesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(categories => {
                this.categories = categories;
            });

        // Subscribe to user roles
        this._rolesHomeService.onRolesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(roles => {
                this.filteredRoles = this.rolesFilteredByCategory = this.userRoles = roles;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Filter roles by category
     */
    filterRolesByCategory(): void {
        // Filter
        if (this.currentCategory === 'all') {
            this.rolesFilteredByCategory = this.userRoles;
            this.filteredRoles = this.userRoles;
        } else {
            this.rolesFilteredByCategory = this.userRoles.filter((knowledge) => {
                return knowledge.category === this.currentCategory;
            });
            this.filteredRoles = [...this.rolesFilteredByCategory];
        }
        if (this.currentType === 'all') {
            this.filteredRoles = this.rolesFilteredByCategory;
        } else {
            this.rolesFilteredByCategory = this.rolesFilteredByCategory.filter((knowledge) => {
                const isCustom = knowledge.tests && knowledge.tests.length > 0 && knowledge.tests[0].hasOwnProperty('custom');
                return (knowledge.custom && this.currentType === 'custom') || (!knowledge.custom && this.currentType === 'default');
            });
            this.filteredRoles = [...this.rolesFilteredByCategory];
        }
        // Re-filter by search term
        this.filterRolesByTerm();
    }

    /**
     * Filter roles by term
     */
    filterRolesByTerm(): void {
        const searchTerm = this.searchTerm.toLowerCase();

        // Search
        if (searchTerm === '') {
            this.filteredRoles = this.rolesFilteredByCategory;
        } else {
            this.filteredRoles = this.rolesFilteredByCategory.filter((roles) => {
                return roles.title.toLowerCase().includes(searchTerm);
                // return (roles.title.toLowerCase().includes(searchTerm) || roles.description.toLowerCase().includes(searchTerm));
            });
        }
    }
    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------
}
