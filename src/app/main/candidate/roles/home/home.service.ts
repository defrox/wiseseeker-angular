import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {AuthenticationService, RoleApiService, RoleCategoryApiService, TestApiService, TestExecutionApiService} from 'app/_services';
import {User, Role} from 'app/_models';


@Injectable()
export class RolesHomeService implements Resolve<any> {
    onRolesChanged: BehaviorSubject<any>;
    onRolesCategoriesChanged: BehaviorSubject<any>;
    executions: any;
    roles: any;
    rolesExecutions: Role[];
    tests: any;
    currentUserSubscription: Subscription;
    currentUser: User;

    /**
     * Constructor
     *
     * @param {RoleApiService} _rolesApiService
     * @param {RoleCategoryApiService} _rolesCategoryApiService
     * @param {TestApiService} _testApiService
     * @param {TestExecutionApiService} _testExecutionApiService
     * @param {AuthenticationService} _authenticationService
     */
    constructor(
        private _rolesApiService: RoleApiService,
        private _rolesCategoryApiService: RoleCategoryApiService,
        private _testApiService: TestApiService,
        private _testExecutionApiService: TestExecutionApiService,
        private _authenticationService: AuthenticationService
    ) {
        // Set the defaults
        this.onRolesChanged = new BehaviorSubject({});
        this.onRolesCategoriesChanged = new BehaviorSubject({});
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getAllRolesExecutions(+this.currentUser.id),
                this.getAllRolesCategories()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get all roles executions
     *
     * @param testId
     * @returns {Promise<any>}
     */
    getAllRolesExecutions(testId: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testExecutionApiService.getByUser(+this.currentUser.id)
                .subscribe((executions: any) => {
                    this.executions = executions;
                    this.getAllRoles(+this.currentUser.id).then(
                        roles => {
                            this.roles = roles;
                            this.rolesExecutions = [];
                            for (const role of roles) {
                                const tmpRoleExecutions = [];
                                let tmpPunctuation = 0;
                                role.execution = { status: 0 };
                                for (const test of role.tests) {
                                    const tmpRoleTestExecutions = this.executions.filter(item => test.id === item.test.id);
                                    if (tmpRoleTestExecutions.length > 0) {
                                        tmpRoleExecutions.push(tmpRoleTestExecutions[0]);
                                        if  (tmpRoleTestExecutions[0].assessed) {
                                            tmpPunctuation = (100 * tmpRoleTestExecutions[0].assessed[0].punctuation ) / tmpRoleTestExecutions[0].assessed[0].maximum;
                                        }
                                    }
                                    role.execution.assessed = {minimum: 0, maximum: 100, punctuation: tmpPunctuation};
                                    role.execution.marks = tmpRoleTestExecutions[0].marks;
                                }
                                role.execution.status = Math.floor(tmpRoleExecutions.reduce((accumulator, currentValue) => accumulator + currentValue.status, 0) / tmpRoleExecutions.length);
                                role.executions = tmpRoleExecutions;
                                this.rolesExecutions.push(role);
                            }
                            this.onRolesChanged.next(this.rolesExecutions);
                            resolve(this.rolesExecutions);
                        });
                }, reject);
        });
    }

    /**
     * Get all roles categories
     *
     * @returns {Promise<any>}
     */
    getAllRolesCategories(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._rolesCategoryApiService.getAll()
                .subscribe((categories: any) => {
                    this.onRolesCategoriesChanged.next(categories);
                    resolve(categories);
                }, reject);
        });
    }

    /**
     * Get all roles
     *
     * @param userId
     * @returns {Promise<any>}
     */
    getAllRoles(userId: number): Promise<any>
    {
        return this._rolesApiService.getAll().toPromise().then(res => res);
    }

    /**
     * Get roles
     *
     * @param userId
     * @returns {Promise<any>}
     */
    getAllTests(userId: number): Promise<any>
    {
        return this._testApiService.getAll().toPromise().then(res => res);
    }

    /**
     * Set Roles
     *
     * @param homes
     */
    setRoles(homes): void {
        this.onRolesChanged.next(homes);
    }

    /**
     * Get Roles
     *
     * @returns {Observable<any>}
     */
    getRoles(): Observable<any> {
        return this.onRolesChanged.asObservable();
    }

    /**
     * Reset Roles
     */
    resetRoles(): void {
        this.onRolesChanged.next(null);
    }

    /**
     * Get all roles categories
     *
     * @returns {Promise<any>}
     */
    getRolesCategories(): Promise<any>
    {
        return this._rolesCategoryApiService.getAll().toPromise().then(res => res);
    }

    /**
     * Get roles categori bi id
     *
     * @returns {Promise<any>}
     */
    getRolesCategory(categoryId: number): Promise<any>
    {
        return this._rolesCategoryApiService.get(categoryId).toPromise().then(res => res);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private _filterByTestId(item: any, testId: number): any {
        return item.filter(obj => obj.id === testId).length > 0;
        // return item.indexOf(testId) !== -1;
    }

    private _getFirst(array, n = 1): any | null {
        return n > 0 && array.length > n ? array.slice(0, n) : array;
    }

    private handleError(error: any): any {
        return throwError(error);
    }
}
