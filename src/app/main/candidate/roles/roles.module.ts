import {NgModule} from '@angular/core';
import {RolesRouting} from 'app/main/candidate/roles/roles.routing';
import {ApiModule, DefroxModule} from 'app/_helpers';

@NgModule({
    declarations: [],
    imports: [
        ApiModule,
        DefroxModule,
        RolesRouting
    ]
})
export class RolesModule {
}
