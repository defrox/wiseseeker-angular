import {ChangeDetectorRef, Component, HostListener, OnDestroy, OnInit, QueryList, ViewChildren, ViewChild, ViewEncapsulation, Inject} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {ActivatedRoute, Router} from '@angular/router';
import {RoleQuestionnaireService} from './questionnaire.service';
import {RoleResultService} from 'app/main/candidate/roles/result';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {TranslatePipe} from '@ngx-translate/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Answer, TestExecution, DialogData, Question} from 'app/_models';
import {ExitTestVarComponent} from 'app/_components';
import {startTimer} from 'app/_helpers';
import {LoaderOverlayService, NotificationService, SnackAlertService} from 'app/_services';
import {CountdownComponent} from 'ngx-countdown';
import {appConfig} from 'app/app.config';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'candidate-roles-questionnaire',
    templateUrl: './questionnaire.component.html',
    styleUrls: ['./questionnaire.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class RoleQuestionnaireComponent extends ExitTestVarComponent implements OnInit, OnDestroy {
    animationDirection: 'left' | 'right' | 'none';
    questionnaire: any;
    nextQuestion: any;
    currentStep: number;
    role: any;
    roleId: string | number | null;
    roleSlug: string | null;
    testId: string | number | null;
    previousExecutions: any | null;
    lastResult: any | null;
    lastAttempt: any | null;
    submitted = false;
    questionnaireReady = false;
    appConfig = appConfig;
    timerConfig = appConfig.timerConfig;
    timerShow = false;
    timerClass = 'snack-alert-success';
    timerFinish = false;
    questionAnswered = false;
    timeTotal = 180;
    timeLeft = 0;
    startTimeLeft = 0;
    timerProgress$: Observable<number>;
    timerProgressColor = 'primary';
    timerTimeStamp: Date;

    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild('timer')
    private countdown: CountdownComponent;

    // Private
    private _unsubscribeAll: Subject<any>;

    @HostListener('document:keydown', ['$event'])
    handleKeyboardEvent($event: KeyboardEvent): void {
        if ($event.key === 'f' && $event.ctrlKey && $event.altKey) {
            this.currentStep = this.questionnaire.questions.length - 1;
        }
    }

    @HostListener('window:focus', ['$event'])
    onFocus(event: FocusEvent): void {
        if (this.timerShow && this.timeLeft) {
            const timeNow = new Date();
            const elapsedTime = (timeNow.getTime() - +(this.timerTimeStamp ? this.timerTimeStamp : 0)) / 1000;
            const newTimeLeft = this.startTimeLeft - elapsedTime;
            // this.timeLeft = newTimeLeft;
            // console.log('Elapsed: ' + elapsedTime + ' Time Left: ' + this.timeLeft + ' New Time Left: ' + newTimeLeft);
            this.timerProgress$ = startTimer(newTimeLeft, this.timeTotal, 100, this);
        }
    }

    @HostListener('window:blur', ['$event'])
    onBlur(event: FocusEvent): void {
        // console.log(event);
        // this.timerTimeStamp = new Date();
    }
    /**
     * Constructor
     *
     * @param {RoleQuestionnaireService} _roleQuestionnaireService
     * @param {RoleResultService} _roleResultService
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslatePipe} _translatePipe
     * @param {NotificationService} _notificationsService
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {SnackAlertService} _snackAlertService
     * @param {MatDialog} _dialog
     * @param {ActivatedRoute} _route
     * @param {Router} _router
     */
    constructor(
        private _roleQuestionnaireService: RoleQuestionnaireService,
        private _roleResultService: RoleResultService,
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseSidebarService: FuseSidebarService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translatePipe: TranslatePipe,
        private _notificationsService: NotificationService,
        private _loaderOverlay: LoaderOverlayService,
        private _snackAlertService: SnackAlertService,
        private _dialog: MatDialog,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        super(_router);
        // Set the defaults
        this._loaderOverlay.show();
        this._cleanUp();
        this.animationDirection = 'none';
        this.currentStep = 0;
        this._translationLoader.loadTranslations(spanish, english);
        this._route.paramMap.subscribe(params => {
            this.roleId = params.get('roleId');
            this.roleSlug = params.get('roleSlug');
            this.testId = params.get('testId');
        });
        this._roleQuestionnaireService.getAllTestExecutions(+this.testId)
            .then(executions => {
                if (executions.length > 0) {
                    this.previousExecutions = executions;
                    const lastFinishedExecution = this._getLastFinishedExecution(executions);
                    if (lastFinishedExecution) {
                        this.lastResult = lastFinishedExecution.answers;
                        // Go to results
                        this._router.navigate([`/candidate/roles/result/${this.roleId}/${this.testId}/${this.roleSlug}`]).then(() => this._loaderOverlay.hide());
                    }
                    const lastUnfinishedExecution = this._getLastUnfinishedExecution(executions);
                    if (lastUnfinishedExecution) {
                        this.lastAttempt = lastUnfinishedExecution.answers;
                        this._continueLastAttempt();
                    }
                    // this._openRetakeExecutionDialog();
                } else {
                    this._newExecutionTest(+this.testId);
                }
                this._loaderOverlay.hide();
            });

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to questionnaires
        this._roleQuestionnaireService.onRoleChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe( role => {
                this.role = role;
                this.questionnaire = role;
                this.questionnaire.execution = {id: 0};
            });

        // Subscribe to questionnaires
        this._roleQuestionnaireService.onQuestionnaireChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(questionnaire => {
                this.questionnaire.questions = questionnaire.questions;
            });

        // Subscribe to next question
        this._roleQuestionnaireService.onNextQuestionChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(nextQuestion => {
                this.nextQuestion = nextQuestion;
                if (nextQuestion && +nextQuestion.type === 3) {
                    const nextResult = this._formatNextResult(nextQuestion);
                }
            });

        // Subscribe to results
        this._roleResultService.onResultChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(results => {
                this.questionnaire.results = results;
            });

        // Set timer configuration
        this.timerConfig = appConfig.timerConfig;
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Save current state of the execution
        if (!this.submitted && !this.lastResult && this.appConfig.appSettings.enableContinueTest && !this.questionnaire.execution  && this.questionnaire.execution.id !== 0) {
            this._saveExecution(this.questionnaire.execution).then();
        }
        // Unsubscribe from all subscriptions
        this._cleanUp();
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this._notificationsService.resetUserNotifications();
    }

    /**
     * @CanDeactivate Confirm exit dialog
     */
    confirmExit(): Promise<any> {
        return new Promise((resolve, reject) => {
            const dialogRef = this._dialog.open(RolesQuestionnaireDialogComponent, {
                data: {confirmExit: true}
            });
            dialogRef.afterClosed().subscribe(result => {
                if (result) {
                    this._loaderOverlay.show();
                    this._saveExecution(this.questionnaire.execution, 2).then(() => {
                        this._router.navigate([`/candidate/roles/result/${this.roleId}/${this.testId}/${this.roleSlug}`]).then(() => {
                            this._loaderOverlay.hide();
                            resolve(result);
                        });
                    });
                } else {
                    resolve(result);
                }
            }, reject);
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Clean up results
     *
     * @returns {void}
     */
    _cleanUp(): void {
        this.questionnaire = null;
        this.previousExecutions = null;
        this.lastResult = null;
        this.lastAttempt = null;
        this.timerConfig = null;
        this.submitted = false;
        // this._roleResultService.resetRoleResults();
    }

    /**
     * Format results
     *
     * @param questions
     * @returns {any}
     */
    _formatResults(questions: Question[] = []): any {
        if (questions.length === 0) {
            return;
        }
        const result = [];
        for (const question of questions) {
            if (+question.type === 3) {
                result[question.id] = [];
                if (question.answers && question.answers.length > 0) {
                    for (const questionAnswer of question.answers){
                        result[question.id][questionAnswer.id] = false;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Format next result
     *
     * @param question
     * @returns {any}
     */
    _formatNextResult(question: Question = null): any {
        if (!question) {
            return;
        }
        const result = [];
        if (+question.type === 3) {
            result[question.id] = [];
            this.questionnaire.results[question.id] = [];
            if (question.answers && question.answers.length > 0) {
                for (const questionAnswer of question.answers){
                    result[question.id][questionAnswer.id] = false;
                    this.questionnaire.results[question.id][questionAnswer.id] = false;
                }
            }
        }
        return result;
    }

    /**
     * Format execution
     *
     * @param answers
     * @returns {any}
     */
    _formatExecution(answers: Answer[] = []): any {
        if (answers.length === 0) {
            return;
        }
        const result = [];
        for (const question of this.questionnaire.questions) {
            if (+question.type === 3) {
                result[question.id] = [];
                if (question.answers && question.answers.length > 0) {
                    for (const questionAnswer of question.answers){
                        result[question.id][questionAnswer.id] = false;
                    }
                }
            }
        }
        for (const answer of answers) {
            const answerType = this._getTypeOfQuestion(answer.question, this.questionnaire.questions);
            if (+answerType === 3) {
                for (const item of answer.answers) {
                    result[answer.question][item] = true;
                }
            } else  {
                for (const item of answer.answers) {
                    result[answer.question] = +item;
                }
            }
        }
        return result;
    }

    /**
     * Get last unfinished execution
     *
     * @param executions
     * @returns {execution}
     */
    _getLastUnfinishedExecution(executions: TestExecution[] = []): TestExecution | null {
        if (executions.length === 0) {
            return;
        }
        return executions.filter(item => +item.status === 1)
            .sort((a, b) => {
                a = a.created;
                b = b.created;
                if (a > b) {
                    return -1;
                } else if (a < b) {
                    return 1;
                }
                return 0;
            })[0];
    }

    /**
     * Get last finished execution
     *
     * @param executions
     * @returns {execution}
     */
    _getLastFinishedExecution(executions: TestExecution[] = []): TestExecution | null {
        if (executions.length === 0) {
            return;
        }
        return executions.filter(item => +item.status === 2 || +item.status === 3)
            .sort((a, b) => {
                a = a.created;
                b = b.created;
                if (a > b) {
                    return -1;
                } else if (a < b) {
                    return 1;
                }
                return 0;
            })[0];
    }

    /**
     * Open retake execution dialog
     *
     * @returns {void}
     */
    _openRetakeExecutionDialog(): void {
        this._loaderOverlay.hide();
        const dialogRef = this._dialog.open(RolesQuestionnaireDialogComponent, {
            data: {lastResult: this.lastResult, lastAttempt: this.lastAttempt}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result === 'cancel') {
                // Cancel retake
                this._loaderOverlay.show();
                this._router.navigate([`/candidate/roles/result/${this.roleId}/${this.testId}/${this.roleSlug}`]).then(() => this._loaderOverlay.hide());
            } else if (result) {
                // Continue last attempt
                this._continueLastAttempt();
            } else {
                // Start again
                this._loaderOverlay.show();
                this._deletePreviousExecutions(this.previousExecutions);
                this._newExecutionTest(+this.testId);
            }
        });
    }

    _continueLastAttempt(): void {
        this.questionnaire.results = this._formatResults(this.questionnaire.questions);
        this.questionnaireReady = true;
        if (this.lastAttempt && this.lastAttempt.length > 0) {
            this.questionnaire.results = this._formatExecution(this.lastAttempt);
        }
        this.questionAnswered = this._checkQuestionAnswered();
        this.questionnaire.execution = this.previousExecutions[0];
        // this.timerConfig.leftTime = this.questionnaire.execution.test.secondsAvailable;
        this.timerConfig.leftTime = this.questionnaire.execution.secondsAvailable;
        this.timeTotal = this.questionnaire.execution.totalTimeInSeconds;
        this._startTimer();
    }

    _checkQuestionAnswered() {
        let isQuestionAnswered = false;
        let currentQuestion = this.questionnaire && this.questionnaire.questions.length > 0 && this.questionnaire.questions[this.currentStep] ? this.questionnaire.questions[this.currentStep] : null;
        console.log(currentQuestion);
        let currentAnswer = this.questionnaire.results.length > 0 && this.questionnaire.results[currentQuestion.id] ? this.questionnaire.results[currentQuestion.id] : null;
        console.log(currentAnswer);
        if (currentAnswer && currentAnswer instanceof Array && currentAnswer.length > 0) {
            for (let answer in currentAnswer) {
                console.log(currentAnswer[answer].toString());
                if (currentAnswer[answer].toString() === 'true') {
                    isQuestionAnswered = true;
                }
            }
        } else if (currentAnswer && !(currentAnswer instanceof Array)) {
            isQuestionAnswered = true;
        }
        console.log(this.questionnaire.results);
        return isQuestionAnswered;
    }

    /**
     * Creates a new test execution
     *
     * @param testId
     * @returns {void}
     */
    _newExecutionTest(testId: number): void {
        this.questionnaire.results = this._formatResults(this.questionnaire.questions);
        this.questionnaireReady = true;
        const execution = {'test_id': testId};
        this._roleQuestionnaireService.newTestExecution(execution).then(
            res => {
                this.submitted = false;
                this.lastResult = false;
                this.questionnaire.execution = res;
                this.questionnaire.results = this._formatResults(this.questionnaire.questions);
                this.questionnaireReady = true;
                this.timerConfig.leftTime = this.timeTotal = this.questionnaire.execution.totalTimeInSeconds;
            }).then(() => {
                this._loaderOverlay.hide();
                this._startTimer();
            });
    }

    _startTimer(): void {
        // this.timerConfig.leftTime = +this.questionnaire.execution.test.secondsAvailable;
        this.timerTimeStamp = new Date();
        this.timerShow = true;
        this.timeLeft = this.startTimeLeft = this.timerConfig.leftTime;
        this._changeDetectorRef.detectChanges();
        this.countdown.restart();
        this.countdown.begin();
        this.timerProgress$ = startTimer(this.timeLeft, this.timeTotal,  100, this);
    }

    _stopTimer(): void {
        if (!this.submitted) {
            this._loaderOverlay.show();
            this.questionnaireReady = false;
            this.timerConfig.leftTime = 0;
            this.timerClass = 'snack-alert-danger';
            this.timerFinish = true;
            this._changeDetectorRef.detectChanges();
            this._saveExecution(this.questionnaire.execution, 2).then(() => {
                this._loaderOverlay.hide();
                const dialogRef = this._dialog.open(RolesQuestionnaireDialogComponent, {
                    data: {timerFinish: true}
                });
                dialogRef.afterClosed().subscribe(() => {
                    this._router.navigate([`/candidate/roles/result/${this.roleId}/${this.testId}/${this.roleSlug}`]).then(() => this._loaderOverlay.hide());
                });
            });
        }
    }

    /**
     * Save the results
     *
     * @param execution
     * @param status
     */
    _saveExecution(execution, status = 1): Promise<any> {
        return new Promise((resolve) => {
            if (!this.submitted) {
                this.submitted = true;
                const executionCopy = Object.assign({}, execution);
                executionCopy.status = status;
                this._roleQuestionnaireService.submitTestExecution(executionCopy).then(res => {
                    this._roleResultService.setCurrentExecution(res);
                    resolve(res);
                }).catch(err => {
                    // do something in case of error
                    this.submitted = false;
                    throw new Error(err);
                });
            }
        });
    }

    /**
     * Deletes all previous test executions
     *
     * @param executions
     * @returns {void}
     */
    _deletePreviousExecutions(executions: TestExecution[] = []): void {
        for (const execution of executions) {
            this._roleQuestionnaireService.deleteTestExecution(execution);
        }
    }

    /**
     * Deletes current test executions
     *
     * @param execution
     * @returns {void}
     */
    _deleteCurrentExecution(execution: TestExecution): any {
        return this._roleQuestionnaireService.deleteTestExecution(execution);
    }

    /**
     * Get type of question
     *
     * @param questionId
     * @param questions
     * @returns {number}
     */
    private _getTypeOfQuestion(questionId: number, questions: any[]): number {
        for (const question of questions) {
            if (+question.id === questionId) {
                return question.type;
            }
        }
        return;
    }

    /**
     * Filters by status id
     *
     * @param item
     * @param statusId
     * @returns {boolean}
     */
    private _filterByStatus(item: any, statusId: number): boolean {
        return +item.status === +statusId;
    }

    /**
     * Filters by statuses id
     *
     * @param item
     * @param statusIds
     * @returns {boolean}
     */
    private _filterByStatuses(item: any, statusIds: number[]): any {
        return statusIds.indexOf(item.status) !== -1;
    }

    /**
     * Converts array of 1 item to singleton
     *
     * @param array
     * @returns {void}
     */
    private _singletonize(array: any): any {
        if (array && array.length > 0) {
            for (const item of array) {
                return item;
            }
        } else {
            return;
        }
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Go to step
     *
     * @param step
     */
    gotoStep(step): void {
        // Decide the animation direction
        this.animationDirection = this.currentStep < step ? 'left' : 'right';

        // Run change detection so the change
        // in the animation direction registered
        this._changeDetectorRef.detectChanges();

        // Set the current step
        this.currentStep = step;
    }

    /**
     * Go to next step
     */
    gotoNextStep(): void {
        if (this.currentStep === this.questionnaire.questions.length - 1) {
            return;
        }

        // Preload the next question
        if (this.nextQuestion) {
            this.questionnaire.questions[this.currentStep + 1] = this.nextQuestion;
        }

        // Reset the validation
        this.questionAnswered = false;

        // Set the animation direction
        this.animationDirection = 'left';

        // Run change detection so the change
        // in the animation direction registered
        this._changeDetectorRef.detectChanges();

        // Increase the current step
        this.currentStep++;

        // Checks if the question has a result and allows to go to next question
        this.questionAnswered = this._checkQuestionAnswered();
    }

    /**
     * Go to previous step
     */
    gotoPreviousStep(): void {
        if (this.currentStep === 0) {
            return;
        }

        // Set the animation direction
        this.animationDirection = 'right';

        // Run change detection so the change
        // in the animation direction registered
        this._changeDetectorRef.detectChanges();

        // Decrease the current step
        this.currentStep--;

        // Disable nav buttons
        this.questionAnswered = false;
    }

    /**
     * Submit the results
     *
     * @param execution
     */
    submitExecution(execution): void {
        if (!this.submitted) {
            this.submitted = true;
            this._loaderOverlay.show();
            const executionCopy = Object.assign({}, execution);
            executionCopy.status = 2;
            this._roleQuestionnaireService.submitTestExecution(executionCopy).then(res => {
                this._roleResultService.setCurrentExecution(res);
                this._router.navigate([`/candidate/roles/result/${this.roleId}/${this.testId}/${this.roleSlug}`]).then(() => {
                    this._loaderOverlay.hide();
                    this.submitted = false;
                });
            }).catch( err => {
                this._loaderOverlay.hide();
                this.submitted = false;
                throw new Error(err);
            });
        }
    }

    /**
     * Save the answer(s)
     *
     * @param answer
     * @param type
     * @param event
     */
    saveAnswer(answer: any, type = 1, event: any = null): void {
        console.log(answer);
        console.log(type);
        console.log(event);
        this.questionAnswered = false;
        if (+type === 3) { // If is checkbox then format, else do nothing
            let newAnswer = [];
            Object.entries(this.questionnaire.results[answer.question]).forEach(([key, value]) => {
                if (value) {
                    newAnswer = [+key, ...newAnswer];
                }
            });
            answer.answers = newAnswer;
        }
        console.log(answer);
        this._roleQuestionnaireService.saveTestAnswer(answer).then( res => {
            if (+type === 3 && answer.answers.length === 0) {
                this.questionAnswered = false;
            } else {
                this.questionAnswered = true;
            }
        });
    }

    /**
     * Save the final answer(s)
     *
     * @param answer
     * @param type
     * @param event
     */
    saveFinalAnswer(answer: any, type = 3, event: any = null): void {
        let newAnswer = [];
        Object.entries(this.questionnaire.results[answer.question]).forEach(([key, value]) => {
            if (value) {
                newAnswer = [+key, ...newAnswer];
            }
        });
        answer.answers = newAnswer;
        console.log(answer);
        this._roleQuestionnaireService.saveTestAnswer(answer).then( res => {
            this.gotoNextStep();
        });
    }

    /**
     * Toggle the sidebar
     *
     * @param name
     */
    toggleSidebar(name): void {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }

    /**
     * Timer event handler
     *
     * @param event
     */
    handleTimerEvent(event): void {
        // console.log(event);
        switch (event.action) {
            case 'notify': {
                switch (event.action.left) {
                    case 60000: {
                        this.timerClass = 'snack-alert-warning';
                        this.timerProgressColor = 'alert';
                        this._snackAlertService.error(this._translatePipe.transform('MAIN.TIMER.60_SECONDS_LEFT'));
                        break;
                        }
                    case 10000: {
                        this.timerClass = 'snack-alert-danger';
                        this.timerProgressColor = 'warn';
                        this._snackAlertService.warning(this._translatePipe.transform('MAIN.TIMER.10_SECONDS_LEFT'));
                        break;
                    }
                    case 0: {
                        this._stopTimer();
                        break;
                    }
                }
                break;
            }
            case 'finished': {
                this._stopTimer();
                break;
            }
        }
        if (event.left <= 0) {
            this._stopTimer();
        } else if (event.left <= 10000) {
            this.timerClass = 'snack-alert-danger';
            this._snackAlertService.error(this._translatePipe.transform('MAIN.TIMER.10_SECONDS_LEFT'));
        } else if (event.left <= 60000) {
            this.timerClass = 'snack-alert-warning';
            this._snackAlertService.warning(this._translatePipe.transform('MAIN.TIMER.60_SECONDS_LEFT'));
        }
        this._changeDetectorRef.detectChanges();
    }

    /**
     * Saves execution on closing browser
     */
    saveExecutionOnExit(execution = this.questionnaire.execution, status = 2): void {
        if (!this.submitted) {
            this.submitted = true;
            const executionCopy = Object.assign({}, execution);
            executionCopy.status = status;
            this._roleQuestionnaireService.submitTestExecution(executionCopy).then(res => {
                this._roleResultService.setCurrentExecution(res);
            }).catch( err => {
                // do something in case of error
                this.submitted = false;
                throw new Error(err);
            });
        }
    }
}

@Component({
    selector: 'candidate-roles-questionnaire-dialog',
    templateUrl: './questionnaire.dialog.component.html',
    styleUrls: ['./questionnaire.component.scss'],
})
export class RolesQuestionnaireDialogComponent {
    constructor(
        private _dialogRef: MatDialogRef<RolesQuestionnaireDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
        _dialogRef.disableClose = true;
    }
}
