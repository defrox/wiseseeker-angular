import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {RolesResultComponent, RoleResultService} from 'app/main/candidate/roles/result';
import {RoleQuestionnaireComponent, RoleQuestionnaireService, RolesQuestionnaireDialogComponent} from 'app/main/candidate/roles/questionnaire';
import {CandidateRolesListComponent, CandidateRolesListService} from 'app/main/candidate/roles/list';
import {CandidateRolesDetailComponent, CandidateRolesDetailService} from 'app/main/candidate/roles/detail';
import {RolesHomeComponent, RolesHomeService} from 'app/main/candidate/roles/home';
import {MaterialModule} from 'app/_helpers/material.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseSidebarModule, FuseWidgetModule} from '@fuse/components';
import {TranslateModule} from '@ngx-translate/core';
import {ChartsModule} from 'ng2-charts';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {DefroxModule} from 'app/_helpers';
import {AuthGuard, ExitTestGuard} from 'app/_guards';
import {Group} from 'app/_models';
import {CountdownModule} from 'ngx-countdown';

const appRoutes: Routes = [
    {path: 'candidate/roles', children: [
        {path: '', component: RolesHomeComponent, resolve: {home: RolesHomeService}, canActivate: [AuthGuard], data: {groups: []}},
        {path: 'list', component: CandidateRolesListComponent, resolve: {list: CandidateRolesListService}, canActivate: [AuthGuard], data: {groups: []}},
        {path: ':roleId/:roleSlug', component: CandidateRolesDetailComponent, resolve: {roles: CandidateRolesDetailService}, canActivate: [AuthGuard], data: {groups: []}},
        {path: 'questionnaire/:roleId/:testId/:roleSlug', component: RoleQuestionnaireComponent, resolve: {questionnaire: RoleQuestionnaireService}, canDeactivate: [ExitTestGuard], canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'result/:roleId/:testId/:roleSlug', component: RolesResultComponent, resolve: {result: RoleResultService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        // otherwise redirect to 404
        {path: '**', redirectTo: '/error/404', pathMatch: 'full'}
    ]}
];

@NgModule({
    declarations: [
        RoleQuestionnaireComponent,
        RolesResultComponent,
        RolesHomeComponent,
        CandidateRolesListComponent,
        CandidateRolesDetailComponent,
        RolesQuestionnaireDialogComponent,
        ],
    imports: [
        RouterModule.forChild(appRoutes),
        MaterialModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseWidgetModule,
        TranslateModule,
        ChartsModule,
        CountdownModule,
        DefroxModule,
        NgxChartsModule
    ],
    providers: [
        RoleQuestionnaireService,
        RoleResultService,
        RolesHomeService,
        CandidateRolesListService,
        CandidateRolesDetailService,
        AuthGuard,
        ExitTestGuard
    ],
    entryComponents: [RolesQuestionnaireDialogComponent],
    exports: [RouterModule]
})
export class RolesRouting { }
