import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {CandidateRolesListService} from './list.service';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'candidate-roles-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.scss'],
    animations: fuseAnimations
})
export class CandidateRolesListComponent implements OnInit, OnDestroy {
    filteredRoles: any[];
    allRoles: any[];
    rolesFilteredByCategory: any[];
    categories: any[] = [];
    types = [{title: 'MAIN.ROLES.TYPES.DEFAULT', type: 'default'}, {title: 'MAIN.ROLES.TYPES.CUSTOM', type: 'custom'}];
    currentCategory: string;
    currentType: string;
    searchTerm: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {CandidateRolesListService} _candidateRolesListService
     */
    constructor(
        private _translationLoader: FuseTranslationLoaderService,
        private _candidateRolesListService: CandidateRolesListService
    ) {
        // Set the defaults
        this.currentCategory = this.currentType = 'all';
        this.searchTerm = '';

        // Set the private defaults
        this._translationLoader.loadTranslations(spanish, english);
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to categories
        this._candidateRolesListService.onRolesCategoriesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(categories => {
                this.categories = categories;
            });

        // Subscribe to all roles
        this._candidateRolesListService.onAllRolesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(roles => {
                this.filteredRoles = this.allRoles = this.rolesFilteredByCategory = roles;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Filter roles by category
     */
    filterRolesByCategory(): void {
        // Filter
        if (this.currentCategory === 'all') {
            this.rolesFilteredByCategory = this.allRoles;
            this.filteredRoles = this.allRoles;
        } else {
            this.rolesFilteredByCategory = this.allRoles.filter((knowledge) => {
                return knowledge.category === this.currentCategory;
            });
            this.filteredRoles = [...this.rolesFilteredByCategory];
        }
        if (this.currentType === 'all') {
            this.filteredRoles = this.rolesFilteredByCategory;
        } else {
            this.rolesFilteredByCategory = this.rolesFilteredByCategory.filter((knowledge) => {
                const isCustom = knowledge.tests && knowledge.tests.length > 0 && knowledge.tests[0].hasOwnProperty('custom');
                return (knowledge.custom && this.currentType === 'custom') || (!knowledge.custom && this.currentType === 'default');
            });
            this.filteredRoles = [...this.rolesFilteredByCategory];
        }
        // Re-filter by search term
        this.filterRolesByTerm();
    }

    /**
     * Filter roles by term
     */
    filterRolesByTerm(): void {
        const searchTerm = this.searchTerm.toLowerCase();
        // Search
        if (searchTerm === '') {
            this.filteredRoles = this.rolesFilteredByCategory;
        } else {
            this.filteredRoles = this.rolesFilteredByCategory.filter((roles) => {
                return roles.title.toLowerCase().includes(searchTerm);
                // return (roles.title.toLowerCase().includes(searchTerm) || roles.description.toLowerCase().includes(searchTerm));
            });
        }
    }

}
