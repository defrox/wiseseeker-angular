import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {AuthenticationService, RoleCategoryApiService, RoleApiService, TestExecutionApiService} from 'app/_services';
import {Role, User} from 'app/_models';

@Injectable()
export class CandidateRolesListService implements Resolve<any> {
    onRoleChanged: BehaviorSubject<any>;
    onAllRolesChanged: BehaviorSubject<any>;
    onRolesCategoriesChanged: BehaviorSubject<any>;
    currentUserSubscription: Subscription;
    currentUser: User;
    rolesExecutions: Role[];
    executions: any;
    roles: any;


    /**
     * Constructor
     *
     * @param {RoleApiService} _rolesApiService
     * @param {RoleCategoryApiService} _rolesCategoryApiService
     * @param {TestExecutionApiService} _testExecutionApiService
     * @param {AuthenticationService} _authenticationService
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _rolesApiService: RoleApiService,
        private _rolesCategoryApiService: RoleCategoryApiService,
        private _testExecutionApiService: TestExecutionApiService,
        private _authenticationService: AuthenticationService,
        private _httpClient: HttpClient
    ) {
        // Set the defaults
        this.onAllRolesChanged = new BehaviorSubject({});
        this.onRolesCategoriesChanged = new BehaviorSubject({});
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getAllRoles(),
                this.getAllRolesCategories()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get roles
     *
     * @params roleId
     * @returns {Promise<any>}
     */
    getRoles(roleId): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._rolesApiService.get(roleId)
                .subscribe((response: any) => {
                    this.onRoleChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }

    /**
     * Get all roles
     *
     * @returns {Promise<any>}
     */
    getAllRoles(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._rolesApiService.getAll()
                .subscribe((roles: any) => {
                    this.roles = roles;
                    this._testExecutionApiService.getByUser(+this.currentUser.id)
                        .subscribe((executions: any) => {
                            this.executions = executions;
                            this.rolesExecutions = [];

                            for (const role of roles) {
                                const tmpRoleExecutions = [];
                                let tmpPunctuation = 0;
                                role.execution = { status: 0 };
                                for (const test of role.tests) {
                                    const tmpRoleTestExecutions = this.executions.filter(item => test.id === item.test.id);
                                    if (tmpRoleTestExecutions.length > 0) {
                                        tmpRoleExecutions.push(tmpRoleTestExecutions[0]);
                                        if  (tmpRoleTestExecutions[0].assessed) {
                                            tmpPunctuation = (100 * tmpRoleTestExecutions[0].assessed[0].punctuation ) / tmpRoleTestExecutions[0].assessed[0].maximum;
                                        }
                                    }
                                    role.execution.assessed = {minimum: 0, maximum: 100, punctuation: tmpPunctuation};
                                    role.execution.marks = tmpRoleTestExecutions[0].marks;
                                }
                                role.execution.status = Math.floor(tmpRoleExecutions.reduce((accumulator, currentValue) => accumulator + currentValue.status, 0) / tmpRoleExecutions.length);
                                role.executions = tmpRoleExecutions;
                                this.rolesExecutions.push(role);
                            }
                            this.roles = this.rolesExecutions;
                            /*for (const execution of executions) {
                                const tmpRoles = this.roles.filter(item => this._filterByTestId(item.tests, execution.test.id));
                                if (tmpRoles.length > 0) {
                                    tmpRoles[0].execution = execution;
                                    this.rolesExecutions = [...this.rolesExecutions, ...tmpRoles[0]];
                                }
                            }*/
                        });
                    this.onAllRolesChanged.next(this.roles);
                    resolve(this.roles);
                }, reject);
        });
    }

    /**
     * Get all roles categories
     *
     * @returns {Promise<any>}
     */
    getAllRolesCategories(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._rolesCategoryApiService.getAll()
                .subscribe((categories: any) => {
                    this.onRolesCategoriesChanged.next(categories);
                    resolve(categories);
                }, reject);
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private _filterByTestId(item: any, testId: number): any {
        return item.filter(obj => obj.id === testId).length > 0;
        // return item.indexOf(testId) !== -1;
    }
}
