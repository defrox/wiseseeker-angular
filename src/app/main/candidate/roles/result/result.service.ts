import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {AuthenticationService, RoleApiService, TestExecutionApiService} from 'app/_services';
import {Role, User} from 'app/_models';

@Injectable()
export class RoleResultService implements Resolve<any> {
    onQuestionnaireChanged: BehaviorSubject<any>;
    onResultChanged: BehaviorSubject<any>;
    onRoleChanged: BehaviorSubject<any>;
    onTestExecutionChanged: BehaviorSubject<any>;
    onCurrentExecutionChanged: BehaviorSubject<any>;
    currentUserSubscription: Subscription;
    currentUser: User;
    role: Role;
    lastExecution: any;
    currentExecution: any;
    result: any;

    /**
     * Constructor
     *
     * @param {RoleApiService} _roleApiService
     * @param {TestExecutionApiService} _testExecutionApiService
     * @param {AuthenticationService} _authenticationService
     */
    constructor(
        private _roleApiService: RoleApiService,
        private _testExecutionApiService: TestExecutionApiService,
        private _authenticationService: AuthenticationService
    ) {
        // Set the defaults
        this.onQuestionnaireChanged = new BehaviorSubject({});
        this.onResultChanged = new BehaviorSubject({});
        this.onTestExecutionChanged = new BehaviorSubject({});
        this.onCurrentExecutionChanged = new BehaviorSubject({});
        this.onRoleChanged = new BehaviorSubject({});
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getAllTestExecutions(route.params.testId, [2, 3]),
                this.getRoleDetails(route.params.roleId)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get result
     *
     * @param resultId
     * @returns {Promise<any>}
     */
    getResult(resultId): Observable<any> | Promise<any>
    {
/*        return new Promise((resolve, reject) => {
            this._httpClient.get<any>('api/roles/result/' + userId)
                .subscribe((response: any) => {
                    // this.result = response;
                    // this.onResultChanged.next(this.result);
                    resolve(this.onResultChanged);
                }, error => {
                    resolve(this.onResultChanged);
                });
        });*/
        return Promise.resolve(this.onResultChanged);
    }

    /**
     * Get all test executions
     *
     * @param testId
     * @param statusIds
     * @returns {Promise<any>}
     */
    getAllTestExecutions(testId: number, statusIds: number[]): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testExecutionApiService.getByUserAndTest(+this.currentUser.id, testId)
                .subscribe((executions: any) => {
                    // this.lastExecution = executions.filter(item => this._filterByTestIdAndStatus(item, testId, statusIds));
                    this.lastExecution = executions;
                    this.onTestExecutionChanged.next(this.lastExecution);
                    resolve(this.lastExecution);
                }, reject);
        });
    }

    /**
     * Get test execution by id
     *
     * @param executionId
     * @returns {Observable<any>}
     */
    getTestExecution(executionId: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testExecutionApiService.get(executionId)
                .subscribe((execution: any) => {
                    this.lastExecution = execution;
                    resolve(this.lastExecution);
                }, reject);
        });
    }

    /**
     * Get Roles details
     *
     * @param roleId
     * @returns {Observable<any>}
     */
    getRoleDetails(roleId: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._roleApiService.get(roleId)
                .subscribe((role: any) => {
                    this.role = role;
                    this.onRoleChanged.next(this.role);
                    resolve(this.role);
                }, reject);
        });
    }

    /**
     * Set Roles Results
     *
     * @param results
     */
    setRolesResults(results): void {
        this.onResultChanged.next(results);
    }

    /**
     * Get Roles Results
     *
     * @returns {Observable<any>}
     */
    getRolesResults(): Observable<any> {
        return this.onResultChanged.asObservable();
    }

    /**
     * Reset Roles Results
     */
    resetRolesResults(): void {
        this.onResultChanged.next(null);
    }

    /**
     * Set Current Execution
     *
     * @param execution
     */
    setCurrentExecution(execution): void {
        this.onCurrentExecutionChanged.next(execution);
    }

    /**
     * Get Current Execution
     *
     * @returns {Observable<any>}
     */
    getCurrentExecution(): Observable<any> {
        return this.onCurrentExecutionChanged.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private _filterByTestIdAndStatus(item: any, testId: number, statusIds: number[]): any {
        return +item.test.id === +testId && statusIds.indexOf(item.status) !== -1;
    }

    private handleError(error: any): any {
        return throwError(error);
    }

}
