import {Component, OnInit, OnDestroy, ViewEncapsulation} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {first, takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {User, TestExecution, Role} from 'app/_models';
import {AuthenticationService, NotificationService} from 'app/_services';
import {RoleResultService} from './result.service';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'roles-result',
    templateUrl: './result.component.html',
    styleUrls: ['./result.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class RolesResultComponent implements OnInit, OnDestroy {
    currentUser: User;
    currentUserSubscription: Subscription;
    role: Role;
    result: any;
    execution: any;
    currentExecution: any;
    assessment: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {RoleResultService} _rolesResultService
     * @param {AuthenticationService} _authenticationService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {NotificationService} _notificationsService
     */
    constructor(
        private _rolesResultService: RoleResultService,
        private _authenticationService: AuthenticationService,
        private _translationLoader: FuseTranslationLoaderService,
        private _notificationsService: NotificationService
    ) {
        // Set the private defaults
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
        this._translationLoader.loadTranslations(spanish, english);
        this._notificationsService.resetUserNotifications();
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to role
        this._rolesResultService.onRoleChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(role => {
                this.role = role;
            });

        // Subscribe to executions
        this._rolesResultService.onTestExecutionChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(execution => {
                this.execution = this._getLastAssessedExecution(execution);
                this.assessment = this._getAssessment(this.execution);
                this.assessment = null;
            });

        // Subscribe to result
        this._rolesResultService.onCurrentExecutionChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(execution => {
                this.currentExecution = execution;
                if (this.currentExecution && this.currentExecution.id && (!(this.execution && this.execution.id) || (this.currentExecution.id !== this.execution.id))) {
                    this.execution = execution;
                    this.assessment = this._getAssessment(this.execution);
                    this.assessment = null;
                }
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();

        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get last assessed execution
     *
     * @param executions
     * @returns {execution}
     */
    _getLastAssessedExecution(executions: TestExecution[] = []): TestExecution | null {
        if (executions.length === 0) {
            return;
        }
        return executions.filter(item => +item.status === 3)
            .sort((a, b) => {
                a = a.created;
                b = b.created;
                if (a > b) {
                    return -1;
                } else if (a < b) {
                    return 1;
                }
                return 0;
            })[0];
    }

    /**
     * Get execution assessment
     *
     * @param execution
     * @returns {execution}
     */
    _getAssessment(execution: TestExecution = null): TestExecution | null {
        if (!execution || !execution.assessed || execution.assessed.length === 0) {
            return;
        }
        return execution.assessed[0];
    }
}
