import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, QueryList, ViewChildren, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {CandidateRolesDetailService} from './detail.service';
import {TestExecution} from 'app/_models';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';


@Component({
    selector: 'candidate-roles',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class CandidateRolesDetailComponent implements OnInit, OnDestroy, AfterViewInit {
    animationDirection: 'left' | 'right' | 'none';
    roles: any;
    execution: any;
    assessment: any;

    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {CandidateRolesDetailService} _candidateRolesDetailService
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {FuseTranslationLoaderService} _translationLoader
     */
    constructor(
        private _candidateRolesDetailService: CandidateRolesDetailService,
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseSidebarService: FuseSidebarService,
        private _translationLoader: FuseTranslationLoaderService
    ) {
        // Set the defaults
        this.animationDirection = 'none';
        this._translationLoader.loadTranslations(spanish, english);

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to all roles
        this._candidateRolesDetailService.onRoleChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(roles => {
                this.roles = roles;
            });
        // Subscribe to executions
        this._candidateRolesDetailService.onTestIdChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(testId => {
                this._candidateRolesDetailService.getAllTestExecutions(testId, [2, 3]).then(execution => {
                    this.execution = this._getLastAssessedExecution(execution);
                    this.assessment = this._getAssessment(this.execution);
                });
            });
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void {}

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get last assessed execution
     *
     * @param executions
     * @returns {execution}
     */
    _getLastAssessedExecution(executions: TestExecution[] = []): TestExecution | null {
        if (executions.length === 0) {
            return;
        }
        return executions.filter(item => +item.status === 3)
            .sort((a, b) => {
                a = a.created;
                b = b.created;
                if (a > b) {
                    return -1;
                } else if (a < b) {
                    return 1;
                }
                return 0;
            })[0];
    }

    /**
     * Get execution assessment
     *
     * @param execution
     * @returns {execution}
     */
    _getAssessment(execution: TestExecution = null): TestExecution | null {
        if (!execution || execution.assessed.length === 0) {
            return;
        }
        return execution.assessed[0];
    }
}
