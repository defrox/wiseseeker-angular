import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {RoleApiService, TestExecutionApiService} from 'app/_services';

@Injectable()
export class CandidateRolesDetailService implements Resolve<any> {
    onRoleChanged: BehaviorSubject<any>;
    onTestIdChanged: BehaviorSubject<any>;
    onTestExecutionChanged: BehaviorSubject<any>;
    role: any;
    testId: any = null;
    lastExecution: any = {};

    /**
     * Constructor
     *
     * @param {TestExecutionApiService} _testExecutionApiService
     * @param {RoleApiService} _roleApiService
     */
    constructor(
        private _testExecutionApiService: TestExecutionApiService,
        private _roleApiService: RoleApiService,
    ) {
        // Set the defaults
        this.onRoleChanged = new BehaviorSubject({});
        this.onTestIdChanged = new BehaviorSubject({});
        this.onTestExecutionChanged = new BehaviorSubject([]);
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getRoles(route.params.roleId)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get roles
     *
     * @returns {Promise<any>}
     */
    getRoles(id): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._roleApiService.get(id)
                .subscribe((response: any) => {
                    this.role = response;
                    this.onRoleChanged.next(this.role);
                    this.onTestIdChanged.next(null);
                    if (this.role.hasOwnProperty('tests')) {
                        for (const test of this.role.tests) {
                            this.testId = test;
                            this.onTestIdChanged.next(this.testId);
                        }
                    }
                    resolve(this.role);
                }, reject);
        });
    }

    /**
     * Get all test executions
     *
     * @param testId
     * @param statusIds
     * @returns {Promise<any>}
     */
    getAllTestExecutions(testId: number, statusIds: number[]): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testExecutionApiService.getAll()
                .subscribe((executions: any) => {
                    this.lastExecution = executions.filter(item => this._filterByTestIdAndStatus(item, testId, statusIds));
                    this.onTestExecutionChanged.next(this.lastExecution);
                    resolve(this.lastExecution);
                }, reject);
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private _filterByTestIdAndStatus(item: any, testId: number, statusIds: number[]): any {
        return +item.test.id === +testId && statusIds.indexOf(item.status) !== -1;
    }

    private handleError(error: any): any {
        return throwError(error);
    }


}
