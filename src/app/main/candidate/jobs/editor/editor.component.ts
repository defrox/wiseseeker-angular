import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {ActivatedRoute, Router} from '@angular/router';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {fuseAnimations} from '@fuse/animations';
import {MatDialog, MatDialogRef} from '@angular/material';
import {CandidateJobsEditorService} from './editor.service';
import {LoaderOverlayService, NotificationService, SnackAlertService, TestApiService} from 'app/_services';
import {PersonalitySkills, PersonalityRelevances} from 'app/_models';
import {TranslatePipe} from '@ngx-translate/core';
import * as _ from 'lodash';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';


@Component({
    selector: 'candidate-jobs-editor',
    templateUrl: './editor.component.html',
    styleUrls: ['./editor.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class CandidateJobsEditorComponent implements OnInit, OnDestroy {
    animationDirection: 'left' | 'right' | 'none';
    personalityRelevances: any[];
    personalitySkills: any[];
    personalityTests: any[];
    rolesTests: any[];
    job: any;
    jobId: string | number | null;
    jobSlug: string | null;
    form: FormGroup;
    submitted = false;
    icon = null;
    image = null;

    // Horizontal Stepper
    horizontalStepperStep1: FormGroup;
    horizontalStepperStep2: FormGroup;
    horizontalStepperStep3: FormGroup;
    horizontalStepperStep4: FormGroup;
    horizontalStepperStep5: FormGroup;
    horizontalStepperStep6: FormGroup;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {CandidateJobsEditorService} _candidateJobsEditorService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslatePipe} _translatePipe
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {SnackAlertService} _snackAlertService
     * @param {TestApiService} _testApiService
     * @param {FormBuilder} _formBuilder
     * @param {MatDialog} _dialog
     * @param {ActivatedRoute} _route
     * @param {NotificationService} _notificationsService
     * @param {Router} _router
     */
    constructor(
        private _candidateJobsEditorService: CandidateJobsEditorService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translatePipe: TranslatePipe,
        private _loaderOverlay: LoaderOverlayService,
        private _snackAlertService: SnackAlertService,
        private _testApiService: TestApiService,
        private _formBuilder: FormBuilder,
        private _dialog: MatDialog,
        private _route: ActivatedRoute,
        private _notificationsService: NotificationService,
        private _router: Router
    ) {
        // Set the defaults
        this._loaderOverlay.show();
        this.animationDirection = 'none';
        this.personalityRelevances = PersonalityRelevances;
        this.personalitySkills = PersonalitySkills;
        this.personalityTests = this.rolesTests = [];
        this._translationLoader.loadTranslations(spanish, english);
        this._route.paramMap.subscribe(params => {
            this.jobId = params.get('jobId');
            this.jobSlug = params.get('jobSlug');
        });

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to all job
        this._candidateJobsEditorService.onJobChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(job => {
                this.job = job;
                this._loaderOverlay.hide();
            });

        // Get all tests
        this._testApiService.getAll()
            .subscribe(tests => {
                this.rolesTests = tests.filter( skill => +skill.type === 2);
                this.personalityTests = tests.filter( skill => +skill.type === 4);
            });

        // Horizontal Stepper form steps
        this.horizontalStepperStep1 = this._formBuilder.group({
            title : [this.job.title, Validators.required],
            description: [this.job.description]
        });

        this.horizontalStepperStep2 = this._formBuilder.group({
            active_from: [this.job.active_from, Validators.required],
            active_to: [this.job.active_to, Validators.required]
        });

        this.horizontalStepperStep3 = this._formBuilder.group({
            icon      : [''],
            image     : ['']
        });

        this.horizontalStepperStep4 = this._formBuilder.group({
            personality_relevance  : [this.job.personality_relevance, Validators.required],
            personality_skills     : [this.job.skills, Validators.required],
        });

        this.horizontalStepperStep5 = this._formBuilder.group({
            test_personality     : [this.job.test_personality],
        });

        this.horizontalStepperStep6 = this._formBuilder.group({
            roles     : [this.job.test, Validators.required]
        });

    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._candidateJobsEditorService.resetJob();
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Upload image file
     *
     * @param event
     * @returns {void}
     */
    uploadImage(event): void {
        const reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.image = {src: reader.result};
                // TODO: upload image through API
            };
        }
    }

    /**
     * Upload icon file
     *
     * @param event
     * @returns {void}
     */
    uploadIcon(event): void {
        const reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.icon = {src: reader.result};
                // TODO: upload image through API
            };
        }
    }

    /**
     * Filter tests
     *
     * @param testArray
     * @param testId
     * @returns {boolean}
     */
    filterTests(testArray: any[], testId: number): boolean {
        let result = false;
        for (const item of testArray) {
            if (+item.id === testId) {
                result = true;
            }
        }
        return result;
    }

    /**
     * Submit job
     */
    submitJob(): void
    {
        if (!this.submitted) {
            this.submitted = true;
            this._loaderOverlay.show();
            const result = _.cloneDeep(this.job);
            result.title = this.horizontalStepperStep1.value.title;
            result.description = this.horizontalStepperStep1.value.description;
            result.active_to = this.horizontalStepperStep2.value.active_to;
            result.active_from = this.horizontalStepperStep2.value.active_from;
            // result.image = this.horizontalStepperStep3.value.image;
            // result.icon = this.horizontalStepperStep3.value.icon;
            /*result.personality_relevance = this.horizontalStepperStep4.value.personality_relevance;
            result.test_personality = this.horizontalStepperStep4.value.test_personality;
            result.skills = this.horizontalStepperStep5.value.personality_skills;
            result.test = this.horizontalStepperStep6.value.roles;*/
            this._candidateJobsEditorService.saveJob(result).then(res => {
                this.job = res;
                this._router.navigate([`/candidate/jobs`]).then(() => {
                    this._loaderOverlay.hide();
                    this._snackAlertService.success(this._translatePipe.transform('CANDIDATE.DASHBOARD.JOBS.SAVED_SUCCESSFULLY'));
                    this._notificationsService.resetUserNotifications();
                });
            }).catch(err => {
                // do something in case of error
                this._loaderOverlay.hide();
                this.submitted = false;
                throw new Error(err);
            });
        }
    }
}


@Component({
    selector: 'candidate-jobs-editor-dialog',
    templateUrl: './editor.dialog.component.html',
    styleUrls: ['./editor.component.scss'],
})
export class CandidateJobsEditorDialogComponent {
    constructor(private _dialogRef: MatDialogRef<CandidateJobsEditorDialogComponent>) {
        _dialogRef.disableClose = true;
    }
}
