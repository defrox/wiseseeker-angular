import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CandidateJobsHomeComponent, CandidateJobsHomeService} from 'app/main/candidate/jobs/home';
import {CandidateJobsDetailComponent, CandidateJobsDetailDialogComponent, CandidateJobsDetailService} from 'app/main/candidate/jobs/detail';
import {CandidateJobsEditorComponent, CandidateJobsEditorDialogComponent, CandidateJobsEditorService} from 'app/main/candidate/jobs/editor';
import {CandidateJobsApplyComponent, CandidateJobsApplyDialogComponent, CandidateJobsApplyService} from 'app/main/candidate/jobs/apply';
import {MaterialModule} from 'app/_helpers/material.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseSidebarModule, FuseWidgetModule} from '@fuse/components';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {TranslateModule} from '@ngx-translate/core';
import {ChartsModule} from 'ng2-charts';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {AuthGuard} from 'app/_guards';
import {DefroxModule} from 'app/_helpers';
import {Group} from 'app/_models';
import {ChartModule} from 'angular-highcharts';

const appRoutes: Routes = [
    {path: 'candidate/jobs', pathMatch: 'prefix', children: [
        {path: '', redirectTo: 'home', pathMatch: 'full'},
        {path: 'home', component: CandidateJobsHomeComponent, resolve: {home: CandidateJobsHomeService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'detail/:jobId/:jobSlug', component: CandidateJobsDetailComponent, resolve: {job: CandidateJobsDetailService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'editor/:jobId/:jobSlug', component: CandidateJobsEditorComponent, resolve: {job: CandidateJobsEditorService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate]}},
        {path: 'apply/:jobId/:jobSlug', component: CandidateJobsApplyComponent},
        // otherwise redirect to 404
        {path: '**', pathMatch: 'full', redirectTo: '/error/404'},
    ]}
];

@NgModule({
    declarations: [
        CandidateJobsHomeComponent,
        CandidateJobsDetailComponent,
        CandidateJobsDetailDialogComponent,
        CandidateJobsEditorComponent,
        CandidateJobsEditorDialogComponent,
        CandidateJobsApplyComponent,
        CandidateJobsApplyDialogComponent
    ],
    imports: [
        RouterModule.forChild(appRoutes),
        MaterialModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseWidgetModule,
        TranslateModule,
        ChartsModule,
        NgxChartsModule,
        FontAwesomeModule,
        DefroxModule,
        ChartModule
    ],
    providers: [
        CandidateJobsHomeService,
        CandidateJobsDetailService,
        CandidateJobsEditorService,
        CandidateJobsApplyService,
        AuthGuard
    ],
    entryComponents: [CandidateJobsDetailDialogComponent, CandidateJobsEditorDialogComponent, CandidateJobsApplyDialogComponent],
    exports: [RouterModule]
})
export class CandidateJobsRouting {
}
