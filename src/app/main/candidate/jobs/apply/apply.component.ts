import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, QueryList, ViewChildren, ViewChild, ViewEncapsulation, Inject} from '@angular/core';
import {merge, Observable, Subject, of} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {ActivatedRoute, Router} from '@angular/router';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {FuseConfigService} from '@fuse/services/config.service';
import {TranslatePipe} from '@ngx-translate/core';
import {CandidateJobsApplyService} from './apply.service';
import {LoaderOverlayService, SnackAlertService, AuthenticationService, NotificationService, AppConfigService} from 'app/_services';
import {DialogData} from 'app/_models';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';


@Component({
    selector: 'candidate-jobs-apply',
    templateUrl: './apply.component.html',
    styleUrls: ['./apply.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class CandidateJobsApplyComponent implements OnInit, OnDestroy, AfterViewInit {
    appConfig: any;
    fuseConfig: any;
    loaded = false;
    jobApplicationsDisplayedColumns: string[] = ['status'];
    job: any = null;
    jobId: number | null;
    jobSlug: string | null;
    jobApplicationsTable = new MatTableDataSource<any>();
    currentUser;

    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {AppConfigService} _appConfigService
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslatePipe} _translatePipe
     * @param {CandidateJobsApplyService} _candidateJobsApplyService
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {SnackAlertService} _snackAlertService
     * @param {MatDialog} _dialog
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {AuthenticationService} _authenticationService
     * @param {NotificationService} _notificationsService
     * @param {ActivatedRoute} _route
     * @param {Router} _router
     */
    constructor(
        private _appConfigService: AppConfigService,
        private _fuseConfigService: FuseConfigService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translatePipe: TranslatePipe,
        private _candidateJobsApplyService: CandidateJobsApplyService,
        private _loaderOverlay: LoaderOverlayService,
        private _snackAlertService: SnackAlertService,
        private _dialog: MatDialog,
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseSidebarService: FuseSidebarService,
        private _authenticationService: AuthenticationService,
        private _notificationsService: NotificationService,
        private _route: ActivatedRoute,
        private _router: Router

    ) {
        this._unsubscribeAll = new Subject();
        this._appConfigService.config
            .subscribe((config) => {
                this.appConfig = config;
                this._route.paramMap.subscribe(params => {
                    this.jobId = +params.get('jobId');
                    this.jobSlug = params.get('jobSlug');
                    if (this.appConfig.appSettings.enableExternalAutoApply) {
                        this._router.navigate(['/auth/login'], { queryParams: { external: 'true', jobId: this.jobId, jobSlug: this.jobSlug }}).then();
                    }
                });
        });

        // Set the private defaults
        this.currentUser = this._authenticationService.currentUserValue;
        if (!this.currentUser) {
            // Configure the layout
            this._fuseConfigService.config = {
                layout: {
                    navbar: {
                        hidden: true
                    },
                    toolbar: {
                        hidden: true
                    },
                    footer: {
                        hidden: true
                    },
                    sidepanel: {
                        hidden: true
                    }
                }
            };
        }
        this._translationLoader.loadTranslations(spanish, english);
        this.sort = new MatSort();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // TODO: use wrapper table https://github.com/angular/material2/blob/master/src/material-examples/table-wrapped/

        // If the user changes the sort order, reset back to the first page.
        // this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        // Subscribe to job
        this._candidateJobsApplyService.getJobByUser(this.jobId, this.currentUser)
            .then(job => {
                this.job = job;
                this.loaded = true;
            }).catch(err => {
                this.loaded = true;
                throw new Error(err);
        });
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void {}

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this._notificationsService.resetUserNotifications();
    }

    deleteJobApplication(jobId): void {
        // delete confirmed
        const dialogRef = this._dialog.open(CandidateJobsApplyDialogComponent, {
            data: {delete: true}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // delete confirmed
                this._loaderOverlay.show();
                this._candidateJobsApplyService.deleteJobApplication(jobId).then( () => {
                    this._candidateJobsApplyService.getJob(this.job.id).then((res) => {
                        this.job = res;
                        this._loaderOverlay.hide();
                        this._snackAlertService.success(this._translatePipe.transform('CANDIDATE.JOBS.DIALOG.DELETED_SUCCESSFULLY'));
                        this._notificationsService.resetUserNotifications();
                    });
                });
            } else {
                // cancel delete
                this._loaderOverlay.hide();
            }
        });
    }

    registerDialog(): void {
        const dialogRef = this._dialog.open(CandidateJobsApplyDialogComponent, {
            data: {register: true}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // register confirmed
                this._router.navigate([`/auth/register/external/${this.jobId}/${this.jobSlug}`]).then();
            }
        });
    }

    createJobApplication(): void {
        this._loaderOverlay.show();
        const jobApplication = {user: this.currentUser.id, job: this.job.id};
        this._candidateJobsApplyService.createJobApplication(jobApplication).then( () => {
            this._candidateJobsApplyService.getJob(this.job.id).then((res) => {
                this.job = res;
                this._loaderOverlay.hide();
                this._snackAlertService.success(this._translatePipe.transform('CANDIDATE.JOBS.DIALOG.ADDED_SUCCESSFULLY'));
                this._notificationsService.resetUserNotifications();
            });
        });
    }

    applyFilter(filterValue: string): void {
        // const data = this.jobApplicationsTable.data;
        // this.jobApplicationsTable = new MatTableDataSource<any>(data);
        this.jobApplicationsTable.filter = filterValue.trim().toLowerCase();
        if (this.paginator) {
            this.paginator.firstPage();
        }
    }
}


@Component({
    selector: 'candidate-jobs-apply-dialog',
    templateUrl: './apply.dialog.component.html',
    styleUrls: ['./apply.component.scss'],
})
export class CandidateJobsApplyDialogComponent {
    constructor(
        private _dialogRef: MatDialogRef<CandidateJobsApplyDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
        _dialogRef.disableClose = true;
    }
}
