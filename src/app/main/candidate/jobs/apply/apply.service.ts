import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {JobApiService, JobApplicationApiService, UserApiService, TestApiService, SoftSkillApiService} from 'app/_services';
import {TranslateService} from '@ngx-translate/core';
import * as _ from 'lodash';

@Injectable()
export class CandidateJobsApplyService {
    job: any;

    /**
     * Constructor
     *
     * @param {TranslateService} _translateService
     * @param {UserApiService} _userApiService
     * @param {JobApiService} _jobApiService
     * @param {JobApplicationApiService} _jobApplicationApiService
     * @param {TestApiService} _testApiService
     * @param {SoftSkillApiService} _softSkillApiService
     */
    constructor(
        private _translateService: TranslateService,
        private _userApiService: UserApiService,
        private _jobApiService: JobApiService,
        private _jobApplicationApiService: JobApplicationApiService,
        private _testApiService: TestApiService,
        private _softSkillApiService: SoftSkillApiService,
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get job
     *
     * @returns {Promise<any>}
     */
    getJob(id): Promise<any>
    {
        return this._jobApiService.getByUser(id).toPromise();
    }

    /**
     * Get job by user
     *
     * @returns {Promise<any>}
     */
    getJobByUser(id, currentUser): Promise<any>
    {
        if (!currentUser) {
            return this._jobApiService.get(id).toPromise();
        }
        return this._jobApiService.getByUser(id).toPromise();
    }

    /**
     * Delete job application
     *
     * @returns {Promise<any>}
     */
    deleteJobApplication(id): Promise<any>
    {
        return this._jobApplicationApiService.delete(id).toPromise();
    }

    /**
     * Creates job application
     *
     * @returns {Promise<any>}
     */
    createJobApplication(jobApplication): Promise<any>
    {
        return this._jobApplicationApiService.add(jobApplication).toPromise();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Format job object
     *
     * @param job
     * @return {any[]}
     */
    _formatJob(job: any): any[]
    {
        const result = _.cloneDeep(job);
        if (result.test_personality) {
            this._testApiService.get(job.test_personality).toPromise().then(res => result.test_personality = res);
        }
        if (result.test) {
            this._testApiService.get(job.test).toPromise().then(res => result.test = res);
        }
        return result;
    }

}
