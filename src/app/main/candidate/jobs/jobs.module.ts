import {NgModule} from '@angular/core';
import {CandidateJobsRouting} from 'app/main/candidate/jobs/jobs.routing';


@NgModule({
    imports: [
        CandidateJobsRouting
    ]
})
export class JobsModule {
}
