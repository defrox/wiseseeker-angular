import {AfterViewInit, ChangeDetectorRef, Component, OnDestroy, OnInit, QueryList, ViewChildren, ViewChild, ViewEncapsulation} from '@angular/core';
import {merge, Observable, Subject, of} from 'rxjs';
import {map, takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {Router} from '@angular/router';
import {MatDialog, MatDialogRef, MatPaginator, MatSort, MatTableDataSource} from '@angular/material';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {TranslatePipe} from '@ngx-translate/core';
import {CandidateJobsDetailService} from './detail.service';
import {LoaderOverlayService, SnackAlertService, AuthenticationService, NotificationService} from 'app/_services';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';


@Component({
    selector: 'candidate-jobs-detail',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class CandidateJobsDetailComponent implements OnInit, OnDestroy, AfterViewInit {
    jobApplicationsDisplayedColumns: string[] = ['status'];
    job: any;
    jobApplicationsTable = new MatTableDataSource<any>();
    currentUser;

    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;
    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslatePipe} _translatePipe
     * @param {CandidateJobsDetailService} _candidateJobsDetailService
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {SnackAlertService} _snackAlertService
     * @param {MatDialog} _dialog
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {AuthenticationService} _authenticationService
     * @param {NotificationService} _notificationsService
     * @param {Router} _router
     */
    constructor(
        private _translationLoader: FuseTranslationLoaderService,
        private _translatePipe: TranslatePipe,
        private _candidateJobsDetailService: CandidateJobsDetailService,
        private _loaderOverlay: LoaderOverlayService,
        private _snackAlertService: SnackAlertService,
        private _dialog: MatDialog,
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseSidebarService: FuseSidebarService,
        private _authenticationService: AuthenticationService,
        private _notificationsService: NotificationService,
        private _router: Router

    ) {
        // Set the private defaults
        this.currentUser = this._authenticationService.currentUserValue;
        this._translationLoader.loadTranslations(spanish, english);
        this._unsubscribeAll = new Subject();
        this.sort = new MatSort();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // TODO: use wrapper table https://github.com/angular/material2/blob/master/src/material-examples/table-wrapped/

        // If the user changes the sort order, reset back to the first page.
        // this.sort.sortChange.subscribe(() => this.paginator.pageIndex = 0);

        // Subscribe to job
        this._candidateJobsDetailService.onJobChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(job => {
                this.job = job;
            });
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void {}

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this._notificationsService.resetUserNotifications();
    }

    deleteJobApplication(jobId): void {
        const dialogRef = this._dialog.open(CandidateJobsDetailDialogComponent);
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // delete confirmed
                this._loaderOverlay.show();
                this._candidateJobsDetailService.deleteJobApplication(jobId).then( () => {
                    this._candidateJobsDetailService.getJob(this.job.id).then((res) => {
                        this.job = res;
                        this._loaderOverlay.hide();
                        this._snackAlertService.success(this._translatePipe.transform('CANDIDATE.JOBS.DIALOG.DELETED_SUCCESSFULLY'));
                        this._notificationsService.resetUserNotifications();
                    });
                });
            } else {
                // cancel delete
                this._loaderOverlay.hide();
            }
        });
    }

    createJobApplication(): void {
        this._loaderOverlay.show();
        const jobApplication = {user: this.currentUser.id, job: this.job.id};
        this._candidateJobsDetailService.createJobApplication(jobApplication).then( () => {
            this._candidateJobsDetailService.getJob(this.job.id).then((res) => {
                this.job = res;
                this._loaderOverlay.hide();
                this._snackAlertService.success(this._translatePipe.transform('CANDIDATE.JOBS.DIALOG.ADDED_SUCCESSFULLY'));
                this._notificationsService.resetUserNotifications();
            });
        });
    }

    applyFilter(filterValue: string): void {
        // const data = this.jobApplicationsTable.data;
        // this.jobApplicationsTable = new MatTableDataSource<any>(data);
        this.jobApplicationsTable.filter = filterValue.trim().toLowerCase();
        if (this.paginator) {
            this.paginator.firstPage();
        }
    }
}


@Component({
    selector: 'candidate-jobs-detail-dialog',
    templateUrl: './detail.dialog.component.html',
    styleUrls: ['./detail.component.scss'],
})
export class CandidateJobsDetailDialogComponent {
    constructor(private _dialogRef: MatDialogRef<CandidateJobsDetailDialogComponent>) {
        _dialogRef.disableClose = true;
    }
}
