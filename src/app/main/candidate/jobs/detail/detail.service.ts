import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {JobApiService, JobApplicationApiService, UserApiService, TestApiService, SoftSkillApiService} from 'app/_services';
import {TranslateService} from '@ngx-translate/core';
import * as _ from 'lodash';

@Injectable()
export class CandidateJobsDetailService implements Resolve<any> {
    onJobChanged: BehaviorSubject<any>;
    job: any;

    /**
     * Constructor
     *
     * @param {TranslateService} _translateService
     * @param {UserApiService} _userApiService
     * @param {JobApiService} _jobApiService
     * @param {JobApplicationApiService} _jobApplicationApiService
     * @param {TestApiService} _testApiService
     * @param {SoftSkillApiService} _softSkillApiService
     */
    constructor(
        private _translateService: TranslateService,
        private _userApiService: UserApiService,
        private _jobApiService: JobApiService,
        private _jobApplicationApiService: JobApplicationApiService,
        private _testApiService: TestApiService,
        private _softSkillApiService: SoftSkillApiService,
    ) {
        // Set the defaults
        this.onJobChanged = new BehaviorSubject({});
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getJob(route.params.jobId)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get job
     *
     * @returns {Promise<any>}
     */
    getJob(id): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._jobApiService.getByUser(id)
                .subscribe((response: any) => {
                    this.job = this._formatJob(response);
                    this.onJobChanged.next(this.job);
                    resolve(this.job);
                }, reject);
        });
    }

    /**
     * Delete job application
     *
     * @returns {Promise<any>}
     */
    deleteJobApplication(id): Promise<any>
    {
        return this._jobApplicationApiService.delete(id).toPromise();
    }

    /**
     * Creates job application
     *
     * @returns {Promise<any>}
     */
    createJobApplication(jobApplication): Promise<any>
    {
        return this._jobApplicationApiService.add(jobApplication).toPromise();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Format job object
     *
     * @param job
     * @return {any[]}
     */
    _formatJob(job: any): any[]
    {
        const result = _.cloneDeep(job);
        if (result.test_personality) {
            this._testApiService.get(job.test_personality).toPromise().then(res => result.test_personality = res);
        }
        if (result.test) {
            this._testApiService.get(job.test).toPromise().then(res => result.test = res);
        }
        return result;
    }

}
