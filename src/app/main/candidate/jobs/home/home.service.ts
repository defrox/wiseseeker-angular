import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable} from 'rxjs';
import {JobApiService} from 'app/_services';

@Injectable()
export class CandidateJobsHomeService implements Resolve<any> {
    onCategoriesChanged: BehaviorSubject<any>;
    onJobsChanged: BehaviorSubject<any>;

    /**
     * Constructor
     *
     * @param {JobApiService} _jobApiService
     */
    constructor(
        private _jobApiService: JobApiService
    ) {
        // Set the defaults
        this.onCategoriesChanged = new BehaviorSubject({});
        this.onJobsChanged = new BehaviorSubject({});
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                // this.getCategories(),
                this.getJobs()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get categories
     *
     * @returns {Promise<any>}
     */
/*    getCategories(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._jobApiService.getCategories()
                .subscribe((response: any) => {
                    this.onCategoriesChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }*/

    /**
     * Get jobs
     *
     * @returns {Promise<any>}
     */
    getJobs(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._jobApiService.getAllByUser()
                .subscribe((response: any) => {
                    this.onJobsChanged.next(response);
                    resolve(response);
                }, reject);
        });
    }

}
