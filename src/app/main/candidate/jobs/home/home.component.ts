import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {CandidateJobsHomeService} from './home.service';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'candidate-jobs-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
    animations: fuseAnimations
})
export class CandidateJobsHomeComponent implements OnInit, OnDestroy {
    categories: any[];
    jobs: any[];
    jobsFilteredByCategory: any[];
    filteredJobs: any[];
    currentCategory: string;
    searchTerm: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {CandidateJobsHomeService} _candidateJobsHomeService
     */
    constructor(
        private _translationLoader: FuseTranslationLoaderService,
        private _candidateJobsHomeService: CandidateJobsHomeService
    ) {
        // Set the defaults
        this.currentCategory = 'all';
        this.searchTerm = '';

        // Set the private defaults
        this._translationLoader.loadTranslations(spanish, english);
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to categories
        /*this._candidateJobsHomeService.onCategoriesChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(categories => {
                this.categories = categories;
            });*/

        // Subscribe to jobs
        this._candidateJobsHomeService.onJobsChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(jobs => {
                this.filteredJobs = this.jobsFilteredByCategory = this.jobs = jobs;
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void
    {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    /**
     * Filter jobs by category
     */
    filterJobsByCategory(): void {
        // Filter
        if (this.currentCategory === 'all') {
            this.jobsFilteredByCategory = this.jobs;
            this.filteredJobs = this.jobs;
        } else {
            this.jobsFilteredByCategory = this.jobs.filter((job) => {
                return job.category === this.currentCategory;
            });

            this.filteredJobs = [...this.jobsFilteredByCategory];

        }

        // Re-filter by search term
        this.filterJobsByTerm();
    }

    /**
     * Filter jobs by term
     */
    filterJobsByTerm(): void {
        const searchTerm = this.searchTerm.toLowerCase();

        // Search
        if (searchTerm === '') {
            this.filteredJobs = this.jobsFilteredByCategory;
        } else {
            this.filteredJobs = this.jobsFilteredByCategory.filter((job) => {
                return (job.title.toLowerCase().includes(searchTerm) || job.description.toLowerCase().includes(searchTerm));
            });
        }
    }
}
