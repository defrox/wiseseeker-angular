import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {CandidateDashboardComponent, CandidateDashboardService} from 'app/main/candidate/dashboard';
import {Big5Module} from 'app/main/candidate/big5/big5.module';
import {ExperienceModule} from 'app/main/candidate/experience/experience.module';
import {JobsModule} from 'app/main/candidate/jobs/jobs.module';
import {KnowledgeModule} from 'app/main/candidate/knowledge/knowledge.module';
import {SkillsModule} from 'app/main/candidate/skills/skills.module';
import {RolesModule} from 'app/main/candidate/roles/roles.module';
import {ExpertiseModule} from 'app/main/candidate/expertise/expertise.module';
// import {TodoModule} from 'app/main/candidate/todo/todo.module';
// import {CandidateJobComponent, CandidateJobService} from 'app/main/candidate/job';
// import {CandidateJobsComponent, CandidateJobsService} from 'app/main/candidate/jobs';
import {CandidatePersonalityComponent, CandidatePersonalityService} from 'app/main/candidate/personality';
import {CandidateProfileComponent, CandidateProfileDialogComponent, CandidateProfileService} from 'app/main/candidate/profile';
import {CandidateHelpComponent} from 'app/main/candidate/help';
import {MaterialModule} from 'app/_helpers/material.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseSidebarModule, FuseWidgetModule} from '@fuse/components';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {TranslateModule} from '@ngx-translate/core';
import {ChartsModule} from 'ng2-charts';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {AuthGuard} from 'app/_guards';
import {DefroxModule} from 'app/_helpers';
import {Group} from 'app/_models';
import {ChartModule} from 'angular-highcharts';


const appRoutes: Routes = [
    {path: 'candidate', pathMatch: 'prefix', children: [
        {path: '', redirectTo: 'dashboard', pathMatch: 'full'},
        {path: 'dashboard', component: CandidateDashboardComponent, resolve: {dashboard: CandidateDashboardService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        // {path: 'jobs', component: CandidateJobsComponent, resolve: {jobs: CandidateJobsService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate]}},
        // {path: 'jobs/:jobId/:jobSlug', component: CandidateJobComponent, resolve: {jobs: CandidateJobService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate]}},
        {path: 'personality', component: CandidatePersonalityComponent, resolve: {personality: CandidatePersonalityService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'profile', component: CandidateProfileComponent, resolve: {jobs: CandidateProfileService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'help', component: CandidateHelpComponent, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'big5', loadChildren: 'app/main/candidate/big5/big5.module#Big5Module'},
        {path: 'experience', loadChildren: 'app/main/candidate/experience/experience.module#ExperienceModule'},
        {path: 'jobs', loadChildren: 'app/main/candidate/jobs/jobs.module#JobsModule'},
        {path: 'knowledge', loadChildren: 'app/main/candidate/knowledge/knowledge.module#KnowledgeModule'},
        {path: 'roles', loadChildren: 'app/main/candidate/roles/roles.module#RolesModule'},
        {path: 'skills', loadChildren: 'app/main/candidate/skills/skills.module#SkillsModule'},
        {path: 'expertise', loadChildren: 'app/main/candidate/expertise/expertise.module#ExpertiseModule'},
        // {path: 'todo', loadChildren: 'app/main/candidate/todo/todo.module#TodoModule'},
        // otherwise redirect to 404
        {path: '**', pathMatch: 'full', redirectTo: '/error/404'},
    ]}
];

@NgModule({
    declarations: [
        CandidateDashboardComponent,
        // CandidateJobComponent,
        // CandidateJobsComponent,
        CandidatePersonalityComponent,
        CandidateProfileComponent,
        CandidateHelpComponent,
        CandidateProfileDialogComponent
    ],
    imports: [
        RouterModule.forChild(appRoutes),
        Big5Module,
        ExperienceModule,
        JobsModule,
        KnowledgeModule,
        RolesModule,
        SkillsModule,
        ExpertiseModule,
        // TodoModule,
        MaterialModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseWidgetModule,
        TranslateModule,
        ChartsModule,
        NgxChartsModule,
        FontAwesomeModule,
        DefroxModule,
        ChartModule
    ],
    providers: [
        CandidateDashboardService,
        // CandidateJobService,
        // CandidateJobsService,
        CandidatePersonalityService,
        CandidateProfileService,
        AuthGuard
    ],
    entryComponents: [CandidateProfileDialogComponent],
    exports: [RouterModule]
})
export class CandidateRouting {
}
