import {locale as main_locale} from 'app/main/i18n/es';

export const locale = {
    lang: 'es',
    data: {
        'CANDIDATE': {
            'DASHBOARD': {
                'PENDING_TASKS_MSG': 'Tienes {tasks} tareas pendientes.',
                'PERSONALITY': {
                    'TITLE': 'Personalidad'
                },
                'KNOWLEDGE': {
                    'TITLE': 'Conocimientos'
                },
                'EXPERTISE': {
                    'TITLE': 'Habilidades Agile'
                },
                'JOBS': {
                    'TITLE': 'Procesos Abiertos',
                    'DATE_APPLIED': 'Fecha aplicación:'
                },
                'EXPERIENCE': {
                    'TITLE': 'Experiencia',
                    'YEARS': 'Años',
                    'COMPANIES': 'Empresas',
                },
                'SKILLS': {
                    'TITLE': 'Habilidades',
                    'GENERAL': 'Disposición general a la venta',
                    'RECEPTIVITY': 'Receptividad comercial',
                    'AGGRESSIVITY': 'Agresividad comercial',
                },
                'ROLES': {
                    'TITLE': 'Roles'
                },
                'PROFILE': {
                    'TITLE': 'Perfil'
                },
                'NONE_YET': 'Esto está muy vacío'
            },
            'HELP': {
                'WELCOME_TEXT': 'AYUDA',
                'WELCOME_SUBTEXT': 'Soluciona tus dudas',
                'CONTENT_TEXT': '<p>En la esquina superior derecha de la pantalla aparece una bola roja que indica el número de tareas que tienes pendiente.</p>' +
                    '<p>Pulsando en ella se desplegará un menú desde el que podrás acceder a las evaluaciones a través de los links que aparecerán en el lateral derecho de la pantalla.</p>' +
                    '<p>Este proceso contempla dos evaluaciones: un test de conocimientos y un test de personalidad.</p>' +
                    '<p>La evaluación de conocimientos tiene una duración de 30’ que será controlada por la propia plataforma y que no podrá ser interrumpida, por lo que es necesario que busques un espacio cómodo y aislado de distracciones para evaluarte.</p>' +
                    '<p>La evaluación de personalidad tiene una duración aproximada de unos 10’ y podrá ser interrumpida y retomada en cualquier momento. Para cualquier duda o incidencia que surja  durante el proceso de evaluación puedes contactarnos a través de la dirección de correo <a href="mailto:help@thewiseseeker.com">help@thewiseseeker.com</a>.</p>',
            },
            'PROFILE': {
                'WELCOME_TEXT': 'DATOS DE USUARIO',
                'WELCOME_SUBTEXT': 'Revisa tu perfil',
            },
            'PERSONALITY': {
                'WELCOME_TEXT': 'PERSONALIDAD',
                'WELCOME_SUBTEXT': 'Revisa los rasgos de tu personalidad',
            },
            'EXPERIENCE': {
                'HOME': {
                    'WELCOME_TEXT': 'EXPERIENCIA',
                    'WELCOME_SUBTEXT': 'Revisa tu curriculum vitae',
                    'CONTENT_TEXT': 'Aún no tienes ninguna experiencia. ¡Añade alguna!',
                },
                'EDITOR': {
                    'WELCOME_TEXT': 'EXPERIENCIA',
                    'WELCOME_SUBTEXT': 'Revisa tu curriculum vitae',
                    'NEW_EXPERIENCE': 'Nueva experiencia',
                },
                'DETAIL': {
                    'WELCOME_TEXT': 'EXPERIENCIA',
                    'WELCOME_SUBTEXT': 'Revisa tu curriculum vitae',
                },
                'DIALOG': {
                    'DELETE_TITLE': 'Confirmar eliminación',
                    'DELETE_TEXT': '¿Está seguro de querer borrar esta experiencia? No se podrá recuperar la información una vez eliminada.',
                    'DELETED_SUCCESSFULLY': '¡Experiencia eliminada con éxito!',
                    'SAVED_SUCCESSFULLY': '¡Experiencia guardada con éxito!',
                },
            },
            'KNOWLEDGE': {
                'HOME': {
                    'WELCOME_TEXT': 'CONOCIMIENTOS',
                    'WELCOME_SUBTEXT': 'Revisa tus conocimientos',
                    'CONTENT_TEXT': 'Aún no tienes conocimientos. ¡Añade algunos!',
                },
                'LIST': {
                    'WELCOME_TEXT': 'CONOCIMIENTOS',
                    'WELCOME_SUBTEXT': 'Revisa tus conocimientos',
                },
                'QUESTIONNAIRE': {
                    'DIALOG_TITLE_RETAKE': 'Volver a realizar',
                    'DIALOG_TEXT_RETAKE': 'Tienes un resultado previo guardado. ¿Deseas repetir el test? Al repetir se perderán todos los resultados e intentos previos.',
                    'DIALOG_TITLE_CONTINUE': 'Continuar intento anterior',
                    'DIALOG_TEXT_CONTINUE': 'Tienes un intento previo guardado. ¿Deseas continuar desde lo dejaste la última vez o prefieres comenzar de nuevo? Al comenzar de nuevo se perderá toda la información previa.',
                },
                'RESULT': {
                    'WELCOME_TEXT': 'RESULTADO DE CONOCIMIENTOS',
                    'WELCOME_SUBTEXT': 'Revisa tus calificaciones',
                    'PROCESSING_RESULTS': 'Aun estamos procesando los resultados, vuelva a intentarlo más tarde.'
                }
            },
            'EXPERTISE': {
                'HOME': {
                    'WELCOME_TEXT': 'HABILIDADES',
                    'WELCOME_SUBTEXT': 'Revisa tus habilidades',
                    'CONTENT_TEXT': 'Aún no tienes habilidades. ¡Añade algunas!',
                },
                'LIST': {
                    'WELCOME_TEXT': 'HABILIDADES',
                    'WELCOME_SUBTEXT': 'Revisa tus habilidades',
                },
                'QUESTIONNAIRE': {
                    'DIALOG_TITLE_RETAKE': 'Volver a realizar',
                    'DIALOG_TEXT_RETAKE': 'Tienes un resultado previo guardado. ¿Deseas repetir el test? Al repetir se perderán todos los resultados e intentos previos.',
                    'DIALOG_TITLE_CONTINUE': 'Continuar intento anterior',
                    'DIALOG_TEXT_CONTINUE': 'Tienes un intento previo guardado. ¿Deseas continuar desde lo dejaste la última vez o prefieres comenzar de nuevo? Al comenzar de nuevo se perderá toda la información previa.',
                },
                'RESULT': {
                    'WELCOME_TEXT': 'RESULTADO DE HABILIDADES AGILE',
                    'WELCOME_SUBTEXT': 'Revisa tus calificaciones',
                }
            },
            'JOBS': {
                'HOME': {
                    'WELCOME_TEXT': 'PROCESOS ABIERTOS',
                    'WELCOME_SUBTEXT': 'Accede a tus evaluaciones',
                },
                'APPLY': {
                    'NOT_FOUND': 'No se ha encontrado la oferta solicitada',
                },
                'DETAIL': {
                    'PERSONALITY_RELEVANCE_TITLE': 'Relevancia de la personalidad',
                    'PERSONALITY_TEST_TITLE': 'Test de personalidad',
                    'TEST_TITLE': 'Roles',
                    'SKILLS_TITLE': 'Habilidades',
                    'APPLICATIONS_TITLE': 'Aplicaciones',
                },
                'DIALOG': {
                    'DELETE_TITLE': 'Confirmar eliminación',
                    'DELETE_TEXT': '¿Está seguro de querer borrar esta candidatura? No se podrá recuperar la información una vez eliminado.',
                    'REGISTER_TITLE': 'Registro requerido',
                    'REGISTER_TEXT': 'Para poder aplicar a la oferta, antes debe registrarse en nuestra plataforma.',
                    'DELETED_SUCCESSFULLY': '¡Candidatura eliminada con éxito!',
                    'ADDED_SUCCESSFULLY': '¡Has aplicado con éxito al empleo!',
                },
                'EDITOR': {
                    'NEW_JOB': 'Nuevo empleo',
                    'STEP_1_LABEL': 'Nombre y descripción',
                    'STEP_1_TEXT_1': 'Dale un nombre al empleo',
                    'STEP_1_TEXT_2': 'Dale una descripción',
                    'STEP_2_LABEL': 'Inicio y Fin',
                    'STEP_3_LABEL': 'Imagen e Icono',
                    'STEP_3_TEXT_A': 'Selecciona la imagen del proceso',
                    'STEP_3_LABEL_A': 'Imagen',
                    'STEP_3_TEXT_B': 'Selecciona el icono del proceso',
                    'STEP_3_LABEL_B': 'Icono',
                    'STEP_4_LABEL': 'Personalidad',
                    'STEP_4_TEXT_A': 'Elige la relevancia que tiene la personalidad en este empleo',
                    'STEP_4_LABEL_A': 'Relevancia de la Personalidad',
                    'STEP_4_TEXT_B': 'Elige los rasgos de personalidad para este empleo',
                    'STEP_4_LABEL_B': 'Rasgos de Personalidad',
                    'STEP_5_TEXT': 'Elige el test de personalidad para este empleo',
                    'STEP_5_LABEL': 'Test Personalidad',
                    'STEP_6_TEXT': 'Selecciona los roles para este empleo',
                    'STEP_6_LABEL': 'Roles',
                    'STEP_FINAL_TEXT': 'Se ha creado un nuevo empleo'
                }
            },
            'BIG5': {
                'QUESTION_VALUES': {
                    '1': 'Totalmente en desacuerdo',
                    '2': 'En desacuerdo',
                    '3': 'Ni de acuerdo ni en desacuerdo',
                    '4': 'De acuerdo',
                    '5': 'Totalmente de acuerdo',
                },
                'START': {
                    'WELCOME_TEXT': 'PERSONALIDAD',
                    'WELCOME_SUBTEXT': 'Cuestionario Big Five',
                    'CONTENT_TEXT': 'Utilizaremos el modelo de los cinco grandes o Big Five, que mide cinco dimensiones de tu personalidad que se determinan a partir de las respuestas a un cuestionario muy simple.' +
                        '</p><p>' +
                        'Nuestra plataforma irá proponiéndote situaciones cotidianas en las que deberás posicionarte con una de las siguientes cinco opciones:' +
                        '</p><div class="big-five-values">' +
                        '<div class="big-five-value-item"><span class="green-fg mat-icon material-icons" role="img" aria-hidden="true">sentiment_very_satisfied</span> Totalmente de acuerdo</div>' +
                        '<div class="big-five-value-item"><span class="lime-fg mat-icon material-icons" role="img" aria-hidden="true">mood</span> De acuerdo</div>' +
                        '<div class="big-five-value-item"><span class="amber-fg mat-icon material-icons" role="img" aria-hidden="true">sentiment_satisfied</span> Ni de acuerdo ni en desacuerdo</div>' +
                        '<div class="big-five-value-item"><span class="orange-fg mat-icon material-icons" role="img" aria-hidden="true">sentiment_dissatisfied</span> En desacuerdo</div>' +
                        '<div class="big-five-value-item"><span class="red-fg mat-icon material-icons" role="img" aria-hidden="true">sentiment_very_dissatisfied</span> Totalmente en desacuerdo</div>' +
                        '</div><p>' +
                        'Un ejemplo de pregunta podría ser: “Me gustan las fiestas”. Ante esta aseveración tú deberás determinar cómo de acuerdo estás con ella, situándote en alguna de las cinco opciones de respuesta anteriores.' +
                        '</p><p>' +
                        'Todo el cuestionario es así de sencillo.' +
                        '</p><p>' +
                        'El resultado determinará donde te encuentras en relación con los siguientes rasgos de personalidad: el factor O (“openness” o apertura a nuevas experiencias), el factor C (“conscientiousness” o responsabilidad), el factor E (“extraversión” o extroversión), el factor A (“agreeableness” o amabilidad) y el actor N (“neuroticism” o inestabilidad emocional).' +
                        '</p><p>' +
                        'Tendrás que contestar a un total de 50 preguntas, y lo normal es que tardes unos 15 minutos en acabarlo.' +
                        '</p><p>' +
                        'Pulsa el botón y empecemos.</p>',
                },
                'QUESTIONNAIRE': {
                    'DIALOG_TITLE_RETAKE': 'Volver a realizar',
                    'DIALOG_TEXT_RETAKE': 'Tienes un resultado previo guardado. ¿Deseas repetir el test? Al repetir se perderán todos los resultados e intentos previos.',
                    'DIALOG_TITLE_CONTINUE': 'Continuar intento anterior',
                    'DIALOG_TEXT_CONTINUE': 'Tienes un intento previo guardado. ¿Deseas continuar desde lo dejaste la última vez o prefieres comenzar de nuevo? Al comenzar de nuevo se perderá toda la información previa.',
                },
                'RESULT': {
                    'WELCOME_TEXT': 'RESULTADOS BIG 5',
                    'WELCOME_SUBTEXT': 'Revisa los rasgos de tu personalidad',
                }
            },
            'ROLES': {
                'HOME': {
                    'WELCOME_TEXT': 'ROLES',
                    'WELCOME_SUBTEXT': 'Revisa tus roles',
                    'CONTENT_TEXT': 'Aún no tienes roles. ¡Añade algunos!',
                },
                'LIST': {
                    'WELCOME_TEXT': 'ROLES',
                    'WELCOME_SUBTEXT': 'Revisa tus roles',
                },
                'DETAIL': {
                    'LAST_ASSESSMENT': 'Última evaluación',
                },
                'QUESTIONNAIRE': {
                    'DIALOG_TITLE_RETAKE': 'Volver a realizar',
                    'DIALOG_TEXT_RETAKE': 'Tienes un resultado previo guardado. ¿Deseas repetir el test? Al repetir se perderán todos los resultados e intentos previos.',
                    'DIALOG_TITLE_CONTINUE': 'Continuar intento anterior',
                    'DIALOG_TEXT_CONTINUE': 'Tienes un intento previo guardado. ¿Deseas continuar desde lo dejaste la última vez o prefieres comenzar de nuevo? Al comenzar de nuevo se perderá toda la información previa.',
                },
                'RESULT': {
                    'WELCOME_TEXT': 'RESULTADO DE ROL',
                    'WELCOME_SUBTEXT': 'Revisa tus calificaciones',
                }
            },
            'SKILLS': {
                'START': {
                    'WELCOME_TEXT': 'HABILIDADES',
                    'WELCOME_SUBTEXT': 'Test de habilidades comerciales',
                    'CONTENT_TEXT': 'En la actualidad nuestra plataforma sólo contiene un módulo de habilidades que está centrado en la identificación de ciertos parámetros e indicadores en el candidato que miden su predisposición a la venta.</p>' +
                        '<p>Para evaluar estas habilidades comerciales nos apoyamos en la metodología IPV (Inventario de Personalidad para Vendedores) que explora diversos rasgos de personalidad que son deseables en personas que se desempeñan como vendedores en todo tipo de ramos de ventas. Estas características comunes han podido agruparse en diez rasgos de personalidad, que se relacionan con la profesión del vendedor y que, por tanto, han sido objeto de constante estudio.</p>' +
                        '<p>Esta metodología está basada en la elaboración de un test creado por el "centre de Psychologie Apliquée" de parís en 1977, que tiene como finalidad, medir la personalidad del vendedor y a través de ella poder identificar el mejor desempeño comercial.</p>' +
                        '<p>Este test consta de 87 preguntas, mide 9 dimensiones esenciales para empleos de ventas y relaciones comerciales.</p>' +
                        '<p>Vamos a ver qué buen comercial eres ;)',
                },
                'QUESTIONNAIRE': {
                    'DIALOG_TITLE_RETAKE': 'Volver a realizar',
                    'DIALOG_TEXT_RETAKE': 'Tienes un resultado previo guardado. ¿Deseas repetir el test? Al repetir se perderán todos los resultados e intentos previos.',
                    'DIALOG_TITLE_CONTINUE': 'Continuar intento anterior',
                    'DIALOG_TEXT_CONTINUE': 'Tienes un intento previo guardado. ¿Deseas continuar desde lo dejaste la última vez o prefieres comenzar de nuevo? Al comenzar de nuevo se perderá toda la información previa.',
                },
                'RESULT': {
                    'WELCOME_TEXT': 'HABILIDADES',
                    'WELCOME_SUBTEXT': 'Resultados de tus habilidades comerciales',
                }
            },
            'COMMON': {
                'TYPE_HERE_YOUR_ANSWER': 'Escriba aquí su respuesta',
                'DIALOG': {
                    'CONFIRM_EXIT_TITLE': 'Terminar evaluación',
                    'CONFIRM_EXIT_TEXT': 'Aún no has finalizado tu evaluación. Si decides salir ahora, no podrás volver a realizar la evaluación. ¿De verdad quieres terminar?',
                    'EXTERNAL_OPENED': {
                        'TITLE': '¡Atención!',
                        'TEXT': 'La evaluación que has seleccionado se realizará en la plataforma de Devskiller.</p>' +
                            '<p>En tu navegador debería haberse abierto una nueva ventana. Si no es así revisa tu configuración por si las ventanas emergentes estuviesen bloqueadas.</p>' +
                            '<p>Una vez realizada la evaluación, los resultados aparecerán en tu zona de conocimientos.'
                    }
                }
            },
        }
    }
};

Object.assign(locale.data, locale.data, main_locale.data);
