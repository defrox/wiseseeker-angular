import {locale as main_locale} from 'app/main/i18n/en';

export const locale = {
    lang: 'en',
    data: {
        'CANDIDATE': {
            'DASHBOARD': {
                'PENDING_TASKS_MSG': 'You have {tasks} pending tasks.',
                'PERSONALITY': {
                    'TITLE': 'Personality'
                },
                'KNOWLEDGE': {
                    'TITLE': 'Knowledge'
                },
                'EXPERTISE': {
                    'TITLE': 'Digital expertise'
                },
                'JOBS': {
                    'TITLE': 'Jobs',
                    'DATE_APPLIED': 'Date applied:'
                },
                'EXPERIENCE': {
                    'TITLE': 'Experience',
                    'YEARS': 'Years',
                    'COMPANIES': 'Companies',
                },
                'SKILLS': {
                    'TITLE': 'Skills',
                    'GENERAL': 'Sales attitude',
                    'RECEPTIVITY': 'Sales receptivity',
                    'AGGRESSIVITY': 'Sales aggressivity',
                },
                'ROLES': {
                    'TITLE': 'Roles'
                },
                'PROFILE': {
                    'TITLE': 'Profile'
                },
                'NONE_YET': 'None yet'
            },
            'HELP': {
                'WELCOME_TEXT': 'HELP',
                'WELCOME_SUBTEXT': 'Answers to your questions',
                'CONTENT_TEXT': 'Your help text.',
            },
            'PROFILE': {
                'WELCOME_TEXT': 'USER DETAILS',
                'WELCOME_SUBTEXT': 'Review your profile.',
            },
            'PERSONALITY': {
                'WELCOME_TEXT': 'PERSONALITY',
                'WELCOME_SUBTEXT': 'Review your personality.',
            },
            'EXPERIENCE': {
                'HOME': {
                    'WELCOME_TEXT': 'EXPERIENCE',
                    'WELCOME_SUBTEXT': 'Review your experience.',
                    'CONTENT_TEXT': 'You have no experience yet. Please add some!',
                },
                'EDITOR': {
                    'WELCOME_TEXT': 'EXPERIENCE',
                    'WELCOME_SUBTEXT': 'Review your experience.',
                    'NEW_EXPERIENCE': 'Nueva experiencia',
                },
                'DETAIL': {
                    'WELCOME_TEXT': 'EXPERIENCIA',
                    'WELCOME_SUBTEXT': 'Revisa tu curriculum vitae',
                },
                'DIALOG': {
                    'DELETE_TITLE': 'Confirmar eliminación',
                    'DELETE_TEXT': '¿Está seguro de querer borrar esta candidatura? No se podrá recuperar la información una vez eliminado.',
                    'DELETED_SUCCESSFULLY': '¡Candidatura eliminada con éxito!',
                    'SAVED_SUCCESSFULLY': '¡Has aplicado con éxito al empleo!',
                },
            },
            'KNOWLEDGE': {
                'HOME': {
                    'WELCOME_TEXT': 'KNOWLEDGE',
                    'WELCOME_SUBTEXT': 'Review your knowledge.',
                    'CONTENT_TEXT': 'You have no knowledge yet. Please add some!',
                },
                'LIST': {
                    'WELCOME_TEXT': 'KNOWLEDGE',
                    'WELCOME_SUBTEXT': 'Review your knowledge.',
                },
                'QUESTIONNAIRE': {
                    'DIALOG_TITLE_RETAKE': 'Repeat test',
                    'DIALOG_TEXT_RETAKE': 'You have a saved result. Would you like to cancel or you prefer to repeat the test? If you repeat all previous results and attempts will be deleted.',
                    'DIALOG_TITLE_CONTINUE': 'Continue saved attempt',
                    'DIALOG_TEXT_CONTINUE': 'You have a saved attempt. Would you like to continue where you left or you prefer to start again from scratch? If you start from scratch the previous attempt will be deleted.',
                },
                'RESULT': {
                    'WELCOME_TEXT': 'KNOWLEDGE RESULT',
                    'WELCOME_SUBTEXT': 'Review your marks.',
                    'PROCESSING_RESULTS': 'Aun estamos procesando los resultados, vuelva a intentarlo más tarde.'
                }
            },
            'EXPERTISE': {
                'HOME': {
                    'WELCOME_TEXT': 'DIGITAL EXPERTISE',
                    'WELCOME_SUBTEXT': 'Review your digital expertise.',
                    'CONTENT_TEXT': 'You have no digital expertise yet. Please add some!',
                },
                'LIST': {
                    'WELCOME_TEXT': 'DIGITAL EXPERTISE',
                    'WELCOME_SUBTEXT': 'Review your digital expertise.',
                },
                'QUESTIONNAIRE': {
                    'DIALOG_TITLE_RETAKE': 'Repeat test',
                    'DIALOG_TEXT_RETAKE': 'You have a saved result. Would you like to cancel or you prefer to repeat the test? If you repeat all previous results and attempts will be deleted.',
                    'DIALOG_TITLE_CONTINUE': 'Continue saved attempt',
                    'DIALOG_TEXT_CONTINUE': 'You have a saved attempt. Would you like to continue where you left or you prefer to start again from scratch? If you start from scratch the previous attempt will be deleted.',
                },
                'RESULT': {
                    'WELCOME_TEXT': 'DIGITAL EXPERTISE RESULT',
                    'WELCOME_SUBTEXT': 'Review your marks.',
                }
            },
            'JOBS': {
                'HOME': {
                    'WELCOME_TEXT': 'WELCOME TO JOBS',
                    'WELCOME_SUBTEXT': 'Review jobs.',
                },
                'APPLY': {
                    'NOT_FOUND': 'We couldn\'t find the requested job position',
                },
                'DETAIL': {
                    'PERSONALITY_RELEVANCE_TITLE': 'Relevancia de la personalidad',
                    'PERSONALITY_TEST_TITLE': 'Test de personalidad',
                    'TEST_TITLE': 'Roles',
                    'SKILLS_TITLE': 'Habilidades',
                    'APPLICATIONS_TITLE': 'Applications',
                },
                'DIALOG': {
                    'DELETE_TITLE': 'Confirm deletion',
                    'DELETE_TEXT': 'Are you sure you want to delete this job application? You will not be able to restore the data once deleted.',
                    'REGISTER_TITLE': 'Registration required',
                    'REGISTER_TEXT': 'In order to apply, you must register first.',
                    'DELETED_SUCCESSFULLY': 'Job application deleted successfully!',
                    'ADDED_SUCCESSFULLY': 'You have applied to the job successfully!',
                },
                'EDITOR': {
                    'NEW_JOB': 'Nuevo empleo',
                    'STEP_1_LABEL': 'Nombre y descripción',
                    'STEP_1_TEXT_1': 'Dale un nombre al empleo',
                    'STEP_1_TEXT_2': 'Dale una descripción',
                    'STEP_2_LABEL': 'Inicio y Fin',
                    'STEP_3_LABEL': 'Imagen e Icono',
                    'STEP_3_TEXT_A': 'Selecciona la imagen del proceso',
                    'STEP_3_LABEL_A': 'Imagen',
                    'STEP_3_TEXT_B': 'Selecciona el icono del proceso',
                    'STEP_3_LABEL_B': 'Icono',
                    'STEP_4_LABEL': 'Personalidad',
                    'STEP_4_TEXT_A': 'Elige la relevancia que tiene la personalidad en este empleo',
                    'STEP_4_LABEL_A': 'Relevancia de la Personalidad',
                    'STEP_4_TEXT_B': 'Elige los rasgos de personalidad para este empleo',
                    'STEP_4_LABEL_B': 'Rasgos de Personalidad',
                    'STEP_5_TEXT': 'Elige el test de personalidad para este empleo',
                    'STEP_5_LABEL': 'Test Personalidad',
                    'STEP_6_TEXT': 'Selecciona los roles para este empleo',
                    'STEP_6_LABEL': 'Roles',
                    'STEP_FINAL_TEXT': 'Se ha creado un nuevo empleo'
                }
            },
            'BIG5': {
                'START': {
                    'WELCOME_TEXT': 'PERSONALITY',
                    'WELCOME_SUBTEXT': 'Big Five Questionnaire',
                    'CONTENT_TEXT': 'We use the Big Five personality traits, also known as the five factor model (FFM) that is a model ' +
                        'based on common language descriptors of personality. When factor analysis (a statistical technique) ' +
                        'is applied to personality survey data, some words used to describe aspects of personality are often\n' +
                        'applied to the same person.</p>' +
                        '<p>The five factors have been defined as openness to ' +
                        'experience, conscientiousness, extraversion, ' +
                        'agreeableness, and neuroticism, often represented by the ' +
                        'acronyms OCEAN.</p>' +
                        '<p>Our 60­question test extracts your personality by ' +
                        'weighing each of these five factors.</p>' +
                        '<p>And remember that it is not about approving but to ' +
                        'extract your true personality.</p>' +
                        '<p>Just be yourself!',
                },
                'QUESTIONNAIRE': {
                    'DIALOG_TITLE_RETAKE': 'Repeat test',
                    'DIALOG_TEXT_RETAKE': 'You have a saved result. Would you like to cancel or you prefer to repeat the test? If you repeat all previous results and attempts will be deleted.',
                    'DIALOG_TITLE_CONTINUE': 'Continue saved attempt',
                    'DIALOG_TEXT_CONTINUE': 'You have a saved attempt. Would you like to continue where you left or you prefer to start again from scratch? If you start from scratch the previous attempt will be deleted.',
                },
                'RESULT': {
                    'WELCOME_TEXT': 'BIG 5 RESULTS',
                    'WELCOME_SUBTEXT': 'Review your big 5 personality traits.',
                }
            },
            'ROLES': {
                'HOME': {
                    'WELCOME_TEXT': 'ROLES',
                    'WELCOME_SUBTEXT': 'Review your roles.',
                    'CONTENT_TEXT': 'You have no roles yet. Please add some!',
                },
                'LIST': {
                    'WELCOME_TEXT': 'ROLES',
                    'WELCOME_SUBTEXT': 'Review your roles.',
                },
                'DETAIL': {
                    'LAST_ASSESSMENT': 'Last assessment',
                },
                'QUESTIONNAIRE': {
                    'DIALOG_TITLE_RETAKE': 'Repeat test',
                    'DIALOG_TEXT_RETAKE': 'You have a saved result. Would you like to cancel or you prefer to repeat the test? If you repeat all previous results and attempts will be deleted.',
                    'DIALOG_TITLE_CONTINUE': 'Continue saved attempt',
                    'DIALOG_TEXT_CONTINUE': 'You have a saved attempt. Would you like to continue where you left or you prefer to start again from scratch? If you start from scratch the previous attempt will be deleted.',
                },
                'RESULT': {
                    'WELCOME_TEXT': 'ROLE RESULT',
                    'WELCOME_SUBTEXT': 'Review your marks.',
                }
            },
            'SKILLS': {
                'START': {
                    'WELCOME_TEXT': 'SKILLS',
                    'WELCOME_SUBTEXT': 'Sales Skills Test',
                    'CONTENT_TEXT': 'IPV test',
                },
                'QUESTIONNAIRE': {
                    'DIALOG_TITLE_RETAKE': 'Repeat test',
                    'DIALOG_TEXT_RETAKE': 'You have a saved result. Would you like to cancel or you prefer to repeat the test? If you repeat all previous results and attempts will be deleted.',
                    'DIALOG_TITLE_CONTINUE': 'Continue saved attempt',
                    'DIALOG_TEXT_CONTINUE': 'You have a saved attempt. Would you like to continue where you left or you prefer to start again from scratch? If you start from scratch the previous attempt will be deleted.',
                },
                'RESULT': {
                    'WELCOME_TEXT': 'SKILLS',
                    'WELCOME_SUBTEXT': 'Sales Skills Test',
                }
            },
            'COMMON': {
                'TYPE_HERE_YOUR_ANSWER': 'Type here your answer',
                'DIALOG': {
                    'CONFIRM_EXIT_TITLE': 'Confirm Exit',
                    'CONFIRM_EXIT_TEXT': 'You have not finished the test. All your progress will be lost if you proceed. Continue anyway?',
                    'EXTERNAL_OPENED': {
                        'TITLE': 'Prueba externa abierta',
                        'TEXT': 'Has abierto una evaluación externa. Cuando termines pulsa sobre el botón terminar para que podamos procesar los resultados. En el caso contrario pulsa sobre cancelar para abandonar la evaluación. '
                    }
                }
            },
        }
    }
};

Object.assign(locale.data, locale.data, main_locale.data);
