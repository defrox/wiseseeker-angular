import {AfterViewInit, ChangeDetectorRef, Component, HostListener, OnDestroy, OnInit, QueryList, ViewChildren, ViewEncapsulation, ViewChild, Inject} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FusePerfectScrollbarDirective} from '@fuse/directives/fuse-perfect-scrollbar/fuse-perfect-scrollbar.directive';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {FuseSidebarService} from '@fuse/components/sidebar/sidebar.service';
import {Router, ActivatedRoute} from '@angular/router';
import {Big5ResultService} from 'app/main/candidate/big5/result/result.service';
import {Big5QuestionnaireService} from './questionnaire.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {Answer, TestExecution, DialogData} from 'app/_models';
import {LoaderOverlayService, NotificationService} from 'app/_services';
import * as _ from 'lodash';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';


@Component({
    selector: 'candidate-big5-questionnaire',
    templateUrl: './questionnaire.component.html',
    styleUrls: ['./questionnaire.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class Big5QuestionnaireComponent implements OnInit, OnDestroy, AfterViewInit {
    animationDirection: 'left' | 'right' | 'none';
    groupingValue = 5;
    questionnaire: any;
    questionnaireStepContent: any;
    currentStep: number;
    questionnaireTpl: Object = {
        id: 'big5',
        title: 'Big Five Questionnaire',
        slug: 'big5-questionnaire',
        description: 'The big 5 questionnaire.'
    };
    previousExecutions: any | null;
    lastResult: any | null;
    lastAttempt: any | null;
    submitted = false;
    testId: string | number | null;
    testSlug: string | null;
    // testId = 22;

    @ViewChildren(FusePerfectScrollbarDirective)
    fuseScrollbarDirectives: QueryList<FusePerfectScrollbarDirective>;

    // Private
    private _unsubscribeAll: Subject<any>;

    @HostListener('document:keydown', ['$event'])
    handleKeyboardEvent($event: KeyboardEvent): void {
        if ($event.key === 'f' && $event.ctrlKey && $event.altKey) {
            this.currentStep = this.questionnaire.steps.length - 1;
        }
    }

    /**
     * Constructor
     *
     * @param {Big5QuestionnaireService} _big5QuestionnaireService
     * @param {Big5ResultService} _big5ResultService
     * @param {ChangeDetectorRef} _changeDetectorRef
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {NotificationService} _notificationsService
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {MatDialog} _dialog
     * @param {ActivatedRoute} _route
     * @param {Router} _router
     */
    constructor(
        private _big5QuestionnaireService: Big5QuestionnaireService,
        private _big5ResultService: Big5ResultService,
        private _changeDetectorRef: ChangeDetectorRef,
        private _fuseSidebarService: FuseSidebarService,
        private _translationLoader: FuseTranslationLoaderService,
        private _notificationsService: NotificationService,
        private _loaderOverlay: LoaderOverlayService,
        private _dialog: MatDialog,
        private _route: ActivatedRoute,
        private _router: Router
    ) {
        // Set the defaults
        this._loaderOverlay.show();
        this._cleanUp();
        this.animationDirection = 'none';
        this.currentStep = 0;
        this._translationLoader.loadTranslations(spanish, english);
        this._route.paramMap.subscribe(params => {
            this.testId = params.get('testId') ? params.get('testId') : 22;
            this.testSlug = params.get('testSlug') ? params.get('testSlug') : 'big5';
            this._big5QuestionnaireService.getAllTestExecutions(+this.testId)
            .then(executions => {
                if (executions.length > 0) {
                    this.previousExecutions = executions;
                    const lastFinishedExecution = this._getLastFinishedExecution(executions);
                    if (lastFinishedExecution) {
                        this.lastResult = this._formatExecution(lastFinishedExecution.answers);
                    }
                    const lastUnfinishedExecution = this._getLastUnfinishedExecution(executions);
                    if (lastUnfinishedExecution) {
                        this.lastAttempt = this._formatExecution(lastUnfinishedExecution.answers);
                    }
                    this._openRetakeExecutionDialog();
                } else {
                    this._newExecutionTest(+this.testId);
                }
            });
        });

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to questionnaires
        this._big5QuestionnaireService.onQuestionnaireChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(questionnaire => {
                this.questionnaire = this._formatQuestionnaire(questionnaire.questions);
            });

        // Subscribe to results
        this._big5ResultService.onResultChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(results => {
                // console.log(results);
            });
    }

    /**
     * After view init
     */
    ngAfterViewInit(): void {
        this.questionnaireStepContent = this.fuseScrollbarDirectives.find((fuseScrollbarDirective) => {
            return fuseScrollbarDirective.elementRef.nativeElement.id === 'questionnaire-step-content';
        });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._cleanUp();
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
        this._notificationsService.resetUserNotifications();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Clean up results
     *
     * @returns {void}
     */
    _cleanUp(): void {
        this.questionnaire = null;
        this.previousExecutions = null;
        this.lastResult = null;
        this.lastAttempt = null;
        this._big5ResultService.resetBig5Results();
    }

    /**
     * Format questionnaire
     *
     * @param questions
     * @returns {any}
     */
    _formatQuestionnaire(questions): any {
        let questionnaireStepContent = [];
        let questionnaireStep = [];
        let questionnaireResults: any = {};
        let counter = 0;
        let step = 1;
        if (questions.length === 0) {
            return;
        }
        for (const question of questions) {
            questionnaireResults[question.id] = 'NA';
            questionnaireStep.push(question);
            counter ++;
            if (counter >= this.groupingValue) {
                questionnaireStepContent.push({
                    'title': step,
                    'questions': questionnaireStep
                });
                questionnaireStep = [];
                counter = 0;
                step ++;
            }
        }
        return Object.assign({steps: questionnaireStepContent, results: questionnaireResults}, this.questionnaireTpl);
    }

    /**
     * Format execution
     *
     * @param answers
     * @returns {any}
     */
    _formatExecution(answers: Answer[] = []): any {
        if (answers.length === 0) {
            return;
        }
        const result = [];
        for (const answer of answers) {
            result[answer.question] = answer.option;
        }
        return result;
    }

    /**
     * Get last unfinished execution
     *
     * @param executions
     * @returns {execution}
     */
    _getLastUnfinishedExecution(executions: TestExecution[] = []): TestExecution | null {
        if (executions.length === 0) {
            return;
        }
        return executions.filter(item => +item.status === 1)
            .sort((a, b) => {
                a = a.created;
                b = b.created;
                if (a > b) {
                    return -1;
                } else if (a < b) {
                    return 1;
                }
                return 0;
            })[0];
    }

    /**
     * Get last finished execution
     *
     * @param executions
     * @returns {execution}
     */
    _getLastFinishedExecution(executions: TestExecution[] = []): TestExecution | null {
        if (executions.length === 0) {
            return;
        }
        return executions.filter(item => +item.status === 2 || +item.status === 3)
            .sort((a, b) => {
                a = a.created;
                b = b.created;
                if (a > b) {
                    return -1;
                } else if (a < b) {
                    return 1;
                }
                return 0;
            })[0];
    }

    /**
     * Open retake execution dialog
     *
     * @returns {void}
     */
    _openRetakeExecutionDialog(): void {
        this._loaderOverlay.hide();
        const dialogRef = this._dialog.open(Big5QuestionnaireDialogComponent, {
            data: {lastResult: this.lastResult, lastAttempt: this.lastAttempt}
        });
        dialogRef.afterClosed().subscribe(result => {
            if (result === 'cancel') {
                // Cancel retake
                this._loaderOverlay.show();
                this._router.navigate([`/candidate/big5/result/${this.testId}/${this.testSlug}`]).then(() => this._loaderOverlay.hide());
            } else if (result) {
                // Continue last attempt
                if (this.lastAttempt && this.lastAttempt.length > 0) {
                    this.questionnaire.results = this.lastAttempt;
                }
                this.questionnaire.execution = this.previousExecutions[0];
            } else {
                // Start again
                this._loaderOverlay.show();
                this._deletePreviousExecutions(this.previousExecutions);
                this._newExecutionTest(+this.testId);
            }
        });
    }

    /**
     * Creates a new test execution
     *
     * @param testId
     * @returns {void}
     */
    _newExecutionTest(testId: number): void {
        const execution = {'test_id': testId};
        this._big5QuestionnaireService.newTestExecution(execution).then(
            res => {
                this.questionnaire.execution = res;
            }).then(() => this._loaderOverlay.hide());
    }

    /**
     * Deletes all previous test executions
     *
     * @param executions
     * @returns {void}
     */
    _deletePreviousExecutions(executions: TestExecution[] = []): void {
        for (const execution of executions) {
            this._big5QuestionnaireService.deleteTestExecution(execution);
        }
    }

    /**
     * Filters by status id
     *
     * @param item
     * @param statusId
     * @returns {boolean}
     */
    private _filterByStatus(item: any, statusId: number): boolean {
        return +item.status === +statusId;
    }

    /**
     * Filters by statuses id
     *
     * @param item
     * @param statusIds
     * @returns {boolean}
     */
    private _filterByStatuses(item: any, statusIds: number[]): any {
        return statusIds.indexOf(item.status) !== -1;
    }


    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Go to step
     *
     * @param step
     */
    gotoStep(step): void {
        // Decide the animation direction
        this.animationDirection = this.currentStep < step ? 'left' : 'right';

        // Run change detection so the change
        // in the animation direction registered
        this._changeDetectorRef.detectChanges();

        // Set the current step
        this.currentStep = step;
    }

    /**
     * Go to next step
     */
    gotoNextStep(): void {
        if (this.currentStep === this.questionnaire.totalSteps - 1) {
            return;
        }

        // Set the animation direction
        this.animationDirection = 'left';

        // Run change detection so the change
        // in the animation direction registered
        this._changeDetectorRef.detectChanges();

        // Increase the current step
        this.currentStep++;
    }

    /**
     * Go to previous step
     */
    gotoPreviousStep(): void {
        if (this.currentStep === 0) {
            return;
        }

        // Set the animation direction
        this.animationDirection = 'right';

        // Run change detection so the change
        // in the animation direction registered
        this._changeDetectorRef.detectChanges();

        // Decrease the current step
        this.currentStep--;
    }

    /**
     * Submit the results
     *
     * @param execution
     */
    submitExecution(execution): void {
        if (!this.submitted) {
            this.submitted = true;
            this._loaderOverlay.show();
            const executionCopy = Object.assign({}, execution);
            executionCopy.status = 2;
            this._big5QuestionnaireService.submitTestExecution(executionCopy).then(res => {
                this._big5ResultService.setBig5Results(res);
                this._notificationsService.resetUserNotifications();
                this._router.navigate([`/candidate/big5/result/${this.testId}/${this.testSlug}`]).then(() => this._loaderOverlay.hide());
            }).catch( err => {
                // do something in case of error
                this._loaderOverlay.hide();
                this.submitted = false;
                throw new Error(err);
            });
        }
    }

    /**
     * Save the answer(s)
     *
     * @param answer
     */
    saveAnswer(answer: Answer | Answer[]): void {
        this._big5QuestionnaireService.saveBig5Answer(answer);
    }

    /**
     * Toggle the sidebar
     *
     * @param name
     */
    toggleSidebar(name): void {
        this._fuseSidebarService.getSidebar(name).toggleOpen();
    }
}

@Component({
    selector: 'candidate-big5-questionnaire-dialog',
    templateUrl: './questionnaire.dialog.component.html',
    styleUrls: ['./questionnaire.component.scss'],
})
export class Big5QuestionnaireDialogComponent {
    constructor(
        private _dialogRef: MatDialogRef<Big5QuestionnaireDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
        _dialogRef.disableClose = true;
    }
}
