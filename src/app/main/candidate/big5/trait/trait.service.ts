import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {AuthenticationService} from 'app/_services';
import {User, Personality} from 'app/_models';

@Injectable()
export class Big5TraitService implements Resolve<any> {
    onTraitsChanged: BehaviorSubject<any>;
    personality: Personality;
    currentUserSubscription: Subscription;
    currentUser: User;

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     * @param {AuthenticationService} _authenticationService
     */
    constructor(
        private _httpClient: HttpClient,
        private _authenticationService: AuthenticationService
    ) {
        // Set the defaults
        this.onTraitsChanged = new BehaviorSubject({});
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getTraits(this.currentUser.id)
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get traits
     *
     * @param userId
     * @returns {Promise<any>}
     */
    getTraits(userId): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._httpClient.get<Personality>('api/candidate-personality/' + userId)
                .subscribe((response: any) => {
                    this.personality = response;
                    this.onTraitsChanged.next(this.personality);
                    resolve(this.personality);
                }, error => {
                    this.personality = null;
                    resolve(this.personality);
                });
        });
    }

    /**
     * Get personality
     *
     * @param userId
     * @returns {Observable<any>}
     */
    getUserPersonality(userId: number): Observable<any>
    {
        return this._httpClient.get<Personality>('api/candidate-personality/' + userId).catch(this.handleError);
    }

    private handleError(error: any): any {
        return throwError(error);
    }

}
