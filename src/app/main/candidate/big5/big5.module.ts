import {NgModule} from '@angular/core';
import {Big5Routing} from 'app/main/candidate/big5/big5.routing';
import {ApiModule, DefroxModule} from 'app/_helpers';

@NgModule({
    declarations: [],
    imports: [
        ApiModule,
        DefroxModule,
        Big5Routing
    ]
})
export class Big5Module {
}
