import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {Big5StartComponent} from 'app/main/candidate/big5/start';
import {Big5QuestionnaireComponent, Big5QuestionnaireService, Big5QuestionnaireDialogComponent} from 'app/main/candidate/big5/questionnaire';
import {Big5ResultComponent, Big5ResultService} from 'app/main/candidate/big5/result';
import {Big5TraitComponent, Big5TraitService} from 'app/main/candidate/big5/trait';
import {MaterialModule} from 'app/_helpers/material.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {FuseSidebarModule, FuseWidgetModule} from '@fuse/components';
import {TranslateModule} from '@ngx-translate/core';
import {ChartModule} from 'angular-highcharts';
import {AuthGuard} from 'app/_guards';
import {Group} from 'app/_models';

const appRoutes: Routes = [
    {path: 'candidate/big5', children: [
        {path: '', redirectTo: 'start', pathMatch: 'full'},
        {path: 'start', component: Big5StartComponent, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'start/:testId/:testSlug', component: Big5StartComponent, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'questionnaire', component: Big5QuestionnaireComponent, resolve: {dashboard: Big5QuestionnaireService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'questionnaire/:testId/:testSlug', component: Big5QuestionnaireComponent, resolve: {dashboard: Big5QuestionnaireService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'result', component: Big5ResultComponent, resolve: {dashboard: Big5ResultService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'result/:testId/:testSlug', component: Big5ResultComponent, resolve: {dashboard: Big5ResultService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate, Group.External]}},
        {path: 'trait', component: Big5TraitComponent, resolve: {dashboard: Big5TraitService}, canActivate: [AuthGuard], data: {groups: [Group.Candidate]}},
        // otherwise redirect to 404
        {path: '**', redirectTo: '/error/404', pathMatch: 'full'}
    ]}
];

@NgModule({
    declarations: [
        Big5StartComponent,
        Big5QuestionnaireComponent,
        Big5ResultComponent,
        Big5TraitComponent,
        Big5QuestionnaireDialogComponent,
        ],
    imports: [
        RouterModule.forChild(appRoutes),
        MaterialModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseWidgetModule,
        TranslateModule,
        ChartModule
    ],
    providers: [
        Big5QuestionnaireService,
        Big5ResultService,
        Big5TraitService,
        AuthGuard
    ],
    entryComponents: [Big5QuestionnaireDialogComponent],
    exports: [RouterModule]
})
export class Big5Routing { }
