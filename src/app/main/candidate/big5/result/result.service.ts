import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {AuthenticationService, TestExecutionApiService} from 'app/_services';
import {User, Personality} from 'app/_models';

@Injectable()
export class Big5ResultService implements Resolve<any> {
    onResultChanged: BehaviorSubject<any>;
    onPersonalityChanged: BehaviorSubject<any>;
    onTestExecutionChanged: BehaviorSubject<any>;
    onCurrentExecutionChanged: BehaviorSubject<any>;
    currentUserSubscription: Subscription;
    currentUser: User;
    personality: Personality;
    lastExecution: any;
    currentExecution: any;
    result: any;
    testId = 22;

    /**
     * Constructor
     *
     * @param {TestExecutionApiService} _testExecutionApiService
     * @param {AuthenticationService} _authenticationService
     */
    constructor(
        private _testExecutionApiService: TestExecutionApiService,
        private _authenticationService: AuthenticationService
    ) {
        // Set the defaults
        this.onResultChanged = new BehaviorSubject({});
        this.onTestExecutionChanged = new BehaviorSubject({});
        this.onCurrentExecutionChanged = new BehaviorSubject({});
        this.onPersonalityChanged = new BehaviorSubject({});
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getAllTestExecutions(route.params.testId ? route.params.testId : this.testId, [2, 3])
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get all test executions
     *
     * @param testId
     * @param statusIds
     * @returns {Promise<any>}
     */
    getAllTestExecutions(testId: number, statusIds: number[]): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testExecutionApiService.getByUserAndTest(+this.currentUser.id, testId)
                .subscribe((executions: any) => {
                    // this.lastExecution = executions.filter(item => this._filterByTestIdAndStatus(item, testId, statusIds));
                    this.lastExecution = executions;
                    this.onTestExecutionChanged.next(this.lastExecution);
                    resolve(this.lastExecution);
                }, reject);
        });
    }

    /**
     * Get test execution by id
     *
     * @param executionId
     * @returns {Observable<any>}
     */
    getTestExecution(executionId: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testExecutionApiService.get(executionId)
                .subscribe((execution: any) => {
                    this.lastExecution = execution;
                    resolve(this.lastExecution);
                }, reject);
        });
    }

    /**
     * Set Big 5 Results
     *
     * @param results
     */
    setBig5Results(results): void {
        this.onResultChanged.next(results);
    }

    /**
     * Get Big 5 Results
     *
     * @returns {Observable<any>}
     */
    getBig5Results(): Observable<any> {
        return this.onResultChanged.asObservable();
    }

    /**
     * Reset Big 5 Results
     */
    resetBig5Results(): void {
        this.onResultChanged.next([]);
    }

    /**
     * Set Current Execution
     *
     * @param execution
     */
    setCurrentExecution(execution): void {
        this.onCurrentExecutionChanged.next(execution);
    }

    /**
     * Get Current Execution
     *
     * @returns {Observable<any>}
     */
    getCurrentExecution(): Observable<any> {
        return this.onCurrentExecutionChanged.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private _filterByTestIdAndStatus(item: any, testId: number, statusIds: number[]): any {
        return +item.test.id === +testId && statusIds.indexOf(item.status) !== -1;
    }

    private handleError(error: any): any {
        return throwError(error);
    }

}
