import {Component, ViewEncapsulation} from '@angular/core';
import {fuseAnimations} from '@fuse/animations';
import {ActivatedRoute} from '@angular/router';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'candidate-big5-start',
    templateUrl: './start.component.html',
    styleUrls: ['./start.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class Big5StartComponent {
    testId: string | number | null;
    testSlug: string | null;

    /**
     * Constructor
     *
     * @param {ActivatedRoute} _route
     * @param {FuseTranslationLoaderService} _translationLoader
     */
    constructor(
        private _route: ActivatedRoute,
        private _translationLoader: FuseTranslationLoaderService
    ) {
        this._translationLoader.loadTranslations(spanish, english);
        this._route.paramMap.subscribe(params => {
            this.testId = params.get('testId') ? params.get('testId') : 22;
            this.testSlug = params.get('testSlug') ? params.get('testSlug') : 'big5';
        });
    }
}
