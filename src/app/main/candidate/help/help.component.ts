import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {TranslatePipe} from '@ngx-translate/core';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {User} from 'app/_models';
import {AuthenticationService, LoaderOverlayService, SnackAlertService} from 'app/_services';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'candidate-help',
    templateUrl: './help.component.html',
    styleUrls: ['./help.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class CandidateHelpComponent implements OnInit, OnDestroy {
    currentUser: User;
    currentUserSubscription: Subscription;
    notifications: any;
    notificationsBadges: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {TranslatePipe} _translatePipe
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {SnackAlertService} _snackAlertService
     * @param {AuthenticationService} _authenticationService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {Router} _router
     */
    constructor(
        private _translatePipe: TranslatePipe,
        private _loaderOverlay: LoaderOverlayService,
        private _snackAlertService: SnackAlertService,
        private _authenticationService: AuthenticationService,
        private _translationLoader: FuseTranslationLoaderService,
        private _router: Router
    ) {
        // Set the private defaults
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
        this._translationLoader.loadTranslations(spanish, english);
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

}
