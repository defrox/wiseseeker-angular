import {Component, OnInit, OnDestroy, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {User, Personality, TestExecution} from 'app/_models';
import {AppConfigService, AuthenticationService} from 'app/_services';
import {CandidatePersonalityService} from './personality.service';
import {Chart} from 'angular-highcharts';
import {centeredGaugeFormatter} from 'app/_helpers/formatters';
import * as _ from 'lodash';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'candidate-personality',
    templateUrl: './personality.component.html',
    styleUrls: ['./personality.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class CandidatePersonalityComponent implements OnInit, OnDestroy {
    appConfig: any;
    options: any;
    gauge: Chart;
    personality: Personality;
    currentUser: User;
    currentUserSubscription: Subscription;
    lastExecution: any;
    assessment: any = null;
    widgets: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {CandidatePersonalityService} _candidatePersonalityService
     * @param {AuthenticationService} _authenticationService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {TranslateService} _translateService
     * @param {AppConfigService} _appConfigService
     * @param {Router} _router
     */
    constructor(
        private _candidatePersonalityService: CandidatePersonalityService,
        private _authenticationService: AuthenticationService,
        private _translationLoader: FuseTranslationLoaderService,
        private _translateService: TranslateService,
        private _appConfigService: AppConfigService,
        private _router: Router
    ) {
        // Set the private defaults
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
        this._appConfigService.config.subscribe(config => { this.appConfig = config; });
        this._translationLoader.loadTranslations(spanish, english);
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.personality = null;

        // Subscribe to personality
        this._candidatePersonalityService.onPersonalityChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(executions => {
                this.lastExecution = this._getLastAssessedExecution(executions);
                if (!this.lastExecution) {
                    this._router.navigate(['/candidate/big5/start']);
                }
                this.assessment = this._getAssessment(this.lastExecution);
                this.widgets = this._formatWidgets(this.assessment);
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
        this.personality = null;

        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Get last assessed execution
     *
     * @param executions
     * @returns {execution}
     */
    _getLastAssessedExecution(executions: TestExecution[] = []): TestExecution | null {
        if (executions.length === 0) {
            return;
        }
        return executions.filter(item => +item.status === 3)
            .sort((a, b) => {
                a = a.created;
                b = b.created;
                if (a > b) {
                    return -1;
                } else if (a < b) {
                    return 1;
                }
                return 0;
            })[0];
    }

    /**
     * Get execution assessment
     *
     * @param execution
     * @returns {execution}
     */
    _getAssessment(execution: TestExecution = null): TestExecution | null {
        if (!execution || execution.assessed.length === 0) {
            return;
        }
        return execution.assessed;
    }

    _formatWidgets(data): any {
        const widgets = {};
        if (data) {
            for (const item of Object.keys(data)) {
                const option = _.cloneDeep(this.appConfig.solidGaugeOptionsCenter);
                const colors = this.appConfig.solidGaugeOptionsCenterColors;
                option.title.text = this._translateService.instant('MAIN.TRAITS.' + data[item].skill.description.toUpperCase() + '.TITLE').toUpperCase();
                option.yAxis.max = data[item].maximum;
                option.yAxis.min = data[item].minimum ? data[item].minimum : 0;
                option.yAxis.stops = [
                    [0, colors[item].min],
                    [0.5, colors[item].min],
                    [0.5, colors[item].max],
                    [1, colors[item].max]
                ];
                option.plotOptions.solidgauge.threshold = (option.yAxis.max - option.yAxis.min) / 2 + option.yAxis.min;
                // option.plotOptions.solidgauge.formatter = function (): number { return (this.y - option.plotOptions.solidgauge.threshold) * 2; };
                option.series = [{data: [data[item].punctuation]}];
                for (const band of Object.keys(option.yAxis.plotBands)) {
                    if (+band === 0) {
                        option.yAxis.plotBands[band].from = option.yAxis.min;
                        option.yAxis.plotBands[band].to = option.plotOptions.solidgauge.threshold;
                        option.yAxis.plotBands[band].color = colors[item].min;
                    }
                    if (+band === 1) {
                        option.yAxis.plotBands[band].from = option.plotOptions.solidgauge.threshold;
                        option.yAxis.plotBands[band].to = option.yAxis.max;
                        option.yAxis.plotBands[band].color = colors[item].max;
                    }
                }
                widgets[item] = new Chart(option as any);
            }
        }
        return widgets;
    }

}
