import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {AuthenticationService, TestExecutionApiService} from 'app/_services';
import {User, Personality} from 'app/_models';

@Injectable()
export class CandidatePersonalityService implements Resolve<any> {
    onPersonalityChanged: BehaviorSubject<any>;
    personality: Personality;
    currentUserSubscription: Subscription;
    lastExecution: any;
    currentUser: User;

    /**
     * Constructor
     *
     * @param {TestExecutionApiService} _testExecutionApiService
     * @param {AuthenticationService} _authenticationService
     */
    constructor(
        private _testExecutionApiService: TestExecutionApiService,
        private _authenticationService: AuthenticationService
    ) {
        // Set the defaults
        this.onPersonalityChanged = new BehaviorSubject({});
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getAllTestExecutions(2, [2, 3])
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get all test executions
     *
     * @param testId
     * @param statusIds
     * @returns {Promise<any>}
     */
    getAllTestExecutions(testId: number, statusIds: number[]): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testExecutionApiService.getAll()
                .subscribe((executions: any) => {
                    this.lastExecution = executions.filter(item => this._filterByTestIdAndStatus(item, testId, statusIds));
                    this.onPersonalityChanged.next(this.lastExecution);
                    resolve(this.lastExecution);
                }, reject);
        });
    }

    /**
     * Get test execution by id
     *
     * @param executionId
     * @returns {Observable<any>}
     */
    getTestExecution(executionId: number): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._testExecutionApiService.get(executionId)
                .subscribe((execution: any) => {
                    this.lastExecution = execution;
                    resolve(this.lastExecution);
                }, reject);
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    private _filterByTestIdAndStatus(item: any, testId: number, statusIds: number[]): any {
        return +item.test.id === +testId && statusIds.indexOf(item.status) !== -1;
    }

    private handleError(error: any): any {
        return throwError(error);
    }
}
