import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {BehaviorSubject, Observable, Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {AuthenticationService, ExperienceApiService, UserApiService} from 'app/_services';
import {User} from 'app/_models';

@Injectable()
export class CandidateProfileService implements Resolve<any> {
    currentUser: User;
    currentUserSubscription: Subscription;
    onProfileChanged: BehaviorSubject<any>;
    profile: any = {};

    /**
     * Constructor
     *
     * @param {TranslateService} _translateService
     * @param {ExperienceApiService} _experienceApiService
     * @param {UserApiService} _userApiService
     * @param {AuthenticationService} _authenticationService
     */
    constructor(
        private _translateService: TranslateService,
        private _experienceApiService: ExperienceApiService,
        private _userApiService: UserApiService,
        private _authenticationService: AuthenticationService,
    ) {
// Set the private defaults
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
        this.onProfileChanged = new BehaviorSubject({});
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Resolver
     *
     * @param {ActivatedRouteSnapshot} route
     * @param {RouterStateSnapshot} state
     * @returns {Observable<any> | Promise<any> | any}
     */
    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Promise<any> | any {
        return new Promise((resolve, reject) => {

            Promise.all([
                this.getProfile(),
                this.getExperience()

            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Get profile
     *
     * @returns {Promise<any>}
     */
    getProfile(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._userApiService.getById(+this.currentUser.id).toPromise().then(
                res => {
                    this.profile.profile = res;
                    this.onProfileChanged.next(this.profile);
                    resolve(this.profile);
                }, reject);
        });
    }

    /**
     * Get experience
     *
     * @returns {Promise<any>}
     */
    getExperience(): Promise<any>
    {
        return new Promise((resolve, reject) => {
            this._experienceApiService.getAllByUser().toPromise().then(
                res => {
                    let years = 0;
                    this.profile.experience = null;
                    if (res && res.length > 0) {
                        for (const company of res) {
                            const from = new Date(company.working_from);
                            const to = new Date(company.working_to);
                            years += Math.abs((to.getTime() - from.getTime()) / (1000 * 60 * 60 * 24 * 365));
                        }
                        this.profile.experience = {companies: res && res.length > 0 ? res.length : 0, years: Math.round(years), table: res };
                    }
                    this.onProfileChanged.next(this.profile);
                    resolve(this.profile);
                }, reject);
        });
    }

    /**
     * Delete experience
     *
     * @returns {Promise<any>}
     */
    deleteExperience(id): Promise<any>
    {
        return this._experienceApiService.delete(id).toPromise();
    }
}
