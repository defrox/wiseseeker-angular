import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {Router} from '@angular/router';
import {TranslatePipe} from '@ngx-translate/core';
import {takeUntil} from 'rxjs/operators';
import {fuseAnimations} from '@fuse/animations';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {MatDialog, MatDialogRef} from '@angular/material';
import {Group, User} from 'app/_models';
import {AuthenticationService, LoaderOverlayService, NotificationService, SnackAlertService} from 'app/_services';
import {CandidateProfileService} from './profile.service';

// Import the locale files
import {locale as english} from 'app/main/candidate/i18n/en';
import {locale as spanish} from 'app/main/candidate/i18n/es';

@Component({
    selector: 'candidate-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class CandidateProfileComponent implements OnInit, OnDestroy {
    currentUser: User;
    currentUserSubscription: Subscription;
    notifications: any;
    notificationsBadges: any;
    profile: any;
    experience: any;
    experienceDisplayedColumns: string[] = ['working_from', 'working_to', 'title', 'enterprise', 'actions'];
    groupCandidate: Group = Group.Candidate;
    groupExternal: Group = Group.External;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {TranslatePipe} _translatePipe
     * @param {MatDialog} _dialog
     * @param {LoaderOverlayService} _loaderOverlay
     * @param {SnackAlertService} _snackAlertService
     * @param {CandidateProfileService} _candidateProfileService
     * @param {NotificationService} _notificationsService
     * @param {AuthenticationService} _authenticationService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {Router} _router
     */
    constructor(
        private _translatePipe: TranslatePipe,
        private _dialog: MatDialog,
        private _loaderOverlay: LoaderOverlayService,
        private _snackAlertService: SnackAlertService,
        private _candidateProfileService: CandidateProfileService,
        private _notificationsService: NotificationService,
        private _authenticationService: AuthenticationService,
        private _translationLoader: FuseTranslationLoaderService,
        private _router: Router
    ) {
        // Set the private defaults
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
        this._translationLoader.loadTranslations(spanish, english);
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // Subscribe to profile
        this._candidateProfileService.onProfileChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(profile => {
                this.profile = profile.profile;
                this.experience = profile.experience ? profile.experience : null;
            });
        // Subscribe to Notifications
        this._notificationsService.onNotificationChanged
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(notifications => {
                this.notifications = notifications;
                this.notificationsBadges = this._notificationsService.getCandidateNotificationsBadges(notifications);
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------
    deleteExperience(experienceId): void {
        const dialogRef = this._dialog.open(CandidateProfileDialogComponent);
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                // delete confirmed
                this._loaderOverlay.show();
                this._candidateProfileService.deleteExperience(experienceId).then( () => {
                    this._candidateProfileService.getExperience().then(() => {
                        this._loaderOverlay.hide();
                        this._snackAlertService.success(this._translatePipe.transform('CANDIDATE.EXPERIENCE.DIALOG.DELETED_SUCCESSFULLY'));
                        this._notificationsService.resetUserNotifications();
                    });
                });
            } else {
                // cancel delete
                this._loaderOverlay.hide();
            }
        });
    }

}

@Component({
    selector: 'candidate-profile-dialog',
    templateUrl: './profile.dialog.component.html',
    styleUrls: ['./profile.component.scss'],
})
export class CandidateProfileDialogComponent {
    constructor(private _dialogRef: MatDialogRef<CandidateProfileDialogComponent>) {
        _dialogRef.disableClose = true;
    }
}
