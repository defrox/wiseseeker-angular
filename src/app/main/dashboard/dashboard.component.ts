﻿import {Component, OnInit, OnDestroy} from '@angular/core';
import {Subject, Subscription} from 'rxjs';
import {User} from 'app/_models';
import {AuthenticationService, UserApiService} from 'app/_services';
import {Router} from '@angular/router';

@Component({templateUrl: 'dashboard.component.html'})
export class DashboardComponent implements OnInit, OnDestroy {
    currentUser: User;
    currentUserSubscription: Subscription;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _authenticationService: AuthenticationService,
        private _userService: UserApiService,
        private _router: Router,
    ) {
        this._unsubscribeAll = new Subject();
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
            if (this.currentUser.groups && this.currentUser.groups.length > 0) {
                const groupUrl = this.currentUser.groups[0].toLowerCase() === 'external' ? 'candidate' : this.currentUser.groups[0].toLowerCase();
                this._router.navigate([`/${groupUrl}/dashboard`]);
            }
        });
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
        // unsubscribe to ensure no memory leaks
        this.currentUserSubscription.unsubscribe();
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
}