import {NgModule} from '@angular/core';
import {DashboardRouting} from 'app/main/dashboard/dashboard.routing';


@NgModule({
    imports: [
        DashboardRouting
    ]
})
export class DashboardModule {
}
