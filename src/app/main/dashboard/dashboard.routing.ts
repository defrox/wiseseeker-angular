import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {MaterialModule} from 'app/_helpers/material.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {DashboardComponent} from 'app/main/dashboard';
import {AuthGuard} from 'app/_guards';

const appRoutes: Routes = [
    {path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
];

@NgModule({
    declarations: [
        DashboardComponent
    ],
    imports: [
        RouterModule.forChild(appRoutes),
        MaterialModule,
        FuseSharedModule
    ],
    exports: [RouterModule]
})
export class DashboardRouting {
}
