import {Component, ViewEncapsulation} from '@angular/core';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {FuseConfigService} from '@fuse/services/config.service';
import {fuseAnimations} from '@fuse/animations';

// Import the locale files
import {locale as english} from 'app/main/authentication/i18n/en';
import {locale as spanish} from 'app/main/authentication/i18n/es';

@Component({
    selector: 'mail-confirm',
    templateUrl: './mail-confirm.component.html',
    styleUrls: ['./mail-confirm.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class MailConfirmComponent {
    /**
     * Constructor
     *
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {FuseConfigService} _fuseConfigService
     */
    constructor(
        private _translationLoader: FuseTranslationLoaderService,
        private _fuseConfigService: FuseConfigService
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        // Load translations
        this._translationLoader.loadTranslations(spanish, english);
    }
}
