import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {TranslatePipe} from '@ngx-translate/core';
import {ActivatedRoute, Router} from '@angular/router';
import {first, takeUntil} from 'rxjs/operators';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {FuseConfigService} from '@fuse/services/config.service';
import {FuseProgressBarService} from '@fuse/components/progress-bar/progress-bar.service';
import {AppConfigService} from 'app/_services/config.service';
import {fuseAnimations} from '@fuse/animations';
import {AuthenticationService, SnackAlertService} from 'app/_services';

// Import the locale files
import {locale as english} from 'app/main/authentication/i18n/en';
import {locale as spanish} from 'app/main/authentication/i18n/es';

@Component({
    selector: 'reset-password',
    templateUrl: './reset-password.component.html',
    styleUrls: ['./reset-password.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class ResetPasswordComponent implements OnInit, OnDestroy {
    resetPasswordForm: FormGroup;
    appConfig: any;
    submitted = false;
    returnUrl: string | null;
    token: string | null;

    // Private
    private _unsubscribeAll: Subject<any>;

    constructor(
        private _appConfigService: AppConfigService,
        private _fuseConfigService: FuseConfigService,
        private _translationLoader: FuseTranslationLoaderService,
        private _fuseProgressBarService: FuseProgressBarService,
        private _authenticationService: AuthenticationService,
        private _snackAlertService: SnackAlertService,
        private _translatePipe: TranslatePipe,
        private _router: Router,
        private _route: ActivatedRoute,
        private _formBuilder: FormBuilder
    ) {
        // redirect to home if already logged in
        if (this._authenticationService.currentUserValue) {
            this._router.navigate(['/']);
        }
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        // Load translations
        this._translationLoader.loadTranslations(spanish, english);

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.resetPasswordForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required],
            passwordConfirm: ['', [Validators.required, confirmPasswordValidator]]
        });

        // Subscribe to config change
        this._appConfigService.config
            .subscribe((config) => {
                this.appConfig = config;
                this._route.paramMap.subscribe(params => {
                    this.token = params.get('token');
                });
            });

        // get return url from _route parameters or default to '/'
        this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';

        // Update the validity of the 'passwordConfirm' field
        // when the 'password' field changes
        this.resetPasswordForm.get('password').valueChanges
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe(() => {
                this.resetPasswordForm.get('passwordConfirm').updateValueAndValidity();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }
    
    // convenience getter for easy access to form fields
    get f() {
        return this.resetPasswordForm.controls;
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.resetPasswordForm.invalid) {
            return;
        }

        this._fuseProgressBarService.show();
        this._authenticationService.resetPassword(this.f.email.value, this.f.password.value, this.token)
            .pipe(first())
            .subscribe(
                data => {
                    this._router.navigate(['/auth/login'], { queryParams: { returnUrl: this.returnUrl}}).then(res => {
                        this._snackAlertService.success(this._translatePipe.transform('RESET_PASSWORD.PASSWORD_CHANGED_SUCCESSFULLY'));
                    });
                },
                err => {
                    let errorMessage = '';
                    if (err.error && err.error.hasOwnProperty('detail')) {
                        // django
                        errorMessage = `Error: ${err['error']['detail']}`;
                    } else if (err.error && err.error.hasOwnProperty('non_field_errors')) {
                        // django
                        errorMessage = `Error: ${err['error']['non_field_errors'][0]}`;
                    } else if (err.error instanceof ErrorEvent) {
                        // client-side error
                        errorMessage = `Error: ${err.error.message}`;
                    } else if (err.error && err.error instanceof Object && Object.keys(err.error).length > 0) {
                        // django iterable
                        let message = '\n';
                        Object.keys(err.error).forEach((key) => {
                            message += `${key}: ${err['error'][key]}` + '\n';
                        });
                        errorMessage = `Error: ${message}`;
                    } else if (err.error && err.status && err.statusText && err.message) {
                        // client-side error
                        errorMessage = `Error Code: ${err.status} ${err.statusText}` + '\n' + `Message: ${err.message}`;
                    } else {
                        // server-side error
                        errorMessage = `Error Code: ${err.status}` + '\n' + `Message: ${err.message}`;
                    }
                    this._snackAlertService.error(errorMessage);
                    this._fuseProgressBarService.hide();
                    this.submitted = false;
                });
    }
    
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return {'passwordsNotMatching': true};
};
