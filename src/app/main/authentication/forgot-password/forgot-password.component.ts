import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {FuseConfigService} from '@fuse/services/config.service';
import {AppConfigService} from 'app/_services/config.service';
import {FuseProgressBarService} from '@fuse/components/progress-bar/progress-bar.service';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {TranslatePipe} from '@ngx-translate/core';
import {fuseAnimations} from '@fuse/animations';
import {first} from 'rxjs/operators';
import {AuthenticationService, SnackAlertService} from 'app/_services';

// Import the locale files
import {locale as english} from 'app/main/authentication/i18n/en';
import {locale as spanish} from 'app/main/authentication/i18n/es';

@Component({
    selector: 'forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class ForgotPasswordComponent implements OnInit {
    forgotPasswordForm: FormGroup;
    appConfig: any;
    external = false;
    jobId: number;
    jobSlug: string;
    returnUrl: string | null;
    submitted = false;

    /**
     * Constructor
     *
     * @param {AppConfigService} _appConfigService
     * @param {FuseConfigService} _fuseConfigService
     * @param {TranslatePipe} _translatePipe
     * @param {Router} _router
     * @param {ActivatedRoute} _route
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {FuseProgressBarService} _fuseProgressBarService
     * @param {AuthenticationService} _authenticationService
     * @param {FormBuilder} _formBuilder
     * @param {SnackAlertService} _snackAlertService
     */
    constructor(
        private _appConfigService: AppConfigService,
        private _fuseConfigService: FuseConfigService,
        private _translatePipe: TranslatePipe,
        private _router: Router,
        private _route: ActivatedRoute,
        private _translationLoader: FuseTranslationLoaderService,
        private _fuseProgressBarService: FuseProgressBarService,
        private _authenticationService: AuthenticationService,
        private _formBuilder: FormBuilder,
        private _snackAlertService: SnackAlertService
    ) {
        // redirect to home if already logged in
        if (this._authenticationService.currentUserValue) {
            this._router.navigate(['/']);
        }
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
        // Load translations
        this._translationLoader.loadTranslations(spanish, english);
        // Set external values
        this.external = this._route.snapshot.queryParams['external'] === 'true';
        this.jobId = this._route.snapshot.queryParams['jobId'];
        this.jobSlug = this._route.snapshot.queryParams['jobSlug'];
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // this._fuseProgressBarService.setMode('indeterminate');
        // Subscribe to config change
        this._appConfigService.config
            .subscribe((config) => {
                this.appConfig = config;
            });
        // disable snackAlertService
        // this._snackAlertService.disable();

        this.forgotPasswordForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });

        // get return url from _route parameters or default to '/'
        this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.forgotPasswordForm.controls;
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.forgotPasswordForm.invalid) {
            return;
        }

        this._fuseProgressBarService.show();
        this._authenticationService.forgotPassword(this.f.email.value)
            .pipe(first())
            .subscribe(
                data => {
                    if (this.external) {
                        this._router.navigate(['/auth/login'], { queryParams: { external: 'true', jobId: this.jobId, jobSlug: this.jobSlug }}).then(res => {
                            this._snackAlertService.success(this._translatePipe.transform('FORGOT_PASSWORD.MAIL_SENT_SUCCESSFULLY'));
                        });
                    } else {
                        this._router.navigate(['/auth/login'], { queryParams: { returnUrl: this.returnUrl}}).then(res => {
                            this._snackAlertService.success(this._translatePipe.transform('FORGOT_PASSWORD.MAIL_SENT_SUCCESSFULLY'));
                        });
                    }
                },
                err => {
                    // For security reasons, no error is returned.
                    this._router.navigate(['/auth/login'], { queryParams: { returnUrl: this.returnUrl}}).then(res => {
                        this._snackAlertService.success(this._translatePipe.transform('FORGOT_PASSWORD.MAIL_SENT_SUCCESSFULLY'));
                    });
                    /*let errorMessage = '';
                    if (err.error && err.error.hasOwnProperty('detail')) {
                        // django
                        errorMessage = `Error: ${err['error']['detail']}`;
                    } else if (err.error && err.error.hasOwnProperty('non_field_errors')) {
                        // django
                        errorMessage = `Error: ${err['error']['non_field_errors'][0]}`;
                    } else if (err.error instanceof ErrorEvent) {
                        // client-side error
                        errorMessage = `Error: ${err.error.message}`;
                    } else if (err.error && err.error instanceof Object && Object.keys(err.error).length > 0) {
                        // django iterable
                        let message = '\n';
                        Object.keys(err.error).forEach((key) => {
                            message += `${key}: ${err['error'][key]}` + '\n';
                        });
                        errorMessage = `Error: ${message}`;
                    } else if (err.error && err.status && err.statusText && err.message) {
                        // client-side error
                        errorMessage = `Error Code: ${err.status} ${err.statusText}` + '\n' + `Message: ${err.message}`;
                    } else {
                        // server-side error
                        errorMessage = `Error Code: ${err.status}` + '\n' + `Message: ${err.message}`;
                    }
                    this._snackAlertService.error(errorMessage);
                    this._fuseProgressBarService.hide();
                    this.submitted = false;*/
                });
    }

}
