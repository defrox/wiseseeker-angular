import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {FuseConfigService} from '@fuse/services/config.service';
import {AppConfigService} from 'app/_services/config.service';
import {FuseProgressBarService} from '@fuse/components/progress-bar/progress-bar.service';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {fuseAnimations} from '@fuse/animations';
import {first} from 'rxjs/operators';
import {AlertService, AuthenticationService, NotificationService, SnackAlertService} from 'app/_services';
import {appConfig} from 'app/app.config';

// Import the locale files
import {locale as english} from 'app/main/authentication/i18n/en';
import {locale as spanish} from 'app/main/authentication/i18n/es';

@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class LoginComponent implements OnInit {
    fuseConfig: any;
    appConfig = appConfig;
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;
    external = false;
    jobId: number;
    jobSlug: string;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseProgressBarService} _fuseProgressBarService
     * @param {FormBuilder} _formBuilder
     * @param {ActivatedRoute} _route
     * @param {Router} _router
     * @param {AuthenticationService} _authenticationService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {NotificationService} _notificationService
     * @param {AlertService} _alertService
     * @param {SnackAlertService} _snackAlertService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _fuseProgressBarService: FuseProgressBarService,
        private _formBuilder: FormBuilder,
        private _route: ActivatedRoute,
        private _router: Router,
        private _authenticationService: AuthenticationService,
        private _translationLoader: FuseTranslationLoaderService,
        private _notificationService: NotificationService,
        private _alertService: AlertService,
        private _snackAlertService: SnackAlertService
    ) {
        // redirect to home if already logged in
        if (this._authenticationService.currentUserValue) {
            this._router.navigate(['/']);
        }
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                },
                alert: {
                    position: 'content'
                }
            }
        };
        // Load translations
        this._translationLoader.loadTranslations(spanish, english);
        // Set external values
        this.external = this._route.snapshot.queryParams['external'] === 'true';
        this.jobId = this._route.snapshot.queryParams['jobId'];
        this.jobSlug = this._route.snapshot.queryParams['jobSlug'];
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this._fuseProgressBarService.setMode('indeterminate');
        // Subscribe to config change
        this._fuseConfigService.config
            .subscribe((config) => {
                this.fuseConfig = config;
            });
        // disable snackAlertService
        this._snackAlertService.disable();

        this.loginForm = this._formBuilder.group({
            // email: ['', [Validators.required, Validators.email]],
            username: ['', Validators.required],
            password: ['', Validators.required]
        });

        // get return url from _route parameters or default to '/'
        this.returnUrl = this._route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    get f() {
        return this.loginForm.controls;
    }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this._fuseProgressBarService.show();
        this._authenticationService.login(this.f.username.value, this.f.password.value)
            .pipe(first())
            .subscribe(
                data => {
                    this._notificationService.reloadUserNotifications();
                    if (this.external) {
                        this._router.navigate(['/candidate/dashboard'], { queryParams: { external: 'true', jobId: this.jobId, jobSlug: this.jobSlug }}).then();
                    } else {
                        this._router.navigate([this.returnUrl]).then();
                    }
                },
                err => {
                    let errorMessage = '';
                    if (err.error && err.error.hasOwnProperty('detail')) {
                        // django
                        errorMessage = `Error: ${err['error']['detail']}`;
                    } else if (err.error && err.error.hasOwnProperty('non_field_errors')) {
                        // django
                        errorMessage = `Error: ${err['error']['non_field_errors'][0]}`;
                    } else if (err.error instanceof ErrorEvent) {
                        // client-side error
                        errorMessage = `Error: ${err.error.message}`;
                    } else if (err.error && err.error instanceof Object && Object.keys(err.error).length > 0) {
                        // django iterable
                        let message = '\n';
                        Object.keys(err.error).forEach((key) => {
                            message += `${key}: ${err['error'][key]}` + '\n';
                        });
                        errorMessage = `Error: ${message}`;
                    } else if (err.error && err.status && err.statusText && err.message) {
                        // client-side error
                        errorMessage = `Error Code: ${err.status} ${err.statusText}` + '\n' + `Message: ${err.message}`;
                    } else {
                        // server-side error
                        errorMessage = `Error Code: ${err.status}` + '\n' + `Message: ${err.message}`;
                    }
                    this._alertService.error(errorMessage);
                    this._fuseProgressBarService.hide();
                    this.submitted = false;
                });
    }
}
