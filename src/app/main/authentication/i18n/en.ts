import {locale as main_locale} from 'app/main/i18n/en';

export const locale = {
    lang: 'en',
    data: {
        'MAIL_CONFIRM': {
            'TITLE': 'Confirm your email address!',
            'TEXT_1': 'A confirmation e-mail has been sent to the email you just provided.',
            'TEXT_2': 'Check your inbox and click on the "Confirm my e-mail" link to confirm your e-mail address.',
            'TEXT_3': 'If you haven\'t received it, you can request a new e-mail in this link',
            'GO_BACK_TO_LOGIN': 'Go back to login',
            'RESEND_MAIL': 'Resend confirmation e-mail',
        },
        'MAIL_VERIFY': {
            'TITLE': 'E-mail address confirmed!',
            'TEXT_1': 'Your e-mail has been confirmed successfully!',
            'TEXT_2': 'You can now login and start using the platform.',
            'GO_BACK_TO_LOGIN': 'Go back to login',
        },
        'FORGOT_PASSWORD': {
            'RECOVER_YOUR_PASSWORD': 'RECOVER YOUR PASSWORD',
            'SEND_RESET_LINK': 'SEND RESET LINK',
            'EMAIL': 'Email',
            'GO_BACK_TO_LOGIN': 'Go back to login',
            'MAIL_SENT_SUCCESSFULLY': 'An email with a link to recover your password was sent successfully. Please check your inbox.',
            'ERROR': {
                'REQ_USERNAME': 'Username is required',
                'REQ_EMAIL': 'Email is required',
                'VALID_EMAIL': 'Please enter a valid email address',
            }
        },
        'RESET_PASSWORD': {
            'RESET_YOUR_PASSWORD': 'RESET YOUR PASSWORD',
            'RESET_MY_PASSWORD': 'RESET MY PASSWORD',
            'EMAIL': 'Email',
            'PASSWORD': 'Password',
            'PASSWORD_CONFIRM': 'Password confirm',
            'GO_BACK_TO_LOGIN': 'Go back to login',
            'PASSWORD_CHANGED_SUCCESSFULLY': 'Your password has been changed successfully.',
            'ERROR': {
                'REQ_PASSWORD': 'Password is required',
                'REQ_PASSWORD_CONFIRM': 'Password confirmation is required',
                'MATCH_PASSWORD': 'Passwords must match',
                'REQ_EMAIL': 'Email is required',
                'VALID_EMAIL': 'Please enter a valid email address',
            }
        },
        'CHANGE_PASSWORD': {
            'CHANGE_YOUR_PASSWORD': 'CHANGE YOUR PASSWORD',
            'CHANGE_MY_PASSWORD': 'CHANGE MY PASSWORD',
            'OLD_PASSWORD': 'Current password',
            'PASSWORD': 'New password',
            'PASSWORD_CONFIRM': 'New password confirm',
            'CANCEL': 'CANCEL',
            'PASSWORD_CHANGED_SUCCESSFULLY': 'Your password has been changed successfully.',
            'ERROR': {
                'REQ_PASSWORD': 'New password is required',
                'REQ_PASSWORD_CONFIRM': 'New password confirmation is required',
                'MATCH_PASSWORD': 'Passwords must match',
                'REQ_OLD_PASSWORD': 'Current password is required',
            }
        },
        'AUTHENTICATION': {
            'LOGIN_TO_YOUR_ACCCOUNT': 'LOGIN TO YOUR ACCOUNT',
            'LOGIN': 'LOGIN',
            'EMAIL': 'Email',
            'USERNAME': 'Username',
            'PASSWORD': 'Password',
            'REMEMBER_ME': 'Remember Me',
            'FORGOT_PASSWORD': 'Forgot Password?',
            'OR': 'OR',
            'GOOGLE_LOGIN': 'Log in with Google',
            'FACEBOOK_LOGIN': 'Log in with Facebook',
            'DONT_HAVE_ACCOUNT': 'Don\'t have an account?',
            'CREATE_ACCOUNT': 'Create an account',
            'ERROR': {
                'REQ_USERNAME': 'Username is required',
                'REQ_EMAIL': 'Email is required',
                'VALID_EMAIL': 'Please enter a valid email address',
                'REQ_PASSWORD': 'Password is required'
            }
        },
        'REGISTRATION': {
            'CREATE_AN_ACCOUNT': 'CREATE AN ACCOUNT',
            'LOGIN': 'Login',
            'USERNAME': 'Username',
            'FIRST_NAME': 'First name',
            'LAST_NAME': 'Last name',
            'PHONE': 'Phone',
            'CITY': 'City',
            'ENTERPRISE': 'Company',
            'POSITION': 'Position',
            'PICTURE': 'Picture',
            'EMAIL': 'Email',
            'PASSWORD': 'Password',
            'PASSWORD_CONFIRM': 'Password confirm',
            'REMEMBER_ME': 'Remember Me',
            'FORGOT_PASSWORD': 'Forgot Password?',
            'OR': 'OR',
            'GOOGLE_LOGIN': 'Log in with Google',
            'FACEBOOK_LOGIN': 'Log in with Facebook',
            'DONT_HAVE_ACCOUNT': 'Don\'t have an account?',
            'ALREADY_HAVE_ACCOUNT': 'Already have an account?',
            'CREATE_ACCOUNT': 'Create an account',
            'CREATE_ACCOUNT_1': 'CREATE ACCOUNT',
            'I_READ_AND_ACCEPT': 'I read and accept',
            'TERMS_AND_CONDITIONS': 'terms and conditions',
            'REGISTER_SUCCESSFULLY': 'You have been registered successfully, check your inbox to activate your account.',
            'DIALOG_TERMS_TITLE': 'Terms and Conditions',
            'DIALOG_TERMS_TEXT': 'Terms and Conditions Text',
            'ERROR': {
                'REQ_USERNAME': 'Username is required',
                'REQ_FIRST_NAME': 'First name is required',
                'REQ_LAST_NAME': 'Last name is required',
                'REQ_EMAIL': 'Email is required',
                'REQ_PHONE': 'Phone is required',
                'REQ_CITY': 'City is required',
                'REQ_POSITION': 'Position is required',
                'REQ_PICTURE': 'Picture is required',
                'VALID_EMAIL': 'Please enter a valid email address',
                'REQ_PASSWORD': 'Password is required',
                'REQ_PASSWORD_CONFIRM': 'Password confirmation is required',
                'MATCH_PASSWORD': 'Passwords must match',
                'REQ_TERMS': 'You must accept the terms and conditions',
            }
        }
    }
};

Object.assign(locale.data, locale.data, main_locale.data);
