import {locale as main_locale} from 'app/main/i18n/es';

export const locale = {
    lang: 'es',
    data: {
        'MAIL_CONFIRM': {
            'TITLE': '¡Confirma tu correo electrónico!',
            'TEXT_1': 'Se ha enviado un email de confirmación al correo electrónico que acaba de proporcionar.',
            'TEXT_2': 'Compruebe su bandeja de entrada y pulse en el enlace "Confirmar mi correo electrónico" para confirmar su dirección de correo electrónico.',
            'TEXT_3': 'Si aun no lo ha recibido, puede volver a solicitar su envío en el siguiente enlace',
            'GO_BACK_TO_LOGIN': 'Volver a la página de inicio de sesión',
            'RESEND_MAIL': 'Reenviar email de confirmación.',
        },
        'MAIL_VERIFY': {
            'TITLE': '¡Ha confirmado su correo electrónico!',
            'TEXT_1': 'Ha confirmado su correo electrónico con éxito.',
            'TEXT_2': 'Ya puede disfrutar de todos los servicios de la plataforma.',
            'GO_BACK_TO_LOGIN': 'Volver a la página de inicio de sesión',
        },
        'FORGOT_PASSWORD': {
            'RECOVER_YOUR_PASSWORD': 'RECUPERAR CONTRASEÑA',
            'SEND_RESET_LINK': 'ENVIAR EMAIL',
            'EMAIL': 'Correo electrónico',
            'GO_BACK_TO_LOGIN': 'Volver a la página de inicio de sesión',
            'MAIL_SENT_SUCCESSFULLY': 'Se le ha enviado un email con el enlace de recuperación de su contraseña. Compruebe su bandeja de entrada.',
            'ERROR': {
                'REQ_USERNAME': 'Se requiere el nombre de usuario',
                'REQ_EMAIL': 'Se requiere el correo electrónico',
                'VALID_EMAIL': 'Introduzca un correo electrónico válido',
            }
        },
        'RESET_PASSWORD': {
            'RESET_YOUR_PASSWORD': 'RESTAURAR CONTRASEÑA',
            'RESET_MY_PASSWORD': 'CAMBIAR MI CONTRASEÑA',
            'EMAIL': 'Correo electrónico',
            'PASSWORD': 'Contraseña',
            'PASSWORD_CONFIRM': 'Confirmación de la contraseña',
            'GO_BACK_TO_LOGIN': 'Volver a la página de inicio de sesión',
            'PASSWORD_CHANGED_SUCCESSFULLY': 'Su contraseña se ha modificado correctamente.',
            'ERROR': {
                'REQ_PASSWORD': 'Se requiere la contraseña',
                'REQ_PASSWORD_CONFIRM': 'Se requiere la confirmación de la contraseña',
                'MATCH_PASSWORD': 'La contraseña no coincide con la confirmación',
                'REQ_EMAIL': 'Se requiere el correo electrónico',
                'VALID_EMAIL': 'Introduzca un correo electrónico válido',
            }
        },
        'CHANGE_PASSWORD': {
            'CHANGE_YOUR_PASSWORD': 'CAMBIAR CONTRASEÑA',
            'CHANGE_MY_PASSWORD': 'CAMBIAR MI CONTRASEÑA',
            'OLD_PASSWORD': 'Contraseña actual',
            'PASSWORD': 'Nueva contraseña',
            'PASSWORD_CONFIRM': 'Confirmación de la nueva contraseña',
            'CANCEL': 'CANCELAR',
            'PASSWORD_CHANGED_SUCCESSFULLY': 'Su contraseña se ha modificado correctamente.',
            'ERROR': {
                'REQ_PASSWORD': 'Se requiere la nueva contraseña',
                'REQ_PASSWORD_CONFIRM': 'Se requiere la confirmación de la nueva contraseña',
                'MATCH_PASSWORD': 'La contraseña no coincide con la confirmación',
                'REQ_OLD_PASSWORD': 'Se la contraseña actual',
            }
        },
        'AUTHENTICATION': {
            'LOGIN_TO_YOUR_ACCCOUNT': 'INICIE SESIÓN EN SU CUENTA',
            'LOGIN': 'INICIAR SESIÓN',
            'EMAIL': 'Correo electrónico',
            'USERNAME': 'Usuario',
            'PASSWORD': 'Contraseña',
            'REMEMBER_ME': 'Recordarme',
            'FORGOT_PASSWORD': '¿Ha olvidado su contraseña?',
            'OR': 'O',
            'GOOGLE_LOGIN': 'Iniciar sesión con Google',
            'FACEBOOK_LOGIN': 'Iniciar sesión con Facebook',
            'DONT_HAVE_ACCOUNT': '¿No tiene cuenta?',
            'CREATE_ACCOUNT': 'Crear una cuenta',
            'ERROR': {
                'REQ_USERNAME': 'Se requiere el nombre de usuario',
                'REQ_EMAIL': 'Se requiere el correo electrónico',
                'VALID_EMAIL': 'Introduzca un correo electrónico válido',
                'REQ_PASSWORD': 'Se requiere la contraseña'
            }
        },
        'REGISTRATION': {
            'CREATE_AN_ACCOUNT': 'CREACIÓN DE CUENTA',
            'LOGIN': 'Iniciar sesión',
            'EMAIL': 'Correo electrónico',
            'USERNAME': 'Usuario',
            'FIRST_NAME': 'Nombre',
            'LAST_NAME': 'Apellidos',
            'PHONE': 'Teléfono',
            'CITY': 'Localidad',
            'ENTERPRISE': 'Empresa',
            'POSITION': 'Puesto',
            'PICTURE': 'Fotografía',
            'PASSWORD': 'Contraseña',
            'PASSWORD_CONFIRM': 'Confirmación de la contraseña',
            'REMEMBER_ME': 'Recordarme',
            'FORGOT_PASSWORD': '¿Ha olvidado su contraseña?',
            'OR': 'O',
            'GOOGLE_LOGIN': 'Iniciar sesión con Google',
            'FACEBOOK_LOGIN': 'Iniciar sesión con Facebook',
            'DONT_HAVE_ACCOUNT': '¿No tiene cuenta?',
            'ALREADY_HAVE_ACCOUNT': '¿Ya dispone de una cuenta?',
            'CREATE_ACCOUNT': 'Crear una cuenta',
            'CREATE_ACCOUNT_1': 'CREAR CUENTA',
            'I_READ_AND_ACCEPT': 'He leído y acepto',
            'TERMS_AND_CONDITIONS': 'los términos y condiciones',
            'REGISTER_SUCCESSFULLY': 'Se ha registrado correctamente, compruebe su bandeja de entrada para activar su cuenta.',
            'DIALOG_TERMS_TITLE': 'Términos y Condiciones',
            'DIALOG_TERMS_TEXT': 'Texto de Términos y Condiciones',
            'ERROR': {
                'REQ_USERNAME': 'Se requiere el nombre de usuario',
                'REQ_FIRST_NAME': 'Se requiere el nombre',
                'REQ_LAST_NAME': 'Se requieren los apellidos',
                'REQ_EMAIL': 'Se requiere el correo electrónico',
                'REQ_PHONE': 'Se requiere el teléfono',
                'REQ_CITY': 'Se requiere la localidad',
                'REQ_POSITION': 'Se requiere el puesto',
                'REQ_PICTURE': 'Se requiere la fotografía',
                'VALID_EMAIL': 'Introduzca un correo electrónico válido',
                'REQ_PASSWORD': 'Se requiere la contraseña',
                'REQ_PASSWORD_CONFIRM': 'Se requiere la confirmación de la contraseña',
                'MATCH_PASSWORD': 'La contraseña no coincide con la confirmación',
                'REQ_TERMS': 'Debe aceptar los términos y condiciones',
            }
        }
    }
};

Object.assign(locale.data, locale.data, main_locale.data);
