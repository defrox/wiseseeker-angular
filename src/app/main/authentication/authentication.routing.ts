import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from 'app/main/authentication/login/login.component';
import {LogoutComponent} from 'app/main/authentication/logout/logout.component';
import {ResetPasswordComponent} from 'app/main/authentication/reset-password/reset-password.component';
import {ChangePasswordComponent} from 'app/main/authentication/change-password/change-password.component';
import {RegisterComponent, RegisterDialogComponent} from 'app/main/authentication/register/register.component';
import {ForgotPasswordComponent} from 'app/main/authentication/forgot-password/forgot-password.component';
import {LockComponent} from 'app/main/authentication/lock/lock.component';
import {MailConfirmComponent} from 'app/main/authentication/mail-confirm/mail-confirm.component';
import {MailVerifyComponent} from 'app/main/authentication/mail-verify/mail-verify.component';
import {AlertModule} from 'app/layout/components/alert/alert.module';
import {MaterialModule} from 'app/_helpers/material.module';
import {FuseSharedModule} from '@fuse/shared.module';
import {TranslateModule} from '@ngx-translate/core';
import {RegisterService} from 'app/main/authentication/register';
import {AuthGuard} from 'app/_guards';
import {Group} from 'app/_models';

const appRoutes: Routes = [
    {path: 'auth', children: [
        {path: 'login', component: LoginComponent},
        {path: 'logout', component: LogoutComponent},
        {path: 'register/:external/:jobId/:jobSlug', component: RegisterComponent},
        {path: 'register', component: RegisterComponent},
        {path: 'forgot-password', component: ForgotPasswordComponent},
        {path: 'lock', component: LockComponent},
        {path: 'mail-confirm', component: MailConfirmComponent},
        {path: 'mail-verify', component: MailVerifyComponent},
        {path: 'reset-password/:token', component: ResetPasswordComponent},
        {path: 'change-password', component: ChangePasswordComponent, canActivate: [AuthGuard], data: {groups: [Group.Recruiter, Group.Candidate, Group.External]}},
        // // otherwise redirect to 404
        {path: '**', redirectTo: '/error/404', pathMatch: 'full'}
    ]},
    {path: 'candidate/change-password', component: ChangePasswordComponent},
];

@NgModule({
    declarations: [
        LoginComponent,
        LogoutComponent,
        RegisterComponent,
        RegisterDialogComponent,
        ForgotPasswordComponent,
        LockComponent,
        MailConfirmComponent,
        MailVerifyComponent,
        ResetPasswordComponent,
        ChangePasswordComponent
    ],
    imports: [
        RouterModule.forChild(appRoutes),
        MaterialModule,
        FuseSharedModule,
        TranslateModule,
        AlertModule
    ],
    exports: [
        RouterModule,
        AlertModule
    ],
    entryComponents: [RegisterDialogComponent],
    providers: [
        RegisterService
    ]
})
export class AuthenticationRouting {
}
