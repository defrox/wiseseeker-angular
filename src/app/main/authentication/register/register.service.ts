import {Injectable} from '@angular/core';
import {JobApplicationApiService, UserApiService} from 'app/_services';


@Injectable()
export class RegisterService {

    /**
     * Constructor
     *
     * @param {UserApiService} _userApiService
     * @param {JobApplicationApiService} _jobApplicationApiService
     */
    constructor(
        private _userApiService: UserApiService,
        private _jobApplicationApiService: JobApplicationApiService,
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Register user
     *
     * @param {any} user
     * @returns {Promise<any>}
     */
    registerUser(user: any): Promise<any>
    {
        return this._userApiService.register(user).toPromise();
    }

    /**
     * Register users
     *
     * @param {any[]} users
     * @returns {Promise<any>}
     */
    registerUsers(users: any[]): Promise<any>
    {
        return this._userApiService.registerBatch(users).toPromise();
    }

    /**
     * Creates job application
     *
     * @returns {Promise<any>}
     */
    createJobApplication(jobApplication): Promise<any>
    {
        return this._jobApplicationApiService.addExternal(jobApplication).toPromise();
    }

}
