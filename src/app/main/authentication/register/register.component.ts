import {Component, Inject, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, ValidatorFn, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {FuseConfigService} from '@fuse/services/config.service';
import {AppConfigService} from 'app/_services/config.service';
import {FuseProgressBarService} from '@fuse/components/progress-bar/progress-bar.service';
import {FuseTranslationLoaderService} from '@fuse/services/translation-loader.service';
import {fuseAnimations} from '@fuse/animations';
import {ActivatedRoute, Router} from '@angular/router';
import {TranslatePipe} from '@ngx-translate/core';
import {first} from 'rxjs/operators';
import {AlertService, AuthenticationService, NotificationService, SnackAlertService} from 'app/_services';
import {RegisterService} from './register.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {DialogData} from 'app/_models';
import {appConfig} from 'app/app.config';

// Import the locale files
import {locale as english} from 'app/main/authentication/i18n/en';
import {locale as spanish} from 'app/main/authentication/i18n/es';

@Component({
    selector: 'register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations,
    providers: [TranslatePipe]
})
export class RegisterComponent implements OnInit, OnDestroy {
    fuseConfig: any;
    appConfig = appConfig;
    registerForm: FormGroup;
    termsAccepted: boolean | null = null;
    termsTouched = false;
    submitted = false;
    external = false;
    jobId: number = null;
    jobSlug: string | null;
    image = null;
    returnUrl: string;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseProgressBarService} _fuseProgressBarService
     * @param {FormBuilder} _formBuilder
     * @param {ActivatedRoute} _route
     * @param {Router} _router
     * @param {MatDialog} _dialog
     * @param {AuthenticationService} _authenticationService
     * @param {RegisterService} _registerService
     * @param {FuseTranslationLoaderService} _translationLoader
     * @param {NotificationService} _notificationService
     * @param {TranslatePipe} _translatePipe
     * @param {AlertService} _alertService
     * @param {SnackAlertService} _snackAlertService
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _fuseProgressBarService: FuseProgressBarService,
        private _formBuilder: FormBuilder,
        private _route: ActivatedRoute,
        private _router: Router,
        private _dialog: MatDialog,
        private _authenticationService: AuthenticationService,
        private _registerService: RegisterService,
        private _translationLoader: FuseTranslationLoaderService,
        private _notificationService: NotificationService,
        private _translatePipe: TranslatePipe,
        private _alertService: AlertService,
        private _snackAlertService: SnackAlertService
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                },
                alert: {
                    position: 'content'
                }
            }
        };
        // Load translations
        this._translationLoader.loadTranslations(spanish, english);

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        // redirect to home if already logged in
        if (this._authenticationService.currentUserValue) {
            this._router.navigate(['/']);
        }

        this._fuseProgressBarService.setMode('indeterminate');
        const registerFormControls = {
            username: ['', Validators.required],
            first_name: ['', Validators.required],
            last_name: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            contact_phone: [''],
            city: [''],
            enterprise: [''],
            position: [''],
            picture: [''],
            password: ['', Validators.required],
            passwordConfirm: ['', [Validators.required, confirmRegisterPasswordValidator]],
            terms: ['']
        };

        // Subscribe to config change
        this._fuseConfigService.config
            .subscribe((config) => {
                this.fuseConfig = config;
            });

        // disable snackAlertService
        this._snackAlertService.disable();

        // get external and jobId from _route parameters or default to '/'
        this._route.paramMap.subscribe(params => {
            this.external = params.get('external') === 'external';
            this.jobId = params.get('jobId') ? +params.get('jobId') : null;
            this.jobSlug = params.get('jobSlug');
            this.returnUrl = `/candidate/jobs/apply/${this.jobId}/${this.jobSlug}`;
            if (!this.appConfig.appSettings.enableExternalUserRegistration && this.external) {
                this._router.navigate(['/auth/login'], { queryParams: { returnUrl: this.returnUrl }}).then();
            }
            if (!this.appConfig.appSettings.enableUserRegistration && !this.external) {
                this._router.navigate(['/auth/login']).then();
            }
            if (this.external || true) {
                registerFormControls.username = [''];
            }
        });
        this.registerForm = this._formBuilder.group(registerFormControls);

        // Update the validity of the 'passwordConfirm' field when the 'password' field changes
        this.registerForm.get('password').valueChanges
            .subscribe(value => {
                this.registerForm.get('passwordConfirm').updateValueAndValidity();
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    /**
     * Upload image file
     *
     * @param event
     * @returns {void}
     */
    uploadImage(event): void {
        const reader = new FileReader();
        if (event.target.files && event.target.files.length) {
            const [file] = event.target.files;
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.image = {src: reader.result};
                // TODO: upload image through API
            };
        }
    }

    get f(): any {
        return this.registerForm.controls;
    }

    public checkTerms(event): boolean {
        this.termsTouched = true;
        this.termsAccepted = event.checked;
        return event.checked;
    }

    onSubmit(): void {
        if (!this.submitted) {
            this.termsTouched = true;
            // stop here if terms are not accepted
            if (!this.termsAccepted) {
                return;
            }
            // stop here if form is invalid
            if (this.registerForm.invalid) {
                return;
            }
            this.submitted = true;
            this._fuseProgressBarService.show();
            const newUser: any = {
                first_name: this.registerForm.get('first_name').value,
                last_name: this.registerForm.get('last_name').value,
                enterprise: this.external ? this.registerForm.get('enterprise').value : '',
                division: '',
                is_active: true,
                directorate: this.external ? this.registerForm.get('position').value : '',
                management: '',
                city: this.external ? this.registerForm.get('city').value : '',
                email: this.registerForm.get('email').value,
                username: this.registerForm.get('email').value,
                telephone: this.external ? this.registerForm.get('contact_phone').value : '',
                pwd: this.registerForm.get('password').value,
                group_ids: this.external || this.appConfig.appSettings.externalSite ? [4] : [1]
            };
            this._registerService.registerUser(newUser).then(user => {
                let loginReturnUrl = '/candidate/dashboard';
                if (this.external || this.appConfig.appSettings.externalSite) {
                    this.createJobApplication(user, this.jobId);
                }
                if (this.external) {
                    loginReturnUrl = this.returnUrl;
                }
                if (this.appConfig.appSettings.autoLoginOnRegister) {
                    this._authenticationService.login(newUser.username, newUser.pwd)
                        .pipe(first())
                        .subscribe(data => {
                            this._notificationService.reloadUserNotifications();
                            this._router.navigate([loginReturnUrl]).then( () => {
                                this._snackAlertService.success(this._translatePipe.transform('REGISTRATION.REGISTER_SUCCESSFULLY'));
                            });
                        });
                } else {
                    this._notificationService.reloadUserNotifications();
                    this._router.navigate(['/auth/mail-confirm'], { queryParams: { returnUrl: loginReturnUrl }}).then( () => {
                        this._snackAlertService.success(this._translatePipe.transform('REGISTRATION.REGISTER_SUCCESSFULLY'));
                    });
                }
                this._fuseProgressBarService.hide();
                this.submitted = false;
            }).catch(err => {
                let errorMessage = '';
                if (err.error && err.error.hasOwnProperty('detail')) {
                    // django
                    errorMessage = `Error: ${err['error']['detail']}`;
                } else if (err.error && err.error.hasOwnProperty('non_field_errors')) {
                    // django
                    errorMessage = `Error: ${err['error']['non_field_errors'][0]}`;
                } else if (err.error instanceof ErrorEvent) {
                    // client-side error
                    errorMessage = `Error: ${err.error.message}`;
                } else if (err.error && err.error instanceof Object && Object.keys(err.error).length > 0) {
                    // django iterable
                    let message = '\n';
                    Object.keys(err.error).forEach((key) => {
                        message += `${key}: ${err['error'][key]}` + '\n';
                    });
                    errorMessage = `Error: ${message}`;
                } else if (err.error && err.status && err.statusText && err.message) {
                    // client-side error
                    errorMessage = `Error Code: ${err.status} ${err.statusText}` + '\n' + `Message: ${err.message}`;
                } else {
                    // server-side error
                    errorMessage = `Error Code: ${err.status}` + '\n' + `Message: ${err.message}`;
                }
                this._alertService.error(errorMessage);
                this._fuseProgressBarService.hide();
                this.submitted = false;
            });
        }
    }

    createJobApplication(user: any, jobId: number | null): void {
        const jobApplication = {user: user.id, job: jobId};
        this._registerService.createJobApplication(jobApplication).then();
    }

    /**
     * Open terms dialog
     *
     * @returns {void}
     */
    openTermsDialog(): void {
        const dialogRef = this._dialog.open(RegisterDialogComponent, {
            data: {}
        });
    }
}

/**
 * Confirm password validator
 *
 * @param {AbstractControl} control
 * @returns {ValidationErrors | null}
 */
export const confirmRegisterPasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {

    if (!control.parent || !control) {
        return null;
    }

    const password = control.parent.get('password');
    const passwordConfirm = control.parent.get('passwordConfirm');

    if (!password || !passwordConfirm) {
        return null;
    }

    if (passwordConfirm.value === '') {
        return null;
    }

    if (password.value === passwordConfirm.value) {
        return null;
    }

    return {'passwordsNotMatching': true};
};


@Component({
    selector: 'register-dialog',
    templateUrl: './register.dialog.component.html',
    styleUrls: ['./register.component.scss'],
})
export class RegisterDialogComponent {
    constructor(
        private _dialogRef: MatDialogRef<RegisterDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: DialogData
    ) {
        _dialogRef.disableClose = true;
    }
}
