import {NgModule} from '@angular/core';
import {AuthenticationRouting} from 'app/main/authentication/authentication.routing';

@NgModule({
    imports: [
        AuthenticationRouting,
    ]
})
export class AuthenticationModule {

}
