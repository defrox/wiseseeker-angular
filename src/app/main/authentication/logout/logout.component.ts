import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from 'app/_services';

@Component({
    selector: 'logout',
    templateUrl: './logout.component.html',
    encapsulation: ViewEncapsulation.None
})
export class LogoutComponent {
    /**
     * Constructor
     *
     * @param {Router} _router
     * @param {AuthenticationService} _authenticationService
     */
    constructor(
        private _router: Router,
        private _authenticationService: AuthenticationService,
    ) {
        this._authenticationService.logout();
        // localStorage.removeItem('currentUser');
        this._router.navigate(['/']);
    }
}
