import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {IpvQuestion} from 'app/_models';

@Injectable({ providedIn: 'root' })
export class IpvQuestionApiService {
    private ipvQuestionsUrl = 'api/ipv/questions';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<IpvQuestion[]> {
        return this._httpClient.get<IpvQuestion[]>(`${this.ipvQuestionsUrl}/`)
            .catch(this.handleError);
    }

    get(id: number): Observable<IpvQuestion> {
        return this._httpClient.get<IpvQuestion>(`${this.ipvQuestionsUrl}/${id}/`)
            .catch(this.handleError);
    }

    update(ipvQuestion: IpvQuestion): Observable<IpvQuestion> {
        return this._httpClient.put<IpvQuestion>(`${this.ipvQuestionsUrl}/${ipvQuestion.id}/`, ipvQuestion)
            .catch(this.handleError);
    }

    add(ipvQuestion: IpvQuestion): Observable<IpvQuestion> {
        const newIpvQuestion = Object.assign({}, ipvQuestion);
        return this._httpClient.post<IpvQuestion>(`${this.ipvQuestionsUrl}/`, newIpvQuestion)
            .catch(this.handleError);
    }

    delete(ipvQuestion: IpvQuestion | number): Observable<IpvQuestion> {
        const id = typeof ipvQuestion === 'number' ? ipvQuestion : ipvQuestion.id;
        return this._httpClient.delete<IpvQuestion>(`${this.ipvQuestionsUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}
