import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {Big5Question} from 'app/_models';

@Injectable({ providedIn: 'root' })
export class Big5QuestionApiService {
    private big5QuestionsUrl = 'api/big5/questions';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<Big5Question[]> {
        return this._httpClient.get<Big5Question[]>(`${this.big5QuestionsUrl}/`)
            .catch(this.handleError);
    }

    get(id: number): Observable<Big5Question> {
        return this._httpClient.get<Big5Question>(`${this.big5QuestionsUrl}/${id}/`)
            .catch(this.handleError);
    }

    update(big5Question: Big5Question): Observable<Big5Question> {
        return this._httpClient.put<Big5Question>(`${this.big5QuestionsUrl}/${big5Question.id}/`, big5Question)
            .catch(this.handleError);
    }

    add(big5Question: Big5Question): Observable<Big5Question> {
        const newBig5Question = Object.assign({}, big5Question);
        return this._httpClient.post<Big5Question>(`${this.big5QuestionsUrl}/`, newBig5Question)
            .catch(this.handleError);
    }

    delete(big5Question: Big5Question | number): Observable<Big5Question> {
        const id = typeof big5Question === 'number' ? big5Question : big5Question.id;
        return this._httpClient.delete<Big5Question>(`${this.big5QuestionsUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}
