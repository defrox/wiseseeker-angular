import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subscription, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {Job, JobApplication, JobSkill, JobApplicationState, User} from 'app/_models';
import {AuthenticationService} from './authentication.service';

@Injectable({ providedIn: 'root' })
export class JobApiService {
    currentUserSubscription: Subscription;
    currentUser: User;
    private jobsUrl = 'api/jobs';

    /**
     * Constructor
     *
     * @param {AuthenticationService} _authenticationService
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _authenticationService: AuthenticationService,
        private _httpClient: HttpClient
    ) {
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<Job[]> {
        return this._httpClient.get<Job[]>(`${this.jobsUrl}/`)
            .catch(this.handleError);
    }

    getAllByUser(): Observable<Job[]> {
        return this._httpClient.get<Job[]>(`${this.jobsUrl}/?user=${this.currentUser.id}`)
            .catch(this.handleError);
    }

    get(id: number): Observable<Job> {
        return this._httpClient.get<Job>(`${this.jobsUrl}/${id}/`)
            .catch(this.handleError);
    }

    getByUser(id: number): Observable<Job> {
        return this._httpClient.get<Job>(`${this.jobsUrl}/${id}/?user=${this.currentUser.id}`)
            .catch(this.handleError);
    }

    update(job: Job): Observable<Job> {
        return this._httpClient.put<Job>(`${this.jobsUrl}/${job.id}/`, job)
            .catch(this.handleError);
    }

    add(job: Job): Observable<Job> {
        const newJob = Object.assign({}, job);
        return this._httpClient.post<Job>(`${this.jobsUrl}/`, newJob)
            .catch(this.handleError);
    }

    delete(job: Job | number): Observable<Job> {
        const id = typeof job === 'number' ? job : job.id;
        return this._httpClient.delete<Job>(`${this.jobsUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}

@Injectable({ providedIn: 'root' })
export class JobApplicationApiService {
    currentUserSubscription: Subscription;
    currentUser: User;
    private jobApplicationsUrl = 'api/jobs/applications';

    /**
     * Constructor
     *
     * @param {AuthenticationService} _authenticationService
     * @param {HttpClient} _httpClient
     */
    constructor(
      private _authenticationService: AuthenticationService,
      private _httpClient: HttpClient
    ) {
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<JobApplication[]> {
        return this._httpClient.get<JobApplication[]>(`${this.jobApplicationsUrl}/`)
            .catch(this.handleError);
    }

    getAllByUser(): Observable<Job[]> {
        return this._httpClient.get<Job[]>(`${this.jobApplicationsUrl}/?user=${this.currentUser.id}`)
            .catch(this.handleError);
    }
    
    get(id: number): Observable<JobApplication> {
        return this._httpClient.get<JobApplication>(`${this.jobApplicationsUrl}/${id}/`)
            .catch(this.handleError);
    }

    update(jobApplication: JobApplication): Observable<JobApplication> {
        return this._httpClient.put<JobApplication>(`${this.jobApplicationsUrl}/${jobApplication.id}/`, jobApplication)
            .catch(this.handleError);
    }

    add(jobApplication: JobApplication): Observable<JobApplication> {
        const newJobApplication = Object.assign({}, jobApplication);
        return this._httpClient.post<JobApplication>(`${this.jobApplicationsUrl}/`, newJobApplication)
            .catch(this.handleError);
    }

    addExternal(jobApplication: JobApplication): Observable<JobApplication> {
        const newJobApplication = Object.assign({}, jobApplication);
        return this._httpClient.post<JobApplication>(`${this.jobApplicationsUrl}/external`, newJobApplication)
            .catch(this.handleError);
    }

    delete(jobApplication: JobApplication | number): Observable<JobApplication> {
        const id = typeof jobApplication === 'number' ? jobApplication : jobApplication.id;
        return this._httpClient.delete<JobApplication>(`${this.jobApplicationsUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}

@Injectable({ providedIn: 'root' })
export class JobSkillApiService {
    private jobSkillsUrl = 'api/jobs/skills';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<JobSkill[]> {
        return this._httpClient.get<JobSkill[]>(`${this.jobSkillsUrl}/`)
            .catch(this.handleError);
    }

    get(id: number): Observable<JobSkill> {
        return this._httpClient.get<JobSkill>(`${this.jobSkillsUrl}/${id}/`)
            .catch(this.handleError);
    }

    update(jobSkill: JobSkill): Observable<JobSkill> {
        return this._httpClient.put<JobSkill>(`${this.jobSkillsUrl}/${jobSkill.id}/`, jobSkill)
            .catch(this.handleError);
    }

    add(jobSkill: JobSkill): Observable<JobSkill> {
        const newJobSkill = Object.assign({}, jobSkill);
        return this._httpClient.post<JobSkill>(`${this.jobSkillsUrl}/`, newJobSkill)
            .catch(this.handleError);
    }

    delete(jobSkill: JobSkill | number): Observable<JobSkill> {
        const id = typeof jobSkill === 'number' ? jobSkill : jobSkill.id;
        return this._httpClient.delete<JobSkill>(`${this.jobSkillsUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}
