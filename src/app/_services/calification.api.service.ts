import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {Calification} from 'app/_models';

@Injectable({ providedIn: 'root' })
export class CalificationApiService {
    private calificationsUrl = 'api/califications';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<Calification[]> {
        return this._httpClient.get<Calification[]>(`${this.calificationsUrl}/`)
            .catch(this.handleError);
    }

    get(id: number): Observable<Calification> {
        return this._httpClient.get<Calification>(`${this.calificationsUrl}/${id}/`)
            .catch(this.handleError);
    }

    update(calification: Calification): Observable<Calification> {
        return this._httpClient.put<Calification>(`${this.calificationsUrl}/${calification.id}/`, calification)
            .catch(this.handleError);
    }

    add(calification: Calification): Observable<Calification> {
        const newCalification = Object.assign({}, calification);
        return this._httpClient.post<Calification>(`${this.calificationsUrl}/`, newCalification)
            .catch(this.handleError);
    }

    delete(calification: Calification | number): Observable<Calification> {
        const id = typeof calification === 'number' ? calification : calification.id;
        return this._httpClient.delete<Calification>(`${this.calificationsUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}
