import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {Question} from 'app/_models';

@Injectable({ providedIn: 'root' })
export class QuestionApiService {
    private questionsUrl = 'api/questions';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<Question[]> {
        return this._httpClient.get<Question[]>(`${this.questionsUrl}/`)
            .catch(this.handleError);
    }

    get(id: number): Observable<Question> {
        return this._httpClient.get<Question>(`${this.questionsUrl}/${id}/`)
            .catch(this.handleError);
    }

    update(question: Question): Observable<Question> {
        return this._httpClient.put<Question>(`${this.questionsUrl}/${question.id}/`, question)
            .catch(this.handleError);
    }

    add(question: Question): Observable<Question> {
        const newQuestion = Object.assign({}, question);
        return this._httpClient.post<Question>(`${this.questionsUrl}/`, newQuestion)
            .catch(this.handleError);
    }

    delete(question: Question | number): Observable<Question> {
        const id = typeof question === 'number' ? question : question.id;
        return this._httpClient.delete<Question>(`${this.questionsUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}
