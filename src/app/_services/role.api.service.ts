import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {Role, RoleCategory} from 'app/_models';

@Injectable({ providedIn: 'root' })
export class RoleApiService {
    private rolesUrl = 'api/roles';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<Role[]> {
        return this._httpClient.get<Role[]>(`${this.rolesUrl}/`)
            .catch(this.handleError);
    }

    get(id: number): Observable<Role> {
        return this._httpClient.get<Role>(`${this.rolesUrl}/${id}/`)
            .catch(this.handleError);
    }

    getFiltered(filters: any = null): Observable<Role> {
        let queryString = '';
        if (filters) {
            queryString = '?';
            const pieces = [];
            Object.keys(filters).map(key => { pieces.push(key + '=' + filters[key]); });
            queryString += pieces.join('&');
        }
        return this._httpClient.get<Role>(`${this.rolesUrl}/${queryString}`)
            .catch(this.handleError);
    }

    update(role: Role): Observable<Role> {
        return this._httpClient.put<Role>(`${this.rolesUrl}/${role.id}/`, role)
            .catch(this.handleError);
    }

    add(role: Role): Observable<Role> {
        const newRole = Object.assign({}, role);
        return this._httpClient.post<Role>(`${this.rolesUrl}/`, newRole)
            .catch(this.handleError);
    }

    delete(role: Role | number): Observable<Role> {
        const id = typeof role === 'number' ? role : role.id;
        return this._httpClient.delete<Role>(`${this.rolesUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}

@Injectable({ providedIn: 'root' })
export class RoleCategoryApiService {
    // private roleCategoryUrl = 'api/roles/categories';
    private roleCategoryUrl = 'api/roles-categories';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<RoleCategory[]> {
        return this._httpClient.get<RoleCategory[]>(`${this.roleCategoryUrl}/`)
            .catch(this.handleError);
    }

    get(id: number): Observable<RoleCategory> {
        return this._httpClient.get<RoleCategory>(`${this.roleCategoryUrl}/${id}/`)
            .catch(this.handleError);
    }

    getFiltered(filters: any = null): Observable<RoleCategory> {
        let queryString = '';
        if (filters) {
            queryString = '?';
            const pieces = [];
            Object.keys(filters).map(key => { pieces.push(key + '=' + filters[key]); });
            queryString += pieces.join('&');
        }
        return this._httpClient.get<RoleCategory>(`${this.roleCategoryUrl}/${queryString}`)
            .catch(this.handleError);
    }

    update(roleCategory: RoleCategory): Observable<RoleCategory> {
        return this._httpClient.put<RoleCategory>(`${this.roleCategoryUrl}/${roleCategory.id}/`, roleCategory)
            .catch(this.handleError);
    }

    add(roleCategory: RoleCategory): Observable<RoleCategory> {
        const newRoleCategory = Object.assign({}, roleCategory);
        return this._httpClient.post<RoleCategory>(`${this.roleCategoryUrl}/`, newRoleCategory)
            .catch(this.handleError);
    }

    delete(roleCategory: RoleCategory | number): Observable<RoleCategory> {
        const id = typeof roleCategory === 'number' ? roleCategory : roleCategory.id;
        return this._httpClient.delete<RoleCategory>(`${this.roleCategoryUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}
