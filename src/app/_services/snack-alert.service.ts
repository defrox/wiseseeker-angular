﻿import {Injectable} from '@angular/core';
import {Router, NavigationStart} from '@angular/router';
import {Observable, Subject} from 'rxjs';

@Injectable({providedIn: 'root'})
export class SnackAlertService {
    private subject = new Subject<any>();
    private keepAfterNavigationChange = false;
    private _enabled = true;

    constructor(private router: Router) {
        // clear alert snackAlertMessage on route change
        router.events.subscribe(event => {
            if (event instanceof NavigationStart) {
                this.enabled = true;
                if (this.keepAfterNavigationChange) {
                    // only keep for a single location change
                    this.keepAfterNavigationChange = false;
                } else {
                    // clear alert
                    this.subject.next();
                }
            }
        });
    }

    set enabled(value) {
        this._enabled = value;
    }

    get enabled(): boolean {
        return this._enabled;
    }

    enable(): void {
        this.enabled = true;
    }

    disable(): void {
        this.enabled = false;
    }

    toggle(): void {
        this.enabled = !this.enabled;
    }

    info(snackAlertMessage: string, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        if (this.enabled) {
            this.subject.next({type: 'info', text: snackAlertMessage});
        }
    }

    success(snackAlertMessage: string, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        if (this.enabled) {
            this.subject.next({type: 'success', text: snackAlertMessage});
        }
    }

    error(snackAlertMessage: string, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        if (this.enabled) {
            this.subject.next({type: 'error', text: snackAlertMessage});
        }
    }

    warning(snackAlertMessage: string, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        if (this.enabled) {
            this.subject.next({type: 'warning', text: snackAlertMessage});
        }
    }

    primary(snackAlertMessage: string, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        if (this.enabled) {
            this.subject.next({type: 'primary', text: snackAlertMessage});
        }
    }

    secondary(snackAlertMessage: string, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        if (this.enabled) {
            this.subject.next({type: 'secondary', text: snackAlertMessage});
        }
    }

    dark(snackAlertMessage: string, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        if (this.enabled) {
            this.subject.next({type: 'dark', text: snackAlertMessage});
        }
    }

    light(snackAlertMessage: string, keepAfterNavigationChange = false) {
        this.keepAfterNavigationChange = keepAfterNavigationChange;
        if (this.enabled) {
            this.subject.next({type: 'light', text: snackAlertMessage});
        }
    }

    getSnackAlertMessage(): Observable<any> {
        return this.subject.asObservable();
    }
}