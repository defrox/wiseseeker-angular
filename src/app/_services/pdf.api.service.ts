import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';

@Injectable({ providedIn: 'root' })
export class PdfApiService {
    private pdfUrl = 'api/render/pdf';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    getPdf(): Observable<any> {
        return this._httpClient.get<any>(`${this.pdfUrl}/`)
            .catch(this.handleError);
    }

    getUserPdf(id: number): Observable<any> {
        return this._httpClient.get<any>(`${this.pdfUrl}/${id}`)
            .catch(this.handleError);
    }

    getAnonymousPdf(id: number): Observable<any> {
        return this._httpClient.get<any>(`${this.pdfUrl}/anonymous/${id}`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}
