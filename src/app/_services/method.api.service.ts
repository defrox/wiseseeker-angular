import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {Method} from 'app/_models';

@Injectable({ providedIn: 'root' })
export class MethodApiService {
    private methodsUrl = 'api/methods';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<Method[]> {
        return this._httpClient.get<Method[]>(`${this.methodsUrl}/`)
            .catch(this.handleError);
    }

    get(id: number): Observable<Method> {
        return this._httpClient.get<Method>(`${this.methodsUrl}/${id}/`)
            .catch(this.handleError);
    }

    update(method: Method): Observable<Method> {
        return this._httpClient.put<Method>(`${this.methodsUrl}/${method.id}/`, method)
            .catch(this.handleError);
    }

    add(method: Method): Observable<Method> {
        const newMethod = Object.assign({}, method);
        return this._httpClient.post<Method>(`${this.methodsUrl}/`, newMethod)
            .catch(this.handleError);
    }

    delete(method: Method | number): Observable<Method> {
        const id = typeof method === 'number' ? method : method.id;
        return this._httpClient.delete<Method>(`${this.methodsUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}
