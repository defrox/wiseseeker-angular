import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {Assessment} from 'app/_models';

@Injectable({ providedIn: 'root' })
export class AssessmentApiService {
    private assessmentUrl = 'api/assessments';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<Assessment[]> {
        return this._httpClient.get<Assessment[]>(`${this.assessmentUrl}/`)
            .catch(this.handleError);
    }

    get(id: number): Observable<Assessment> {
        return this._httpClient.get<Assessment>(`${this.assessmentUrl}/${id}/`)
            .catch(this.handleError);
    }

    update(assessment: Assessment): Observable<Assessment> {
        return this._httpClient.put<Assessment>(`${this.assessmentUrl}/${assessment.id}/`, assessment)
            .catch(this.handleError);
    }

    add(assessment: Assessment): Observable<Assessment> {
        const newAssessment = Object.assign({}, assessment);
        return this._httpClient.post<Assessment>(`${this.assessmentUrl}/`, newAssessment)
            .catch(this.handleError);
    }

    delete(assessment: Assessment | number): Observable<Assessment> {
        const id = typeof assessment === 'number' ? assessment : assessment.id;
        return this._httpClient.delete<Assessment>(`${this.assessmentUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}
