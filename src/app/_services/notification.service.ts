import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {FuseNavigationService} from '@fuse/components/navigation/navigation.service';
import {AuthenticationService, EvaluationApiService, ExperienceApiService, JobApiService, JobApplicationApiService} from 'app/_services';
import {Group, User} from 'app/_models';
import {BehaviorSubject, Observable} from 'rxjs';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/first';
import * as _ from 'lodash';

@Injectable({providedIn: 'root'})
export class NotificationService {
    onNotificationChanged: BehaviorSubject<any>;
    evaluations: any;

    private _currentUser: User;
    private _notificationSubject: BehaviorSubject<any>;

    constructor (
        private _authenticationService: AuthenticationService,
        private _evaluationApiService: EvaluationApiService,
        private _experienceApiService: ExperienceApiService,
        private _jobApiService: JobApiService,
        private _jobApplicationsApiService: JobApplicationApiService,
        private _fuseNavigationService: FuseNavigationService,
        private _http: HttpClient
    ) {
        this.onNotificationChanged = new BehaviorSubject({});
        // Initialize the service
        this._init();
    }

    /**
     * Set and get the notifications
     */
    set notifications(value) {
        // Get the value from the behavior subject
        let notifications = this._notificationSubject.getValue();

        // Merge the new config
        notifications = _.merge({}, notifications, value);

        // Notify the observers
        this._notificationSubject.next(notifications);
        this.onNotificationChanged.next(notifications);
    }

    get notifications(): any | Observable<any> {
        return this._notificationSubject.asObservable();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Private methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Initialize
     *
     * @private
     */
    private _init(): void {
        // Set the config from the default config
        this._notificationSubject = new BehaviorSubject({});
        this.reloadUserNotifications();

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Reset notifications
     *
     * @private
     */
    public resetUserNotifications(): void {
        this._init();
    }

    /**
     * Set visible navigation menu depending on user group
     *
     * @public
     */
    public setCandidateNavigationBadges(notifications: any = null): any {
        if (notifications) {
            if (notifications.hasOwnProperty('candidate')) {
                if (notifications.candidate.hasOwnProperty('evaluations') || notifications.candidate.hasOwnProperty('experience') || notifications.candidate.hasOwnProperty('jobs') || notifications.candidate.hasOwnProperty('profile')) {
                    const isExternal = this._currentUser.groups.includes(Group.External);
                    let role_todo = 0;
                    let dashboard_todo = 0;
                    let knowledge_todo = 0;
                    let jobs_todo = 0;
                    let experience_todo = 0;
                    let skills_todo = 0;
                    let personality_todo = 0;
                    let profile_todo = 0;
                    let red_badge_color  = '#f44336';
                    let amber_badge_color  = '#ffc107';
                    let knowledge_badge_color = amber_badge_color;
                    let role_badge_color = amber_badge_color;
                    let tests_added = [];
                    this._fuseNavigationService.updateNavigationItem('candidate.skills', {
                        badge: null
                    });
                    this._fuseNavigationService.updateNavigationItem('candidate.personality', {
                        badge: null
                    });
                    this._fuseNavigationService.updateNavigationItem('external.skills', {
                        badge: null
                    });
                    this._fuseNavigationService.updateNavigationItem('external.personality', {
                        badge: null
                    });

                    if (notifications.candidate.profile && notifications.candidate.profile.length > 0 && !isExternal) {
                        for (const item of notifications.candidate.profile) {
                            profile_todo++;
                        }
                    }
                    if (notifications.candidate.experience && notifications.candidate.experience.length > 0 && !isExternal) {
                        experience_todo = notifications.candidate.experience.length;
                    }
                    if (notifications.candidate.jobs && notifications.candidate.jobs.length > 0 && !isExternal) {
                        for (const job of notifications.candidate.jobs) {
                            jobs_todo++;
                        }
                    }
                    for (const item of notifications.candidate.evaluations) {
                        if (item.hasOwnProperty('tests')) {
                            for (const test of item.tests) {
                                if (tests_added.indexOf(test.id) < 0) {
                                    tests_added.push(test.id);
                                    if (test.type === 1 && test.executions && test.executions.length > 0) {
                                        for (const execution of test.executions) {
                                            if ((execution.status && execution.status < 2) || !execution.status) {
                                                dashboard_todo++;
                                                knowledge_todo++;
                                                if (!execution.status) {
                                                    knowledge_badge_color = red_badge_color;
                                                }
                                            }
                                        }
                                    }
                                    if (test.type === 2 && test.executions && test.executions.length > 0) {
                                        for (const execution of test.executions) {
                                            if ((execution.status && execution.status < 2) || !execution.status) {
                                                role_todo++;
                                                dashboard_todo++;
                                                if (!execution.status) {
                                                    role_badge_color = red_badge_color;
                                                }
                                            }
                                        }
                                    }
                                    if (test.type === 3 && test.executions && test.executions.length > 0) {
                                        for (const execution of test.executions) {
                                            if ((execution.status && execution.status < 2) || !execution.status) {
                                                dashboard_todo++;
                                                this._fuseNavigationService.updateNavigationItem('candidate.skills', {
                                                    badge: {
                                                        title: '1',
                                                        bg: !execution.status ? red_badge_color : amber_badge_color,
                                                        fg: '#FFFFFF'
                                                    }
                                                });
                                                this._fuseNavigationService.updateNavigationItem('external.skills', {
                                                    badge: {
                                                        title: '1',
                                                        bg: !execution.status ? red_badge_color : amber_badge_color,
                                                        fg: '#FFFFFF'
                                                    }
                                                });
                                            } else {
                                                this._fuseNavigationService.updateNavigationItem('candidate.skills', {
                                                    badge: null
                                                });
                                                this._fuseNavigationService.updateNavigationItem('external.skills', {
                                                    badge: null
                                                });
                                            }
                                        }
                                    }
                                    if (test.type === 4 && test.executions && test.executions.length > 0) {
                                        for (const execution of test.executions) {
                                            if ((execution.status && execution.status < 2) || !execution.status) {
                                                dashboard_todo++;
                                                this._fuseNavigationService.updateNavigationItem('candidate.personality', {
                                                    badge: {
                                                        title: '1',
                                                        icon: 'settings',
                                                        bg: !execution.status ? red_badge_color : amber_badge_color,
                                                        fg: '#FFFFFF'
                                                    }
                                                });
                                                this._fuseNavigationService.updateNavigationItem('external.personality', {
                                                    badge: {
                                                        title: '1',
                                                        icon: 'settings',
                                                        bg: !execution.status ? red_badge_color : amber_badge_color,
                                                        fg: '#FFFFFF'
                                                    }
                                                });
                                            } else {
                                                this._fuseNavigationService.updateNavigationItem('candidate.personality', {
                                                    badge: null
                                                });
                                                this._fuseNavigationService.updateNavigationItem('external.personality', {
                                                    badge: null
                                                });
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (profile_todo > 0) {
                        this._fuseNavigationService.updateNavigationItem('candidate.profile', {
                            badge: {
                                title: profile_todo,
                                bg   : profile_todo > 2 ? amber_badge_color : red_badge_color,
                                fg   : '#FFFFFF'
                            }
                        });
                    } else {
                        this._fuseNavigationService.updateNavigationItem('candidate.profile', {
                            badge: null
                        });
                    }
                    if (experience_todo < 1) {
                        dashboard_todo++;
                        this._fuseNavigationService.updateNavigationItem('candidate.experience', {
                            badge: {
                                title: '1',
                                bg   : experience_todo > 0 ? amber_badge_color : red_badge_color,
                                fg   : '#FFFFFF'
                            }
                        });
                    } else {
                        this._fuseNavigationService.updateNavigationItem('candidate.experience', {
                            badge: null
                        });
                    }
                    if (jobs_todo < 1) {
                        dashboard_todo++;
                        this._fuseNavigationService.updateNavigationItem('candidate.jobs', {
                            badge: {
                                title: '1',
                                bg   : jobs_todo > 0 ? amber_badge_color : red_badge_color,
                                fg   : '#FFFFFF'
                            }
                        });
                    } else {
                        this._fuseNavigationService.updateNavigationItem('candidate.jobs', {
                            badge: null
                        });
                    }
                    if (role_todo > 0) {
                        this._fuseNavigationService.updateNavigationItem('candidate.roles', {
                            badge: {
                                title: role_todo,
                                bg   : role_badge_color,
                                fg   : '#FFFFFF'
                            }
                        });
                    } else {
                        this._fuseNavigationService.updateNavigationItem('candidate.roles', {
                            badge: null
                        });
                    }
                    if (knowledge_todo > 0) {
                        this._fuseNavigationService.updateNavigationItem('candidate.knowledge', {
                            badge: {
                                title: knowledge_todo,
                                bg   : knowledge_badge_color,
                                fg   : '#FFFFFF'
                            }
                        });
                        this._fuseNavigationService.updateNavigationItem('external.knowledge', {
                            badge: {
                                title: knowledge_todo,
                                bg   : knowledge_badge_color,
                                fg   : '#FFFFFF'
                            }
                        });
                    } else {
                        this._fuseNavigationService.updateNavigationItem('candidate.knowledge', {
                            badge: null
                        });
                        this._fuseNavigationService.updateNavigationItem('external.knowledge', {
                            badge: null
                        });
                    }
                    /*if (dashboard_todo > 0) {
                        this._fuseNavigationService.updateNavigationItem('candidate.dashboard', {
                            badge: {
                                title: dashboard_todo,
                                bg   : '#f44336',
                                fg   : '#FFFFFF'
                            }
                        });
                    } else {
                        this._fuseNavigationService.updateNavigationItem('candidate.dashboard', {
                            badge: null
                        });
                    }*/
                }
            }
        }
    }

    public getCandidateNotificationsBadges(notifications: any = null): any {
        let result = {role: null, role_pending: [], skills: null, skills_pending: [], personality: null, personality_pending: [], knowledge: null, knowledge_pending: [], experience: null, jobs: null, profile: null};
        if (notifications) {
            if (notifications.hasOwnProperty('candidate')) {
                if (notifications.candidate.hasOwnProperty('evaluations') || notifications.candidate.hasOwnProperty('experience') || notifications.candidate.hasOwnProperty('jobs') || notifications.candidate.hasOwnProperty('profile')) {
                    let role_todo = 0;
                    let skills_todo = 0;
                    let personality_todo = 0;
                    let knowledge_todo = 0;
                    let dashboard_todo = 0;
                    let experience_todo = 0;
                    let jobs_todo = 0;
                    let profile_todo = 0;
                    let tests_added = [];
                    const isExternal = this._currentUser.groups.includes(Group.External);
                    if (notifications.candidate.experience && notifications.candidate.experience.length > 0 && !isExternal) {
                        experience_todo = notifications.candidate.experience.length;
                    }
                    if (notifications.candidate.jobs && notifications.candidate.jobs.length > 0 && !isExternal) {
                        for (const job of notifications.candidate.jobs) {
                            jobs_todo++;
                        }
                    }
                    if (notifications.candidate.profile && notifications.candidate.profile.length > 0 && !isExternal) {
                        for (const item of notifications.candidate.profile) {
                            profile_todo++;
                            dashboard_todo++;
                        }
                    }
                    for (const item of notifications.candidate.evaluations) {
                        if (item.hasOwnProperty('tests')) {
                            for (const test of item.tests) {
                                if (tests_added.indexOf(test.id) < 0) {
                                    tests_added.push(test.id);
                                    if (test.type === 1 && test.executions && test.executions.length > 0) {
                                        for (const execution of test.executions) {
                                            if ((execution.status && execution.status < 2) || !execution.status) {
                                                knowledge_todo++;
                                                dashboard_todo++;
                                                result['knowledge'] = !execution.status ? 'red' : 'yellow';
                                                result['knowledge_pending'].push({id: test.id, target: test.target, status: execution.status, title: test.title});
                                            }
                                        }
                                    }
                                    if (test.type === 2 && test.executions && test.executions.length > 0) {
                                        for (const execution of test.executions) {
                                            if ((execution.status && execution.status < 2) || !execution.status) {
                                                role_todo++;
                                                dashboard_todo++;
                                                result['role'] = !execution.status ? 'red' : 'yellow';
                                                result['role_pending'].push({id: test.id, target: test.target, status: execution.status, title: test.title});
                                            }
                                        }
                                    }
                                    if (test.type === 3 && test.executions && test.executions.length > 0) {
                                        for (const execution of test.executions) {
                                            if ((execution.status && execution.status < 2) || !execution.status) {
                                                skills_todo++;
                                                dashboard_todo++;
                                                result['skills'] = !execution.status ? 'red' : 'yellow';
                                                result['skills_pending'][0] = {id: test.id, target: test.target, status: execution.status, title: test.title, subtype: test.subtype};
                                                // result['skills_pending'].push({id: test.id, status: execution.status, title: test.title});
                                            }
                                        }
                                    }
                                    if (test.type === 4 && test.executions && test.executions.length > 0) {
                                        for (const execution of test.executions) {
                                            if ((execution.status && execution.status < 2) || !execution.status) {
                                                personality_todo++;
                                                dashboard_todo++;
                                                result['personality'] = !execution.status ? 'red' : 'yellow';
                                                result['personality_pending'][0] = {id: test.id, target: test.target, status: execution.status, title: test.title};
                                                // result['personality_pending'].push({id: test.id, status: execution.status, title: test.title});
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (experience_todo < 1 && !isExternal) {
                        result['experience'] = experience_todo > 0 ? 'yellow' : 'red';
                        dashboard_todo++;
                    }
                    if (jobs_todo < 1 && !isExternal) {
                        result['jobs'] = jobs_todo > 0 ? 'yellow' : 'red';
                        dashboard_todo++;
                    }
                    if (profile_todo > 0 && !isExternal) {
                        result['profile'] = profile_todo < 2 ? 'red' : 'yellow';
                        dashboard_todo++;
                    }
                    if (skills_todo > 0 ) {
                        result['skills_todo'] = skills_todo;
                    }
                    if (personality_todo > 0) {
                        result['personality_todo'] = personality_todo;
                    }
                    if (role_todo > 0) {
                        result['role_todo'] = role_todo;
                    }
                    if (knowledge_todo > 0) {
                        result['knowledge_todo'] = knowledge_todo;
                    }
                    if (dashboard_todo > 0) {
                        result['dashboard_todo'] = dashboard_todo;
                    }
                }
            }
        }
        return result;
    }

    /**
     * Set User Notifications
     *
     * @param value
     * @param {{emitEvent: boolean}} opts
     */
    public setUserNotifications(value, opts = {emitEvent: true}): void {
        // Get the value from the behavior subject
        let notifications = this._notificationSubject.getValue();

        // Merge the new config
        notifications = _.merge({}, notifications, value);

        // If emitEvent option is true...
        if (opts.emitEvent === true) {
            // Notify the observers
            this._notificationSubject.next(notifications);
            this.onNotificationChanged.next(notifications);
        }
    }

    /**
     * Load User Notifications
     */
    public loadUserNotifications(): Observable<any> | Promise<any> | any {

        return new Promise((resolve, reject) => {

            Promise.all([
                this.getCandidateNotifications()
            ]).then(
                () => {
                    resolve();
                },
                reject
            );
        });
    }

    /**
     * Update User Notifications
     */
    async updateUserNotifications(): Promise<any> {
        this._notificationSubject = new BehaviorSubject({});
        let notifications = {};
        if (this._currentUser) {
            if (this._currentUser.groups.includes(Group.Candidate) || this._currentUser.groups.includes(Group.External)) {
                notifications = {...notifications, candidate: await this.getCandidateNotifications()};
            }
            if (this._currentUser.groups.includes(Group.Recruiter)) {
                // notifications = {...notifications, recruiter: this.getRecruiterNotifications()};
                notifications = _.merge({}, notifications, {recruiter: this.getRecruiterNotifications()});
            }
            if (this._currentUser.groups.includes(Group.Admin)) {
                // notifications = {...notifications, admin: this.getAdminNotifications()};
                notifications = _.merge({}, notifications, {admin: this.getAdminNotifications()});
            }
        }
        this._notificationSubject.next(notifications);
        this.onNotificationChanged.next(notifications);
    }

    public reloadUserNotifications(): void {
        this._currentUser = this._authenticationService.getCurrentUser();
        this.updateUserNotifications();
        this.setUserNotifications(this.evaluations);
    }

    /**
     * Get User Notifications
     *
     * @returns {Observable<any>}
     */
    public getUserNotifications(): any | Observable<any> {
        return this._notificationSubject.asObservable();
    }
    
    async getCandidateNotifications(): Promise<any> {
        let evaluations = null;
        let experience = null;
        let jobs = null;
        let profile = null;
        if (this._currentUser.groups.includes(Group.Candidate) || this._currentUser.groups.includes(Group.External)) {
            evaluations = await this.mergeEvaluationsJobs();
        }
        if (this._currentUser.groups.includes(Group.Candidate)) {
            experience = await this.getCandidateExperience();
        }
        if (this._currentUser.groups.includes(Group.Candidate)) {
            jobs = await this.getCandidateJobs();
        }
        if (this._currentUser.groups.includes(Group.Candidate)) {
            profile = await this.getCandidateProfile();
        }
        let notifications = {};
        return _.merge({}, notifications, {evaluations: evaluations, experience: experience, jobs: jobs, profile: profile});
    }

    async mergeEvaluationsJobs(): Promise<any> {
        return this.getCandidateEvaluations().then(rese => {
            let evaluations = rese;
            let result = _.cloneDeep(evaluations);
            return this.getCandidateJobs().then(resj => {
                let jobs = resj;
                let tests = [];
                for (const item of evaluations) {
                    if (item.hasOwnProperty('tests')) {
                        tests = item.tests;
                    }
                }
                for (const item of jobs) {
                    if (item.hasOwnProperty('test')) {
                        if (tests.findIndex((element) => element.id === item.test.id) < 0) {
                            tests.push(item.test);
                        }
                    }
                    if (item.hasOwnProperty('test_personality')) {
                        if (tests.findIndex((element) => element.id === item.test_personality.id) < 0) {
                            tests.push(item.test_personality);
                        }
                    }
                }
                if (result.length > 0) {
                    for (const item of result) {
                        item['tests'] = tests;
                    }
                } else {
                    result.push({tests: tests});
                }
                return result;
            });
        });
    }

    async getCandidateEvaluations(): Promise<any> {
        return await this._evaluationApiService.getAllByUser().toPromise();
    }

    async getCandidateExperience(): Promise<any> {
        return await this._experienceApiService.getAllByUser().toPromise();
    }

    async getCandidateJobs(): Promise<any> {
        return await this._jobApplicationsApiService.getAllByUser().toPromise();
    }

    async getCandidateProfile(): Promise<any> {
        // return await this._profileApiService.getAll().toPromise();
        return null;
    }

    async getRecruiterNotifications(): Promise<any> {
        return await null;
    }

    async getAdminNotifications(): Promise<any> {
        return await null;
    }

}
