﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BehaviorSubject, Observable, throwError} from 'rxjs';
import {map} from 'rxjs/operators';
import {Group, JwtObject, User} from 'app/_models';
import {environment} from 'environments/environment';
import * as _ from 'lodash/fp';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
    public currentUser: Observable<User>;
    public token: string;
    appConfig = environment.appConfig;

    constructor(
        private http: HttpClient
    ) {
        if (!JSON.parse(localStorage.getItem('currentUser'))) {
            this.setGuestUser();
        }
        this.currentUserSubject = new BehaviorSubject<User>(this.getCurrentUser());
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    public getCurrentUser(): User {
        const storedUser = JSON.parse(localStorage.getItem('currentUser'));
        let user = null;
        if (storedUser) {
            user = new User(
                storedUser._id,
                storedUser._email,
                storedUser._firstName,
                storedUser._lastName,
                storedUser._groups,
                storedUser._username,
                storedUser._is_active,
                storedUser._is_staff,
                null,
                storedUser._avatar,
                storedUser._token,
                storedUser._jwt);
        }
        return user;
    }

    public setCurrentUser(user: User): User {
        if (!this.currentUserSubject) {
            localStorage.setItem('currentUser', JSON.stringify(user));
            this.currentUserSubject.next(user);
        }
        return this.currentUserSubject.value;
    }

    public login(username: string, password: string): any {
        const loggingUser = `{"username": "${username}", "password": "${password}"}`;
        return this.http.post<any>(`api/token-auth/`, loggingUser)
            .pipe(map(data => {
                    return this.updateData(data);
                }
            ));
    }

    // Verifies the JWT token
    public verifyToken(): any {
        this.http.post('api/token-verify/', JSON.stringify({token: this.currentUserValue.token})).subscribe(
            data => {
                this.updateData(data);
            }
        );
    }

    // Refreshes the JWT token, to extend the time the user is logged in
    public refreshToken(): any {
        this.http.post('api/token-refresh/', JSON.stringify({token: this.currentUserValue.token})).subscribe(
            data => {
                this.updateData(data);
            }
        );
    }

    private updateData(data): any {
        const JwtObj = this._decodeToken(data['token']);
        const user = new User(
            data['user']['id'],
            data['user']['email'],
            data['user']['first_name'],
            data['user']['last_name'],
            data['user']['groups'].map(group => Group[_.capitalize(group.name === 'candidate' && this.appConfig.appSettings.externalSite ? 'external' : group.name)]),
            data['user']['username'],
            data['user']['is_active'],
            data['user']['is_staff'],
            null,
            data['user']['avatar'],
            data['token'],
            JwtObj);
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
    }

    public updateUserToken(token): void {
        const user = this.getCurrentUser();
        user.jwt = this._decodeToken(token);
        user.token = token;
        this.token = token;
        this.setCurrentUser(user);
    }

    public logout(): any {
        // remove user from local storage to log user out
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

    public setGuestUser(): any {
        return this.login('guest', 'none');
    }

    public getTokenExpirationDate(token: string): Date {
        const decoded = this._decodeToken(token);
        if (decoded.exp === undefined) {
            return null;
        }
        return decoded.exp;
    }

    public isTokenExpired(token?: string): boolean {
        if (!token) {
            token = this.getCurrentUser().token;
        }
        if (!token) {
            return true;
        }
        const date = this.getTokenExpirationDate(token);
        if (date === undefined) {
            return false;
        }
        return !(date.valueOf() > new Date().valueOf());
    }

    public forgotPassword(email: string): Observable<any> {
        return this.http.post('api/forgot-password', {email: email}).catch(this.handleError);
    }

    public resetPassword(email: string, password: string, token: string): Observable<any> {
        return this.http.post('api/reset-password', {email: email, password: password, token: token}).catch(this.handleError);
    }

    public changePassword(oldPassword: string, newPassword: string, userId: string | number = this.currentUserSubject.value.id): Observable<any> {
        return this.http.post(`api/users/${userId}/pwd/`, {old_password: oldPassword, new_password: newPassword}).catch(this.handleError);
    }

    private _decodeToken(token): JwtObject {
        // decode the token to read the username and expiration timestamp
        const token_parts = token.split(/\./);
        const token_decoded = JSON.parse(window.atob(token_parts[1]));
        const token_expires = new Date(token_decoded.exp * 1000);
        return new JwtObject(token_decoded.orig_iat, token_expires, token_decoded.username, token_decoded.email, token_decoded.user_id);
    }

    private handleError(err: any) {
        return throwError(err);
    }

}
