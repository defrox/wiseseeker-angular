import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {User} from 'app/_models/user';

@Injectable({ providedIn: 'root' })
export class UserApiService {

    private usersUrl = 'api/users';
    private registerUrl = 'api/register';
    private authenticateUrl = 'api/authenticate';

    constructor(private http: HttpClient) {
    }

    getAll(): Observable<User[]> {
        return this.http.get<User[]>(`${this.usersUrl}/`)
            .catch(this.handleError);
    }

    getUsers(): Observable<User[]> {
        return this.http.get<User[]>(`${this.usersUrl}/`)
            .catch(this.handleError);
    }

    getById(id: number): Observable<User> {
        return this.http.get<User>(`${this.usersUrl}/${id}/`)
            .catch(this.handleError);
    }

    register(user: User): Observable<User> {
        return this.http.post<User>(this.registerUrl, user)
            .catch(this.handleError);
    }

    registerBatch(users: User[]): Observable<User[]> {
        return this.http.post<User[]>(this.registerUrl, users)
            .catch(this.handleError);
    }

    authenticate(email: string, password: string): Observable<User> {
        return this.http.post<User>(this.authenticateUrl, {email, password})
            .catch(this.handleError);
    }

    update(user: User): Observable<User> {
        return this.http.put<User>(`${this.usersUrl}/${user.id}/`, user)
            .catch(this.handleError);
    }

    getUser(id: string): Observable<User> {
        return this.http.get<User>(`${this.usersUrl}/${id}/`)
            .catch(this.handleError);
    }

    addUser(user: User): Observable<User> {
        const newUser = Object.assign({}, user);
        return this.http.post<User>(`${this.usersUrl}/`, newUser)
            .catch(this.handleError);
    }

    addUsers(users: User[]): Observable<User[]> {
        const newUsers = Object.assign([], users);
        return this.http.post<User[]>(`${this.usersUrl}/`, newUsers)
            .catch(this.handleError);
    }

    deleteUser(user: User | number): Observable<User> {
        const id = typeof user === 'number' ? user : user.id;
        return this.http.delete<User>(`${this.usersUrl}/${id}/`)
            .catch(this.handleError);
    }

    updateUser(user: User): Observable<Object> {
        return this.http.put(`${this.usersUrl}/${user.id}/`, user)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}
