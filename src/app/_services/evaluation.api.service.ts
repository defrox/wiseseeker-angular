import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subscription, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {Evaluation, User} from 'app/_models';
import {AuthenticationService} from './authentication.service';

@Injectable({ providedIn: 'root' })
export class EvaluationApiService {
    currentUserSubscription: Subscription;
    currentUser: User;
    private evaluationsUrl = 'api/evaluations';

    /**
     * Constructor
     *
     * @param {AuthenticationService} _authenticationService
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _authenticationService: AuthenticationService,
        private _httpClient: HttpClient
    ) {
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<Evaluation[]> {
        return this._httpClient.get<Evaluation[]>(`${this.evaluationsUrl}/`)
            .catch(this.handleError);
    }

    getAllByUser(): Observable<Evaluation[]> {
        return this._httpClient.get<Evaluation[]>(`${this.evaluationsUrl}/?username=${this.currentUser.username}`)
            .catch(this.handleError);
    }

    get(id: number): Observable<Evaluation> {
        return this._httpClient.get<Evaluation>(`${this.evaluationsUrl}/${id}/`)
            .catch(this.handleError);
    }

    update(evaluation: Evaluation): Observable<Evaluation> {
        return this._httpClient.put<Evaluation>(`${this.evaluationsUrl}/${evaluation.id}/`, evaluation)
            .catch(this.handleError);
    }

    add(evaluation: Evaluation): Observable<Evaluation> {
        const newEvaluation = Object.assign({}, evaluation);
        return this._httpClient.post<Evaluation>(`${this.evaluationsUrl}/`, newEvaluation)
            .catch(this.handleError);
    }

    delete(evaluation: Evaluation | number): Observable<Evaluation> {
        const id = typeof evaluation === 'number' ? evaluation : evaluation.id;
        return this._httpClient.delete<Evaluation>(`${this.evaluationsUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}
