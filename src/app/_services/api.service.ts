import {Injectable} from '@angular/core';
import {HttpClient, HttpEvent, HttpHeaders, HttpParams} from '@angular/common/http';
import {environment} from 'environments/environment';
import {Observable} from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class ApiService {
    apiURL: string = environment.apiConfig.url + environment.apiConfig.version;

    constructor(private http: HttpClient) {
    }

    request(method: string, url: string, options?: any | null): Observable<any> {
        return this.http.request(method, `${this.apiURL}/${url}`, options);
    }

    get(url: string, options?: any | null): Observable<any> {
        return this.http.get(`${this.apiURL}/${url}`, options);
    }

    delete(url: string, options?: any | null): Observable<any> {
        return this.http.delete(`${this.apiURL}/${url}`, options);
    }

    head(url: string, options?: any | null): Observable<any> {
        return this.http.head(`${this.apiURL}/${url}`, options);
    }

    jsonp(url: string, callbackParam: string): Observable<any> {
        return this.http.jsonp(`${this.apiURL}/${url}`, callbackParam);
    }

    options(url: string, options?: any | null): Observable<any> {
        return this.http.options(`${this.apiURL}/${url}`, options);
    }

    patch(url: string, body: any | null, options?: any | null): Observable<any> {
        return this.http.patch(`${this.apiURL}/${url}`, body, options);
    }

    post(url: string, body: any | null, options?: any | null): Observable<any> {
        return this.http.post(`${this.apiURL}/${url}`, body, options);
    }

    put(url: string, body: any | null, options?: any | null): Observable<any> {
        return this.http.put(`${this.apiURL}/${url}`, body, options);
    }

}
