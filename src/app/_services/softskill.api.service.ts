import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {SoftSkill} from 'app/_models';

@Injectable({ providedIn: 'root' })
export class SoftSkillApiService {
    private softSkillsUrl = 'api/softskills';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<SoftSkill[]> {
        return this._httpClient.get<SoftSkill[]>(`${this.softSkillsUrl}/`)
            .catch(this.handleError);
    }

    get(id: number): Observable<SoftSkill> {
        return this._httpClient.get<SoftSkill>(`${this.softSkillsUrl}/${id}/`)
            .catch(this.handleError);
    }

    update(softSkill: SoftSkill): Observable<SoftSkill> {
        return this._httpClient.put<SoftSkill>(`${this.softSkillsUrl}/${softSkill.id}/`, softSkill)
            .catch(this.handleError);
    }

    add(softSkill: SoftSkill): Observable<SoftSkill> {
        const newSoftSkill = Object.assign({}, softSkill);
        return this._httpClient.post<SoftSkill>(`${this.softSkillsUrl}/`, newSoftSkill)
            .catch(this.handleError);
    }

    delete(softSkill: SoftSkill | number): Observable<SoftSkill> {
        const id = typeof softSkill === 'number' ? softSkill : softSkill.id;
        return this._httpClient.delete<SoftSkill>(`${this.softSkillsUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}
