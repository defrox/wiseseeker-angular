import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {Test, TestExecution, TestType} from 'app/_models';

@Injectable({ providedIn: 'root' })
export class TestApiService {
    private testsUrl = 'api/tests';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<Test[]> {
        return this._httpClient.get<Test[]>(`${this.testsUrl}/`)
            .catch(this.handleError);
    }

    getFiltered(filters: any = null): Observable<Test> {
        let queryString = '';
        if (filters) {
            queryString = '?';
            const pieces = [];
            Object.keys(filters).map(key => { pieces.push(key + '=' + filters[key]); });
            queryString += pieces.join('&');
        }
        return this._httpClient.get<Test>(`${this.testsUrl}/${queryString}`)
            .catch(this.handleError);
    }

    get(id: number): Observable<Test> {
        return this._httpClient.get<Test>(`${this.testsUrl}/${id}/`)
            .catch(this.handleError);
    }

    update(test: Test): Observable<Test> {
        return this._httpClient.put<Test>(`${this.testsUrl}/${test.id}/`, test)
            .catch(this.handleError);
    }

    add(test: Test): Observable<Test> {
        const newTest = Object.assign({}, test);
        return this._httpClient.post<Test>(`${this.testsUrl}/`, newTest)
            .catch(this.handleError);
    }

    delete(test: Test | number): Observable<Test> {
        const id = typeof test === 'number' ? test : test.id;
        return this._httpClient.delete<Test>(`${this.testsUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}

@Injectable({ providedIn: 'root' })
export class TestExecutionApiService {
    private testExecutionsUrl = 'api/tests/executions';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<TestExecution[]> {
        return this._httpClient.get<TestExecution[]>(`${this.testExecutionsUrl}/`)
            .catch(this.handleError);
    }

    get(id: number): Observable<TestExecution> {
        return this._httpClient.get<TestExecution>(`${this.testExecutionsUrl}/${id}/`)
            .catch(this.handleError);
    }

    getByUserAndTest(userId: number, testId: number): Observable<TestExecution> {
        return this._httpClient.get<TestExecution>(`${this.testExecutionsUrl}/?user=${userId}&test=${testId}`)
            .catch(this.handleError);
    }

    getByUser(userId: number): Observable<TestExecution> {
        return this._httpClient.get<TestExecution>(`${this.testExecutionsUrl}/?user=${userId}`)
            .catch(this.handleError);
    }

    getFiltered(filters: any = null): Observable<TestExecution> {
        let queryString = '';
        if (filters) {
            queryString = '?';
            const pieces = [];
            Object.keys(filters).map(key => { pieces.push(key + '=' + filters[key]); });
            queryString += pieces.join('&');
        }
        return this._httpClient.get<TestExecution>(`${this.testExecutionsUrl}/${queryString}`)
            .catch(this.handleError);
    }

    update(testExecution: TestExecution): Observable<TestExecution> {
        return this._httpClient.put<TestExecution>(`${this.testExecutionsUrl}/${testExecution.id}/`, testExecution)
            .catch(this.handleError);
    }

    add(testExecution: TestExecution): Observable<TestExecution> {
        const newTest = Object.assign({}, testExecution);
        return this._httpClient.post<TestExecution>(`${this.testExecutionsUrl}/`, newTest)
            .catch(this.handleError);
    }

    delete(testExecution: TestExecution | number): Observable<TestExecution> {
        const id = typeof testExecution === 'number' ? testExecution : testExecution.id;
        return this._httpClient.delete<TestExecution>(`${this.testExecutionsUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}

@Injectable({ providedIn: 'root' })
export class TestTypeApiService {
    private testTypesUrl = 'api/tests/types';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<TestType[]> {
        return this._httpClient.get<TestType[]>(`${this.testTypesUrl}/`)
            .catch(this.handleError);
    }

    get(id: number): Observable<TestType> {
        return this._httpClient.get<TestType>(`${this.testTypesUrl}/${id}/`)
            .catch(this.handleError);
    }

    update(testType: TestType): Observable<TestType> {
        return this._httpClient.put<TestType>(`${this.testTypesUrl}/${testType.id}/`, testType)
            .catch(this.handleError);
    }

    add(testType: TestType): Observable<TestType> {
        const newTest = Object.assign({}, testType);
        return this._httpClient.post<TestType>(`${this.testTypesUrl}/`, newTest)
            .catch(this.handleError);
    }

    delete(testType: TestType | number): Observable<TestType> {
        const id = typeof testType === 'number' ? testType : testType.id;
        return this._httpClient.delete<TestType>(`${this.testTypesUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}
