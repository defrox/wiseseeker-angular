import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {Dashboard} from 'app/_models';

@Injectable({ providedIn: 'root' })
export class DashboardApiService {
    private dashboardUrl = 'api/dashboard';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    get(): Observable<Dashboard[]> {
        return this._httpClient.get<Dashboard[]>(`${this.dashboardUrl}/`)
            .catch(this.handleError);
    }

    getNewDash(): Observable<Dashboard[]> {
        return this._httpClient.get<Dashboard[]>(`${this.dashboardUrl}2/`)
            .catch(this.handleError);
    }

    update(dashboard: Dashboard): Observable<Dashboard> {
        return this._httpClient.put<Dashboard>(`${this.dashboardUrl}/`, dashboard)
            .catch(this.handleError);
    }

    add(dashboard: Dashboard): Observable<Dashboard> {
        const newDashboard = Object.assign({}, dashboard);
        return this._httpClient.post<Dashboard>(`${this.dashboardUrl}/`, newDashboard)
            .catch(this.handleError);
    }

    delete(dashboard: Dashboard | number): Observable<Dashboard> {
        const id = typeof dashboard === 'number' ? dashboard : dashboard.id;
        return this._httpClient.delete<Dashboard>(`${this.dashboardUrl}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}
