import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {Knowledge, KnowledgeCategory} from 'app/_models';

@Injectable({ providedIn: 'root' })
export class KnowledgeApiService {
    private knowledgeUrl = 'api/knowledge';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<Knowledge[]> {
        return this._httpClient.get<Knowledge[]>(`${this.knowledgeUrl}/`)
            .catch(this.handleError);
    }

    get(id: number): Observable<Knowledge> {
        return this._httpClient.get<Knowledge>(`${this.knowledgeUrl}/${id}/`)
            .catch(this.handleError);
    }

    getFiltered(filters: any = null): Observable<Knowledge> {
        let queryString = '';
        if (filters) {
            queryString = '?';
            const pieces = [];
            Object.keys(filters).map(key => { pieces.push(key + '=' + filters[key]); });
            queryString += pieces.join('&');
        }
        return this._httpClient.get<Knowledge>(`${this.knowledgeUrl}/${queryString}`)
            .catch(this.handleError);
    }

    update(knowledge: Knowledge): Observable<Knowledge> {
        return this._httpClient.put<Knowledge>(`${this.knowledgeUrl}/${knowledge.id}/`, knowledge)
            .catch(this.handleError);
    }

    add(knowledge: Knowledge): Observable<Knowledge> {
        const newKnowledge = Object.assign({}, knowledge);
        return this._httpClient.post<Knowledge>(`${this.knowledgeUrl}/`, newKnowledge)
            .catch(this.handleError);
    }

    delete(knowledge: Knowledge | number): Observable<Knowledge> {
        const id = typeof knowledge === 'number' ? knowledge : knowledge.id;
        return this._httpClient.delete<Knowledge>(`${this.knowledgeUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}

@Injectable({ providedIn: 'root' })
export class KnowledgeCategoryApiService {
    // private knowledgeCategoryUrl = 'api/knowledge/categories';
    private knowledgeCategoryUrl = 'api/knowledge-categories';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<KnowledgeCategory[]> {
        return this._httpClient.get<KnowledgeCategory[]>(`${this.knowledgeCategoryUrl}/`)
            .catch(this.handleError);
    }

    get(id: number): Observable<KnowledgeCategory> {
        return this._httpClient.get<KnowledgeCategory>(`${this.knowledgeCategoryUrl}/${id}/`)
            .catch(this.handleError);
    }

    getFiltered(filters: any = null): Observable<KnowledgeCategory> {
        let queryString = '';
        if (filters) {
            queryString = '?';
            const pieces = [];
            Object.keys(filters).map(key => { pieces.push(key + '=' + filters[key]); });
            queryString += pieces.join('&');
        }
        return this._httpClient.get<KnowledgeCategory>(`${this.knowledgeCategoryUrl}/${queryString}`)
            .catch(this.handleError);
    }

    update(knowledgeCategory: KnowledgeCategory): Observable<KnowledgeCategory> {
        return this._httpClient.put<KnowledgeCategory>(`${this.knowledgeCategoryUrl}/${knowledgeCategory.id}/`, knowledgeCategory)
            .catch(this.handleError);
    }

    add(knowledgeCategory: KnowledgeCategory): Observable<KnowledgeCategory> {
        const newKnowledgeCategory = Object.assign({}, knowledgeCategory);
        return this._httpClient.post<KnowledgeCategory>(`${this.knowledgeCategoryUrl}/`, newKnowledgeCategory)
            .catch(this.handleError);
    }

    delete(knowledgeCategory: KnowledgeCategory | number): Observable<KnowledgeCategory> {
        const id = typeof knowledgeCategory === 'number' ? knowledgeCategory : knowledgeCategory.id;
        return this._httpClient.delete<KnowledgeCategory>(`${this.knowledgeCategoryUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}
