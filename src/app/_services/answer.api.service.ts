import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {Answer} from 'app/_models';

@Injectable({ providedIn: 'root' })
export class AnswerApiService {
    private answerUrl = 'api/answers';

    /**
     * Constructor
     *
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _httpClient: HttpClient
    ) {}

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<Answer[]> {
        return this._httpClient.get<Answer[]>(`${this.answerUrl}/`)
            .catch(this.handleError);
    }

    get(id: number): Observable<Answer> {
        return this._httpClient.get<Answer>(`${this.answerUrl}/${id}/`)
            .catch(this.handleError);
    }

    update(answer: Answer): Observable<Answer> {
        return this._httpClient.put<Answer>(`${this.answerUrl}/${answer.id}/`, answer)
            .catch(this.handleError);
    }

    add(answer: Answer): Observable<Answer> {
        const newAnswer = Object.assign({}, answer);
        return this._httpClient.post<Answer>(`${this.answerUrl}/`, newAnswer)
            .catch(this.handleError);
    }

    delete(answer: Answer | number): Observable<Answer> {
        const id = typeof answer === 'number' ? answer : answer.id;
        return this._httpClient.delete<Answer>(`${this.answerUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}
