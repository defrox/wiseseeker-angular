import {Injectable} from '@angular/core';
import {registerLocaleData} from '@angular/common';
import localeEnglish from '@angular/common/locales/en';
import localeSpanish from '@angular/common/locales/es';

@Injectable({providedIn: 'root'})
export class LocaleService {

    private _locale: string;

    set locale(value: string) {
        this._locale = value;
    }

    get locale(): string {
        return this._locale || 'en';
    }

    registerLocale(localeId: string): void {
        if (!localeId) {
            return;
        }
        this.locale = localeId;

        // Register locale data since only the en-US locale data comes with Angular
        switch (localeId) {
            case 'es': {
                registerLocaleData(localeSpanish);
                break;
            }
            case 'en': {
                registerLocaleData(localeEnglish);
                break;
            }
        }
        /*import(`@angular/common/locales/${localeId}.js`).then(
            module => {
                registerLocaleData(module.default);
            },
            () => {
                import(`@angular/common/locales/${localeId.split('-')[0]}.js`).then(module => {
                    registerLocaleData(module.default);
                });
            }
        );*/
    }
}
