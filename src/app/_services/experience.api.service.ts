import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable, Subscription, throwError} from 'rxjs';
import 'rxjs/add/operator/catch';
import {Experience, User} from 'app/_models';
import {AuthenticationService} from './authentication.service';

@Injectable({ providedIn: 'root' })
export class ExperienceApiService {
    currentUserSubscription: Subscription;
    currentUser: User;
    private experiencesUrl = 'api/experiences';

    /**
     * Constructor
     *
     * @param {AuthenticationService} _authenticationService
     * @param {HttpClient} _httpClient
     */
    constructor(
        private _authenticationService: AuthenticationService,
        private _httpClient: HttpClient
    ) {
        this.currentUserSubscription = this._authenticationService.currentUser.subscribe(user => {
            this.currentUser = user;
        });
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Basic CRUD functions
     */

    getAll(): Observable<Experience[]> {
        return this._httpClient.get<Experience[]>(`${this.experiencesUrl}/`)
            .catch(this.handleError);
    }

    getAllByUser(): Observable<Experience[]> {
        return this._httpClient.get<Experience[]>(`${this.experiencesUrl}/?user=${this.currentUser.id}`)
            .catch(this.handleError);
    }

    get(id: number): Observable<Experience> {
        return this._httpClient.get<Experience>(`${this.experiencesUrl}/${id}/`)
            .catch(this.handleError);
    }

    update(evaluation: Experience): Observable<Experience> {
        return this._httpClient.put<Experience>(`${this.experiencesUrl}/${evaluation.id}/`, evaluation)
            .catch(this.handleError);
    }

    add(evaluation: Experience): Observable<Experience> {
        const newExperience = Object.assign({}, evaluation);
        return this._httpClient.post<Experience>(`${this.experiencesUrl}/`, newExperience)
            .catch(this.handleError);
    }

    delete(evaluation: Experience | number): Observable<Experience> {
        const id = typeof evaluation === 'number' ? evaluation : evaluation.id;
        return this._httpClient.delete<Experience>(`${this.experiencesUrl}/${id}/`)
            .catch(this.handleError);
    }

    private handleError(err: any) {
        return throwError(err);
    }
}
