export const environment = {
    production: false,
    external: false,
    preproduction: false,
    test: false,
    develop: false,
    hmr: true,
    enableDebug: true,
    enableAuth: true,
    apiConfig: {
        url: 'https://api.wiseseeker.com/',
        version: 'v1.0.0',
    },
    appConfig: {
        logo: {
            icon: 'assets/images/logos/Icon Wise Seeker.png',
            iconInvert: 'assets/images/logos/Icon Wise Seeker white.png',
            iconBig: 'assets/images/logos/Icon Wise Seeker.png',
            small: 'assets/images/logos/Icon Wise Seeker.png',
            medium: 'assets/images/logos/Icon Wise Seeker.png',
            big: 'assets/images/logos/Icon Wise Seeker.png',
            small_var1: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
            medium_var1: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
            big_var1: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
            small_var2 : 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
            medium_var2: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
            big_var2: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png'
        },
        appName: 'The Wise Seeker',
        installedLangs: ['es'],
        defaultLang: 'es',
        appSettings: {
            anonymousSite: false,
            externalSite: false,
            enableExternalAutoApply: false,
            enableUserRegistration: false,
            enableExternalUserRegistration: false,
            autoLoginOnRegister: false,
            enableContinueTest: false,
        },
    },
    firebaseConfig: {
        apiKey: 'AIzaSyBbofRdnkiQ_UV4jiJhnJOTEf1H1PvD0ck',
        authDomain: 'wiseseeker-dev.firebaseapp.com',
        databaseURL: 'https://wiseseeker-dev.firebaseio.com',
        projectId: 'wiseseeker-dev',
        storageBucket: 'wiseseeker-dev.appspot.com',
        messagingSenderId: '505186927916'
    }
};
