// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production    : false,
    external      : false,
    preproduction : false,
    test          : false,
    develop       : false,
    hmr           : false,
    enableDebug   : true,
    enableAuth    : true,
    apiConfig   : {
        url: 'http://192.168.1.52:8000/api/',
        version: 'v1.0.0',
    },
    appConfig: {
        logo: {
            icon: 'assets/images/logos/Icon Wise Seeker.png',
            iconInvert: 'assets/images/logos/Icon Wise Seeker white.png',
            iconBig: 'assets/images/logos/Icon Wise Seeker.png',
            small: 'assets/images/logos/Icon Wise Seeker.png',
            medium: 'assets/images/logos/Icon Wise Seeker.png',
            big: 'assets/images/logos/Icon Wise Seeker.png',
            small_var1: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
            medium_var1: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
            big_var1: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
            small_var2 : 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
            medium_var2: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
            big_var2: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png'
        },
        appName: 'The Wise Seeker',
        installedLangs: ['es'],
        defaultLang: 'es',
        appSettings: {
            anonymousSite: true, // MODO TELEFONICA: oculta los nombres de los candidatos, tanto en el portal de candidato como en el de reclutador
            externalSite: false, // habilita el modo sitio externo: diferencia a los candidatos que provienen de ofertas externas y oculta los jobs
            enableExternalAutoApply: false, // en modo external (ofertas de fuentes externas) habilita la función de auto aplicar al job al hacer login o registro
            enableUserRegistration: false, // permite el registro de nuevos usuarios (se muestra el link en el login)
            enableExternalUserRegistration: false, // permite el registro de nuevos usuarios provenientes de ofertas externas
            autoLoginOnRegister: false, // habilita la función de auto login después de realizar el registro de nuevo usuario
            enableContinueTest: false, // permite continuar los test desde donde se dejaron
        },
    },
    firebaseConfig: {
        apiKey: 'AIzaSyBbofRdnkiQ_UV4jiJhnJOTEf1H1PvD0ck',
        authDomain: 'wiseseeker-dev.firebaseapp.com',
        databaseURL: 'https://wiseseeker-dev.firebaseio.com',
        projectId: 'wiseseeker-dev',
        storageBucket: 'wiseseeker-dev.appspot.com',
        messagingSenderId: '505186927916'
    }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
