// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production    : false,
    external      : false,
    preproduction : true,
    test          : false,
    develop       : false,
    hmr           : false,
    enableDebug   : true,
    enableAuth    : true,
    apiConfig   : {
        url: 'https://safe-savannah-36795.herokuapp.com/api/',
        version: 'v1.0.0'
    },
    appConfig: {
        logo: {
            icon: 'assets/images/logos/Icon Wise Seeker.png',
            iconInvert: 'assets/images/logos/Icon Wise Seeker white.png',
            iconBig: 'assets/images/logos/Icon Wise Seeker.png',
            small: 'assets/images/logos/Icon Wise Seeker.png',
            medium: 'assets/images/logos/Icon Wise Seeker.png',
            big: 'assets/images/logos/Icon Wise Seeker.png',
            small_var1: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
            medium_var1: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
            big_var1: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
            small_var2 : 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
            medium_var2: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png',
            big_var2: 'assets/images/logos/Logotipo Wise Seeker_horizontal-03.png'
        },
        appName: 'The Wise Seeker',
        installedLangs: ['es'],
        defaultLang: 'es',
        appSettings: {
            anonymousSite: true,
            externalSite: true,
            enableExternalAutoApply: false,
            enableUserRegistration: true,
            enableExternalUserRegistration: false,
            autoLoginOnRegister: false,
            enableContinueTest: false,
        },
    },
    firebaseConfig: {
        apiKey: 'AIzaSyCN7M_wbsgAsVLIBa80EG5L0GW17hOdUHo',
        authDomain: 'wiseseeker-pre.firebaseapp.com',
        databaseURL: 'https://wiseseeker-pre.firebaseio.com',
        projectId: 'wiseseeker-pre',
        storageBucket: 'wiseseeker-pre.appspot.com',
        messagingSenderId: '152113712873'
    }
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
